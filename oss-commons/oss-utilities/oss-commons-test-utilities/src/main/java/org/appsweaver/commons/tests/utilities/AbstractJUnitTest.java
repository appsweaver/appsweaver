/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.tests.utilities;

import java.io.File;
import java.net.InetAddress;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author UD
 * 
 */
public abstract class AbstractJUnitTest {
	final protected Logger logger = LoggerFactory.getLogger(getClass());
	
	protected String methodName;
	
	@Rule
	public TestName testName = new TestName();

	public String getQualifiedHostname() {
		try {
			final InetAddress inetAddress = InetAddress.getLocalHost();
			final String qualifiedHostname = inetAddress.getHostName();
			
			return qualifiedHostname;
		} catch (Throwable t) {
		}

		return null;
	}

	public String getSimpleHostname() {
		final String qualifiedHostname = getQualifiedHostname();
		
		if (qualifiedHostname != null && qualifiedHostname.length() > 0) {
			final int indexOfHostnameSeparator = qualifiedHostname.indexOf(".");
			
			final String simpleHostname;
			if(indexOfHostnameSeparator <= 0) {
				simpleHostname = qualifiedHostname.substring(0);
			} else {
				simpleHostname = qualifiedHostname.substring(0, indexOfHostnameSeparator);
			}
			
			return simpleHostname;
		} else {
			return null;
		}
	}

	public boolean isSimpleHostnameEquals(String hostname) {
		try {
			final String simpleHostname = getSimpleHostname();
			if (simpleHostname != null && simpleHostname.equals(hostname)) {
				return true;
			}
		} catch (Throwable t) {
		}
		
		return false;
	}

	public File getResourceAsFile(String resource) {
		final ClassLoader classLoader = this.getClass().getClassLoader();
		final File file = new File(classLoader.getResource(resource).getFile());
		return file;
	}

	protected final String getResourceByClassAndFileName(String baseDir, String filename) {
		return getClass().getResource(baseDir).getPath() + "/" + getClass().getSimpleName() + "/" + filename;
	}

	public void printDashedLine() {
		printLineWith("-");
	}

	public void printLineWith(int width, String marker) {
		System.out.println(String.format("%0" + width + "d", 0).replace("0", marker));
	}
	
	public void printLineWith(String marker) {
		System.out.println(String.format("%0" + 80 + "d", 0).replace("0", marker));
	}

	@Before
	public void setUp() {
		methodName=testName.getMethodName();
		System.out.println(String.format("Begin test [Class=%s; Method=%s]", getClass().getSimpleName(), testName.getMethodName()));
		printDashedLine();
	}

	@After
	public void tearDown() {
		printDashedLine();
		System.out.println(String.format("End test [Class=%s; Method=%s]", getClass().getSimpleName(), testName.getMethodName()));
		printLineWith("=");
		System.out.println();
	}
}
