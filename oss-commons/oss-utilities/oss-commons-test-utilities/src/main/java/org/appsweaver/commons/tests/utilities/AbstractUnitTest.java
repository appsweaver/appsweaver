/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.tests.utilities;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.mockito.MockitoAnnotations;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author AN
 * 
 */
public abstract class AbstractUnitTest extends AbstractJUnitTest {

	/** The map of object name to file path. */
	protected Map<String, String> mapOfObjectNameToFilePath = new HashMap<>();

	/** The object mapper. */
	protected ObjectMapper objectMapper = new ObjectMapper();

	/**
	 * 
	 * Setup test prerequisites
	 *
	 */
	@Before
	@Override
	public void setUp() {
		super.setUp();
		
		MockitoAnnotations.initMocks(this);
		final File objectFolder = new File("src/test/resources/objects");
		final File[] files = objectFolder.listFiles();
		for (final File file : files) {
			mapOfObjectNameToFilePath.put(file.getName().substring(0, file.getName().indexOf(".")),
					file.getAbsolutePath());
		}
	}

	/**
	 * Gets the object from json.
	 *
	 * @param <T>
	 *            the generic type
	 * @param fileName
	 *            the file name
	 * @param clazz
	 *            the clazz
	 * @return the object from json
	 */
	private <T> T getObjectFromJson(String fileName, Class<T> clazz) {
		File file = null;
		if (null != mapOfObjectNameToFilePath.get(fileName)) {
			try {
				file = new File(mapOfObjectNameToFilePath.get(fileName));
				return objectMapper.readValue(file, clazz);
			} catch (final Throwable e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	/**
	 * Gets the object.
	 *
	 * @param <T>
	 *            the generic type
	 * @param clazz
	 *            the clazz
	 * @return the object
	 */
	public <T> T getObject(Class<T> clazz) {
		if (null != clazz) {
			final String fileName = clazz.getSimpleName();
			return getObjectFromJson(fileName, clazz);
		}

		return null;
	}

	/**
	 * Gets the object by file name.
	 *
	 * @param <T>
	 *            the generic type
	 * @param fileName
	 *            the file name excluding the extension
	 * @param clazz
	 *            the clazz
	 * @return the object by file name
	 */
	public <T> T getObjectByFileName(String fileName, Class<T> clazz) {
		if (StringUtils.isNotBlank(fileName) && null != clazz) {
			return getObjectFromJson(fileName, clazz);
		}

		return null;
	}

	/**
	 * Gets the object by class name.
	 *
	 * @param <T>
	 *            the generic type
	 * @param className
	 *            the class name
	 * @param clazz
	 *            the clazz
	 * @return the object by class name
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public <T> T getObjectByClassName(String className, Class<T> clazz) throws IOException {
		if (StringUtils.isNotBlank(className) && null != clazz) {
			return getObjectByClassName(className, clazz);
		}

		return null;
	}
}
