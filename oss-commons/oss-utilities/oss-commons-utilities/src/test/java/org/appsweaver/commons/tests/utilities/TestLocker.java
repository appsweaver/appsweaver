/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.tests.utilities;

import java.util.concurrent.TimeUnit;

import org.appsweaver.commons.utilities.Locker;
import org.appsweaver.commons.utilities.Locker.ExpiringCache;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.common.cache.LoadingCache;

/**
 * 
 * @author UD
 * 
 */
public class TestLocker extends AbstractJUnitTest {
	final static ExpiringCache minExpiringCache = new ExpiringCache(1, TimeUnit.MINUTES);
	final static ExpiringCache millisExpiringCache = new ExpiringCache(10, TimeUnit.MILLISECONDS);
	final static ExpiringCache defaultExpiringCache = new ExpiringCache();

	final static LoadingCache<String, Object> minCache = minExpiringCache.getCache();
	final static LoadingCache<String, Object> milliCache = millisExpiringCache.getCache();
	final static LoadingCache<String, Object> defaultCache = defaultExpiringCache.getCache();
	
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void testPutIntoMinuteCache() throws Throwable {
		minCache.put("A1", 100);

		Assert.assertEquals(1, minExpiringCache.getDuration());
		Assert.assertEquals(TimeUnit.MINUTES, minExpiringCache.getTimeUnit());

		Assert.assertEquals(100, minCache.get("A1"));
	}
	
	@Test
	public void testPutIntoDefaultCache() throws Throwable {
		defaultCache.put("A1", 100);

		Assert.assertEquals(24, defaultExpiringCache.getDuration());
		Assert.assertEquals(TimeUnit.HOURS, defaultExpiringCache.getTimeUnit());

		Assert.assertEquals(100, defaultCache.get("A1"));
	}

	@Test
	public void testPutIntoMillisCache() throws Throwable {
		milliCache.put("B1", 15);
		
		Assert.assertEquals(10, millisExpiringCache.getDuration());
		Assert.assertEquals(TimeUnit.MILLISECONDS, millisExpiringCache.getTimeUnit());
		
		Assert.assertEquals(15, milliCache.get("B1"));

		TimeUnit.MILLISECONDS.sleep(100);	// Sleep for just over 10 millis 

		Assert.assertNotEquals(15, milliCache.get("B1"));
	}
	
	@Test
	public void testLocker() throws Throwable {
		final Object lock = Locker.lock("H1");
		Assert.assertNotEquals(new Object(), lock);
	}
}
