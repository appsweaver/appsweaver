/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.tests.utilities;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Fileinator;
import org.appsweaver.commons.utilities.Jsonifier;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

/**
 * 
 * @author UD
 * 
 */
public class TestJsonifier extends AbstractJUnitTest {
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void testToJson() throws Throwable {
		final SomeObject so1 = new SomeObject();
		Assert.assertEquals("{}", Jsonifier.toJson(so1));
		
		final SomeObject so2 = new SomeObject(10, 99.99, "Hello World");
		final String json = Fileinator.toString(getResourceAsFile("TestJsonifier/so2.json"));
		Assert.assertEquals(json, Jsonifier.toJson(so2));
		
		final SomeObject so3 = Jsonifier.toInstance(json.getBytes(), SomeObject.class);
		Assert.assertEquals(so3, so2);
		
		final SomeObject so4 = Jsonifier.toInstance("{'integer': null, 'decimal': null, 'string': null}", SomeObject.class);
		Assert.assertEquals(new SomeObject(), so4);
		
		final SomeObject so5 = Jsonifier.toInstance("{'integer': 12, 'decimal': 98.23, 'string': 'Bingo!'}", SomeObject.class);
		Assert.assertEquals(new SomeObject(12, 98.23, "Bingo!"), so5);
		
		final SomeObject so6 = Jsonifier.toInstance("{'integer': null, 'decimal': 198.11, 'string': null}", SomeObject.class);
		Assert.assertEquals(new SomeObject(null, 198.11, null), so6);
		
		final SomeObject so7 = Jsonifier.toInstance("{'integer': null, 'decimal': null, 'string': null, 'unknown_1': 1234, 'unknown_2': 'Dingo!'}", SomeObject.class);
		Assert.assertEquals(new SomeObject(), so7);
		
		final Map<Class<?>, JsonDeserializer<?>> map = new HashMap<>();
		map.put(SomeObject.class, new SomeObjectJsonDeserializer());
		
		final String prettyJson = "{\n  \"integer\": 10,\n  \"decimal\": 190.999,\n   \"string\": \"Welcome!\"\n}";
		final SomeObject deserialized = Jsonifier.toInstance(prettyJson, SomeObject.class, map);
		Assert.assertEquals(new SomeObject(10, 190.999, "Welcome!"), deserialized);
	}
	
	@Test
	public void testFromJsonToBean() throws Throwable {
		final SomeObject so2 = new SomeObject(10, 99.99, "Hello World");
		final String json = Fileinator.toString(getResourceAsFile("TestJsonifier/so2.json"));
		Assert.assertEquals(json, Jsonifier.toJson(so2));

		final SomeObject so1 = Jsonifier.toInstance(json, SomeObject.class);
		Assert.assertEquals(so2, so1);
	}
	
	@Test
	public void testFromJsonToListBean() throws Throwable {
		final List<String> initial = Collections.newList("How", "are", "you");
		final String listJson = initial.toString();
		final List<String> list = Jsonifier.toList(listJson, String.class);
		
		Assert.assertEquals(initial, list);
	}
	
	@Test
	public void testMapToAndFrom() throws Throwable {
		final Map<String, String> map = Jsonifier.toMap("{}");
		Assert.assertEquals(new HashMap<String, String>(), map);
		
		final HashMap<String, Object> expectedHashMap = Collections.newHashMap();
		expectedHashMap.put("name", "Bingo");
		expectedHashMap.put("age", 45.0);
		
		final String jsonMap = "{\n  \"name\": \"Bingo\",\n  \"age\": 45.0\n}";
		final Map<String, String> hashMap = Jsonifier.toMap(jsonMap);
		Assert.assertEquals(expectedHashMap, hashMap);
		
		final String jsonText = null;
		Assert.assertNull(Jsonifier.toMap(jsonText));
		
		final byte[] jsonBytes = null;
		Assert.assertNull(Jsonifier.toMap(jsonBytes));

		final byte[] jsonMapBytes = jsonMap.getBytes();
		
		Assert.assertEquals(expectedHashMap, Jsonifier.toMap(jsonMapBytes));

		final byte[] mapToBytes = Jsonifier.toBytes(expectedHashMap);
		
		Assert.assertArrayEquals(jsonMapBytes, mapToBytes);
		
		Assert.assertNull(Jsonifier.toBytes(null));
	}
	
	@Test
	public void testValidity() throws Throwable {
		Assert.assertTrue(Jsonifier.isValid("{}"));
		Assert.assertFalse(Jsonifier.isValid("{[}"));
	}
	
	@Test
	public void testListArrayAndObject() throws Throwable {
		Assert.assertEquals(Collections.newList(10, 20, 30), Jsonifier.toList("[10, 20, 30]", Integer.class));
		
		final JsonArray jsonArray = new JsonArray();
		jsonArray.add(10);
		jsonArray.add(20);
		jsonArray.add(30);
		Assert.assertEquals(Collections.newList(10, 20, 30), Jsonifier.toList(jsonArray, Integer.class));
		
		
		final JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("integer", new Integer(12));
		jsonObject.addProperty("decimal", 98.23);
		jsonObject.addProperty("string", "Bingo!");
		Assert.assertEquals(new SomeObject(12, 98.23, "Bingo!"), Jsonifier.toInstance(jsonObject, SomeObject.class));
	}
	
	public class SomeObjectJsonDeserializer implements JsonDeserializer<SomeObject> {
		@Override
		public SomeObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
				throws JsonParseException {
			
			final JsonObject jObject = json.getAsJsonObject();
			final Integer integerValue = jObject.get("integer").getAsInt();
			final Double decimalValue = jObject.get("decimal").getAsDouble();
			final String stringValue = jObject.get("string").getAsString();
			
			return new SomeObject(integerValue, decimalValue, stringValue);
		}
	}
	
	class SomeObject {
		private Integer integer;
		private Double decimal;
		private String string;
		
		public SomeObject() {}
		
		public SomeObject(Integer integer, Double decimal, String string) {
			this.integer = integer;
			this.decimal = decimal;
			this.string = string;
		}

		public int getInteger() {
			return integer;
		}

		public void setInteger(int integer) {
			this.integer = integer;
		}

		public Double getDecimal() {
			return decimal;
		}

		public void setDecimal(Double decimal) {
			this.decimal = decimal;
		}

		public String getString() {
			return string;
		}

		public void setString(String string) {
			this.string = string;
		}
		
		@Override
		public String toString() {
			return String.format("integer=%d, decimal=%f, string=%s", integer, decimal, string);
		}
		
		@Override
		public boolean equals(Object object) {
			// Self comparison  
	        if (object == this) { 
	            return true; 
	        } 
	  
	        // Is instance of required class
	        if (!(object instanceof SomeObject)) { 
	            return false; 
	        } 
	        
			if(object instanceof SomeObject) {
				final SomeObject rhs = (SomeObject)object;
				
				if(
					this.integer == null && rhs.integer == null &&
					this.decimal == null && rhs.decimal == null &&
					this.string == null && rhs.string == null
				) return true;
				
				return (
					(this.integer == rhs.integer) || (Integer.compare(this.integer, rhs.integer) == 0) &&
					(this.decimal == rhs.decimal) || (Double.compare(this.decimal, rhs.decimal) == 0) &&
					(this.string == rhs.string) || (this.string.compareTo(rhs.string) == 0)
				);
			}
			
			return false;
		}
	}
}
