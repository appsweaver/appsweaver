/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.tests.utilities;

import java.util.ArrayList;
import java.util.List;

import org.appsweaver.commons.utilities.CustomPatternLayout;
import org.junit.Assert;
import org.junit.Test;

/**
 * 
 * @author AM
 * 
 */
public class TestCustomPatternLayout extends AbstractJUnitTest {

	@Test
	public void testEmailMask() throws Throwable {
		final List<String> maskPatterns = new ArrayList<>();
		maskPatterns.add("(\\w+@\\w+\\.\\w+)");
		final CustomPatternLayout customPatterLayout = new CustomPatternLayout(maskPatterns);
		Assert.assertEquals("*************", customPatterLayout.mask("abc@gmail.com"));
	}

	@Test
	public void testJsonPatternMask() throws Throwable {
		final List<String> maskPatterns = new ArrayList<>();
		maskPatterns.add("\\\"secondaryLoginId\\\"\\s*:\\s*\\\"(.*?)\\\"");
		final CustomPatternLayout customPatterLayout = new CustomPatternLayout(maskPatterns);
		Assert.assertEquals("{\"id\":\"a03028b1-34ed-44f1-8fa4-ad02bb0b6fcd\",\"secondaryLoginId\":\"***\"}",
				customPatterLayout
						.mask("{\"id\":\"a03028b1-34ed-44f1-8fa4-ad02bb0b6fcd\",\"secondaryLoginId\":\"abc\"}"));
	}

	@Test
	public void testJsonMaskMobileNumberPattern() throws Throwable {
		final List<String> maskPatterns = new ArrayList<>();
		maskPatterns.add("\\\"mobileNumber\\\"\\s*:\\s*\\\"(.*?)\\\"");
		final CustomPatternLayout customPatterLayout = new CustomPatternLayout(maskPatterns);
		Assert.assertEquals("{\"id\":\"a03028b1-34ed-44f1-8fa4-ad02bb0b6fcd\",\"mobileNumber\":\"************\"}",
				customPatterLayout.mask(
						"{\"id\":\"a03028b1-34ed-44f1-8fa4-ad02bb0b6fcd\",\"mobileNumber\":\"111-111-1111\"}"));
	}

	@Test
	public void testIPv4PatternMask() throws Throwable {
		final List<String> maskPatterns = new ArrayList<>();
		maskPatterns.add("(\\d+\\.\\d+\\.\\d+\\.\\d+)");
		final CustomPatternLayout customPatterLayout = new CustomPatternLayout(maskPatterns);
		Assert.assertEquals("*************", customPatterLayout.mask("000.000.00.00"));
	}

	@Test
	public void testNestedJsonPatternMask() throws Throwable {
		final String nestedJSON = "{\"LanguageLevels\":{\"1\":\"Pocz\\u0105tkuj\\u0105cy\",\"2\":\"\\u015arednioZaawansowany\",\"3\":\"Zaawansowany\",\"4\":\"Ekspert\"}}\n";
		final List<String> maskPatterns = new ArrayList<>();
		maskPatterns.add("\\\"1\\\"\\s*:\\s*\\\"(.*?)\\\"");
		final CustomPatternLayout customPatterLayout = new CustomPatternLayout(maskPatterns);
		
		final String expectedJSON = "{\"LanguageLevels\":{\"1\":\"**********************\",\"2\":\"\\u015arednioZaawansowany\",\"3\":\"Zaawansowany\",\"4\":\"Ekspert\"}}\n"
				+ "";
		Assert.assertEquals(expectedJSON, customPatterLayout.mask(nestedJSON));
	}

}
