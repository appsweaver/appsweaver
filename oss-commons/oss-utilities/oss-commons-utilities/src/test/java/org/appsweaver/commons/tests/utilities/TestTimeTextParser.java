/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.tests.utilities;

import java.util.concurrent.TimeUnit;

import org.appsweaver.commons.utilities.TimeTextParser;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 * @author UD
 * 
 */
public class TestTimeTextParser extends AbstractJUnitTest {
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void testForMilliseconds() {
		/*
		 * Test for default or milliseconds
		 */
		final TimeTextParser parser1 = new TimeTextParser("10000");
		Assert.assertEquals((Integer)10000, parser1.getValue());
		Assert.assertEquals(TimeUnit.MILLISECONDS, parser1.getUnit());

		final TimeTextParser parser2 = new TimeTextParser("10000mil");
		Assert.assertEquals((Integer)10000, parser2.getValue());
		Assert.assertEquals(TimeUnit.MILLISECONDS, parser2.getUnit());

		final TimeTextParser parser3 = new TimeTextParser("10000 milli");
		Assert.assertEquals((Integer)10000, parser3.getValue());
		Assert.assertEquals(TimeUnit.MILLISECONDS, parser3.getUnit());

		final TimeTextParser parser4 = new TimeTextParser("12500 ms");
		Assert.assertEquals((Integer)12500, parser4.getValue());
		Assert.assertEquals(TimeUnit.MILLISECONDS, parser4.getUnit());
	}	

	@Test
	public void testForSeconds() {
		/*
		 * Test for seconds
		 */
		final TimeTextParser parser1 = new TimeTextParser("50 s");
		Assert.assertEquals((Integer)50, parser1.getValue());
		Assert.assertEquals(TimeUnit.SECONDS, parser1.getUnit());
		Assert.assertEquals(50*1000, parser1.toMilliseconds());

		final TimeTextParser parser2 = new TimeTextParser("50s");
		Assert.assertEquals((Integer)50, parser2.getValue());
		Assert.assertEquals(TimeUnit.SECONDS, parser2.getUnit());

		final TimeTextParser parser3 = new TimeTextParser("50 second");
		Assert.assertEquals((Integer)50, parser3.getValue());
		Assert.assertEquals(TimeUnit.SECONDS, parser3.getUnit());
		
		final TimeTextParser parser4 = new TimeTextParser("130 sec");
		Assert.assertEquals((Integer)130, parser4.getValue());
		Assert.assertEquals(TimeUnit.SECONDS, parser4.getUnit());
	}

	@Test
	public void testForNanoseconds() {
		/*
		 * Test for nanoseconds
		 */
		final TimeTextParser parser1 = new TimeTextParser("500000 n");
		Assert.assertEquals((Integer)500000, parser1.getValue());
		Assert.assertEquals(TimeUnit.NANOSECONDS, parser1.getUnit());

		final TimeTextParser parser2 = new TimeTextParser("500000n");
		Assert.assertEquals((Integer)500000, parser2.getValue());
		Assert.assertEquals(TimeUnit.NANOSECONDS, parser2.getUnit());

		final TimeTextParser parser3 = new TimeTextParser("500000 nano");
		Assert.assertEquals((Integer)500000, parser3.getValue());
		Assert.assertEquals(TimeUnit.NANOSECONDS, parser3.getUnit());
		
		final TimeTextParser parser4 = new TimeTextParser("12500 nanoseconds");
		Assert.assertEquals((Integer)12500, parser4.getValue());
		Assert.assertEquals(TimeUnit.NANOSECONDS, parser4.getUnit());
	}

	@Test
	public void testForMocroseconds() {
		/*
		 * Test for microseconds
		 */
		final TimeTextParser parser1 = new TimeTextParser("12500 u");
		Assert.assertEquals((Integer)12500, parser1.getValue());
		Assert.assertEquals(TimeUnit.MICROSECONDS, parser1.getUnit());

		final TimeTextParser parser2 = new TimeTextParser("12500u");
		Assert.assertEquals((Integer)12500, parser2.getValue());
		Assert.assertEquals(TimeUnit.MICROSECONDS, parser2.getUnit());

		final TimeTextParser parser3 = new TimeTextParser("12500 microsecond");
		Assert.assertEquals((Integer)12500, parser3.getValue());
		Assert.assertEquals(TimeUnit.MICROSECONDS, parser3.getUnit());

		final TimeTextParser parser4 = new TimeTextParser("12500 microseconds");
		Assert.assertEquals((Integer)12500, parser4.getValue());
		Assert.assertEquals(TimeUnit.MICROSECONDS, parser4.getUnit());
	}

	@Test
	public void testForHours() {

		/*
		 * Test for hours
		 */
		final TimeTextParser parser1 = new TimeTextParser("1 h");
		Assert.assertEquals((Integer)1, parser1.getValue());
		Assert.assertEquals(TimeUnit.HOURS, parser1.getUnit());

		final TimeTextParser parser2 = new TimeTextParser("1h");
		Assert.assertEquals((Integer)1, parser2.getValue());
		Assert.assertEquals(TimeUnit.HOURS, parser2.getUnit());

		final TimeTextParser parser3 = new TimeTextParser("1 hour");
		Assert.assertEquals((Integer)1, parser3.getValue());
		Assert.assertEquals(TimeUnit.HOURS, parser3.getUnit());
	}

	@Test
	public void testForMinutes() {
		/*
		 * Test for minutes
		 */
		final TimeTextParser parser1 = new TimeTextParser("30 min");
		Assert.assertEquals((Integer)30, parser1.getValue());
		Assert.assertEquals(TimeUnit.MINUTES, parser1.getUnit());

		final TimeTextParser parser2 = new TimeTextParser("30min");
		Assert.assertEquals((Integer)30, parser2.getValue());
		Assert.assertEquals(TimeUnit.MINUTES, parser2.getUnit());

		final TimeTextParser parser3 = new TimeTextParser("30 minute");
		Assert.assertEquals((Integer)30, parser3.getValue());
		Assert.assertEquals(TimeUnit.MINUTES, parser3.getUnit());
	}

	@Test
	public void testForDays() {
		/*
		 * Test for days
		 */
		final TimeTextParser parser1 = new TimeTextParser("5 d");
		Assert.assertEquals((Integer)5, parser1.getValue());
		Assert.assertEquals(TimeUnit.DAYS, parser1.getUnit());

		final TimeTextParser parser2 = new TimeTextParser("5d");
		Assert.assertEquals((Integer)5, parser2.getValue());
		Assert.assertEquals(TimeUnit.DAYS, parser2.getUnit());

		final TimeTextParser parser3 = new TimeTextParser("5 day");
		Assert.assertEquals((Integer)5, parser3.getValue());
		Assert.assertEquals(TimeUnit.DAYS, parser3.getUnit());
		
		final TimeTextParser parser4 = new TimeTextParser("5 days");
		Assert.assertEquals((Integer)5, parser4.getValue());
		Assert.assertEquals(TimeUnit.DAYS, parser4.getUnit());
	}

	@Test
	public void testForUnsupportedTimeUnit() {
		/*
		 * Test for invalid #1
		 */
		try {
			new TimeTextParser("5 zeros");
		} catch(Throwable t) {
			Assume.assumeTrue(t instanceof IllegalArgumentException);
			Assert.assertEquals("Unsupported time unit text 'zeros'", t.getMessage());
			Assume.assumeNoException(t);
		}
	}

	@Test
	public void testForEmptyStringInput() {
		/*
		 * Test for invalid #2
		 */
		try {
			new TimeTextParser("");
		} catch(Throwable t) {
			Assume.assumeTrue(t instanceof IllegalArgumentException);
			Assert.assertEquals("Time text parser needs a non-emtpy value", t.getMessage());
			Assume.assumeNoException(t);
		}
	}

	@Test
	public void testForNullInput() {
		/*
		 * Test for invalid #3
		 */
		try {
			new TimeTextParser(null);
		} catch(Throwable t) {
			Assume.assumeTrue(t instanceof IllegalArgumentException);
			Assert.assertEquals("Time text parser needs a non-emtpy value", t.getMessage());
			Assume.assumeNoException(t);
		}
	}
}