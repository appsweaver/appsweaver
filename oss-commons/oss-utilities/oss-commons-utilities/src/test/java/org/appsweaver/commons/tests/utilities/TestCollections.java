/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.tests.utilities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import org.appsweaver.commons.utilities.Collections;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 * @author UD
 * 
 */
public class TestCollections extends AbstractJUnitTest {
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void testNewEmptyArray() {
		Assert.assertArrayEquals(new Object[] {}, Collections.newArray());
	}
	
	@Test
	public void testNewArrayWithNulls() {
		Assert.assertArrayEquals(new Object[] {null, null}, Collections.newArray(null, null));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testToArrayWithNullParameters() {
		Assert.assertArrayEquals(new Object[] {}, Collections.toArray(null, null));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testToArrayWithFirstParameterAsNull() {
		Assert.assertArrayEquals(new Object[] {}, Collections.toArray(null, String.class));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testToArrayWithSecondParameterAsNull() {
		Assert.assertArrayEquals(new Object[] {}, Collections.toArray(Collections.newList(), null));
	}
	
	@Test
	public void testToArrayWithNullValues() {
		final List<String> strings = new ArrayList<>();
		strings.add(null);
		strings.add("Test");
		
		Assert.assertArrayEquals(new String[] {"Test"}, Collections.toArray(strings, String.class));
	}
	
	@Test
	public void testNewArray() {
		Assert.assertArrayEquals(new Integer[] {100, 200, 300}, Collections.newArray(100, 200, 300));
	}
	
	@Test
	public void testNewListAndToArray() {
		final List<Long> longs = Collections.newList(100L, 200L, 300L, 400L);
		Assert.assertArrayEquals(new Long[] {100L, 200L, 300L, 400L}, Collections.toArray(longs, Long.class));
	}
	
	@Test
	public void testNewListAndToArrayWithNullAndValidValues() {
		final List<Long> longs = Collections.newList(100L, null, 200L, null, 300L, 400L);
		Assert.assertArrayEquals(new Long[] {100L, 200L, 300L, 400L}, Collections.toArray(longs, Long.class));
	}
	
	@Test
	public void testNewListAndToArrayWithEmptyCollection() {
		final List<Long> longs = Collections.newList();
		Assert.assertArrayEquals(new Long[] {}, Collections.toArray(longs, Long.class));
	}
	
	@Test
	public void testNewListAndToArrayWithOnlyNullValues() {
		final List<Long> longs = Collections.newList(10L, null, null, null);
		Assert.assertArrayEquals(new Long[] {10L}, Collections.toArray(longs, Long.class));
	}
	
	@Test
	public void testNewSortedSetWithNullValues() {
		final TreeSet<Long> longs = Collections.newSortedSet(100L, 200L, null, 300L, 400L);
		Assert.assertArrayEquals(new Long[] {100L, 200L, 300L, 400L}, Collections.toArray(longs, Long.class));
	}
	
	@Test
	public void testNewHashSetWith() {
		final HashSet<Long> longs = Collections.newHashSet(100L, 200L, 300L, 400L);
		Assert.assertArrayEquals(new Long[] {400L, 100L, 200L, 300L}, Collections.toArray(longs, Long.class));
	}
	
	@Test
	public void testNewHashSetWithNull() {
		final HashSet<Long> longs = Collections.newHashSet(100L, null, 200L, null, 300L, null, 400L);
		Assert.assertArrayEquals(new Long[] {400L, 100L, 200L, 300L}, Collections.toArray(longs, Long.class));
	}
	
	@Test
	public void testNewListWithCollections() {
		final List<Long> longsFirst = Collections.newList(100L, 200L, 300L, 400L);
		Assert.assertArrayEquals(new Long[] {100L, 200L, 300L, 400L}, Collections.toArray(longsFirst, Long.class));

		final List<Long> longsSecond = Collections.newList(500L, 600L, 700L, 800L);
		Assert.assertArrayEquals(new Long[] {500L, 600L, 700L, 800L}, Collections.toArray(longsSecond, Long.class));

		final List<Long> longsExtra = Collections.newList(longsFirst, longsSecond);
		Assert.assertArrayEquals(new Long[] {100L, 200L, 300L, 400L, 500L, 600L, 700L, 800L}, Collections.toArray(longsExtra, Long.class));
	}
	
	@Test
	public void testNewListWithCollection() {
		final List<Long> longs = Collections.newList(100L, 200L, 300L, 400L);
		final List<Long> longsExtra = Collections.newList(longs, 500L, 600L, 700L, 800L);

		
		Assert.assertArrayEquals(new Long[] {100L, 200L, 300L, 400L, 500L, 600L, 700L, 800L}, Collections.toArray(longsExtra, Long.class));
	}
	
	@Test
	public void testNewListWithCollectionAndEmptyElements() {
		final List<Long> longs = Collections.newList(100L, 200L, null, 300L, 400L);
		final List<Long> longsExtra = Collections.newList(longs, new Long[] {});
		
		Assert.assertArrayEquals(new Long[] {100L, 200L, 300L, 400L}, Collections.toArray(longsExtra, Long.class));
	}
	
	@Test
	public void testNewListWithNullPrimaryCollection() {
		final List<Long> longsFirst = Collections.newList(100L, 200L, null, 300L, 400L);
		Assert.assertArrayEquals(new Long[] {100L, 200L, 300L, 400L}, Collections.toArray(longsFirst, Long.class));

		final List<Long> longsSecond = Collections.newList(500L, null, 600L, null, 700L, 800L);
		Assert.assertArrayEquals(new Long[] {500L, 600L, 700L, 800L}, Collections.toArray(longsSecond, Long.class));

		final List<Long> longsExtra = Collections.newList(longsFirst, null, longsSecond, null);
		Assert.assertArrayEquals(new Long[] {100L, 200L, 300L, 400L, 500L, 600L, 700L, 800L}, Collections.toArray(longsExtra, Long.class));
	}
	
	@Test
	public void testNewEmptyListWithEmptyArray() {
		final Integer[] integers = new Integer[] {};
		Assert.assertArrayEquals(new Integer[] {}, Collections.toArray(Collections.newList(integers), Integer.class));
	}
	
	@Test
	public void testNewSortedSetWithEmptyArray() {
		final Integer[] integers = new Integer[] {};
		Assert.assertArrayEquals(new Integer[] {}, Collections.toArray(Collections.newSortedSet(integers), Integer.class));
	}
	
	@Test
	public void testNewHashSetWithEmptyArray() {
		final Integer[] integers = new Integer[] {};
		Assert.assertArrayEquals(new Integer[] {}, Collections.toArray(Collections.newHashSet(integers), Integer.class));
	}
	
	@Test
	public void testNewListWithEmptyCollectionAndMoreValidAndNullValues() {
		final List<Integer> integers = Collections.newList(Collections.newList(), 10, 20, null, 30);
		Assert.assertArrayEquals(new Integer[] {10, 20, 30}, Collections.toArray(integers, Integer.class));
	}
	
	@Test
	public void testNewListWithEmptyAndNullCollectionAndMoreValidAndNullValues() {
		final List<Integer> integers1 = null;
		final List<Integer> integers2 = new ArrayList<>();
		integers2.add(null);
		integers2.add(10);
		integers2.add(20);
		integers2.add(30);

		final List<Integer> list = Collections.newList(integers1, integers2);
		Assert.assertArrayEquals(new Integer[] {10, 20, 30}, Collections.toArray(list, Integer.class));
	}
	
	@Test
	public void testNewListWithNullCollectionAndMoreValidAndNullValues() {
		final List<Integer> integers1 = null;

		final List<Integer> list = Collections.newList(integers1, 10, 20, 30);
		Assert.assertArrayEquals(new Integer[] {10, 20, 30}, Collections.toArray(list, Integer.class));
	}
	
	@Test
	public void testExtendedNewListWithEmptyAndNullCollectionAndMoreValidAndNullValues() {
		final List<Integer> integers1 = new ArrayList<>();
		final List<Integer> integers2 = null;
		
		final List<Integer> integers3 = new ArrayList<>();
		integers3.add(null);
		integers3.add(10);
		integers3.add(20);
		integers3.add(30);
		
		final List<Integer> list = Collections.newList(integers2, integers1, integers2, integers3);
		Assert.assertArrayEquals(new Integer[] {10, 20, 30}, Collections.toArray(list, Integer.class));
	}
	
	@Test
	public void testEmptyHashSet() {
		final Map<Long, String> map = Collections.newHashMap();
		map.put(10L, "Hello");
		map.put(20L, "World");
		
		Assert.assertEquals("Hello", map.get(10L));
		Assert.assertEquals("World", map.get(20L));
	}
	
	@Test
	public void testListWithCollectionsAndNull() {
		final List<Long> longsFirst = Collections.newList(100L, 200L, null, 300L, 400L);
		Assert.assertArrayEquals(new Long[] {100L, 200L, 300L, 400L}, Collections.toArray(longsFirst, Long.class));

		final List<Long> longsSecond = Collections.newList(500L, null, 600L, null, 700L, 800L);
		Assert.assertArrayEquals(new Long[] {500L, 600L, 700L, 800L}, Collections.toArray(longsSecond, Long.class));

		final List<Long> longsExtra = Collections.newList(longsFirst, null, longsSecond, null);
		Assert.assertArrayEquals(new Long[] {100L, 200L, 300L, 400L, 500L, 600L, 700L, 800L}, Collections.toArray(longsExtra, Long.class));
	}
}
