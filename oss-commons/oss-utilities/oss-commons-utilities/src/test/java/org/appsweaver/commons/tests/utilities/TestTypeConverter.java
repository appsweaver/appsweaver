/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.tests.utilities;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Randomizer;
import org.appsweaver.commons.utilities.TypeConverter;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import com.github.drapostolos.typeparser.TypeParserException;

/**
 * 
 * @author UD
 * 
 */
public class TestTypeConverter extends AbstractJUnitTest {
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void testSimpleTypeConversions() throws Throwable {
		final Integer integerValue = TypeConverter.valueOf("100", Integer.class);
		Assert.assertEquals((Integer)100, integerValue);
		
		final Long longValue = TypeConverter.valueOf("100", Long.class);
		Assert.assertEquals((Long)100L, longValue);
		
		final String stringValue = TypeConverter.valueOf("Hello World", String.class);
		Assert.assertEquals("Hello World", stringValue);
		
		final String randomUUID = Randomizer.asString();
		final UUID uuidValue = TypeConverter.valueOf(randomUUID, UUID.class);
		Assert.assertEquals(Randomizer.uuid(randomUUID), uuidValue);
		
		final String enumString = EnumType.HONEY.name();
		final EnumType enumValue = TypeConverter.valueOf(enumString, EnumType.class);
		Assert.assertEquals(EnumType.valueOf(enumString), enumValue);
		
		try {
			final String invalidEnumString = "BadValue";
			TypeConverter.valueOf(invalidEnumString, EnumType.class);
		} catch(Throwable t) {
			Assume.assumeTrue(t instanceof TypeParserException);
			Assume.assumeNoException(t);
		}
	}
	
	@Test
	public void testCollectionTypeConversions() throws Throwable {
		final List<Integer> integerList = TypeConverter.toList("100, 200, 300", Integer.class);
		Assert.assertEquals(Collections.newList(100, 200, 300), integerList);
		
		final Set<Integer> integerSet = TypeConverter.toSet("100, 200, 300", Integer.class);
		Assert.assertEquals(Collections.newHashSet(100, 200, 300), integerSet);
		
		final List<Long> longList = TypeConverter.toList("100, 200, 300", Long.class);
		Assert.assertEquals(Collections.newList(100L, 200L, 300L), longList);
		
		final Set<Long> longSet = TypeConverter.toSet("100, 200, 300", Long.class);
		Assert.assertEquals(Collections.newHashSet(100L, 200L, 300L), longSet);
		
		final List<String> stringList = TypeConverter.toList("Bingo, Dingo, Xingo, Xingo", String.class);
		Assert.assertEquals(Collections.newList("Bingo", "Dingo", "Xingo", "Xingo"), stringList);
		
		final Set<String> stringSet = TypeConverter.toSet("Bingo, Dingo, Xingo, Xingo", String.class);
		Assert.assertEquals(Collections.newHashSet("Bingo", "Dingo", "Xingo"), stringSet);
		
		/*
		 * Invalid type will be ignored with a warning in the logs 
		 */
		final Set<UUID> uuidSet = TypeConverter.toSet("bbff159c-18ef-4854-a0a0-8c7224f037c8, 65b99c97-b567-46e7-a541-6496f6347adf, 28f0445e-4b29-4a5e-bc83-606de1939944, Invalid-UUID", UUID.class);
		Assert.assertEquals(Collections.newHashSet(
				Randomizer.uuid("bbff159c-18ef-4854-a0a0-8c7224f037c8"), 
				Randomizer.uuid("65b99c97-b567-46e7-a541-6496f6347adf"),
				Randomizer.uuid("28f0445e-4b29-4a5e-bc83-606de1939944")
			), uuidSet
		);
		
		final Set<EnumType> enumSet = TypeConverter.toSet("GINGER, HONEY, LEMON, BadValue", EnumType.class);
		Assert.assertEquals(Collections.newHashSet(EnumType.GINGER, EnumType.HONEY, EnumType.LEMON), enumSet);
	}
	
	public enum EnumType {
		GINGER,
		HONEY,
		LEMON
	}
}
