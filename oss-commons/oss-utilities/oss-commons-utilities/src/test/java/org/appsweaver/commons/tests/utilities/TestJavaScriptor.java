/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.tests.utilities;

import java.io.File;

import org.appsweaver.commons.utilities.JavaScriptor;
import org.appsweaver.commons.utilities.TypeConverter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 * @author UD
 * 
 */
public class TestJavaScriptor extends AbstractJUnitTest {
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void testEchoScript() throws Throwable {
		final File script = getResourceAsFile("TestJavaScriptor/echo.js");
		final String message = "AppsWeavers are fantabulous people!";
		final Object object = JavaScriptor.execute(script, "echo", message);
		
		final String value = TypeConverter.valueOf(object, String.class);
		System.out.println("echo: " + value);
		
		Assert.assertEquals(String.format("ECHO: %s", message), value);
	}
	
	@Test
	public void testDecisionScript() throws Throwable {
		final File script = getResourceAsFile("TestJavaScriptor/decision.js");
		
		Assert.assertEquals(
			"ELECTRIC-CARS-US-WEST",
			String.valueOf(
				JavaScriptor.execute(script, "decision", "TESLA", "CAR", "US-WEST")
			)
		);
		
		Assert.assertEquals(
			"ELECTRIC-CARS-US-EAST",
			String.valueOf(
				JavaScriptor.execute(script, "decision", "NISSAN", "CAR", "US-EAST")
			)
		);
		
		Assert.assertEquals(
			"HP-SOFTWARE-WORLD-WIDE",
			String.valueOf(
				JavaScriptor.execute(script, "decision", "HP", "SOFTWARE", "WORLD-WIDE")
			)
		);
	}
}
