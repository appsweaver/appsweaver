/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.tests.utilities;

import org.appsweaver.commons.models.types.ParameterizedEnum;
import org.appsweaver.commons.utilities.Ruminator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 * @author UD
 * 
 */
public class TestRuminator extends AbstractJUnitTest {
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void testNewEmptyArray() throws Throwable {
		final Thing a = Thing.A;
		Assert.assertEquals(Thing.A, a);
		
		final Thing c = Ruminator.toEnum(Thing.class, "Cat");
		Assert.assertEquals(Thing.C, c);
		
		final Thing b = c.from("Ball");
		Assert.assertEquals(Thing.B, b);
		
		final SomeObject someObject = new SomeObject();
		Ruminator.setProperty(
			someObject, "something",
			Ruminator.toEnum(Thing.class, "Zero")
		);
		
		final Integer number = 100;
		someObject.setNumber(number);
		
		Assert.assertEquals(Thing.Z, someObject.getSomething());
		Assert.assertEquals(number, someObject.getNumber());
	}
	
	public class SomeObject {
		private Thing something;
		private Integer number;
		
		public SomeObject() {}
		
		public SomeObject(Thing something, Integer number) {
			this.something = something;
			this.number = number;
		}

		public Thing getSomething() {
			return something;
		}

		public void setSomething(Thing something) {
			this.something = something;
		}

		public Integer getNumber() {
			return number;
		}

		public void setNumber(Integer number) {
			this.number = number;
		}
	}
	
	public enum Thing implements ParameterizedEnum {
		A("Apple"),
		B("Ball"),
		C("Cat"),
		Z("Zero")
		;
		
		Thing(String value) {
			this.value = value;
		}
		
		private String value;
		
		@Override
		public String getValue() {
			return value;
		}
	}
}
