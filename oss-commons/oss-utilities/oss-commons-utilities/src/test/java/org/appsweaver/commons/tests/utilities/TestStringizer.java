/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.tests.utilities;

import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Stringizer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 * @author UD
 * 
 */
public class TestStringizer extends AbstractJUnitTest {
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void testFileToLines() {
		final File someFile = getResourceAsFile("TestFileDiffer/first.txt");
		
		try {
			final List<String> fileToLines = Stringizer.fileToLines(someFile.getAbsolutePath());
			Assert.assertTrue(fileToLines.size() == 15);
			Assert.assertEquals("Software Engineer leadership is where you lead/manage a number of software engineers, directly or via another manager(s).", fileToLines.get(0));
		} catch(Throwable t) {

		}
	}
	
	@Test
	public void testToStringList() {
		final List<String> list1 = Stringizer.toList("HELLO|WORLD|IS|HERE!", "\\|");
		Assert.assertEquals(Collections.newList("HELLO", "WORLD", "IS", "HERE!"), list1);
		
		final List<String> list2 = Stringizer.toList("HELLO", ",");
		Assert.assertEquals(Collections.newList("HELLO"), list2);
		
		final List<String> list3 = Stringizer.toList("To change the world, you need to see it differently! *'s will shine", "[ ,]");
		Assert.assertEquals(
			Collections.newList("To", "change", "the", "world", "you", "need", "to", "see", "it", "differently!", "*'s", "will", "shine")
			, list3
		);
		
		Assert.assertEquals(Collections.newList("10", "20", "30", "40")
			, Stringizer.toList("10, 20, 30, 40")
		);
		
		Assert.assertEquals(Collections.newList("10", "20", "30", "40")
			, Stringizer.toList("10; 20; 30; 40", ";")
		);
		
		Assert.assertEquals(Collections.newList("10", "20", "30", "40")
				, Stringizer.toList("10; , 20; 30; 40", "[;,]")
			);
	}
	
	@Test
	public void testGenericStringOperations() throws Throwable {
		Assert.assertEquals("HelloWorld!", Stringizer.trimAll("		Hello        World!     "));
		
		Assert.assertFalse(Stringizer.isNumeric("10.009.00"));
		Assert.assertTrue(Stringizer.isNumeric("10.009"));
		Assert.assertTrue(Stringizer.isNumeric("10"));
		Assert.assertTrue(Stringizer.isNumeric("-10"));
		Assert.assertTrue(Stringizer.isNumeric("-10.23"));
		Assert.assertFalse(Stringizer.isNumeric(""));
		Assert.assertFalse(Stringizer.isNumeric(null));


		Assert.assertFalse(Stringizer.isNumeric("10ABC"));

		Assert.assertTrue(Stringizer.isEmpty("        "));
		Assert.assertTrue(Stringizer.isEmpty(null));
		
		Assert.assertFalse(Stringizer.isEmpty("   Bingo!     "));
		
		Assert.assertEquals("Appsweaver", Stringizer.capitalize("appsWeaver"));

		Assert.assertEquals("world!", Stringizer.getLastWord("This is a simple yet beautiful world!"));
		
		Assert.assertEquals("one world, one sun", Stringizer.dehyphenate("one-world, one_sun"));
		
		final String someWords = "Some things are great in life, other things can be improved!";
		final String someWordsHexed = Stringizer.toHex(someWords);
		Assert.assertEquals(someWords, Stringizer.fromHex(someWordsHexed));
		
		Collections.newList("", "A", "World", "of", "pandora", "is", "simply beautiful", "and", "avatar", "showed it rightly!").forEach(item -> {
			final String hexed = Stringizer.toHex(item);
			Assert.assertEquals(item, Stringizer.fromHex(hexed));
		});
		
		final String someBytes = new String("��3����.�@�l�e�");
		final String someBytesInHex = Stringizer.toHex(someBytes);
		Assert.assertEquals(someBytes, Stringizer.fromHex(someBytesInHex));
		
		Assert.assertEquals("=====", Stringizer.repeat("=", 5));
		Assert.assertEquals("", Stringizer.repeat("=", 0));
		Assert.assertEquals("1", Stringizer.repeat("1", 1));
		
		final Map<String, Object> map = new HashMap<>();
		map.put("A1", 100.00);
		map.put("A2", 200);
		map.put("A3", "Bin");
		map.put("A4", new SomeObject());
		map.put("A5", new SomeObject(30, "Dinner is ready!"));
		
		final byte[] serialized = Stringizer.toBytes(map);
		Assert.assertNotNull(serialized);
		
		final Map<String, Object> deserialized = Stringizer.toMap(serialized);
		Assert.assertNotNull(deserialized);
		
		Assert.assertEquals(map, deserialized);
		
		final byte[] someObjectAsBytes = Stringizer.toBytes(new SomeObject());
		Assert.assertNull(Stringizer.toMap(someObjectAsBytes));
		
		Assert.assertNotNull(Stringizer.encodeSHA512("Hello World!"));
		Assert.assertNull(Stringizer.extract("", ",", 2));
		Assert.assertEquals("1", Stringizer.extract("expression,10,1,2", ",", 2));
		Assert.assertEquals("2", Stringizer.extract("expression,10,1,2", ",", 3));
		Assert.assertNull(Stringizer.extract("expression,10,1,2", ",", 5));
	}
	
	@Test
	public void testToCSV() {
		Assert.assertEquals("10, 20, 30", Stringizer.toCSVString(Collections.newList("10, 20, 30")));
		String csvString = Stringizer.toCSVString(Collections.newList(10, 20, 30), Optional.ofNullable('"'));
		Assert.assertEquals("\"10\", \"20\", \"30\"", csvString);
	}
	
	@Test
	public void testUrlDecoding() throws Throwable {
		Assert.assertEquals("https://www.amazon.com/gcx/-/gfhz/api/stats/firstLoad?__token_=g79tfOVMk9H3RFHX2OmcjN3J4pGI2bgx6yXHZ7P61mafAAAAAQAAAABd+SIgcmF3AAAAACwKhgyOhVRGNdUbAfrPuA==", 
			Stringizer.decodeUrl("https://www.amazon.com/gcx/-/gfhz/api/stats/firstLoad?__token_=g79tfOVMk9H3RFHX2OmcjN3J4pGI2bgx6yXHZ7P61mafAAAAAQAAAABd%2BSIgcmF3AAAAACwKhgyOhVRGNdUbAfrPuA%3D%3D"));
	
		Assert.assertEquals("https://www.amazon.com/gcx/Gifts-for-Everyone/gfhz/?_encoding=UTF8&ref_=topnav_storetab_cm_gft_hol19",
			Stringizer.decodeUrl("https://www.amazon.com/gcx/Gifts-for-Everyone/gfhz/?_encoding=UTF8&ref_=topnav_storetab_cm_gft_hol19"));
	}
	
	@Test
	public void testCaseConversions() {
		Assert.assertEquals("hello_world", Stringizer.upperCamelToLowerCaseUnderscored("HelloWorld"));
		Assert.assertEquals("hello_world", Stringizer.upperCamelToLowerCaseUnderscored("HelloWorld"));
		
		Assert.assertEquals("HELLO_WORLD", Stringizer.lowerCamelToUpperCaseUnderscored("HelloWorld"));
		Assert.assertEquals("HELLO_WORLD", Stringizer.lowerCamelToUpperCaseUnderscored("HelloWorld"));
		Assert.assertEquals("HELLO", Stringizer.lowerCamelToUpperCaseUnderscored("hello"));
		
		Assert.assertEquals("hello_world", Stringizer.upperCamelToLowerCaseUnderscored("HelloWorld"));
		Assert.assertEquals("hello", Stringizer.upperCamelToLowerCaseUnderscored("hello"));
		Assert.assertEquals("hello_world", Stringizer.upperCamelToLowerCaseUnderscored("helloWorld"));
		
		Assert.assertEquals("HELLO_WORLD", Stringizer.upperCamelToUpperUnderscored("HelloWorld"));
		Assert.assertEquals("HELLO", Stringizer.upperCamelToUpperUnderscored("hello"));
		Assert.assertEquals("HELLO_WORLD", Stringizer.upperCamelToUpperUnderscored("helloWorld"));
	}
	
	@Test
	public void testBetweens() {
		Assert.assertEquals("numbers are good", Stringizer.between("This is an expression (numbers are good) hey!", "(", ")"));
	}
	
	@Test
	public void testEmailAddress() {
		Assert.assertTrue(Stringizer.isValidEmailAddress("simple@example.com"));
		Assert.assertTrue(Stringizer.isValidEmailAddress("very.common@example.com"));
		Assert.assertTrue(Stringizer.isValidEmailAddress("disposable.style.email.with+symbol@example.com"));
		Assert.assertTrue(Stringizer.isValidEmailAddress("other.email-with-hyphen@example.com"));
		Assert.assertTrue(Stringizer.isValidEmailAddress("fully-qualified-domain@example.com"));
		Assert.assertTrue(Stringizer.isValidEmailAddress("user.name+tag+sorting@example.com"));
		Assert.assertTrue(Stringizer.isValidEmailAddress("x@example.com"));
		Assert.assertTrue(Stringizer.isValidEmailAddress("example-indeed@strange-example.com"));
		Assert.assertTrue(Stringizer.isValidEmailAddress("1234567890123456789012345678901234567890123456789012345678901234+x@example.com"));
		Assert.assertTrue(Stringizer.isValidEmailAddress("example@s.example"));
		// Assert.assertTrue(Stringizer.isValidEmailAddress("admin@mailserver1"));
		// Assert.assertTrue(Stringizer.isValidEmailAddress("\" \"@example.org"));
		// Assert.assertTrue(Stringizer.isValidEmailAddress("\"john..doe\"@example.org"));
		// Assert.assertTrue(Stringizer.isValidEmailAddress("mailhost!username@example.org"));
		// Assert.assertTrue(Stringizer.isValidEmailAddress("user%example.com@example.org"));
		
		Assert.assertFalse(Stringizer.isValidEmailAddress(null));
		Assert.assertFalse(Stringizer.isValidEmailAddress(""));
		Assert.assertFalse(Stringizer.isValidEmailAddress("Abc.example.com"));
		Assert.assertFalse(Stringizer.isValidEmailAddress("A@b@c@example.com"));
		Assert.assertFalse(Stringizer.isValidEmailAddress("a\"b(c)d,e:f;g<h>i[j\\k]l@example.com"));
		Assert.assertFalse(Stringizer.isValidEmailAddress("just\"not\"right@example.com"));
		Assert.assertFalse(Stringizer.isValidEmailAddress("this is\"not\\allowed@example.com"));
		Assert.assertFalse(Stringizer.isValidEmailAddress("this\\ still\\\"not\\\\allowed@example.com"));
		Assert.assertFalse(Stringizer.isValidEmailAddress("@example.com"));
		Assert.assertFalse(Stringizer.isValidEmailAddress("@"));
	}
	
	@Test
	public void testBase64EncodeDecode() throws Throwable {
		final String text = "AppsWeaver-Team";
		final String encoded = Stringizer.encodeBase64(text);
		final String encodeAgain = Stringizer.encodeBase64(encoded);
		final String decoded = Stringizer.decodeBase64(encoded);
		final String decodeAgain = Stringizer.decodeBase64(decoded);

		System.out.println("Enocded: " + encoded);
		System.out.println("Encode Again: " + encodeAgain);
		System.out.println("Decoded: " + decoded);
		System.out.println("Decode Again: " + decodeAgain);

		Assert.assertEquals(encoded, encodeAgain);
		Assert.assertEquals(decoded, decodeAgain);
		Assert.assertEquals(text, decoded);
		Assert.assertEquals(text, decodeAgain);
	}
	
	@Test
	public void testBase64DecodeEncode() throws Throwable {
		final String text = "AppsWeaver Team Rocks!";
		final String encoded = "QXBwc1dlYXZlciBUZWFtIFJvY2tzIQ==";
		
		final String decoded = Stringizer.decodeBase64(encoded);
		final String encodeAgain = Stringizer.encodeBase64(encoded);
		final String decodeAgain = Stringizer.decodeBase64(encodeAgain);
		
		final String stringizerEncoded = Stringizer.encodeBase64(text);
		final String stringizerDecoded = Stringizer.decodeBase64(encoded);
		final String decodedByStringizer = Stringizer.decodeBase64(encoded);
		
		System.out.println("Stringizer Encoded: " + stringizerEncoded);
		System.out.println("Stringizer Decoded: " + stringizerDecoded);
		System.out.println("Decoded By Stringizer: " + decodedByStringizer);
		System.out.println("Enocded: " + encoded);
		System.out.println("Encode Again: " + encodeAgain);
		System.out.println("Decoded: " + decoded);
		System.out.println("Decode Again: " + decodeAgain);
		
		Assert.assertEquals(text, decoded);
		Assert.assertEquals(encoded, encodeAgain);
		Assert.assertEquals(text, decodeAgain);
		Assert.assertEquals(stringizerEncoded, encoded);
		Assert.assertEquals(text, stringizerDecoded);
		Assert.assertEquals(decodedByStringizer, text);
	}
	
	static class SomeObject implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 3517364944446058100L;
		
		Integer number;
		String text;
		
		public SomeObject() {
		}

		public SomeObject(Integer number, String text) {
			this.number = number;
			this.text = text;
		}

		public int getNumber() {
			return number;
		}

		public void setNumber(int number) {
			this.number = number;
		}

		public String getText() {
			return text;
		}

		public void setText(String text) {
			this.text = text;
		}
		
		@Override
		public boolean equals(Object object) {
			// Self comparison  
	        if (object == this) { 
	            return true; 
	        } 
	  
	        // Is instance of required class
	        if (!(object instanceof SomeObject)) { 
	            return false; 
	        } 
	        
			if(object instanceof SomeObject) {
				final SomeObject rhs = (SomeObject)object;
				
				if(
					this.number == null && rhs.number == null &&
					this.text == null && rhs.text == null
				) return true;
				
				return (
					(this.number == rhs.number) || (Integer.compare(this.number, rhs.number) == 0) &&
					(this.text == rhs.text) || (this.text.compareTo(rhs.text) == 0)
				);
			}
			
			return false;
		}
	}
}
