/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.tests.utilities;

import java.util.UUID;

import org.appsweaver.commons.utilities.Randomizer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 * @author UD
 * 
 */
public class TestRandomizer extends AbstractJUnitTest {
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void testRandomBoolean() throws Throwable {
		final Boolean nextBoolean = Randomizer.nextBoolean();

		Assert.assertNotEquals(null, nextBoolean);
	}
	
	@Test
	public void testRandomBetween() throws Throwable {
		final int random = Randomizer.randomBetween(100, 100);
		Assert.assertEquals(100, random);
	}
	
	@Test
	public void testRandomBytes() throws Throwable {
		final String randomBytes = Randomizer.randomBytes(10);
		Assert.assertEquals(10, randomBytes.length());
	}
	
	@Test
	public void testUUIDs() throws Throwable {
		final String uuidString = "934f8039-9a0a-4234-99b6-e08298ddb552";
		
		Assert.assertTrue(Randomizer.isUUID(uuidString));
		
		final UUID uuid0 = Randomizer.uuid(uuidString);
		Assert.assertEquals(uuidString, uuid0.toString());
		
		// UUID value for this call, should be same always!
		final UUID uuid1 = Randomizer.nameUUID("ChangeIsInevitable");
		System.out.println(uuid1);
		Assert.assertEquals("0555e767-ae3c-368c-a96a-63d489716767", uuid1.toString());
		
		final String asString = Randomizer.asString();
		Assert.assertTrue(Randomizer.isUUID(asString));
		
		final String invalid = "123-934f8039-9a0a-4234-99b6-e08298ddb552";
		Assert.assertFalse(Randomizer.isUUID(invalid));
	}
}
