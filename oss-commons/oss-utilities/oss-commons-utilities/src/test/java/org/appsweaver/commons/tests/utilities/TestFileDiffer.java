/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.tests.utilities;

import java.io.File;

import org.appsweaver.commons.utilities.FileDiffer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 * @author UD
 * 
 */
public class TestFileDiffer extends AbstractJUnitTest {
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void testForDiffs() throws Throwable {
		final File first = getResourceAsFile("TestFileDiffer/first.txt");
		final File second = getResourceAsFile("TestFileDiffer/second.txt");
		final File third = getResourceAsFile("TestFileDiffer/third.txt");
		
		final FileDiffer fileDiffer = new FileDiffer();
		final boolean fsDiff = fileDiffer.diff(first, second);
		Assert.assertTrue(fsDiff);
		
		final boolean ftDiff = fileDiffer.diff(first, third);
		Assert.assertFalse(ftDiff);
	}
}