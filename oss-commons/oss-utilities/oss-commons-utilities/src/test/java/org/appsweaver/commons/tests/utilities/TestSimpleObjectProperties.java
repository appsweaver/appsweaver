/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.tests.utilities;

import org.appsweaver.commons.utilities.Ruminator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 * @author UD
 * 
 */
public class TestSimpleObjectProperties extends AbstractJUnitTest {
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void testPropertyValueReader() throws Throwable {
		final A aObject = new A(100);
		final B bObject = new B(200, "AppsWeaver");
	
		final Object aIntProperty = Ruminator.getProperty(aObject, "intValue");
		Assert.assertEquals(Integer.class, aIntProperty.getClass());
		Assert.assertEquals(100, aIntProperty);
		System.out.println("Read int value from aObject : " + aIntProperty);
		
		final Object bIntProperty = Ruminator.getProperty(bObject, "intValue");
		Assert.assertEquals(Integer.class, bIntProperty.getClass());
		Assert.assertEquals(200, bIntProperty);
		System.out.println("Read int value from bObject : " + bIntProperty);

		
		final Object bStringProperty = Ruminator.getProperty(bObject, "stringValue");
		Assert.assertEquals(String.class, bStringProperty.getClass());
		Assert.assertEquals("AppsWeaver", bStringProperty);
		System.out.println("Read string value from bStringProperty : " + bStringProperty);
	}
	
	public class A {
		protected int intValue;

		public A(int intValue) {
			setIntValue(intValue);
		}
		
		public int getIntValue() {
			return intValue;
		}

		public void setIntValue(int intValue) {
			this.intValue = intValue;
		}
	}
	
	public class B extends A {
		private String stringValue;
		
		public B(int intValue, String stringValue) {
			super(intValue);
			
			setStringValue(stringValue);
		}

		public String getStringValue() {
			return stringValue;
		}

		public void setStringValue(String stringValue) {
			this.stringValue = stringValue;
		}
	}
}
