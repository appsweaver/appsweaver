/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.tests.utilities;

import java.time.Month;
import java.util.Date;
import java.util.Map;

import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Dates;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Test;

/**
 * 
 * @author UD
 * 
 */
public class TestDates extends AbstractJUnitTest {
	final static String customDateTimeFormat = "EEE MMM d kk:mm:ss yyyy";
	final static String dateTimeFormat = "yyyy-MM-dd kk:mm:ss.SSS zzz";
	final static String dateFormat = "yyyy-MM-dd";

	@Test
	public void testDatesToAndFromFormats() throws Throwable {
		final long now = Dates.toTime();
		final String nowString = Dates.toString(now, dateTimeFormat);
		final long decodedNow = Dates.toTime(nowString, dateTimeFormat);
		final String decodedNowString = Dates.toString(decodedNow, dateTimeFormat);
		final long anotherDecodedValue = Dates.toTime(decodedNowString, dateTimeFormat, null);
		
		Assert.assertTrue(Long.compare(now, decodedNow) == 0);
		Assert.assertEquals(nowString, decodedNowString);
		Assert.assertTrue(Long.compare(decodedNow, anotherDecodedValue) == 0);

		
		final long nowAgain = Dates.toTime();
		final String nowAgainString = Dates.toString(nowAgain, dateTimeFormat);
		final long decodedNowAgain = Dates.toTime(nowAgainString, dateTimeFormat);
		final String decodedNowAgainString = Dates.toString(decodedNowAgain, dateTimeFormat);

		Assert.assertTrue(Long.compare(nowAgain, decodedNowAgain) == 0);
		Assert.assertEquals(nowAgainString, decodedNowAgainString);

		final String simpleDateString = "2015-12-07";
		final long simpleDateTimestamp = Dates.toTime(simpleDateString, dateFormat);
		final String simpleDateDecodedString = Dates.toString(simpleDateTimestamp, dateFormat);
		final long simpleDateDecodedTimeStamp = Dates.toTime(simpleDateDecodedString, dateFormat);
		
		Assert.assertTrue(Long.compare(simpleDateTimestamp, simpleDateDecodedTimeStamp) == 0);
 		Assert.assertEquals(simpleDateString, simpleDateDecodedString);
 		
 		final String futureDateString = "2089-02-11 02:40:55.678 IST";
 		final String futureDateStringTZ = "IST";
		final long futureDateTimestamp = Dates.toTime(futureDateString, dateTimeFormat, futureDateStringTZ);
		final String futureDateDecodedString = Dates.toString(futureDateTimestamp, dateTimeFormat, futureDateStringTZ);
		final long futureDateDecodedTimeStamp = Dates.toTime(futureDateDecodedString, dateTimeFormat, futureDateStringTZ);
		
		Assert.assertTrue(Long.compare(futureDateTimestamp, futureDateDecodedTimeStamp) == 0);
 		Assert.assertEquals(futureDateString, futureDateDecodedString);

 		final String pastDateString = "2019-12-21 14:35:25.378 PST";
 		final String pastDateStringTZ = "PST";
		final long pastDateTimestamp = Dates.toTime(pastDateString, dateTimeFormat, pastDateStringTZ);
		final String pastDateDecodedString = Dates.toString(pastDateTimestamp, dateTimeFormat, pastDateStringTZ);
		final long pastDateDecodedTimeStamp = Dates.toTime(pastDateDecodedString, dateTimeFormat, pastDateStringTZ);
		
		Assert.assertTrue(Long.compare(pastDateTimestamp, pastDateDecodedTimeStamp) == 0);
 		Assert.assertEquals(pastDateString, pastDateDecodedString);
	}
	
	@Test
	public void testValidAndInvalidDateFacets() throws Throwable {
 		final String dateString = "1945-08-15 02:40:55.678 PST";
 		final String dateStringTZ = "PST";
 		final long timestamp = Dates.toTime(dateString, dateTimeFormat, dateStringTZ);
 		
 		Assert.assertEquals(1945, Dates.yearValue(timestamp));
 		
 		Assert.assertEquals(Month.AUGUST, Dates.month(timestamp));
 		Assert.assertEquals(8, Dates.monthValue(timestamp));
 		Assert.assertEquals("AUGUST", Dates.monthName(timestamp));
 		Assert.assertEquals("AUGUST", Dates.monthName(Dates.monthValue(timestamp)));
 		Assert.assertEquals("AUGUST", Dates.monthName(Dates.month(timestamp)));
 		
 		Assert.assertEquals((Integer)3, Dates.quarterValue(timestamp));
 		Assert.assertEquals("Q3", Dates.quarterName(timestamp));
 		Assert.assertEquals("Q3", Dates.quarterName(Dates.quarterValue(timestamp)));
 		
 		Assert.assertEquals((Integer)2, Dates.biannualValue(timestamp));
 		Assert.assertEquals("H2", Dates.biannualName(timestamp));
 		Assert.assertEquals("H2", Dates.biannualName(Dates.biannualValue(timestamp)));
 		
 		try {
 			// Impossible
 			Dates.biannualName(100);
 		} catch(Throwable t) {
 			Assert.assertTrue(t instanceof IllegalArgumentException);
 			Assert.assertEquals("Invalid bi-annual number 100", t.getMessage());
 			Assume.assumeNoException(t);
 		}
 		
 		try {
 			// Impossible
 			Dates.quarterName(8);
 		} catch(Throwable t) {
 			Assert.assertTrue(t instanceof IllegalArgumentException);
 			Assert.assertEquals("Invalid quarter number 8", t.getMessage());
 			Assume.assumeNoException(t);
 		}
 		
 		try {
 			// Impossible
 			Dates.monthName(82);
 		} catch(Throwable t) {
 			Assert.assertTrue(t instanceof IllegalArgumentException);
 			Assert.assertEquals("Invalid month number 82", t.getMessage());
 			Assume.assumeNoException(t);
 		}
	}
	
	@Test
	public void testDatesToAndFromFormatsWithTimeZone() throws Throwable {
		final String indeDateString = "1945-08-15";
		final long indeDateTimestamp = Dates.toTime(indeDateString, dateFormat, "IST");
		final String indeDateDecoded = Dates.toString(indeDateTimestamp, dateFormat, "IST");
 		Assert.assertEquals(indeDateString, indeDateDecoded);
 		
 		final String flightDateString = "2004-04-15";
		final long flightDateTimestamp = Dates.toTime(flightDateString, dateFormat, "GMT");
		final String flightDateDecoded = Dates.toString(flightDateTimestamp, dateFormat, "GMT");
 		Assert.assertEquals(flightDateString, flightDateDecoded);
	}
	
	@Test
	public void testCorrectConverstion() throws Throwable {
		final String dateValue = "Wed Jan 17 00:13:19 2018";
		final Long expected = Dates.toTime(dateValue, customDateTimeFormat, "PST");
		
		// Using long to compare as string's have time zone info
		final Date date = Dates.toDate(expected);
		final Long actual = Dates.toTime(date);
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void testWrongConverstion() throws Throwable {
		final String dateValue = "Wed Jan 17 00:13:19 2018";
		final Long time = Dates.toTime(dateValue, customDateTimeFormat);
		final Date date = Dates.toDate(time);
		Assert.assertNotEquals(dateValue, date.toString());
	}
	
	@Test
	public void testTimeDifference() throws Throwable {
		{
			final String dateValue1 = "Wed Jan 17 00:13:19 2018";
			final Long time1 = Dates.toTime(dateValue1, customDateTimeFormat);
			
			final String dateValue2 = "Wed Jan 18 00:13:19 2018";
			final Long time2 = Dates.toTime(dateValue2, customDateTimeFormat);
			
			final Map<String, Long> actual = Dates.timeDifferenceAsChronoUnits(time2, time1);
			System.out.println(actual);
			
			final Map<String, Long> expected = Collections.newHashMap();
			expected.put(Dates.ChronoUnit.DAYS, 1L);
			expected.put(Dates.ChronoUnit.HOURS, 0L);
			expected.put(Dates.ChronoUnit.MINUTES, 0L);
			expected.put(Dates.ChronoUnit.SECONDS, 0L);
			
			Assert.assertEquals(expected, actual);
		}
		
		{
			final String dateValue1 = "Wed Jan 17 00:13:19 2018";
			final Long time1 = Dates.toTime(dateValue1, customDateTimeFormat);
			
			final String dateValue2 = "Wed Jan 18 00:13:19 2018";
			final Long time2 = Dates.toTime(dateValue2, customDateTimeFormat);
			
			final Map<String, Long> actual = Dates.timeDifferenceAsChronoUnits(time2, time1, "hours", "minutes", "seconds");
			System.out.println(actual);
			
			final Map<String, Long> expected = Collections.newHashMap();
			expected.put(Dates.ChronoUnit.HOURS, 24L);
			expected.put(Dates.ChronoUnit.MINUTES, 0L);
			expected.put(Dates.ChronoUnit.SECONDS, 0L);
			
			Assert.assertEquals(expected, actual);
		}
		
		{
			final String dateValue1 = "Wed Jan 17 00:13:19 2018";
			final Long time1 = Dates.toTime(dateValue1, customDateTimeFormat);
			
			final String dateValue2 = "Wed Jan 18 00:13:19 2018";
			final Long time2 = Dates.toTime(dateValue2, customDateTimeFormat);
			
			final Map<String, Long> actual = Dates.timeDifferenceAsChronoUnits(time2, time1, "minutes", "seconds");
			System.out.println(actual);
			
			final Map<String, Long> expected = Collections.newHashMap();
			expected.put(Dates.ChronoUnit.MINUTES, 1440L);
			expected.put(Dates.ChronoUnit.SECONDS, 0L);
			
			Assert.assertEquals(expected, actual);
		}
		
		{
			final String dateValue1 = "Wed Jan 17 00:13:19 2018";
			final Long time1 = Dates.toTime(dateValue1, customDateTimeFormat);
			
			final String dateValue2 = "Wed Jan 18 00:13:19 2018";
			final Long time2 = Dates.toTime(dateValue2, customDateTimeFormat);
			
			final Map<String, Long> actual = Dates.timeDifferenceAsChronoUnits(time2, time1, "seconds");
			System.out.println(actual);
			
			final Map<String, Long> expected = Collections.newHashMap();
			expected.put(Dates.ChronoUnit.SECONDS, 86400L);
			
			Assert.assertEquals(expected, actual);
		}
		
		{
			final String dateValue1 = "Wed Jan 17 00:13:19 2018";
			final Long time1 = Dates.toTime(dateValue1, customDateTimeFormat);
			
			final String dateValue2 = "Wed Jan 18 01:13:19 2018";
			final Long time2 = Dates.toTime(dateValue2, customDateTimeFormat);
			
			final Map<String, Long> actual = Dates.timeDifferenceAsChronoUnits(time2, time1);
			System.out.println(actual);
			
			final Map<String, Long> expected = Collections.newHashMap();
			expected.put(Dates.ChronoUnit.DAYS, 1L);
			expected.put(Dates.ChronoUnit.HOURS, 1L);
			expected.put(Dates.ChronoUnit.MINUTES, 0L);
			expected.put(Dates.ChronoUnit.SECONDS, 0L);
			
			Assert.assertEquals(expected, actual);
		}
		
		{
			final String dateValue1 = "Wed Jan 17 00:13:19 2018";
			final Long time1 = Dates.toTime(dateValue1, customDateTimeFormat);
			
			final String dateValue2 = "Wed Jan 18 01:17:19 2018";
			final Long time2 = Dates.toTime(dateValue2, customDateTimeFormat);
			
			final Map<String, Long> actual = Dates.timeDifferenceAsChronoUnits(time2, time1);
			System.out.println(actual);
			
			final Map<String, Long> expected = Collections.newHashMap();
			expected.put(Dates.ChronoUnit.DAYS, 1L);
			expected.put(Dates.ChronoUnit.HOURS, 1L);
			expected.put(Dates.ChronoUnit.MINUTES, 4L);
			expected.put(Dates.ChronoUnit.SECONDS, 0L);
			
			Assert.assertEquals(expected, actual);
		}
		
		{
			final String dateValue1 = "Wed Jan 17 00:13:19 2018";
			final Long time1 = Dates.toTime(dateValue1, customDateTimeFormat);
			
			final String dateValue2 = "Wed Jan 18 01:17:24 2018";
			final Long time2 = Dates.toTime(dateValue2, customDateTimeFormat);
			
			final Map<String, Long> actual = Dates.timeDifferenceAsChronoUnits(time2, time1);
			System.out.println(actual);
			
			final Map<String, Long> expected = Collections.newHashMap();
			expected.put(Dates.ChronoUnit.DAYS, 1L);
			expected.put(Dates.ChronoUnit.HOURS, 1L);
			expected.put(Dates.ChronoUnit.MINUTES, 4L);
			expected.put(Dates.ChronoUnit.SECONDS, 5L);
			
			Assert.assertEquals(expected, actual);
		}
		
		{
			final String dateValue1 = "Wed Mar 17 00:13:05 2018";
			final Long time1 = Dates.toTime(dateValue1, customDateTimeFormat);
			
			final String dateValue2 = "Wed Feb 11 05:21:51 2019";
			final Long time2 = Dates.toTime(dateValue2, customDateTimeFormat);
			
			final Map<String, Long> actual = Dates.timeDifferenceAsChronoUnits(time2, time1);
			System.out.println(actual);
			
			final Map<String, Long> expected = Collections.newHashMap();
			expected.put(Dates.ChronoUnit.DAYS, 331L);
			expected.put(Dates.ChronoUnit.HOURS, 5L);
			// The difference must be 5 hours, need to dig deeper to see why the 6 Hours was valid :-(
			expected.put(Dates.ChronoUnit.MINUTES, 8L);
			expected.put(Dates.ChronoUnit.SECONDS, 46L);
			
			Assert.assertEquals(expected, actual);
		}
	}
}
