/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.tests.utilities;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Ruminator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 * @author UD
 * 
 */
public class TestNestedObjectProperties extends AbstractJUnitTest {
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void testPropertyValueReader() throws Throwable {
		final NumberClass aObject = new NumberClass(100);
		final StringClass bObject = new StringClass("Hello World");
		final ComplexClass cObject = new ComplexClass();
		
		final Object numberFromObjectA = Ruminator.getProperty(aObject, "number");
		System.out.println(String.format("NumberClass.number: %s", numberFromObjectA));
		Assert.assertEquals(100, numberFromObjectA);

		final Object stringFromObjectB = Ruminator.getProperty(bObject, "string");
		System.out.println(String.format("NumberClass.string: %s", stringFromObjectB));
		Assert.assertEquals("Hello World", stringFromObjectB);

		final Object numbersFromObjectC = Ruminator.getProperty(cObject, "numbers.number");
		System.out.println(String.format("ComplexClass.numbers: %s", String.valueOf(numbersFromObjectC)));
		Assert.assertEquals("[1, 99, 31, 919]", numbersFromObjectC);
		
		final Object stringsFromObjectC = Ruminator.getProperty(cObject, "strings.string");
		System.out.println(String.format("ComplexClass.strings: %s", String.valueOf(stringsFromObjectC)));
		Assert.assertEquals("[Bingo]", stringsFromObjectC);
	}
	
	public class NumberClass {
		private int number = 100;

		public NumberClass(int number) {
			this.number = number;
		}

		public int getNumber() {
			return number;
		}

		public void setNumber(int number) {
			this.number = number;
		}
	}
	
	public class StringClass {
		private String string;

		public StringClass(String string) {
			this.string = string;
		}

		public String getString() {
			return string;
		}

		public void setString(String string) {
			this.string = string;
		}
	}
	
	public class ComplexClass {
		private List<NumberClass> numbers = Collections.newList(new NumberClass(1), new NumberClass(99), new NumberClass(31), new NumberClass(919));
		private Set<StringClass> strings = Collections.newHashSet(new StringClass("Bingo"));
		private Map<String, UUID> nameUuidMap = Collections.newHashMap();

		public List<NumberClass> getNumbers() {
			return numbers;
		}

		public void setNumbers(List<NumberClass> numbers) {
			this.numbers = numbers;
		}

		public Set<StringClass> getStrings() {
			return strings;
		}

		public void setStrings(Set<StringClass> strings) {
			this.strings = strings;
		}

		public Map<String, UUID> getNameUuidMap() {
			return nameUuidMap;
		}

		public void setNameUuidMap(Map<String, UUID> nameUuidMap) {
			this.nameUuidMap = nameUuidMap;
		}
	}
}
