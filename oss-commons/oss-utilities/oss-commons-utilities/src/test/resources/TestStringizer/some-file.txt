Software Engineer leadership is where you lead/manage a number of software engineers, directly or via another manager(s).
Your title could be Engineering lead, Software Engineer manager, R&D Manager, Director of Engineering or VP of Engineering or CTO.

Your work will be split between four main areas: People, Process, Product and Technology. The higher you go the more your system 
design/architecture skills will matter and your coding less (unless you are in a small company).  You will be wrangling with 
managing people, process, product whilst evolving the technical vision to fit all of your constraints. Hopefully you will be 
delegating and keeping a lot of things out of the way of your engineers, so that your engineers can code.

In the end your short/medium term success will be determined by whether you can deliver the needs of product, keep your engineer 
team motivated and fully explain what is going on e.g. translating complex technical things into a language everyone can understand.
Your long term success will be based in whether you made the right choice/balance between building out features vs platform and 
whether you upgraded your team in the right way – all why you helped the company stay in business.

A layer of good, well-trained, and experienced managers makes a massive impact on allowing engineers to deliver on features and a 
solid platform, whilst growing those engineers to their full potential. A courageous leader will be copied, by others.