/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import ch.qos.logback.classic.spi.ILoggingEvent;

/**
 * 
 * @author amathew
 *
 */
public class CustomPatternLayout extends ch.qos.logback.classic.PatternLayout {

	private Pattern multilinePattern;
	private final List<String> maskPatterns = new ArrayList<>();

	public CustomPatternLayout(List<String> maskPatterns) {
		super();
		for (String maskPattern : maskPatterns) {
			addMaskPattern(maskPattern);
		}
	}

	public CustomPatternLayout() {
		super();
	}

	public void addMaskPattern(String maskPattern) {
		/* invoked for every single entry in the xml */
		maskPatterns.add(maskPattern);
		/* build pattern using logical OR */
		multilinePattern = Pattern.compile(maskPatterns.stream().collect(Collectors.joining("|")), Pattern.MULTILINE);
	}

	@Override
	public String doLayout(ILoggingEvent event) {
		/* calling superclass method is required */
		return mask(super.doLayout(event));
	}

	public String mask(String message) {
		if (multilinePattern == null) {
			return message;
		}
		final StringBuilder messageBuilder = new StringBuilder(message);
		final Matcher matcher = multilinePattern.matcher(messageBuilder);
		while (matcher.find()) {
			IntStream.rangeClosed(1, matcher.groupCount()).forEach(group -> {
				if (matcher.group(group) != null) {
					/* replace each character with asterisk */
					IntStream.range(matcher.start(group), matcher.end(group))
							.forEach(i -> messageBuilder.setCharAt(i, '*'));
				}
			});
		}
		return messageBuilder.toString();
	}

}
