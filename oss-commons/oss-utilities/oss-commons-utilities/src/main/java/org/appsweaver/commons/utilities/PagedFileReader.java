
/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */package org.appsweaver.commons.utilities;
 
import java.io.File;
import java.util.List;
import java.util.TreeMap;

import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Fileinator;

/**
 * 
 * @author UD
 * 
 */
public class PagedFileReader {
	final List<String> lines = Collections.newList();

	final File file;
	
	final boolean lineNumbered;
	
	final Long size;
	
	public PagedFileReader(String path, boolean lineNumbered) throws Throwable {
		if(path == null) {
			throw new Throwable("Given file path is null");
		}
		
		this.lineNumbered = lineNumbered;
		this.file = new File(path);
		
		if(!this.file.exists()) {
			throw new Throwable(String.format("File [%s] does not exist!", this.file.getAbsolutePath()));
		}
		
		this.size = Fileinator.countLines(this.file);
	}
	
	public void readLines(int start, int count) throws Exception {
		final String format = String.format("%%%dd: %%s", String.valueOf(size).length() - 1); 
		final TreeMap<Long, String> lines = Fileinator.read(file, start, count);
		
		final int keyOffset = (start <= 0) ? 1 : 0;
		
		lines.forEach((key, value) -> {	
			if(lineNumbered) {
				this.lines.add(String.format(format, key + keyOffset, value));
			} else {
				this.lines.add(String.format("%s", value));
			}
		});
	}

	public List<String> getLines() {
		return lines;
	}

	public boolean isLineNumbered() {
		return lineNumbered;
	}

	public Long getSize() {
		return size;
	}
}
