/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.utilities.delayed;

import java.io.File;

/**
 *  
 * @author UD
 * 
 * Wait for file to be ready to read
 * 
 */
public class FileExistsDelayStrategy implements DelayStrategy {
	final File file;

	public FileExistsDelayStrategy(File file) {
		this.file = file;
	}

	@Override
	public boolean ok() {
		return file.exists();
	}
}
