/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.utilities.delayed;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * 
 * @author UD
 * 
 * Delayed looping and execution
 * 
 */
public class Delayer {
	private static void doDelayedWork(DelayStrategy delayStrategy, int delay, TimeUnit timeUnit, DelayedWork work) {
		// Do work (Optional)
		final Optional<DelayedWork> optionalWork = Optional.ofNullable(work);
		if(optionalWork.isPresent()) {
			optionalWork.get().execute();
		}

		// Delay until strategy condition is met or when max retries is reached
		if(!delayStrategy.ok()) {
			try {
				timeUnit.sleep(delay);
			} catch(Throwable t) {
			}
		}
	}
	
	public static void runWithMaxRetries(DelayStrategy delayStrategy, int delay, TimeUnit timeUnit, int max) {
		runWithMaxRetries(delayStrategy, delay, timeUnit, max, null);
	}
	
	public static void runWithMaxRetries(DelayStrategy delayStrategy, int delay, TimeUnit timeUnit, int max, DelayedWork work) {
		int count = 0;
		do {
			// Do delayed work
			doDelayedWork(delayStrategy, delay, timeUnit, work);
			
			// Break out of loop when strategy condition is met or when max retries is reached
		} while (count++ < max && !delayStrategy.ok());
	}

	public static void runUntilDone(DelayStrategy delayStrategy, int delay, TimeUnit timeUnit) {
		runUntilDone(delayStrategy, delay, timeUnit, null);
	}
	
	public static void runUntilDone(DelayStrategy delayStrategy, int delay, TimeUnit timeUnit, DelayedWork work) {
		do {
			// Do delayed work
			doDelayedWork(delayStrategy, delay, timeUnit, work);
			
			// Break out of loop when strategy condition is met
		} while (!delayStrategy.ok());
	}
}
