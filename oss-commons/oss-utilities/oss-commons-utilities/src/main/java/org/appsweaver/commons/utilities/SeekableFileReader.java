/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.NoSuchElementException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;


/**
 * 
 * @author UD
 * 
 */
public final class SeekableFileReader implements AutoCloseable {

	/** The Constant defaultCharset. */
	private final static Charset defaultCharset = Charset.forName("UTF-8");

	/** The input stream. */
	final InputStream inputStream;

	/** The line iterator. */
	final LineIterator lineIterator;

	/** The line read count. */
	long lineReadCount = -1L;

	/**
	 * Instantiates a new seekable file reader.
	 *
	 * @param inputStream the input stream
	 * @param charset the charset
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public SeekableFileReader(final InputStream inputStream, final Charset charset) throws IOException {
		this.inputStream = inputStream;
		this.lineReadCount = 1L;

		try {
			this.lineIterator = IOUtils.lineIterator(inputStream, charset);
		} catch (IOException e) {
			throw e;
		}
	}

	/**
	 * Instantiates a new seekable file reader.
	 *
	 * @param inputStream the input stream
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public SeekableFileReader(final InputStream inputStream) throws IOException {
		this(inputStream, defaultCharset);
	}

	/**
	 * Instantiates a new seekable file reader.
	 *
	 * @param file the file
	 * @param charsetName the charset name
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public SeekableFileReader(final File file, final Charset charsetName) throws IOException {
		this(new FileInputStream(file), charsetName);
	}

	/**
	 * Instantiates a new seekable file reader.
	 *
	 * @param file the file
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public SeekableFileReader(final File file) throws IOException {
		this(file, defaultCharset);
	}

	/**
	 * Seek.
	 *
	 * @return the long
	 */
	public long seek() {
		return seek(lineReadCount);
	}

	/**
	 * Seek.
	 *
	 * @param line the line
	 * @return the long
	 */
	public long seek(final long line) {
		long lineCount = 1L;
		while ((lineIterator != null) && (lineIterator.hasNext()) && (lineCount < line)) {
			lineIterator.nextLine();
			lineCount++;
		}
		if (lineCount < line) {
			throw new NoSuchElementException(String.format("Invalid line number [lineNumber=%s]", line));
		}
		lineReadCount = lineCount;
		return lineCount;
	}

	/* (non-Javadoc)
	 * @see java.lang.AutoCloseable#close()
	 */
	@Override
	public void close() {
		try {
			inputStream.close();
		} catch(Throwable t) {
		}

		try {
			lineIterator.close();
		} catch(Throwable t) {
		}
	}

	/**
	 * Checks for next.
	 *
	 * @return true, if successful
	 */
	public boolean hasNext() {
		return lineIterator.hasNext();
	}

	/**
	 * Read line.
	 *
	 * @return the string
	 * @throws Exception on any errors
	 */
	public String readLine() throws Exception {
		try {
			if(hasNext()) {
				final String nextLine = lineIterator.nextLine();
				lineReadCount++;
				return nextLine;
			} else {
				return null;
			}
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Gets the line read count.
	 *
	 * @return the line read count
	 */
	public long getLineReadCount() {
		return lineReadCount;
	}
}
