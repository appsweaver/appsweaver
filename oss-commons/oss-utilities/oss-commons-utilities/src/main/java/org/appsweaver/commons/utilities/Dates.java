/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.utilities;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TimeZone;

/**
 * 
 * @author UD
 * 
 */
public class Dates {
	public interface ChronoUnit {
		public static String DAYS = "days";
		public static String HOURS = "hours";
		public static String MINUTES = "minutes";
		public static String SECONDS = "seconds";
	}
	
	private Dates() {}
	
	static final Map<Month, Integer> monthToQuarterMap = new HashMap<Month, Integer>() {
		private static final long serialVersionUID = 956494200361042014L;
		{
			put(Month.JANUARY, 		1);
			put(Month.FEBRUARY, 	1);
			put(Month.MARCH, 		1);
			put(Month.APRIL, 		2);
			put(Month.MAY, 			2);
			put(Month.JUNE, 		2);
			put(Month.JULY, 		3);
			put(Month.AUGUST, 		3);
			put(Month.SEPTEMBER,	3);
			put(Month.OCTOBER, 		4);
			put(Month.NOVEMBER, 	4);
			put(Month.DECEMBER, 	4);
		}
	};
	
	static final Map<Integer, String> quarterNameMap = new HashMap<Integer, String>() {
		private static final long serialVersionUID = 956494200361042014L;
		{
			put(1, "Q1");
			put(2, "Q2");
			put(3, "Q3");
			put(4, "Q4");
		}
	};
	
	static final Map<Integer, String> biannualNameMap = new HashMap<Integer, String>() {
		private static final long serialVersionUID = 956494200361042014L;
		{
			put(1, "H1");
			put(2, "H2");
		}
	};
	
	static final Map<Integer, String> monthNameMap = new HashMap<Integer, String>() {
		private static final long serialVersionUID = 956494200361042014L;
		{
			put(1, String.valueOf(Month.JANUARY));
			put(2, String.valueOf(Month.FEBRUARY));
			put(3, String.valueOf(Month.MARCH));
			put(4, String.valueOf(Month.APRIL));
			put(5, String.valueOf(Month.MAY));
			put(6, String.valueOf(Month.JUNE));
			put(7, String.valueOf(Month.JULY));
			put(8, String.valueOf(Month.AUGUST));
			put(9, String.valueOf(Month.SEPTEMBER));
			put(10, String.valueOf(Month.OCTOBER));
			put(11, String.valueOf(Month.NOVEMBER));
			put(12, String.valueOf(Month.DECEMBER));
		}
	};
	
	static final Map<Month, Integer> monthToBiannualMap = new HashMap<Month, Integer>() {
		private static final long serialVersionUID = 956494200361042014L;
		{
			put(Month.JANUARY, 		1);
			put(Month.FEBRUARY, 	1);
			put(Month.MARCH, 		1);
			put(Month.APRIL, 		1);
			put(Month.MAY, 			1);
			put(Month.JUNE, 		1);
			put(Month.JULY, 		2);
			put(Month.AUGUST, 		2);
			put(Month.SEPTEMBER,	2);
			put(Month.OCTOBER, 		2);
			put(Month.NOVEMBER, 	2);
			put(Month.DECEMBER, 	2);
		}
	};
	
	
	private static DateFormat newDateFormat(String format, Optional<TimeZone> timeZone) {
		final SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		
		if(timeZone.isPresent()) {
			final TimeZone zone = timeZone.get();
			dateFormat.setTimeZone(zone);
		}
		
		return dateFormat;
	}
	public enum Operation {
		PLUS, MINUS
	};
	
	public static int yearValue(Long timestamp) {
		return Dates.toLocalDateTime(timestamp).getYear();
	}
	
	public static Month month(Long timestamp) {
		return Dates.toLocalDateTime(timestamp).getMonth();
	}
	
	public static Integer quarterValue(Long timestamp) {
		return monthToQuarterMap.get(Dates.toLocalDateTime(timestamp).getMonth());
	}
	
	public static Integer biannualValue(Long timestamp) {
		return monthToBiannualMap.get(Dates.toLocalDateTime(timestamp).getMonth());
	}
	
	public static int monthValue(Long timestamp) {
		return Dates.toLocalDateTime(timestamp).getMonthValue();
	}
	
	public static String monthName(int month) {
		if (monthNameMap.containsKey(month)) {
			return monthNameMap.get(month);
		}
		else {
			throw new IllegalArgumentException(String.format("Invalid month number %d", month));
		}
	}
	
	public static String monthName(Long timestamp) {
		final int monthValue = monthValue(timestamp);
		return monthName(monthValue);
	}
	
	public static String monthName(Month month) throws Throwable {
		final int monthValue = month.getValue();
		return monthName(monthValue);
	}
	
	public static String biannualName(int biannual) throws Throwable {
		if (biannualNameMap.containsKey(biannual)) {
			return biannualNameMap.get(biannual);
		}
		else {
			throw new IllegalArgumentException(String.format("Invalid bi-annual number %d", biannual));
		}
	}
	
	public static String biannualName(Long timestamp) throws Throwable {
		final int biannualValue = biannualValue(timestamp);
		return biannualName(biannualValue);
	}
	
	public static String quarterName(int quarter) {
		if (quarterNameMap.containsKey(quarter)) {
			return quarterNameMap.get(quarter);
		}
		else {
			throw new IllegalArgumentException(String.format("Invalid quarter number %d", quarter));
		}
	}
	
	public static String quarterName(Long timestamp) {
		final int quarterValue = quarterValue(timestamp);
		return quarterName(quarterValue);
	}

	public static Long toTime() {
		return System.currentTimeMillis();
	}
	
	public static String toString(long time, String format) {
		return toString(time, format, Optional.empty());
	}
	
	public static String toString(long time, String format, String zone) {
		final TimeZone timeZone = (zone != null) ? TimeZone.getTimeZone(zone) : null;
		return toString(time, format, Optional.ofNullable(timeZone));
	}
	
	public static String toString(long time, String format, Optional<TimeZone> timeZone) {
		final DateFormat dateFormat = newDateFormat(format, timeZone);
		final Date date = new Date(time);
		return dateFormat.format(date);
	}
	
	public static Long toTime(String dateValue, String format) throws Throwable {
		return toTime(dateValue, format, null);
	}
	
	public static Long toTime(Date date) throws ParseException {
		return date.toInstant().toEpochMilli();
	}
	
	public static Long toTime(String dateValue, String format, String zone) throws Throwable {
		final TimeZone timeZone = (zone != null) ? TimeZone.getTimeZone(zone) : null;
		final Date date = toDate(dateValue, format, Optional.ofNullable(timeZone));
		return date.toInstant().toEpochMilli();
	}
	
	public static final Date toDate(String dateValue, String format, String zone) throws Throwable {
		final TimeZone timeZone = (zone != null) ? TimeZone.getTimeZone(zone) : null;
		return toDate(dateValue, format, Optional.ofNullable(timeZone));
	}
	
	public static final Date toDate(String dateValue, String format, Optional<TimeZone> timeZone) throws Throwable {
		final DateFormat dateFormat = newDateFormat(format, timeZone);
		final Date date = dateFormat.parse(dateValue);
		return date;
	}
	
	public static final Date toDate(String dateValue, String format) throws Throwable {
		return toDate(dateValue, format, Optional.empty());
	}
	
	public static final Date toDate(Long timestamp) throws Throwable {
		final Date date = new Date(timestamp);
		return date;
	}
	
	public static LocalDateTime toLocalDateTime(Long timestamp) {
		final LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp), ZoneId.systemDefault());
		return localDateTime;
	}
	
	public static Map<String, Long> timeDifferenceAsChronoUnits(Long recent, Long oldest, String ... unitNames) {
		final List<String> units = unitNames != null ? Collections.newList(unitNames) : Collections.newList();
		
		final long difference = recent - oldest;
		final long days = difference / (1000 * 24 * 60 * 60);
	    
		long reminder = 0;
		if(hasChronoUnit(units, ChronoUnit.DAYS)) {
			reminder = difference - (days * 1000 * 24 * 60 * 60);
		} else {
			reminder = difference;
		}
		
	    final long hours = reminder / (1000 * 60 * 60);
	    if(hasChronoUnit(units, ChronoUnit.HOURS)) {
	    	reminder = reminder - (hours * 1000 * 60 * 60);
	    } else {
	    	reminder = difference;
	    }
	    
	    final long minutes = reminder / (1000 * 60);  

	    final long seconds;
	    if(hasChronoUnit(units, ChronoUnit.MINUTES)) {
	    	seconds = (difference / 1000) % 60;
	    } else {
	    	seconds = (difference / 1000);
	    }
		
	    final Map<String, Long> map = Collections.newHashMap();

	    if(hasChronoUnit(units, ChronoUnit.DAYS)) {
	    	map.put(ChronoUnit.DAYS, days);
	    }

	    if(hasChronoUnit(units, ChronoUnit.HOURS)) {
	    	map.put(ChronoUnit.HOURS, hours);
	    }
	   
	    if(hasChronoUnit(units, ChronoUnit.MINUTES)) {
	    	map.put(ChronoUnit.MINUTES, minutes);
	    }
	   
	    if(hasChronoUnit(units, ChronoUnit.SECONDS)) {
	    	map.put(ChronoUnit.SECONDS, seconds);
	    }

		return map;
	}
	
	static boolean hasChronoUnit(final List<String> units, final String unitName) {
		// Default return true
		if(units == null || units.isEmpty()) return true;
		
		for(String part : units) {
			if(unitName.toLowerCase().startsWith(part.toLowerCase())) return true;
		}
		
		return false;
	}
}
