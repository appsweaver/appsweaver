/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.utilities.command;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * 
 * @author SP
 * 
 */
public class CommandRunner {
	
	public List<String> runCommand(String[] arguments) throws IOException, InterruptedException {
		return runCommand(Optional.empty(), arguments);
	}
	
	public List<String> runCommand(Optional<String> optionalDirectory, String[] arguments) throws IOException, InterruptedException {
		final List<String> lines = new ArrayList<String>();
		
		final ProcessBuilder builder = new ProcessBuilder();
		builder.command(arguments);
		
		if(optionalDirectory.isPresent()) {
			final String directory = optionalDirectory.get();
			builder.directory(new File(directory));
		} else {
			builder.directory(new File("/tmp"));
		}
		

		final Process process = builder.start();
		
		final BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
		String line;
		while ((line = br.readLine()) != null) {
			lines.add(line);
		}

		int exitCode = process.waitFor();
		if (exitCode == 0) {
			process.destroy();
		}

		return lines;
	}
		
}
