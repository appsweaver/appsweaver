/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.utilities;

import java.util.Random;
import java.util.UUID;

/**
 * 
 * @author UD
 * 
 */
public class Randomizer {
	final static char[] CHARACTERS = {
		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
		'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
		'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a',
		'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
		'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
		't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1',
		'2', '3', '4', '5', '6', '7', '8', '9', '!',
		'@', '#', '$', '%', '^', '&', '*', '(', ')',
		'-', '_', '+', '=', '{', '}', '[', ']', ':',
		';', '"', '\'', '?', '/', '>', '<', '.', ',',
		' ', '|', '\\', '~', '`'
	};
	
	private Randomizer() {}
	
	private static Random random = new Random();
	
	public static Boolean nextBoolean() {
		return random.nextBoolean();
	}

	public static int randomBetween(int min, int max) {
		return random.nextInt(max - min + 1) + min;
	}
	
	public static long nextLong() {
		return random.nextLong();
	}
	
	public static String asString() {
		return uuid().toString();
	}
	
	public static UUID uuid() {
		return UUID.randomUUID();
	}
	
	public static UUID uuid(String uuid) {
		return UUID.fromString(uuid);
	}
	
	public static UUID nameUUID(String name) {
		return UUID.nameUUIDFromBytes(name.getBytes());
	}
	
	public static boolean isUUID(String uuid) {
		try {
			UUID.fromString(uuid);
			return true;
		} catch(Throwable t) {
		}
		
		return false;
	}
	
	public static String randomBytes(int length) {
		final StringBuilder stringBuilder = new StringBuilder();

		for(int index = 0; index < length; index++) {
			final int position = randomBetween(0, CHARACTERS.length - 1);
			stringBuilder.append(CHARACTERS[position]);
		}
		
		return stringBuilder.toString();
	}
}
