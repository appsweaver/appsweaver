/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.utilities;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

/**
 * 
 * @author UD
 *
 */
public class Locker {
	private final static LoadingCache<String, Object> cache;
	
	private Locker() {}
	
	static {
		cache = new ExpiringCache(24, TimeUnit.HOURS).getCache();
	}
	
	public static Object lock(String key) throws ExecutionException {
		return cache.get(key);
	}
	
	public static class ExpiringCache {
		final long duration;
		final TimeUnit timeUnit;
		final LoadingCache<String, Object> cache; 

		public ExpiringCache() {
			this(24, TimeUnit.HOURS);
		}
		
		public ExpiringCache(long duration, TimeUnit timeUnit) {
			this.duration = duration;
			this.timeUnit = timeUnit;

			this.cache = CacheBuilder.newBuilder().expireAfterWrite(duration, timeUnit).build(new CacheLoader<String, Object>() {
				@Override
				public Object load(String key) throws Exception {
					return new Object();
				}
			});
		}

		public long getDuration() {
			return duration;
		}

		public TimeUnit getTimeUnit() {
			return timeUnit;
		}

		public LoadingCache<String, Object> getCache() {
			return cache;
		}
	}
}
