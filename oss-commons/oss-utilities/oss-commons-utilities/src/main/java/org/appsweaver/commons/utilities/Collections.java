/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.utilities;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * 
 * @author UD
 *
 */
public class Collections {
	private Collections() {}
	
	public static <T> T[] toArray(Collection<T> collection, Class<T> clazz) {
		if(collection == null || clazz == null) throw new IllegalArgumentException("toArray needs non-null arguments");

		final List<T> filtered = collection.stream().filter(value -> value != null).collect(Collectors.toList());
		
		@SuppressWarnings("unchecked")
		final T[] array = (T[])Array.newInstance(clazz, filtered.size());
		
		final Iterator<T> iterator = filtered.iterator();
		int index = 0;
		while(iterator.hasNext()) {
			array[index++] = iterator.next();
		}
		
		return array;
	}
	
	@SafeVarargs
	public static <T> T[] newArray(T... elements) {
		return elements;
	}
	
	public static <T> List<T> newList() {
		return new ArrayList<T>();
	}
	
	public static <K, V> HashMap<K, V> newHashMap() {
		return new HashMap<K, V>();
	}

	public static <T> HashSet<T> newHashSet() {
		return new HashSet<T>();
	}
	
	public static <T> TreeSet<T> newTreeSet() {
		return new TreeSet<T>();
	}
	
	@SafeVarargs
	public static <T> HashSet<T> newHashSet(T... elements) {
		final HashSet<T> set = newHashSet();
		
		if(elements.length > 0) {
			for(T element : elements) {
				if(element != null) {
					set.add(element);
				}
			}
		}
		
		return set;
	}
	
	@SafeVarargs
	public static <T> TreeSet<T> newSortedSet(T... elements) {
		final TreeSet<T> set = newTreeSet();
		
		if(elements.length > 0) {
			for(T element : elements) {
				if(element != null) {
					set.add(element);
				}
			}
		}
		
		return set;
	}
	
	@SafeVarargs
	public static <T> List<T> newList(T... elements) {
		final List<T> list = newList();
		
		if(elements.length > 0) {
			for(T element : elements) {
				if(element != null) {
					list.add(element);
				}
			}
		}
		
		return list;
	}
	
	
	@SafeVarargs
	public static <T> List<T> newList(Collection<T> collection, T... elements) {
		final List<T> union = newList();
		
		if(collection != null) {
			union.addAll(collection);
		}
		
		if(elements.length > 0) {
			for(T element : elements) {
				if(element != null) {
					union.add(element);
				}
			}
		}
		
		return union;
	}
	
	@SafeVarargs
	public static <T> List<T> newList(Collection<T> first, Collection<T> second, Collection<T>... more) {
		final List<T> union = newList();
		
		if(first != null) {
			union.addAll(first);
		}
		
		if(second != null) {
			union.addAll(second);
		}

		if(more != null) {
			for(Collection<T> c : more) {
				if(c != null && c.size() > 0) {
					final List<T> filtered = c.stream().filter(value -> value != null).collect(Collectors.toList());
					union.addAll(filtered);
				}
			}
		}
		
		return union;
	}
	
	public static void sort(List<String> values) {
		values.sort((v1, v2) -> v1.compareTo(v2));
	}
}
