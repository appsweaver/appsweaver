
/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */package org.appsweaver.commons.utilities;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.WordUtils;

import com.google.common.base.CaseFormat;

/**
 * 
 * @author UD
 * 
 */
public class Stringizer {
	static final Pattern BASE64_TEXT_PATTERN = Pattern.compile("^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)?$");
	static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile("^[aA-zZ0-9-_\\.\\+]+([aA-zZ0-9-_\\.\\+]+)*@[aA-zZ0-9-_]+(\\.[aA-zZ0-9]+)*(\\.[aA-zZ]{2,})$");

	private Stringizer() {}
	
	/**
	 * File to lines.
	 *
	 * @param filename
	 *            the filename
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	public static List<String> fileToLines(String filename) throws Exception {
		final List<String> lines = new LinkedList<String>();

		try (BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(filename), "UTF-8"))) {
			String line;

			while ((line = in.readLine()) != null) {
				lines.add(line);
			}

			return lines;
		}
	}

	/**
	 * Extract.
	 *
	 * @param text
	 *            the text
	 * @param delimiter
	 *            the regex
	 * @param position
	 *            the position
	 * @return the string
	 */
	public static String extract(final String text, final String delimiter, final int position) {
		if(isEmpty(text)) return null;
		
		final String[] tokens = text.split(delimiter);
		if (position < tokens.length) {
			return tokens[position];
		}

		return null;
	}
	
	/**
	 * To string list.
	 *
	 * @param arrayString
	 *            the list of values text
	 * @param delimiter
	 *            the delimiter
	 * @return the list
	 */
	public static List<String> toList(String arrayString, String delimiter) {
		return toList(arrayString, delimiter, -1);
	}

	/**
	 * To string list.
	 *
	 * @param arrayString
	 *            the text
	 * @param delimiter
	 *            the delimiter
	 * @param limit
	 *            the limit
	 * @return the list
	 */
	public static List<String> toList(String arrayString, String delimiter, int limit) {
		arrayString = arrayString.replaceAll("\\[", "")
			.replaceAll("\\]", "")
			.replaceAll("\\s", " ")
			.replaceAll("\"", "");
		
		final String[] values = arrayString.split(delimiter, limit);
		final List<String> list = Collections.newList();

		for (final String value : values) {
			final String trimmedValue = value.trim();
			
			if(!Stringizer.isEmpty(trimmedValue)) {
				list.add(trimmedValue);
			}
		}

		return list;
	}

	public static List<String> toList(String arrayString) {
		return toList(arrayString, ",");
	}
	
	public static <T> String toCSVString(T[] values) {
		return toCSVString(values, Optional.empty());
	}
	
	public static <T> String toCSVString(T[] values, Optional<Character> quote) {
		return toCSVString(Arrays.asList(values), quote);
	}
	
	public static <T extends Object> String toCSVString(Iterable<T> items) {
		return toCSVString(items, Optional.empty());
	}
	
	public static <T extends Object> String toCSVString(Iterable<T> items, Optional<Character> quote) {
		if(items != null) {
			final List<String> stringValues = Collections.newList();

			items.forEach(item -> {
				final String itemAsString = String.valueOf(item);

				if(quote.isPresent()) {
					final Character quoteCharacter = quote.get();
					final String quoted = String.format("%c%s%c", quoteCharacter, itemAsString, quoteCharacter);
					stringValues.add(quoted);
				} else {
					stringValues.add(String.format("%s", itemAsString));
				}
			});
			
			return String.join(", ", stringValues);
		}

		return String.format("");
	}

	public static String trimAll(String text) {
		return text.replaceAll("\\s+", "");
	}

	public static byte[] toBytes(Object object) throws Throwable {
		try (final ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
			try (ObjectOutput output = new ObjectOutputStream(outputStream)) {
				output.writeObject(object);
				return outputStream.toByteArray();
			}
		}
	}

	public static <K, V> Map<K, V> toMap(byte[] bytes) throws Throwable {
		try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes)) {
			try (ObjectInput in = new ObjectInputStream(bis)) {
				final Object object = in.readObject();
				if (object instanceof Map) {
					@SuppressWarnings("unchecked")
					final Map<K, V> map = (Map<K, V>) object;
					return map;
				}
			}
		}
		
		return null;
	}

	
	private static byte[] digest(String text) throws Throwable {
		final MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
		return messageDigest.digest(text.getBytes());
	}
	
	public static String encodeSHA512(String text) throws Throwable {
		final byte[] digest = digest(text);

		final String encoded = encodeBase64(digest);
		return toHex(encoded);
	}
	
	/**
	 * From hex.
	 *
	 * @param hex
	 *            the hex
	 * @return the string
	 */
	public static String fromHex(String hex) {
		final byte[] binary = new byte[hex.length() / 2];
		for (int i = 0; i < binary.length; i++) {
			binary[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
		}
		return new String(binary);
	}

	/**
	 * To hex.
	 *
	 * @param text
	 *            the text
	 * @return the string
	 */
	public static String toHex(String text) {
		final byte[] array = text.getBytes();
		final BigInteger bi = new BigInteger(1, array);
		final String hex = bi.toString(16);
		final int paddingLength = array.length * 2 - hex.length();
		if (paddingLength > 0) {
			return String.format("%0" + paddingLength + "d", 0) + hex;
		} else {
			return hex;
		}
	}
	
	public static boolean isBase64(String text) {
		final Matcher matcher = BASE64_TEXT_PATTERN.matcher(text);
		return matcher.matches();
	}

	public static String encodeBase64(byte[] text) throws Throwable {
		return Base64.encodeBase64String(text);
	}

	public static String decodeBase64(byte[] text, Charset charset) throws Throwable {
		final byte[] base64Decoded = Base64.decodeBase64(text);
		return new String(base64Decoded, charset);
	}

	public static String encodeBase64(String text) throws Throwable {
		if(!isBase64(text) ) {
			return encodeBase64(text.getBytes());
		} else {
			// Do not double encode
			return text;
		}
	}

	public static String decodeBase64(String text) throws Throwable {
		// Cannot decode if it is not encoded
		if(isBase64(text)) {
			return decodeBase64(text.getBytes(), Charset.forName("UTF-8"));
		} else {
			return text;
		}
	}

	public static String repeat(String text, int count) {
		return StringUtils.repeat(text, count);
	}

	public static String getLastWord(String text) {
		final String[] words = text.trim().split("\\s+");
		final String lastWord = words[words.length - 1];
		return lastWord.trim();
	}

	public static boolean isNumeric(String text) {
		if(isEmpty(text)) return false;
		
		if(text.contains(".")) {
			try {
				Double.parseDouble(text);
				return true;
			} catch(Throwable t) {
				return false;
			}
		}
		
		try {
			Integer.parseInt(text);
			return true;
		} catch(Throwable t) {
			return false;
		}
	}

	public static String capitalize(String text) {
		final String capitalized = WordUtils.capitalizeFully(text);
		return capitalized.trim();
	}

	public static String dehyphenate(String text) {
		final String dehypenated = RegExUtils.replaceAll(text, "[_-]", " ");
		return dehypenated.trim();
	}
	
	public static boolean isEmpty(String text) {
		return isEmpty(text, true);
	}
	
	public static boolean isEmpty(String text, boolean trim) {
		return text == null || (trim ? text.trim().isEmpty() : text.isEmpty());
	}

	public static boolean isValidEmailAddress(String emailAddress) {
		if(isEmpty(emailAddress)) return false;

		final Matcher matcher = EMAIL_ADDRESS_PATTERN.matcher(emailAddress);
		
		return matcher.matches();
	}

	public static String decodeUrl(String text) throws Throwable {
		return decodeUrl(text, StandardCharsets.UTF_8);
	}
	
	public static String decodeUrl(String text, Charset charset) throws Throwable {
		final String decoded = URLDecoder.decode(text, charset.name());
		return decoded;
	}
	
	public static String between(String text, String beginDelimeter, String endDelimiter) {
		final String substringBetween = StringUtils.substringBetween(text, beginDelimeter, endDelimiter);
		return substringBetween.trim();
	}
	
	/**
	 * 
	 * Convert given string list to array
	 * 
	 * @param list list of strings
	 * @return string array
	 */
	public static String[] toArray(List<String> list) {
		final String[] array = list.stream().toArray(String[]::new);
		return array;
	}
	
	/**
	 * Convert Upper Camel Case to Upper Underscored
	 * 
	 * Upper Camel: Java and C++ class naming convention, e.g., "UpperCamel".
	 * Upper Underscore: UPPER_CAMEL
	 * 
	 * @param text to be formatted
	 * @return string formatted text
	 */
	public static String upperCamelToUpperUnderscored(String text) {
		final String transformed = CaseFormat.UPPER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, text);
		return transformed;
	}
	
	/**
	 * Convert Upper Camel Case to Lower Underscored
	 * 
	 * Upper Camel: Java and C++ class naming convention, e.g., "UpperCamel".
	 * Upper Underscore: upper_camel
	 * 
	 * @param text to be formatted
	 * @return string formatted text
	 */
	public static String upperCamelToLowerCaseUnderscored(String text) {
		final String transformed = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, text);
		return transformed;
	}
	
	/**
	 * Convert Lower Camel Case to Upper Underscored
	 * 
	 * Upper Camel: Java and C++ class naming convention, e.g., "UpperCamel".
	 * Upper Underscore: upper_camel
	 * 
	 * @param text to be formatted
	 * @return string formatted text
	 */
	public static String lowerCamelToUpperCaseUnderscored(String text) {
		final String transformed = CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, text);
		return transformed;
	}
}
