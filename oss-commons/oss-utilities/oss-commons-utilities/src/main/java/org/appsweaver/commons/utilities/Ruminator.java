/*
 *
 * Copyright (c) 2016 appsweaver.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.appsweaver.commons.utilities;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.beanutils.PropertyUtils;
import org.appsweaver.commons.models.types.ParameterizedEnum;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

/**
 *
 * @author UD
 *
 */
public class Ruminator {
	private Ruminator() {}

	/**
	 * Gets the method name by depth.
	 *
	 * @param depth the depth
	 * @return the method name by depth
	 */
	public static String getMethodNameByDepth(final int depth) {
		final StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();

		if (depth >= stackTraceElements.length) {
			throw new RuntimeException("Depth is beyond the current stacktrace");
		}

		return stackTraceElements[depth].getMethodName();
	}

	/**
	 * Gets the current method name.
	 *
	 * @return the current method name
	 */
	public static String getCurrentMethodName() {
		return getMethodNameByDepth(3);
	}

	public static Collection<Field> getAllFields(Class<?> clazz) {
		final Map<String, Field> fields = Collections.newHashMap();
		getAllFields(clazz, fields);
		return fields.values();
	}

	public static Collection<Field> getAllNonStaticFields(Class<?> clazz) {
		final Collection<Field> fields = getAllFields(clazz);
		final Set<Field> filtered = fields.stream().filter(f -> !Ruminator.isStatic(f)).collect(Collectors.toSet());
		return filtered;
	}

	static Map<String, Field> getAllFields(Class<?> clazz, Map<String, Field> fields) {
		final Field[] declaredFields = clazz.getDeclaredFields();
		for(final Field field : declaredFields) {
			fields.put(field.getName(), field);
		}

		if (clazz.getSuperclass() != null) {
			fields = getAllFields(clazz.getSuperclass(), fields);
		}

		return fields;
	}

	public static Field getField(Class<?> clazz, String fieldName) {
		final Map<String, Field> fields = getAllFields(clazz, Collections.newHashMap());
		return fields.get(fieldName);
	}

	public static boolean hasField(Class<?> clazz, String fieldName) {
		final Map<String, Field> fields = getAllFields(clazz, Collections.newHashMap());
		return fields.containsKey(fieldName);
	}

	public static Map<String, Method> getAllMethods(Class<?> clazz, Map<String, Method> methods) {
		final Method[] declaredMethods = clazz.getDeclaredMethods();
		for(final Method method : declaredMethods) {
			methods.put(method.getName(), method);
		}

		if (clazz.getSuperclass() != null) {
			methods = getAllMethods(clazz.getSuperclass(), methods);
		}

		return methods;
	}

	public static boolean isNumericField(Class<?> clazz, String fieldName) {
		final Field field = getField(clazz, fieldName);
		
		if(field != null) {
			final Class<?> type = field.getType();
			return Number.class.isAssignableFrom(type);
		}
		
		return false;
	}
	
	public static boolean hasMethod(Class<?> clazz, String methodName) {
		final Map<String, Method> fields = getAllMethods(clazz, new HashMap<>(0));
		return fields.containsKey(methodName);
	}

	public static void setProperty(Object bean, String name, Object value) throws Throwable {
		final Class<? extends Object> clazz = bean.getClass();
		final Field field = getField(clazz, name);

		PropertyUtils.setProperty(bean, name, TypeConverter.valueOf(value, field.getType()));
	}

	public static Object getProperty(Object bean, String name) {
		try {
			if(!isNestedPropertyName(name)) {
				final Object object = PropertyUtils.getNestedProperty(bean, name);
				return object;
			} else {
				final Class<? extends Object> type = getPropertyType(bean, name, true);

				// Check if this is a collection and should not be a map
				if(Collection.class.isAssignableFrom(type) && !Map.class.isAssignableFrom(type)) {
					final String parentProperty = name.substring(0, name.lastIndexOf("."));
					final Object parentObjects = PropertyUtils.getNestedProperty(bean, parentProperty);

					final String childProperty = name.substring(name.lastIndexOf(".") + 1);
					final Collection<? extends Object> collection = (Collection<?>)parentObjects;

					final String values[] = new String[collection.size()];

					int index = 0;
					for(final Object item : collection) {
						try {
							final Object childPropertyValue = PropertyUtils.getNestedProperty(item, childProperty);
							values[index++] = String.valueOf(childPropertyValue);
						} catch (final Throwable t) {
						}
					}

					// Return value as CSV
					return String.format("[%s]", Stringizer.toCSVString(values));
				}

				// Handle as a simple type
				return PropertyUtils.getNestedProperty(bean, name);
			}
		} catch(final Throwable t) {
			return null;
		}
	}
	
	public static <T> Class<?> getFieldType(Class<T> clazz, String name) {
		if(Ruminator.hasField(clazz, name)) {
			final Field field = getField(clazz, name);
			return field.getType();
		}

		// Default to String
		return String.class;
	}

	static Class<? extends Object> getPropertyType(Object bean, String name) {
		return getPropertyType(bean, name, false);
	}

	static Class<? extends Object> getPropertyType(Object bean, String name, boolean parent) {
		try {
			final Object object;
			if(parent && isNestedPropertyName(name)) {
				final String parentName = name.substring(0, name.lastIndexOf("."));
				object = PropertyUtils.getNestedProperty(bean, parentName);
			} else {
				object = PropertyUtils.getNestedProperty(bean, name);
			}

			final Class<?> type = object.getClass();
			return type;
		} catch(final Throwable t) {
			t.printStackTrace();
			return null;
		}
	}
	
	static boolean isNestedPropertyName(String name) {
		// NO-OP
		if(name == null) return false;

		// Tokens
		final String[] tokens = name.split("\\.");

		// Evaluate
		return tokens != null && tokens.length > 1;
	}

	public static boolean isAnnotatedWith(Class<?> entityClass, Class<? extends Annotation> annotationClass) {
		return entityClass.isAnnotationPresent(annotationClass);
	}

	public static boolean isStatic(Field field) {
		return java.lang.reflect.Modifier.isStatic(field.getModifiers());
	}

	public static void mergeNonNullValues(Object src, Object target, Optional<Set<String>> optionalIgnoreProperties) {
		final Set<String> nullPropertiesSet = getNullPropertyNames(src);
		if (optionalIgnoreProperties.isPresent()) {
			final Set<String> ignoreProperties = optionalIgnoreProperties.get();
			for (String nullSourceProperty : ignoreProperties) {
				nullPropertiesSet.remove(nullSourceProperty);
			}
		}
		final String[] nullPropertiesArray = nullPropertiesSet.stream().toArray(String[]::new);
		BeanUtils.copyProperties(src, target, nullPropertiesArray);
	}

	public static void mergeNonNullValues(Object src, Object target) {
		mergeNonNullValues(src, target, Optional.empty());
	}

	static Set<String> getNullPropertyNames(Object source) {
		final BeanWrapper src = new BeanWrapperImpl(source);
		final java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

		final Set<String> emptyNames = new HashSet<String>();
		for (final java.beans.PropertyDescriptor pd : pds) {
			final Object srcValue = src.getPropertyValue(pd.getName());
			if (srcValue == null)
				emptyNames.add(pd.getName());
		}
		return emptyNames;
	}

	public static <T extends Enum<?>> T toEnum(Class<?> clazz, String value) {
		if(ParameterizedEnum.class.isAssignableFrom(clazz) && clazz.isEnum()) {
			@SuppressWarnings("unchecked")
			final T[] constants = (T[]) clazz.getEnumConstants();

			for(final T constant : constants) {
				final Object object = getProperty(constant, "value");
				final String stringValue = String.valueOf(object);

				if(stringValue.compareTo(value) == 0) {
					return constant;
				}
			}
		}

		return null;
	}

}
