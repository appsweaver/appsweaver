/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.utilities;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;

/**
 * 
 * @author UD
 *
 */
public class FileDiffer {
	public boolean diff(File first, File second) {
		try {
			verifyDirsAreEqual(first.toPath(), second.toPath());
		} catch (Throwable t) {
			return false;
		}
		return true;
	}

	public void verifyDirsAreEqual(Path first, Path second) throws Exception {
		Files.walkFileTree(first, new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				FileVisitResult result = super.visitFile(file, attrs);
				final Path relativize = first.relativize(file);
				final Path fileInOther = second.resolve(relativize);
				final byte[] otherBytes = Files.readAllBytes(fileInOther);
				final byte[] thisBytes = Files.readAllBytes(file);
				if (!Arrays.equals(otherBytes, thisBytes)) {
					result = null;
					throw new RuntimeException(file + " is not equal to " + fileInOther);
				}
				return result;
			}
		});
	}
}
