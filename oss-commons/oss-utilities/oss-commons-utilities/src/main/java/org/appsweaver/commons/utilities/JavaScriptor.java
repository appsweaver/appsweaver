/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.utilities;

import java.io.File;
import java.io.FileReader;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author UD
 *
 */
public class JavaScriptor {
	static final Logger logger = LoggerFactory.getLogger(TypeConverter.class);

	private JavaScriptor() {}
	
	static ScriptEngine getJavaScriptEngine() {
		final ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
		final ScriptEngine scriptEngine = scriptEngineManager.getEngineByName("nashorn");
		
		return scriptEngine;
	}
	
	public static Object execute(String script, String function, Object ... arguments) throws Throwable {
		try {
			final ScriptEngine scriptEngine = getJavaScriptEngine();
			scriptEngine.eval(script);
			
			final Invocable invocable = (Invocable) scriptEngine;
			final Object object = invocable.invokeFunction(function, arguments);
			
			return object;
		} catch(ScriptException e) {
			logger.debug("Error executing function [function={}, arguments={}]", function, arguments, e);
			throw e;
		}
	}
	
	public static Object execute(File script, String function, Object ... arguments) throws Throwable {
		try(FileReader reader = new FileReader(script)) {
			final ScriptEngine scriptEngine = getJavaScriptEngine();
			scriptEngine.eval(reader);
			
			final Invocable invocable = (Invocable) scriptEngine;
			final Object object = invocable.invokeFunction(function, arguments);
			
			return object;
		} catch(ScriptException e) {
			logger.debug("Error executing function [file={}, function={}, arguments={}]", script.getAbsolutePath(), function, arguments, e);
			throw e;
		}
	}
}
