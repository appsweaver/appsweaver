/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.utilities;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.drapostolos.typeparser.TypeParser;

/**
 *
 * @author UD
 *
 * TypeHelper wraps frequently used TypeParser capabilities<br>
 * Refer to: https://github.com/drapostolos/type-parser<br>
 *
 */
public class TypeConverter {
	static final Logger logger = LoggerFactory.getLogger(TypeConverter.class);

	private TypeConverter() {}

	public static <T> T valueOf(Object value, Class<T> clazz) {
		if (UUID.class == clazz) {
			final String stringValue = String.valueOf(value);

			@SuppressWarnings("unchecked")
			final T uuid = (T) UUID.fromString(stringValue);
			return uuid;
		}

		final TypeParser parser = TypeParser.newBuilder().build();
		return parser.parse(String.valueOf(value), clazz);
	}

	public static <T> List<T> toList(String value, Class<T> clazz, boolean sorted) {
		final Collection<T> elements;
		
		if(sorted) {
			elements = toSet(value, clazz);
		} else {
			elements = convert(value, clazz);
		}

		final List<T> list = Collections.newList();
		list.addAll(elements);
		
		return list;
	}
	
	public static <T> List<T> toList(String value, Class<T> clazz) {
		return toList(value, clazz, false);
	}

	public static <T> Set<T> toSet(String value, Class<T> clazz) {
		final Collection<T> elements = convert(value, clazz);

		final Set<T> set = Collections.newSortedSet();
		set.addAll(elements);

		return set;
	}

	static <T> Collection<T> convert(String value, Class<T> clazz) {
		final Collection<T> collection = Collections.newList();

		final String[] tokens = value.split(",");
		for (final String token : tokens) {
			try {
				final T object = valueOf(token.trim(), clazz);
				collection.add(object);
			} catch (final Throwable t) {
				logger.warn("Error parsing collection [value={}; class={}]", token, clazz, t);
			}
		}

		return collection;
	}
}
