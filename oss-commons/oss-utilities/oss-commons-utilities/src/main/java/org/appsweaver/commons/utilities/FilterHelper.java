package org.appsweaver.commons.utilities;
import org.appsweaver.commons.utilities.Stringizer;

/**
 *
 * @author bpolat
 * @since Jul 27, 2020
 *
 */
public class FilterHelper {

	private FilterHelper() {}
	
	public static Object sanitizeFilterValue(Object valueObject) {
		String stringValue = String.valueOf(valueObject);
		try {
			return Stringizer.decodeUrl(stringValue);
		} catch (Throwable e) {
			return stringValue;
		}
	}
}
