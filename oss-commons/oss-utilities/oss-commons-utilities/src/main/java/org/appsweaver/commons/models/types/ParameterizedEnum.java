/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.models.types;

import org.apache.commons.beanutils.PropertyUtils;

/**
 * 
 * @author UD
 * 
 */
public interface ParameterizedEnum {
	String getValue();
	
	default <T extends Enum<?>> T from(String value) throws Throwable {
		if(ParameterizedEnum.class.isAssignableFrom(getClass()) && getClass().isEnum()) {
			@SuppressWarnings("unchecked")
			final T[] constants = (T[]) getClass().getEnumConstants();
			
			for(T constant : constants) {
				final Object object = PropertyUtils.getNestedProperty(constant, "value");
				final String stringValue = String.valueOf(object);
				if(stringValue.compareTo(value) == 0) {
					return constant;
				}
			}
		}
		
		return null;
	}
}
