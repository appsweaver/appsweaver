/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.utilities;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import org.appsweaver.commons.models.types.ParameterizedListType;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.LongSerializationPolicy;
import com.google.gson.reflect.TypeToken;

/**
 * 
 * @author UD
 * 
 */
public class Jsonifier {
	private Jsonifier() {}
	
	/**
	 * json.
	 *
	 * @param <T>
	 *            the generic type
	 * @param object
	 *            the object
	 * @return the string
	 */
	public static <T> String toJson(T object) {
		final Gson gson = newGson();
		return gson.toJson(object);
	}
	

	/**
	 * instance
	 *
	 * @param <T>
	 *            the generic type
	 * @param json
	 *            the json
	 * @param clazz
	 *            the clazz
	 * @return the t
	 */
	public static <T> T toInstance(JsonElement json, Class<T> clazz) {
		final Gson gson = newGson();
		return gson.fromJson(json, clazz);
	}
	
	public static <T> T toInstance(String json, Class<T> clazz, Map<Class<?>, JsonDeserializer<?>> map) {
		final Gson gson = newGson(map);
		return gson.fromJson(json, clazz);
	}
	
	/**
	 * List.
	 *
	 * @param <T> the generic type
	 * @param json the json
	 * @param clazz the clazz
	 * @return the list
	 */
	public static <T> List<T> toList(JsonElement json, Class<T> clazz) {
		final Gson gson = newGson();
		return gson.fromJson(json, new ParameterizedListType<T>(clazz));
	}
	
	/**
	 * List.
	 *
	 * @param <T> the generic type
	 * @param json the json
	 * @param clazz the clazz
	 * @return the list
	 */
	public static <T> List<T> toList(String json, Class<T> clazz) {
		final Gson gson = newGson();
		return gson.fromJson(json, new ParameterizedListType<T>(clazz));
	}

	/**
	 * New gson.
	 *
	 * @return the gson
	 */
	
	private static Gson newGson() {
		return newGson(null);
	}
	
	private static Gson newGson(Map<Class<?>, JsonDeserializer<?>> map) {
		final GsonBuilder gsonBuilder = new GsonBuilder()
			.setPrettyPrinting()
			.disableHtmlEscaping()
			.setLongSerializationPolicy(LongSerializationPolicy.STRING)
			.serializeSpecialFloatingPointValues();
		
		if(map != null) {
			map.forEach((clazz, deserializer) -> {
				gsonBuilder.registerTypeAdapter(clazz, deserializer);
			});
		}
		
		return gsonBuilder.create();
	}

	public static <T> T toInstance(byte[] json, Type type) {
		final Gson gson = newGson();
		return gson.fromJson(new String(json), type);
	}
	
	public static <T> T toInstance(byte[] json, Class<T> clazz) {
		final Gson gson = newGson();
		return gson.fromJson(new String(json), clazz);
	}
	
	public static <T> T toInstance(String json, Class<T> clazz) {
		final Gson gson = newGson();
		return gson.fromJson(json, clazz);
	}
	
	public static <K, V> byte[] toBytes(Map<K, V> map) {
		return (map == null) ? null : toJson(map).getBytes();
	}
	
	public static <K, V> Map<K, V> toMap(byte[] bytes) {
		final Type type = new TypeToken<Map<K, V>>(){}.getType();
		return (bytes == null) ? null : toInstance(bytes, type);
	}
	
	public static <K, V> Map<K, V> toMap(String data) {
		final Type type = new TypeToken<Map<K, V>>(){}.getType();
		return (data == null) ? null : toInstance(data.getBytes(), type);
	}
	
	public static boolean isValid(String json) {
		try {
			final Gson gson = newGson();
			gson.fromJson(json, Object.class);
			return true;
		} catch (Throwable t) {
		}
		return false;
	}
}
