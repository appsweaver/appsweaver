/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TreeMap;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.appsweaver.commons.utilities.command.CommandRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;

import com.google.common.io.LineProcessor;

/**
 * 
 * @author UD
 * 
 */
public class Fileinator {

	final static Logger logger = LoggerFactory.getLogger(Fileinator.class);
	
	final static String BACKUP_FILE_SUFFIX_PART = ".bak";
	
	private Fileinator() {}

	/**
	 * Find pattern in a file and return matching lines as a stream
	 * 
	 * @param pattern the pattern to look for in the file
	 * @param file the file handle on which pattern will be searched
	 * @return the stream of strings
	 * @throws Throwable on any errors
	 */
	public static Stream<String> grep(String pattern, File file) throws Throwable {
		return Files.lines(file.toPath()).filter(line -> line.matches(pattern));
	}

	/**
	 * Tests whether the file denoted by this abstract pathname is a directory
	 * 
	 * @param path the path to the file to test
	 * @return true if and only if the file denoted by this abstract pathname exists and is a directory; false otherwise
	 */
	public static boolean exists(String path) {
		final File file = new File(path);
		return file.exists();
	}

	/**
	 * Creates the directory named by this abstract pathname, including any necessary but nonexistent 
	 * parent directories. Note that if this operation fails it may have succeeded in creating some 
	 * of the necessary parent directories.
	 * 
	 * @param path the path to directories that will be created
	 * @return true if and only if the directory was created, along with all necessary parent directories; false otherwise
	 */
	public static boolean mkdirs(String path) {
		final File file = new File(path);
		return file.mkdirs();
	}

	/**
	 * Save content to file
	 * 
	 * @param file handle to file where content will be written
	 * @param content as a string
	 * @param append true if content must be appended, false otherwise
	 * @throws Throwable on any errors
	 */
	public static void saveToFile(File file, String content, boolean append) throws Throwable {
		saveToFile(file, content.getBytes(), append);
	}

	/**
	 * Save content to file
	 * 
	 * @param file handle to file where content will be written
	 * @param content as byte array
	 * @param append true if content must be appended, false otherwise
	 * @throws Throwable on any errors 
	 */
	public static void saveToFile(File file, byte[] content, boolean append) throws Throwable {
		FileUtils.writeByteArrayToFile(file, content, append);
	}

	/**
	 * Save lines to file
	 * 
	 * @param file handle to file where content will be written
	 * @param content as list of strings
	 * @param append true if content must be appended, false otherwise
	 * @throws Throwable on any errors
	 */
	public static void saveToFile(File file, List<String> content, boolean append) throws Throwable {
		FileUtils.writeLines(file, Charset.defaultCharset().name(), content, append);
	}

	/**
	 * Save content to file
	 * 
	 * @param file handle to file where content will be written
	 * @param content as string
	 * @param append true if content must be appended to existing file
	 * @param archive true if existing file must be backed up
	 * @throws Throwable on any errors
	 */
	public static void saveToFile(File file, String content, boolean append, boolean archive) throws Throwable {
		saveToFile(file, content.getBytes(), append, archive);
	}

	/**
	 * Save content to file
	 * 
	 * @param file handle to file where content will be written
	 * @param content as byte[]
	 * @param append true if content must be appended to existing file
	 * @param archive true if existing file must be backed up
	 * @throws Throwable on any errors
	 */
	public static void saveToFile(File file, byte[] content, boolean append, boolean archive) throws Throwable {
		if (file.exists() && file.isFile() && file.length()>0 && archive) {
			logger.debug("To backup file {} with size {} ",file.getPath(), file.length());
			backupFile(file, BACKUP_FILE_SUFFIX_PART);
		}
		
		FileUtils.writeByteArrayToFile(file, content, append);
	}

	/**
	 * Convert BufferedReader to string
	 * 
	 * @param bufferedReader Converts BufferedReader to String
	 * @return string
	 * @throws Throwable on any errors
	 */
	public static String toString(BufferedReader bufferedReader) throws Throwable {
		return IOUtils.toString(bufferedReader);
	}

	/**
	 * Convert stream to string
	 * 
	 * @param inputStream Converts InputStream to String
	 * @return string
	 * @throws Throwable on any errors
	 */
	public static String toString(InputStream inputStream) throws Throwable {
		return IOUtils.toString(inputStream, Charset.forName("UTF-8"));
	}

	/**
	 * Convert file to string
	 * 
	 * @param file Converts File to String
	 * @return string
	 * @throws Throwable on any errors
	 */
	public static String toString(File file) throws Throwable {
		try (InputStream stream = new FileInputStream(file)) {
			return toString(stream);
		}
	}

	/**
	 * Delete given file/directory. Directories will be deleted recursively
	 * 
	 * @param file handle to be deleted
	 * @throws Throwable on any errors
	 */
	public static void delete(File file) throws Throwable {
		if (file.isFile()) {
			FileUtils.forceDelete(file);
		} else if (file.isDirectory()) {
			FileUtils.deleteDirectory(file);
		}
	}

	/**
	 * Backup files simulate deletion
	 * 
	 * @param filePath path to file/directory that must be backed up
	 * @param archive enable backup if true
	 */
	public static void delete(String filePath, boolean archive) {
		final File file = new File(filePath);
		if (file.exists() && file.isFile()) {
			if (archive && file.length()>0) {
				logger.debug("Before delete : To backup file {} with size {} ",file.getPath(), file.length());
				backupFile(file, BACKUP_FILE_SUFFIX_PART);
			}
			
			file.delete();
		}
	}

	private static void backupFile(File file, String suffix) {
		final StringBuilder newFileName = new StringBuilder();
		final String existingFilePath = file.getAbsolutePath();

		newFileName.append(existingFilePath).append(FilenameUtils.EXTENSION_SEPARATOR_STR).append(suffix)
				.append(Calendar.getInstance().getTimeInMillis());
		
		final String newFilePath = newFileName.toString();

		final File backUpFile = new File(newFilePath);
		try {
			Files.copy(file.toPath(), backUpFile.toPath());
			logger.debug("Copied file {} to file = {} with size = {}", existingFilePath, newFilePath,backUpFile.length());
		} catch (IOException e) {
			logger.warn("Error copying file [source={}, target={}]", existingFilePath, newFilePath, e);
		}
	}

	/**
	 * Stream content of file
	 * 
	 * @param file handle to file that will be streamed
	 * @return stream of strings
	 * @throws Throwable on any errors
	 */
	public static Stream<String> stream(File file) throws Throwable {
		return Files.lines(file.toPath());
	}

	/**
	 * Check if a given pattern occurs in a file
	 * 
	 * @param file handle to a file that must be searched
	 * @param pattern - to find in file
	 * @return true if pattern is found, otherwise false
	 */
	public static boolean contains(File file, String pattern) {
		if (file.exists()) {
			try (final Stream<String> matchingLines = grep(pattern, file)) {
				return (matchingLines.count() > 0);
			} catch (Throwable t) {
			}
		}

		return false;
	}

	/**
	 * Check if a given pattern occurs in a file recursively
	 * 
	 * @param directory path to directory in files will be searched for pattern
	 * @param pattern to find in file
	 * @return list of files where pattern occurs
	 */
	public static File[] find(String directory, String pattern) {
		final File dir = new File(directory);
		final FileFilter fileFilter = new WildcardFileFilter(pattern);
		final File[] files = dir.listFiles(fileFilter);

		return files;
	}

	/**
	 * Directory size in bytes
	 * 
	 * @param path path to directory
	 * @param followLinks follow symbolic links if true, not otherwise
	 * @return directory/file size in bytes
	 * @throws Exception on any errors
	 */
	public static Long getDirectorySizeInBytes(String path, boolean followLinks) throws Exception {
		final List<String> command = new ArrayList<>();
		command.add("du");
		if (followLinks) {
			command.add("-skL");
		} else {
			command.add("-sk");
		}
		command.add(path);
		logger.info("Executing command: {}", command.toString());

		final CommandRunner commandRunner = new CommandRunner();
		final List<String> output = commandRunner.runCommand(command.stream().toArray(String[]::new));
		
		logger.info("Result command: {} {}", command.toString(), output);
		if (output.size() > 0) {
			return Long.parseLong(output.get(0).replaceAll("^\\D*(\\d+).*", "$1")) * 1024;
		}
		
		return 0L;
	}

	/**
	 * Count lines in a file
	 * 
	 * @param file handle to file to count lines
	 * @return count of lines in file
	 * @throws Exception on any errors
	 */
	public static long countLines(File file) throws Exception {
		try {
			final long count = com.google.common.io.Files.asCharSource(file, Charset.defaultCharset())
					.readLines(new LineProcessor<Long>() {
						long count = 0;

						public Long getResult() {
							return count;
						}

						public boolean processLine(String line) {
							count++;
							return true;
						}
					});

			return count;
		} catch (Exception e) {
			logger.warn("Error counting lines [file={}]", file.getAbsolutePath(), e);
			throw e;
		}
	}

	/**
	 * Simulate pageable file
	 * 
	 * @param file handle to file
	 * @param start offset line number
	 * @param count number of lines to fetch
	 * @return map of line numbers and lines
	 * @throws Exception on any errors
	 */
	public static TreeMap<Long, String> read(File file, int start, int count) throws Exception {
		final long startValue;
		if (start < 0) {
			startValue = 0;
		} else {
			startValue = start;
		}

		final long countValue;
		if (count < -1 || count > Long.MAX_VALUE) {
			countValue = 25;
		} else {
			countValue = count;
		}

		final TreeMap<Long, String> lines = new TreeMap<>();
		try (SeekableFileReader seekableFileReader = new SeekableFileReader(file)) {
			seekableFileReader.seek(startValue);

			long lineCount = 0;
			String line = null;
			while (((line = seekableFileReader.readLine()) != null) && (lineCount < countValue)) {
				lines.put(startValue + lineCount, line);
				lineCount++;
			}

			return lines;
		} catch (Exception e) {
			logger.warn("Error reading file [file={}]", file.getAbsolutePath(), e);
			throw e;
		}
	}
	
	/**
	 * Append content to file
	 * 
	 * @param path path to file
	 * @param content content of file
	 * @throws Throwable on any errors 
	 */
	public static void append(String path, String content) throws Throwable {
		final Path pathToFile = Paths.get(path);
		Files.createDirectories(pathToFile.getParent());
		Files.createFile(pathToFile);
		
		final File file = new File(path);
		
		try(final FileWriter writer = new FileWriter(file, true)) {
			writer.write(content);
		}
	}
	
	/**
	 * Obtain handle to file as a resource
	 * 
	 * @param file path to file
	 * @return handle to resource
	 * @throws Throwable on any errors
	 */
	public static Resource toResource(String file) throws Throwable {
		final File fileInstance = new File(file);
		final Resource resource = new UrlResource(fileInstance.toURI());
		if (resource.exists()) {
			return resource;
		} else {
			throw new FileNotFoundException("File not found " + file);
		}
	}
}
