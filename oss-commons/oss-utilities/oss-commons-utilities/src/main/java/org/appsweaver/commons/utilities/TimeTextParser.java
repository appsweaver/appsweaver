/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.utilities;

import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author UD
 * @since 2019-11-05
 *
 */
public class TimeTextParser {
	static final Logger logger = LoggerFactory.getLogger(TypeConverter.class);

	static final Pattern pattern = Pattern.compile("^([0-9]+)([aA-zZ].*)");

	TimeUnit unit;
	Integer value;

	public TimeUnit getUnit() {
		return unit;
	}

	public void setUnit(TimeUnit unit) {
		this.unit = unit;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public TimeTextParser(String value) {
		if(Stringizer.isEmpty(value)) {
			throw new IllegalArgumentException("Time text parser needs a non-emtpy value");
		}
		
		parse(Stringizer.trimAll(value));
	}

	public long toMilliseconds() {
		final long milliseconds = getUnit().toMillis(value);
		return milliseconds;
	}
	
	private void parse(String value) {
		final Matcher matcher = pattern.matcher(value);
		if(matcher.find()) {
			final String intGroup = matcher.group(1);
			setValue(Integer.valueOf(intGroup));
			
			final String text = matcher.group(2);
			setUnit(toTimeUnit(text));
		} else {
			setValue(Integer.valueOf(value));
			setUnit(TimeUnit.MILLISECONDS);
		}
	}
	
	private TimeUnit toTimeUnit(String text) {
		final String unitText = text.toUpperCase();
		
		/*
		 * Text is upper cased 
		 */
		if (unitText.startsWith("MS") || TimeUnit.MILLISECONDS.name().startsWith(unitText)) {
			return TimeUnit.MILLISECONDS;
		} else if (unitText.startsWith("U") || TimeUnit.MICROSECONDS.name().startsWith(unitText)) {
			return TimeUnit.MICROSECONDS;
		} else if (TimeUnit.NANOSECONDS.name().startsWith(unitText)) {
			return TimeUnit.NANOSECONDS;
		} else if (TimeUnit.SECONDS.name().startsWith(unitText)) {
			return TimeUnit.SECONDS;
		} else if (TimeUnit.MINUTES.name().startsWith(unitText)) {
			return TimeUnit.MINUTES;
		} else if (TimeUnit.HOURS.name().startsWith(unitText)) {
			return TimeUnit.HOURS;
		} else if (TimeUnit.DAYS.name().startsWith(unitText)) {
			return TimeUnit.DAYS;
		} else {
			throw new IllegalArgumentException(
				String.format("Unsupported time unit text '%s'", text)
			);
		}
	}
}