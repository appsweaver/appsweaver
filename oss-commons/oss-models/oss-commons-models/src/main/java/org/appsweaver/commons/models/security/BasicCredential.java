/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.models.security;

import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author UD
 * 
 */
public class BasicCredential {
	static final Pattern BASE64_TEXT_PATTERN = Pattern.compile("^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)?$");
	
	private boolean isBase64(String text) {
		final Matcher matcher = BASE64_TEXT_PATTERN.matcher(text);
		return matcher.matches();
	}
	
	private String username;
	private String password;

	public BasicCredential() {
	}
	
	public BasicCredential(String username, String password) {
		setUsername(username);
		setPassword(password);
	}
	
	public String getUsername() {
		if(username != null && username.length() > 0) {
			return username.toLowerCase();
		} else {
			return username;
		}
	}

	public String getPassword() {
		/*
		 * Assume that password is encoded
		 */
		if(password != null && password.length() > 0) {
			if (isBase64(password)) {
				return new String(Base64.getDecoder().decode(password));
			}
		}
		
		return password;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isGood() {
		return (
			(getUsername() != null && !getUsername().isEmpty()) 
			&& 	
			(getPassword() != null && !getPassword().isEmpty())
		);
	}
}
