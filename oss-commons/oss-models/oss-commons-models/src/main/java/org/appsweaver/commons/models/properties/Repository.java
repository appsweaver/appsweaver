/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.models.properties;

/**
 * 
 * @author UD
 *
 */
public class Repository {
	private boolean enabled;
	private boolean cleanOnStart;
	private int port;
	private String hostname;
	private String username;
	private String password;
	private String schema;
	private boolean migrationEnabled;
	
	private Auditing auditing;

	public boolean isCleanOnStart() {
		return cleanOnStart;
	}

	public void setCleanOnStart(boolean cleanOnStart) {
		this.cleanOnStart = cleanOnStart;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public boolean isMigrationEnabled() {
		return migrationEnabled;
	}

	public void setMigrationEnabled(boolean migrationEnabled) {
		this.migrationEnabled = migrationEnabled;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	public Auditing getAuditing() {
		return auditing;
	}

	public void setAuditing(Auditing auditing) {
		this.auditing = auditing;
	}

	public class Auditing {
		boolean enabled = true;

		public boolean isEnabled() {
			return enabled;
		}

		public void setEnabled(boolean enabled) {
			this.enabled = enabled;
		}
	}
}
