/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.models.criteria;

/**
 * 
 * @author UD
 *
 */
public class FilterableField {
	private final String name;
	private final String path;
	private final Class<?> type;
	
	/**
	 * Attribute must be resolvable path to actual object
	 * 
	 * @param name - attribute name
	 * @param path - canonical path
	 * @param type - type
	 * 
	 */
	public FilterableField(String name, String path, Class<?> type) {
		this.name = name;
		this.path = path;
		this.type = type;
	}

	/**
	 * Get path
	 * 
	 * @return path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * Get attribute class type
	 * 
	 * @return type
	 */
	public Class<?> getType() {
		return type;
	}

	/**
	 * 
	 * Get name
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}
}
