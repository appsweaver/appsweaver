/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.models.properties;

import java.util.ArrayList;
import java.util.List;

import org.appsweaver.commons.models.properties.attributes.AuthenticatonProviderDetails;
import org.appsweaver.commons.models.security.AuthenticationProviderType;

/**
 *
 * @author UD
 * 
 */
public class Security {
	private AuthenticationProviderType authenticationProvider;

	private String defaultRole;

	// NOTE: Password was generated with Cryptographer.encrypt("PASSWORD");
	private String defaultUserPassword;

	private String customUsersFile;

	private String rolesAndPermissionsFile;

	private ActiveDirectory activeDirectory;

	private String domain;

	private List<SecuredResource> securedResources = new ArrayList<>();

	private boolean enableCommonSecurity;

	private boolean enableCommonTokenAuthorization;
	
	private List<AuthenticatonProviderDetails> authenticationProviders = new ArrayList<>();

	/**
	 * Gets active directory.
	 *
	 * @return the active directory
	 */
	public ActiveDirectory getActiveDirectory() {
		return activeDirectory;
	}

	/**
	 * Sets active directory.
	 *
	 * @param activeDirectory
	 *            the active directory
	 */
	public void setActiveDirectory(ActiveDirectory activeDirectory) {
		this.activeDirectory = activeDirectory;
	}

	/**
	 * Gets default user password.
	 *
	 * @return the default user password
	 */
	public String getDefaultUserPassword() {
		return defaultUserPassword;
	}

	/**
	 * Sets default user password.
	 *
	 * @param defaultUserPassword
	 *            the default user password
	 */
	public void setDefaultUserPassword(String defaultUserPassword) {
		this.defaultUserPassword = defaultUserPassword;
	}

	/**
	 * Gets authentication type.
	 *
	 * @return the authentication type
	 */
	public AuthenticationProviderType getAuthenticationProvider() {
		return authenticationProvider;
	}

	/**
	 * Sets authentication type.
	 *
	 * @param authenticationProvider
	 *            the authentication type
	 */
	public void setAuthenticationProvider(AuthenticationProviderType authenticationProvider) {
		this.authenticationProvider = authenticationProvider;
	}

	/**
	 * Gets default role.
	 *
	 * @return the default role
	 */
	public String getDefaultRole() {
		return defaultRole;
	}

	/**
	 * Sets default role.
	 *
	 * @param defaultRole
	 *            the default role
	 */
	public void setDefaultRole(String defaultRole) {
		this.defaultRole = defaultRole;
	}

	/**
	 * Gets custom users file.
	 *
	 * @return the custom users file
	 */
	public String getCustomUsersFile() {
		return customUsersFile;
	}

	/**
	 * Sets custom users file.
	 *
	 * @param customUsersFile
	 *            the custom users file
	 */
	public void setCustomUsersFile(String customUsersFile) {
		this.customUsersFile = customUsersFile;
	}

	/**
	 * Gets domain.
	 *
	 * @return the domain
	 */
	public String getDomain() {
		return domain;
	}

	/**
	 * Sets domain.
	 *
	 * @param domain
	 *            the domain
	 */
	public void setDomain(String domain) {
		this.domain = domain;
	}

	/**
	 * Gets roles file.
	 *
	 * @return the roles file
	 */
	public String getRolesAndPermissionsFile() {
		return rolesAndPermissionsFile;
	}

	/**
	 * Sets roles file.
	 *
	 * @param rolesAndPermissionsFile
	 *            the roles file
	 */
	public void setRolesAndPermissionsFile(String rolesAndPermissionsFile) {
		this.rolesAndPermissionsFile = rolesAndPermissionsFile;
	}

	/**
	 * Gets secured resources.
	 *
	 * @return the secured resources
	 */
	public List<SecuredResource> getSecuredResources() {
		return securedResources;
	}

	/**
	 * Sets secured resources.
	 *
	 * @param securedResources
	 *            the secured resources
	 */
	public void setSecuredResources(List<SecuredResource> securedResources) {
		this.securedResources = securedResources;
	}

	/**
	 * Is enable common security boolean.
	 *
	 * @return the boolean
	 */
	public boolean isEnableCommonSecurity() {
		return enableCommonSecurity;
	}

	/**
	 * Sets enable common security.
	 *
	 * @param enableCommonSecurity
	 *            the enable common security
	 */
	public void setEnableCommonSecurity(boolean enableCommonSecurity) {
		this.enableCommonSecurity = enableCommonSecurity;
	}

	/**
	 * Is enable common token authorization boolean.
	 *
	 * @return the boolean
	 */
	public boolean isEnableCommonTokenAuthorization() {
		return enableCommonTokenAuthorization;
	}

	/**
	 * Sets enable common token authorization.
	 *
	 * @param enableCommonTokenAuthorization
	 *            the enable common token authorization
	 */
	public void setEnableCommonTokenAuthorization(boolean enableCommonTokenAuthorization) {
		this.enableCommonTokenAuthorization = enableCommonTokenAuthorization;
	}

	public List<AuthenticatonProviderDetails> getAuthenticationProviders() {
		return authenticationProviders;
	}

	public void setAuthenticationProviders(List<AuthenticatonProviderDetails> authenticationProviders) {
		this.authenticationProviders = authenticationProviders;
	}
}
