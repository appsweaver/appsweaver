/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.models.criteria;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 
 * @author UD
 *
 */
public class SearchableField {
	private final String name;
	
	private final Float boost;
	
	@JsonIgnore
	private final String[] facets;
	
	@JsonIgnore
	private final boolean enabled;
	
	@JsonIgnore
	private final boolean lookup;
	
	@JsonIgnore
	private final String range;
	
	/**
	 * Create attribute with name and boost
	 * 
	 * @param name - field name
	 * @param boost - boost factor
	 */
	public SearchableField(boolean enabled, boolean lookup, String name, Float boost, String[] facets, String range) {
		this.enabled = enabled;
		this.lookup = lookup;
		this.name = name;
		this.boost = boost;
		this.facets = facets;
		this.range = range;
	}
	
	/**
	 * field name
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Boost value
	 * 
	 * @return boost
	 */
	public Float getBoost() {
		return boost;
	}

	/**
	 * Facet names
	 * 
	 * @return facet name array
	 */
	public String[] getFacets() {
		return facets;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public boolean isLookup() {
		return lookup;
	}

	public String getRange() {
		return range;
	}
}
