/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.models.properties;

import org.springframework.http.HttpMethod;

/**
 *
 * @author AC
 *
 */
public class SecuredResource {
	private HttpMethod method;
	private String url;
	private AllowedType type = AllowedType.PERMIT;

	/**
	 * The enum Allowed type.
	 */
	public enum AllowedType {
		/**
		 * Permit allowed type.
		 */
		PERMIT,
		/**
		 * Deny allowed type.
		 */
		DENY
	}

	/**
	 * Gets method.
	 *
	 * @return the method
	 */
	public HttpMethod getMethod() {
		return method;
	}

	/**
	 * Sets method.
	 *
	 * @param method the method
	 */
	public void setMethod(HttpMethod method) {
		this.method = method;
	}

	/**
	 * Gets url.
	 *
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Sets url.
	 *
	 * @param url the url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Gets type.
	 *
	 * @return the type
	 */
	public AllowedType getType() {
		return type;
	}

	/**
	 * Sets type.
	 *
	 * @param type the type
	 */
	public void setType(AllowedType type) {
		this.type = type;
	}
}
