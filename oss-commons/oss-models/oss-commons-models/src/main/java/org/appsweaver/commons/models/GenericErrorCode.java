/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.models;

import org.appsweaver.commons.api.ErrorCode;

/**
 * 
 * @author UD
 * 
 */
public enum GenericErrorCode implements ErrorCode {
	EXPECTED_VALID_INPUT, 
	FAILED_TO_CHANGE_PASSWORD, 
	FAILED_TO_CREATE_ITEM, 
	FAILED_TO_CREATE_MANY_ITEMS, 
	FAILED_TO_DELETE_ITEM, 
	FAILED_TO_DELETE_MANY_ITEMS, 
	FAILED_TO_FETCH_METRICS, 
	FAILED_TO_GET_BUILD_INFORMATION, 
	FAILED_TO_GET_ENTITY_ATTRIBUTES, 
	FAILED_TO_GET_ITEM_BY_ID, 
	FAILED_TO_GET_ITEM_BY_UUID, 
	FAILED_TO_GET_MANY_ITEMS,
	FAILED_TO_GET_RELEASE_NOTES, 
	FAILED_TO_GET_SEARCH_RESULTS,
	FAILED_TO_RANK_ITEM,
	FAILED_TO_UPDATE_ITEM,
	INSUFFICIENT_DATA, 
	INVALID_CREDENTIALS, 
	NO_OPERATION, 
	PASSWORDS_DO_NOT_MATCH,
	REQUESTED_OPERATION_IS_FORBIDDEN, 
	REQUESTED_RESOURCE_NOT_FOUND, 
	UNKNOWN_OR_INVALID_USER,
	FAILED_TO_READ_LINES_FROM_FILE, 
	FAILED_TO_GET_FACETS,
	FAILED_TO_EXPORT_DATA,
}
