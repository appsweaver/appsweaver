/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.builders;

import java.util.HashMap;
import java.util.Map;

import org.appsweaver.commons.models.criteria.FilterableField;

/**
 * 
 * @author UD
 * 
 */
public class FilterableFieldBuilder {
	private Map<String, FilterableField> map;

	
	public Map<String, FilterableField> build() {
		return map;
	}
	
	public FilterableFieldBuilder with() {
		this.map = new HashMap<>();
		
		return this;
	}
	
	public FilterableFieldBuilder with(Map<String, FilterableField> map) {
		this.map = map;
		
		return this;
	}
	
	public FilterableFieldBuilder and(String name, String path, Class<?> type) {
		map.put(name, new FilterableField(name, path, type));
		return this;
	}
}
