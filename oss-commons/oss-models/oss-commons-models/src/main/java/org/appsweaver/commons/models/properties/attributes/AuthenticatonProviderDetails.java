/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.models.properties.attributes;

import org.appsweaver.commons.models.security.AuthenticationProviderType;

/**
 * 
 * @author UD
 *
 */
public class AuthenticatonProviderDetails {
	private AuthenticationProviderType authenticationProvider;
	private boolean enabled;
	private boolean syncData;


	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isSyncData() {
		return syncData;
	}

	public void setSyncData(boolean syncData) {
		this.syncData = syncData;
	}

	public AuthenticationProviderType getAuthenticationProvider() {
		return authenticationProvider;
	}

	public void setAuthenticationProvider(AuthenticationProviderType authenticationProvider) {
		this.authenticationProvider = authenticationProvider;
	}
}
