/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons;

import org.apache.commons.lang3.ArrayUtils;

/**
 * 
 * @author UD
 * 
 */
public interface SystemConstants {
	public static final int HASH_ID_LENGTH = 8;
	public static final String HASH_ID_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	public static final String JASYPT_ALGORITHM = "PBEWithMD5AndDES";

	public static final String ANONYMOUS_USER = "anonymous";
	
	public static final String DATE_FORMAT_MM_YYYY = "MM/yyyy";
	public static final String DATE_FORMAT_MM_DD_YYYY = "MM/dd/yyyy";
	public static final String DATE_FORMAT_YYYY_MM_DD = "yyyy/MM/dd";

	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String HEADER = "Header";
	public static final String AUTHORIZATION = "Authorization";

	public static final String API_KEY_NAME = "accessToken";
	public static final String API_SCOPE = "server";

	/*
	 * Access token valid for 4 hours if not overridden in the environment
	 * Applications can override this with APPLICATION_ACCESS_TOKEN_VALIDITY_DURATION
	 */
	public static final String DEFAULT_APPLICATION_ACCESS_TOKEN_VALIDITY_DURATION = "2 hours";

	public static final String AUTHENTICATION_TYPE = "authenticationType";

	public static final String NEWLINE = "\n";
	public static final String SPACE_SLASH_NEWLINE = " \\\n";

	public static final String STOMP_APPLICATION_DESTINATION_PREFIX = "/app";
	public static final String STOMP_MESSAGE_BROKER_BASE_PATH = "/secured";
	public static final String STOMP_MESSAGE_BROKER_MESSAGES_ENDPOINT = STOMP_MESSAGE_BROKER_BASE_PATH + "/messages";
	public static final String STOMP_WEBSOCKET_ENDPOINT = "/websocket";
	
	public static final String STOMP_WEBSOCKET_ENDPOINTS[] = ArrayUtils.toArray(
		"/websocket",
		"/websocket/**"
	);
	
	public static final String STOMP_WHITELISTED_ENDPOINTS[] = ArrayUtils.toArray(
		STOMP_WEBSOCKET_ENDPOINT,
		STOMP_MESSAGE_BROKER_BASE_PATH + "/**",
		STOMP_MESSAGE_BROKER_BASE_PATH + "/socket",
		STOMP_MESSAGE_BROKER_BASE_PATH + "/success"
	);
	
	public static final String AUTHENTICATION_ENDPOINT = "/api/v1/authenticate";
	public static final String API_VERSIONED_PATH_WILD = "/api/v1/**";

	public static final String OBFUSCATED_ID = "oid";
	public static final String ENTITY_ID = "id";
}
