/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.messaging.impl.components.rmq;

import java.io.IOException;

import org.appsweaver.commons.messaging.MessageQueueConsumer;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownSignalException;

/**
 * 
 * @author AN
 *
 */
public class RMQConsumer implements Consumer {

	private final MessageQueueConsumer mqConsumer;
	
	public RMQConsumer(MessageQueueConsumer mqConsumer) {
		this.mqConsumer = mqConsumer;
	}
	
	@Override
	public void handleCancel(String arg0) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleCancelOk(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleConsumeOk(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleDelivery(String consumerTag, Envelope env,
			BasicProperties props, byte[] body) throws IOException {
		String message = new String(body, "UTF-8");
        long deliveryTag = env.getDeliveryTag();
        mqConsumer.handleDelivery(Long.toString(deliveryTag), message);
	}

	@Override
	public void handleRecoverOk(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleShutdownSignal(String arg0, ShutdownSignalException arg1) {
		// TODO Auto-generated method stub
		
	}

}


