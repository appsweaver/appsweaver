/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.messaging.impl.components.rmq;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.appsweaver.commons.messaging.MessageQueueConsumer;
import org.appsweaver.commons.messaging.MessageQueueConsumerContext;
import org.appsweaver.commons.messaging.MessagingException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * 
 * @author AN
 *
 */
public class RMQService {
	
	protected final Logger logger = LogManager.getLogger(getClass().getName());

	final ConnectionFactory connectionFactory;
	
	public RMQService(Object connectionFactory) {
		this.connectionFactory = (ConnectionFactory) connectionFactory;
	}
	
	public void sendMessage(String queueName, String message) throws MessagingException {
		Connection connection = null;
		Channel channel = null;
		try {
			connection = connectionFactory.newConnection();
	        channel = connection.createChannel();
	        channel.queueDeclare(queueName, true, false, false, null);
	        channel.basicPublish("", queueName, null, message.getBytes());
		} catch (IOException | TimeoutException e) {
			logger.error("Error sending message to queue [queueName={}, message={}]", queueName, message);
			throw new MessagingException(e);
		} finally {
	         try {
				channel.close();
		        connection.close();
			} catch (IOException | TimeoutException e) {
				throw new MessagingException(e);
			}
		}
	}

	public MessageQueueConsumerContext createMessageConsumer(String queueName, MessageQueueConsumer mqConsumer) throws MessagingException {
		Connection connection = null;
		final Channel channel;
		try {
			connection = connectionFactory.newConnection();
			channel = connection.createChannel();
	        channel.queueDeclare(queueName, true, false, false, null);
	        RMQConsumer consumer = new RMQConsumer(mqConsumer);
	        MessageQueueConsumerContext messageConsumerContext = new RMQConsumerContext(queueName, channel, consumer);
	        	return messageConsumerContext;
		} catch (IOException | TimeoutException e) {
			logger.error("Error creating mesage consumer for queue [queueName={}]", queueName, e);
			try {
		        connection.close();
			} catch (Exception e1) {
				logger.error("Error closing connection", e1);
			}
			throw new MessagingException(e);
		}
	}
	
	public MessageQueueConsumerContext createMessageConsumer(String queueName, MessageQueueConsumer mqConsumer, int qos) throws MessagingException {
		Connection connection = null;
		final Channel channel;
		try {
			connection = connectionFactory.newConnection();
			channel = connection.createChannel();
	        channel.queueDeclare(queueName, true, false, false, null);
	        channel.basicQos(qos);
	        RMQConsumer consumer = new RMQConsumer(mqConsumer);
	        MessageQueueConsumerContext messageConsumerContext = new RMQConsumerContext(queueName, channel, consumer);
	        	return messageConsumerContext;
		} catch (IOException | TimeoutException e) {
			logger.error("Error creating mesage consumer for queue [queueName={}]", queueName);
			try {
		        connection.close();
			} catch (Exception e1) {
				throw new MessagingException(e1);
			}
			throw new MessagingException(e);
		}
	}


	public void sendAck(String messageId, Channel channel) throws MessagingException {
		Long deliveryTag = Long.parseLong(messageId);
		try {
			channel.basicAck(deliveryTag, false);
		} catch (Throwable t) {
			logger.error("Error sending acknowledgement {}", deliveryTag);
			throw new MessagingException(t);

		}
	}

	public void sendRejectAck(String messageId, Channel channel, boolean requeue) throws MessagingException {
		Long deliveryTag = Long.parseLong(messageId);
		try {
			channel.basicReject(deliveryTag, requeue);
		} catch (Throwable t) {
			logger.error("Error sending acknowledgement {}", deliveryTag);
			throw new MessagingException(t);
		}
	}
		
}


