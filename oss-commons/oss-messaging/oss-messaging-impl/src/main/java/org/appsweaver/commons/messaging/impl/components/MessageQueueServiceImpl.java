/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.messaging.impl.components;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.appsweaver.commons.messaging.MessageQueueConsumer;
import org.appsweaver.commons.messaging.MessageQueueConsumerContext;
import org.appsweaver.commons.messaging.MessageQueueService;
import org.appsweaver.commons.messaging.MessageQueueType;
import org.appsweaver.commons.messaging.MessagingException;
import org.appsweaver.commons.messaging.impl.components.rmq.RMQService;

import com.rabbitmq.client.Channel;

/**
 * 
 * @author AN
 *
 */
@Deprecated
public class MessageQueueServiceImpl implements MessageQueueService {
	
	protected final Logger logger = LogManager.getLogger(getClass().getName());

	final Object connectionFactory;
	final MessageQueueType messageQueueType;
	
	public MessageQueueServiceImpl(MessageQueueType messageQueueType, Map<String, String> config) {
		this.messageQueueType = messageQueueType;
		this.connectionFactory = MessageQueueConnectionFactory.getConnectionFactory(messageQueueType, config);
	}

	@Override
	public void send(String queueName, String message) throws MessagingException {
		switch(messageQueueType) {
		case RABBITMQ:
			RMQService rmqService = new RMQService(connectionFactory);
			rmqService.sendMessage(queueName, message);
			break;
		default:
			throw new MessagingException("Message Queue Type Not supported");
		}

	}

	@Override
	public MessageQueueConsumerContext registerConsumer(String queueName, MessageQueueConsumer mqConsumer) throws MessagingException {
		switch(messageQueueType) {
		case RABBITMQ:
			RMQService rmqService = new RMQService(connectionFactory);
			MessageQueueConsumerContext context = rmqService.createMessageConsumer(queueName, mqConsumer);
			mqConsumer.setMessageConsumerContext(context);
			return context;
		default:
			throw new MessagingException("Message Queue Type Not supported");
		}

	}
	
	@Override
	public MessageQueueConsumerContext registerConsumer(String queueName, MessageQueueConsumer mqConsumer, int qos) throws MessagingException {
		switch(messageQueueType) {
		case RABBITMQ:
			RMQService rmqService = new RMQService(connectionFactory);
			MessageQueueConsumerContext context = rmqService.createMessageConsumer(queueName, mqConsumer, qos);
			mqConsumer.setMessageConsumerContext(context);
			return context;
		default:
			throw new MessagingException("Message Queue Type Not supported");
		}

	}

	@Override
	public void acknowledge(String messageId, MessageQueueConsumerContext mqConsumerContext) throws Exception {
		switch(mqConsumerContext.getMessageQueueType()) {
		case RABBITMQ:
			RMQService rmqService = new RMQService(connectionFactory);
			rmqService.sendAck(messageId, (Channel)mqConsumerContext.getMessage());
			break;
		default:
			throw new MessagingException("Message Queue Type Not supported");
		}

	}

	@Override
	public void reject(String messageId, MessageQueueConsumerContext mqContext, boolean requeue) throws Exception {
		switch(mqContext.getMessageQueueType()) {
		case RABBITMQ:
			RMQService rmqService = new RMQService(connectionFactory);
			rmqService.sendRejectAck(messageId, (Channel)mqContext.getMessage(), requeue);
			break;
		default:
			throw new MessagingException("Message Queue Type Not supported");
		}		
	}
	
}


