/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.messaging.impl.components;

import org.appsweaver.commons.messaging.MessageQueueConsumer;
import org.appsweaver.commons.messaging.MessageQueueConsumerContext;
import org.appsweaver.commons.messaging.MessagingException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import com.rabbitmq.client.Channel;

/**
 *
 * @author AN
 *
 * The Class RMQService.
 *
 * Class to create the RabbitMQ service.
 * This bean is a conditional bean and is created only when the property @linkplain webservice.messaging.properties.mqservice-enabled
 * is present and value is true
 *
 */
@Component
@ConditionalOnProperty(name="webservice.messaging.properties.mqservice-enabled", havingValue = "true")
public class RMQService  {

	/** The rabbit template. */
	private final RabbitTemplate rabbitTemplate;

    /**
     * Instantiates a new RMQ service.
     *
     * @param rabbitTemplate the rabbit template
     */
    public RMQService(RabbitTemplate rabbitTemplate) {
    		this.rabbitTemplate = rabbitTemplate;
    }

	/**
	 * Send message.
	 *
	 * @param queueName the queue name
	 * @param message the message
	 * @throws MessagingException the messaging exception
	 */
	public void sendMessage(String queueName, String message) throws MessagingException {
		rabbitTemplate.convertAndSend(queueName, message.getBytes());
	}

	/**
	 * Send ack.
	 *
	 * @param messageId the message id
	 * @param channel the channel
	 * @throws Exception the exception
	 */
	public void sendAck(String messageId, Channel channel) throws Exception {
		// TODO Auto-generated method stub

	}

	/**
	 * Send reject ack.
	 *
	 * @param messageId the message id
	 * @param channel the channel
	 * @param requeue the requeue
	 * @throws Exception the exception
	 */
	public void sendRejectAck(String messageId, Channel channel, boolean requeue) throws Exception {
		// TODO Auto-generated method stub

	}

	/**
	 * Creates the message consumer.
	 *
	 * @param queueName the queue name
	 * @param genericConsumer the generic consumer
	 * @return the message queue consumer context
	 * @throws MessagingException the messaging exception
	 */
	public MessageQueueConsumerContext createMessageConsumer(String queueName, MessageQueueConsumer genericConsumer) throws MessagingException {
		// TODO Auto-generated method stub
		return null;
	}

	public MessageQueueConsumerContext createMessageConsumer(String queueName, MessageQueueConsumer mqConsumer, int qos) {
		// TODO Auto-generated method stub
		return null;
	}

}


