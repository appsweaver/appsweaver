/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.messaging.impl.components;

import java.util.Map;

import org.appsweaver.commons.messaging.MessageQueueType;
import org.springframework.stereotype.Component;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.jms.admin.RMQConnectionFactory;

/**
 * 
 * @author AN
 *
 */

@Component
public class MessageQueueConnectionFactory {
	
	public static Object getConnectionFactory(MessageQueueType mqType, Map<String, String> config) {
		switch (mqType) {
		case RABBITMQ:
	         ConnectionFactory factory = new ConnectionFactory();
	         //hostname of your rabbitmq server
	         factory.setHost(config.get("host"));
	         factory.setPort(Integer.parseInt(config.get("port")));
	         factory.setUsername(config.get("username"));
	         factory.setPassword(config.get("password"));
	         factory.setChannelRpcTimeout(200000);
			return factory;
		case JMS:
			RMQConnectionFactory rmqConnectionFactory = new RMQConnectionFactory();
			rmqConnectionFactory.setHost(config.get("host"));
			rmqConnectionFactory.setPort(Integer.parseInt(config.get("port")));
			rmqConnectionFactory.setUsername(config.get("username"));
			rmqConnectionFactory.setPassword(config.get("password"));
			rmqConnectionFactory.setRequeueOnMessageListenerException(true);
			// set the message timeout to 10 seconds
			rmqConnectionFactory.setOnMessageTimeoutMs(200000);
			return rmqConnectionFactory;
		default:
			throw new UnsupportedOperationException(String.format("Unsupported provider type: %s", mqType));
		}
	}

}


