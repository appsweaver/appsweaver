/*
 *
 * Copyright (c) 2016 appsweaver.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.appsweaver.commons.api;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.appsweaver.commons.jpa.models.criteria.SortOrder;
import org.appsweaver.commons.jpa.models.search.FacetMenuItem;
import org.appsweaver.commons.jpa.models.search.TextSearchType;
import org.appsweaver.commons.jpa.types.FilterConjunction;
import org.appsweaver.commons.jpa.types.Rank;
import org.appsweaver.commons.json.views.SelectedFacet;
import org.appsweaver.commons.models.persistence.Entity;
import org.appsweaver.commons.spring.utilities.FrameworkHelper;
import org.appsweaver.commons.types.FileType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author UD
 *
 */
public interface DataService<T extends Entity> extends SecurityService {
	/**
	 * Find by id (UUID) from a default repository
	 * @param id object id
	 * @return matching object
	 */
	Optional<T> findById(UUID id);

	/**
	 * Find by id list (UUID) from a default repository
	 *
	 * @param ids object ids
	 * @return matching objects
	 */
	List<T> findByIdList(List<UUID> ids);



	/**
	 * Find objects
	 *
	 * @param entityClass entity class
	 * @param textSearchType text search type
	 * @param filterMap the filter map
	 * @param filterConjunction filter conjunction
	 * @param sortOrders sort orders
	 * @param selectedFacets selected facets
	 * @param page page number
	 * @param size page size
	 * @return objects page of objects
	 * @throws Throwable if exception occurs
	 */
	Page<T> find(
		Class<T> entityClass, TextSearchType textSearchType,
		Map<String, String> filterMap, FilterConjunction filterConjunction,
		List<SortOrder> sortOrders,
		List<SelectedFacet> selectedFacets,
		Integer page, Integer size
	) throws Throwable;

	/**
	 * Find objects
	 *
	 * @param entityClass entity class
	 * @param textSearchType text search type
	 * @param filterMap filter map
	 * @param filterConjunction filter conjunction
	 * @param sortOrders sort orders
	 * @param selectedFacets selected facets
	 * @param pageNumber page
	 * @param pageSize size
	 * @param since time since created or updated
	 * @return objects objects
	 * @throws Throwable if exception occurs
	 */
	Page<T> find(
		Class<T> entityClass,
		TextSearchType textSearchType,
		Map<String, String> filterMap,
		FilterConjunction filterConjunction,
		List<SortOrder> sortOrders,
		List<SelectedFacet> selectedFacets,
		Integer pageNumber,
		Integer pageSize,
		Long since
	) throws Throwable;

	/**
	 * Find rows that match given filters and export
	 *
	 * @param entityClass class of the entity
	 * @param textSearchType text search type
	 * @param filterMap filter map
	 * @param filterConjunction filter conjunction
	 * @param sortOrders sort options
	 * @param selectedFacets facet filters
	 * @param pageNumber page number
	 * @param pageSize page size
	 * @param fileType output file type
	 * @param columnMapping attribute mapping
	 * @return handle to exported content
	 * @throws Throwable exception is thrown if it occurs
	 */
	Map<String, Object> export(
		Class<T> entityClass, TextSearchType textSearchType,
		Map<String, String> filterMap, FilterConjunction filterConjunction,
		List<SortOrder> sortOrders,
		List<SelectedFacet> selectedFacets,
		Integer pageNumber, Integer pageSize,
		FileType fileType,
		Map<String, String> columnMapping
	) throws Throwable;

	/**
	 * Save input
	 *
	 * @param input object to be saved
	 * @return saved object
	 * @throws SQLException on any errors
	 */
	T save(T input) throws SQLException;

	/**
	 * Save using rank without references
	 *
	 * @param input object to be saved
	 * @param rank rank
	 * @return saved object
	 * @throws SQLException on any errors
	 */
	T save(T input, Optional<Rank> rank) throws SQLException;

	/**
	 * Save input based on rank and references
	 *
	 * Examples:
	 * 		rank(input, TOP);					- Place data at the top most
	 * 		rank(input, BOTTOM);				- Place data at the bottom most
	 * 		rank(input, TOP_OF, 10L);			- Place data above top
	 * 		rank(input, BOTTOM_OF, 20L);		- Place data below bottom
	 * 		rank(input, BETWEEN, 15L, 16L);		- Place data between top and bottom
	 *
	 * @param input object to be saved
	 * @param rank rank
	 * @param references entity ids (Long)
	 * @return saved object
	 * @throws SQLException on any errors
	 */
	T save(T input, Optional<Rank> rank, List<T> references) throws SQLException;

	/**
	 * Save all provided inputs
	 *
	 * @param input objects to be saved
	 * @return saved objects
	 */
	List<T> saveAll(List<T> input);

	/**
	 * Delete provided object
	 *
	 * @param t object to be deleted
	 */
	void delete(T t);

	/**
	 * Merge source to target and save
	 *
	 * @param source source object
	 * @param target target object
	 * @return merged and saved object
	 */
	T mergeAndSave(T source, T target);

	/**
	 * Merge source to target and save
	 *
	 * @param source source object
	 * @param target target object
	 * @param optionalIgnoreProperties properties to ignore when merging
	 * @return merged and saved object
	 */
	T mergeAndSave(T source, T target, Optional<Set<String>> optionalIgnoreProperties);

	/**
	 * Get facets
	 *
	 * @param entityClass entity class name
	 * @param queryText query text
	 * @param filterMap additional filters
	 * @return selected facets
	 * @throws SQLException on any errors
	 */
	Map<String, List<FacetMenuItem>> getFacets(Class<T> entityClass, String queryText, Map<String, String> filterMap) throws SQLException;

	/**
	 * Determine if entity is rankable or not
	 *
	 * @param entityClass class type
	 * @return true if rankable falser otherwise
	 */
	default boolean isRankable(Class<T> entityClass) {
		return FrameworkHelper.isRankable(entityClass);
	}

	/**
	 * Fetch metadata for provided class
	 *
	 * @param entityClass class of the object
	 * @return attribute map
	 * @throws SQLException on any errors
	 */
	default Map<String, Object> getMetadata(Class<T> entityClass) throws SQLException {
		return FrameworkHelper.getMetadata(entityClass);
	}

	void forEachRecord(RecordProcessor<T> recordProcessor, Pageable pageable) throws SQLException;
}
