/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.services;

import java.util.List;
import java.util.Optional;

import org.appsweaver.commons.jpa.models.custom.CustomDataType;
import org.appsweaver.commons.jpa.repositories.CustomDataTypeRepository;
import org.appsweaver.commons.jpa.repositories.CustomPrimaryRepository;
import org.appsweaver.commons.jpa.types.FieldType;
import org.appsweaver.commons.utilities.Stringizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

/**
 * 
 * @author UD
 * 
 */
@Service("customDataTypeService")
@ConditionalOnBean(name = "customDataTypeRepository")
public class CustomDataTypeService extends AbstractDataService<CustomDataType> {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired CustomDataTypeRepository customDataTypeRepository;

	@Override
	public CustomPrimaryRepository<CustomDataType> getRepository() {
		return customDataTypeRepository;
	}
	
	public Optional<CustomDataType> findByName(String name) {
		return customDataTypeRepository.findByName(name);
	}
	
	public void register(String fieldName, FieldType fieldType) {
		register(fieldName, fieldType, null, null);
	}
	
	public void register(String fieldName, FieldType fieldType, List<String> listOfValues) {
		register(fieldName, fieldType, null, listOfValues);
	}

	public void register(String fieldName, FieldType fieldType, String defaultValue, List<String> listOfValues) {
		try {
			final Optional<CustomDataType> optionalDataType = findByName(fieldName);
			if(!optionalDataType.isPresent()) {
				final CustomDataType customDataType = new CustomDataType();
				customDataType.setName(fieldName);
				customDataType.setType(fieldType);
				
				if(!Stringizer.isEmpty(defaultValue)) {
					customDataType.setDefaultValue(defaultValue);
				}
				
				if(listOfValues != null) {
					customDataType.setListOfValues(listOfValues);
				}
				
				save(customDataType);
			} else {
				final CustomDataType dataType = optionalDataType.get();
				logger.warn(
					"Data type exists! Requested=[name={}, type={}]; existing=[{}]", 
					fieldName, fieldType, 
					dataType
				);
			}
		} catch(Throwable t) {
			logger.warn("Data type [name={}, type={}] was not registered due to errors", fieldName, fieldType, t);
		}
	}
}