/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.services;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.appsweaver.commons.api.DataExporter;
import org.appsweaver.commons.api.DataService;
import org.appsweaver.commons.api.EntityRanker;
import org.appsweaver.commons.api.SearchService;
import org.appsweaver.commons.jpa.models.criteria.SortOrder;
import org.appsweaver.commons.jpa.models.search.FacetMenuItem;
import org.appsweaver.commons.jpa.models.search.TextSearchType;
import org.appsweaver.commons.jpa.types.FilterConjunction;
import org.appsweaver.commons.jpa.types.Rank;
import org.appsweaver.commons.json.views.SelectedFacet;
import org.appsweaver.commons.models.FieldMetadata;
import org.appsweaver.commons.models.criteria.FilterableField;
import org.appsweaver.commons.models.criteria.SearchableField;
import org.appsweaver.commons.models.persistence.Entity;
import org.appsweaver.commons.spring.utilities.FrameworkHelper;
import org.appsweaver.commons.types.FileType;
import org.appsweaver.commons.utilities.Collections;
import org.springframework.data.domain.Page;

/**
 *
 * @author UD
 *
 */
public abstract class AbstractEntityDataService<T extends Entity> extends DefaultSecurityService implements DataService<T> {
	protected abstract SearchService getSearchService();

	protected abstract EntityRanker getEntityRanker();
	
	protected abstract DataExporter<T> getDataExporter();

	/*
	 * (non-Javadoc)
	 * @see org.appsweaver.commons.services.DataService#find(java.lang.Class, java.util.Map, java.util.List, java.lang.Integer, java.lang.Integer, org.appsweaver.commons.jpa.types.FilterConjunction)
	 */
	@Override
	public Map<String, Object> export(
		Class<T> entityClass, TextSearchType type, 
		Map<String, String> filterMap, FilterConjunction filterConjunction, 
		List<SortOrder> sortOrders, 
		List<SelectedFacet> selectedFacets,
		Integer page, Integer size,
		FileType fileType,
		final Map<String, String> columnMapping
	) throws Throwable {
		final Page<T> pages = find(entityClass, type, 
			filterMap, filterConjunction, 
			sortOrders, 
			selectedFacets,
			page, size);
		
		final Map<String, Object> result = getDataExporter().export(entityClass, fileType, pages.get(), columnMapping);
		result.put("rows", pages.getTotalElements());

		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see org.appsweaver.commons.services.DataService#getEntityMetadata(java.lang.Class)
	 */
	@Override
	public Map<String, Object> getMetadata(Class<T> entityClass) throws SQLException {
		final Map<String, Object> metadata = Collections.newHashMap();

		final List<FieldMetadata> properties = FrameworkHelper.getProperties(entityClass);
		metadata.put("properties", properties);

		final Map<String, FilterableField> filterableFields = FrameworkHelper.getFilterableFields(entityClass);
		metadata.put("filterable", filterableFields.values());
		
		// Show only fields allowed to be looked up during query
		final Set<SearchableField> searchableFields = getSearchService().getAllSearchableFields(entityClass, Optional.of(true));
		final Set<SearchableField> filteredSearchableFields = searchableFields.stream().filter(searchableField -> searchableField.isLookup()).collect(Collectors.toSet());
		metadata.put("searchable", Arrays.asList(filteredSearchableFields));

		return metadata;
	}

	/*
	 * (non-Javadoc)
	 * @see org.appsweaver.commons.services.DataService#save(org.appsweaver.commons.jpa.models.Entity, java.util.Optional)
	 */
	@Override
	public T save(T input, Optional<Rank> optionalRank) throws SQLException {
		return save(input, optionalRank, Collections.newList());
	}

	/*
	 * (non-Javadoc)
	 * @see org.appsweaver.commons.services.DataService#save(org.appsweaver.commons.jpa.models.Entity)
	 */
	@Override
	public T save(T input) throws SQLException {
		return save(input, Optional.empty());
	}

	/*
	 * (non-Javadoc)
	 * @see org.appsweaver.commons.services.DataService#getFacets(java.lang.Class, org.appsweaver.commons.jpa.models.search.TextSearchType, java.lang.String)
	 */
	@Override
	public Map<String, List<FacetMenuItem>> getFacets(Class<T> entityClass, String queryText, Map<String, String> filterMap) throws SQLException {
		final Map<String, List<FacetMenuItem>> facets = getSearchService().getFacets(entityClass, queryText, filterMap);
		return facets;
	}
}
