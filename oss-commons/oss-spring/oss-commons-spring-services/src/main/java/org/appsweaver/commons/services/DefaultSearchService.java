/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.services;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;

import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.BooleanQuery.Builder;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.appsweaver.commons.api.SearchService;
import org.appsweaver.commons.jpa.components.HibernateHelper;
import org.appsweaver.commons.jpa.helpers.FacetRangeParser;
import org.appsweaver.commons.jpa.models.criteria.SortOrder;
import org.appsweaver.commons.jpa.models.search.FacetMenuItem;
import org.appsweaver.commons.jpa.models.search.TextSearchType;
import org.appsweaver.commons.json.views.SelectedFacet;
import org.appsweaver.commons.models.criteria.SearchableField;
import org.appsweaver.commons.models.persistence.Entity;
import org.appsweaver.commons.spring.utilities.FrameworkHelper;
import org.appsweaver.commons.spring.utilities.IDHelper;
import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Ruminator;
import org.appsweaver.commons.utilities.Stringizer;
import org.hibernate.search.MassIndexer;
import org.hibernate.search.SearchFactory;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.FacetRangeAboveBelowContext;
import org.hibernate.search.query.dsl.FacetRangeAboveContext;
import org.hibernate.search.query.dsl.FacetRangeBelowContinuationContext;
import org.hibernate.search.query.dsl.FacetRangeEndContext;
import org.hibernate.search.query.dsl.FuzzyContext;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.hibernate.search.query.dsl.RangeContext;
import org.hibernate.search.query.dsl.RangeMatchingContext;
import org.hibernate.search.query.dsl.TermContext;
import org.hibernate.search.query.dsl.TermMatchingContext;
import org.hibernate.search.query.dsl.WildcardContext;
import org.hibernate.search.query.dsl.sort.SortContext;
import org.hibernate.search.query.dsl.sort.SortFieldContext;
import org.hibernate.search.query.engine.spi.FacetManager;
import org.hibernate.search.query.facet.Facet;
import org.hibernate.search.query.facet.FacetSortOrder;
import org.hibernate.search.query.facet.FacetingRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author UD
 * 
 */
public abstract class DefaultSearchService implements SearchService {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	static final String RANGE_ABOVE = "above";
	static final String RANGE_BELOW = "below";
	static final String RANGE_BETWEEN = "between";
	
	@Value("${webservice.search.max-clause-count:2048}")
	private Integer maxClauseCount;
	
	@Value("${webservice.search.max-facet-count:10000}")
	private Integer maxFacetCount;
	
	@Value("${webservice.search.max-facet-results-count:0}")
	private Integer maxFacetResultsCount;

	@Value("${webservice.search.index-on-start:false}")
	private boolean indexOnStart;

	protected abstract HibernateHelper getHibernateHelper();

	/*
	 * Initialize hibernate search index via Spring configuration.
	 * This operation is critical for initializing index on boot
	 */
	@Override
	@PostConstruct
	public void initialize() {
		/*
		 * Enable search queries with large number of filters
		 */
		if(maxClauseCount != null) {
			logger.info("Updating BooleanQuery maxClauseCount to {}", maxClauseCount);
			BooleanQuery.setMaxClauseCount(maxClauseCount);
		}

		if (indexOnStart) {
			final EntityManager entityManager = getHibernateHelper().createEntityManager();
			try {
				logger.info("Running hibernate search indexer");

				final FullTextEntityManager fullTextEntityManager = getFullTextEntityManager(entityManager);
				final Class<?>[] object = { Object.class };
				final MassIndexer massIndexer = fullTextEntityManager.createIndexer(object);
				massIndexer.start();
			} catch (final Throwable t) {
				logger.debug("Error initializing hibernate search", t);
			} finally {
				entityManager.close();
			}
		}
	}

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	@Override
	public <T extends Entity> Collection<T> search(
		Class<T> entityClass,
		TextSearchType textSearchType,
		String queryString,
		Map<String, String> filters,
		List<SortOrder> sortOrders, 
		List<SelectedFacet> selectedFacets,
		Integer offset, 
		Integer size,
		Long since,
		AtomicLong total
	) throws SQLException {
		// Must be done first
		IDHelper.resolveObfuscatedId(filters, null);

		final EntityManager entityManager = getHibernateHelper().createEntityManager();
		try {
			logger.debug("Start executing fuzzy search [query={}, offset={}, size={}]", queryString, offset, size);

			final QueryBuilder queryBuilder = getQueryBuilder(entityManager, entityClass);

			final Query luceneSearchQuery = createLuceneSearchQuery(entityClass, textSearchType, queryString, TextSearchType.KEYWORD, filters, Optional.ofNullable(since), queryBuilder);
			
			final FullTextQuery fullTextQuery = getFullTextQuery(entityManager, entityClass, luceneSearchQuery);

			// Apply facets
			if(selectedFacets.size() > 0) {
				final Map<String, List<FacetMenuItem>> availableFacets = getFacets(entityClass, queryString, filters);

				final FacetManager facetManager = fullTextQuery.getFacetManager();
				applyFacets(facetManager, availableFacets, selectedFacets);
			}

			fullTextQuery.setProjection(FullTextQuery.THIS, FullTextQuery.SCORE);
			fullTextQuery.setFirstResult(offset * size);
			fullTextQuery.setMaxResults(size);

			final Sort sort = createSort(queryBuilder, sortOrders);
			if(sort != null) {
				fullTextQuery.setSort(sort);
			}

			final List<Object[]> resultList = fullTextQuery.getResultList();

			final Collection<T> collection = Collections.newList();
			resultList.forEach(object -> {
				final T casted = (T)object[0];
				if(casted != null && !collection.contains(casted)) {
					collection.add(casted);
				}
			});
			total.set(fullTextQuery.getResultSize());

			logger.debug("End executing fuzzy search [query={}, offset={}, size={}, total={}]", queryString, offset, size, total.get());

			return collection;
		} catch (SQLException t) {
			logger.error("Error executing fuzzy search [query={}, offset={}, size={}, total={}]", queryString, offset, size, total.get(), t);

			throw t;
		} finally {
			entityManager.close();
		}
	}

	@Transactional(readOnly = true)
	@Override
	public <T extends Entity> Collection<T> search(
			Class<T> entityClass, TextSearchType textSearchType,
			String query, Map<String, String> filters,
			List<SortOrder> sortOrders, 
			List<SelectedFacet> selectedFacets,
			Integer offset, Integer size,
			AtomicLong total
			) throws SQLException {
		return search(entityClass, textSearchType, query, filters, sortOrders, selectedFacets, offset, size, /* since */null, total);
	}

	@Override
	public <T extends Entity> Map<String, List<FacetMenuItem>> getFacets(Class<T> entityClass, String queryString, Map<String, String> filters) throws SQLException {
		final Map<String, List<FacetMenuItem>> menuItems = Collections.newHashMap();

		final Map<String, String> facetsMetadata = getFacetsMetadata(entityClass);

		final EntityManager entityManager = getHibernateHelper().createEntityManager();

		try {
			final QueryBuilder queryBuilder = getQueryBuilder(entityManager, entityClass);
			
			final Query luceneSearchQuery = createLuceneSearchQuery(entityClass, TextSearchType.WILDCARD, "*", TextSearchType.KEYWORD, filters, Optional.empty(), queryBuilder);

			// Get handle to facet manager
			final FacetManager facetManager = getFacetManager(entityManager, entityClass, luceneSearchQuery);
			registerFacets(
				facetManager,
				queryBuilder,
				luceneSearchQuery, 
				facetsMetadata
			);

			facetsMetadata.forEach((facetRequestName, facetNamePattern) -> {
				// Parse facet name
				final FacetRangeParser parser = new FacetRangeParser(facetNamePattern);
				final String facetName = parser.getName();

				// Build facet menu items
				final List<FacetMenuItem> facetMenuItems = Collections.newList();

				int index = 0;
				for (final Facet facet : facetManager.getFacets(facetRequestName)) {
					facetMenuItems.add(new FacetMenuItem(facet, false, index++));
				}

				// Update menu item
				menuItems.put(facetName, facetMenuItems);
			});
		} catch (final Throwable t) {
			logger.error("Error generating facets", t);
		} finally {
			entityManager.close();
		}

		return menuItems;
	}

	FullTextEntityManager getFullTextEntityManager(EntityManager entityManager) {
		final FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
		return fullTextEntityManager;
	}

	Sort createSort(QueryBuilder queryBuilder, List<SortOrder> sortOrders) {
		// Apply sorting
		final SortContext sortContext = queryBuilder.sort();

		SortFieldContext sortFieldContext = null;
		for(int index = 0; index < sortOrders.size(); index++) {
			final SortOrder sortOrder = sortOrders.get(index);

			// Build sort field context
			
			if(sortFieldContext == null) {
				sortFieldContext = sortContext.byField(sortOrder.getField());
			} else {
				sortFieldContext = sortFieldContext.andByField(sortOrder.getField());
			}
			
			// Set order
			if(sortOrder.getOrder().isAscending()) {
				sortFieldContext.asc();
			} else {
				sortFieldContext.desc();
			}
		}

		return sortFieldContext != null ? sortFieldContext.createSort() : null;
	}

	void applyFacets(FacetManager facetManager, Map<String, List<FacetMenuItem>> availableFacets, List<SelectedFacet> selectedFacets) {
		// Process selection
		selectedFacets.forEach(facet -> {
			final String name = facet.getName();
			final List<String> values = facet.getValues();

			// toggle selection
			final List<FacetMenuItem> menuItems = availableFacets.get(name);
			menuItems.forEach(menuItem -> {
				if(values.contains(menuItem.getValue())) {
					logger.info("Enabling facet selection [facetName={}, selected={}]", name, menuItem.getValue());
					menuItem.setSelected(true);
				}
			});
			// toggle selection
		});
		// Process selection

		// Apply facets to queries - Begin
		selectedFacets.forEach(selectedFacet -> {
			final String facetName = selectedFacet.getName();
			final List<FacetMenuItem> menuItems = availableFacets.get(facetName);
			final List<FacetMenuItem> selectedMenuItems = menuItems.stream().filter(c -> c.isSelected()).collect(Collectors.toList());

			final List<Facet> selectFacetList = Collections.newList();
			selectedMenuItems.forEach(item -> {
				final Facet facet = item.getFacet();
				selectFacetList.add(facet);
			});

			final Facet[] selectFacetArray = Collections.toArray(selectFacetList, Facet.class);
			facetManager.getFacetGroup(facetName).selectFacets(selectFacetArray);
		});
		// Apply facets to queries - End
	}

	<T extends Entity> Query createSinceRangeQuery(
		Class<T> entityClass,
		TextSearchType textSearchType,
		Optional<Long> optionalSince,
		QueryBuilder queryBuilder
	) {
		if(optionalSince.isPresent()) {
			final Long since = optionalSince.get();
			final SearchableField createdOnSearchField = new SearchableField(true, true, "createdOn", null, null, null);
			final RangeMatchingContext createdSinceRangeMatchingContext = newRangeMatchingContext(entityClass,
				queryBuilder,
				createdOnSearchField
			);
			final Query createdOnQuery = createdSinceRangeMatchingContext.above(since).createQuery();

			final SearchableField updatedOnSearchField = new SearchableField(true, true, "updatedOn", null, null, null);
			final RangeMatchingContext updatedSinceRangeMatchingContext = newRangeMatchingContext(entityClass,
				queryBuilder,
				updatedOnSearchField
			);
			
			final Query updatedOnQuery = updatedSinceRangeMatchingContext.above(since).createQuery();
			return queryBuilder.bool().should(createdOnQuery).should(updatedOnQuery).createQuery();				
		}

		return null;
	}

	boolean hasWildcards(String query) {
		return query.contains("*") || query.contains("?");
	}

	TermMatchingContext newFieldCustomization(WildcardContext wildcardContext, SearchableField searchableField) {
		final TermMatchingContext termMatchingContext = wildcardContext.onField(searchableField.getName());

		// Add Boost Factor
		if(searchableField.getBoost() != null) {
			termMatchingContext.boostedTo(searchableField.getBoost());
		}

		return termMatchingContext;
	}

	TermMatchingContext newFieldCustomization(FuzzyContext fuzzyContext, SearchableField searchableField) {
		final TermMatchingContext termMatchingContext = fuzzyContext.onField(searchableField.getName());

		// Add Boost Factor
		if (searchableField.getBoost() != null) {
			termMatchingContext.boostedTo(searchableField.getBoost());
		}

		return termMatchingContext;
	}

	RangeMatchingContext newFieldCustomization(RangeContext rangeContext, SearchableField searchableField) {
		final RangeMatchingContext rangeMatchingContext = rangeContext.onField(searchableField.getName());

		// Add Boost Factor
		if (searchableField.getBoost() != null) {
			rangeMatchingContext.boostedTo(searchableField.getBoost());
		}

		return rangeMatchingContext;
	}

	TermMatchingContext newFieldCustomization(TermContext termContext, SearchableField searchableField) {
		final TermMatchingContext termMatchingContext = termContext.onField(searchableField.getName());

		// Add Boost Factor
		if(searchableField.getBoost() != null) {
			termMatchingContext.boostedTo(searchableField.getBoost());
		}

		return termMatchingContext;
	}

	<T extends Entity> RangeMatchingContext newRangeMatchingContext(
		Class<T> entityClass,
		QueryBuilder queryBuilder,
		SearchableField searchableField
	) {
		final RangeContext rangeContext = queryBuilder.range();
		final RangeMatchingContext rangeMatchingContext = newFieldCustomization(
			rangeContext, 
			searchableField
		);

		return rangeMatchingContext;
	}

	<T extends Entity> TermMatchingContext newTermMatchingContext(
			Class<T> entityClass,
			TextSearchType textSearchType,
			QueryBuilder queryBuilder,
			SearchableField ... searchableFields
			) throws SQLException {
		if(searchableFields == null || searchableFields.length == 0) {
			throw new SQLException("Cannot complete search request, search fields not available!");
		}

		final TermContext termContext = queryBuilder.keyword();
		TermMatchingContext termMatchingContext = null;

		/*
		 * Item at index 0 is already processed, so must
		 * start from index 1 to process remaining search fields
		 */
		for(int index = 0; index < searchableFields.length; index++) {
			final SearchableField searchableField = searchableFields[index];

			final String fieldName = searchableField.getName();
			final Class<?> fieldType = Ruminator.getFieldType(entityClass, fieldName);
			final boolean isNumeric = Ruminator.isNumericField(entityClass, fieldName);

			logger.debug("Processing field [name={}, type={}, isNumeric={}]", fieldName, fieldType, isNumeric);
			if(searchableField.isEnabled() && searchableField.isLookup() && !isNumeric) {
				if(termMatchingContext == null) {
					logger.debug("Setting up context [name={}, type={}, isNumeric={}]", fieldName, fieldType, isNumeric);

					if(textSearchType == TextSearchType.WILDCARD) {
						final WildcardContext wildcardContext = termContext.wildcard();
						termMatchingContext = newFieldCustomization(wildcardContext, searchableField);
					} else if(textSearchType == TextSearchType.FUZZY) {
						final FuzzyContext fuzzyContext = termContext.fuzzy();
						termMatchingContext = newFieldCustomization(fuzzyContext, searchableField);
					} else {
						termMatchingContext = newFieldCustomization(termContext, searchableField);
					}
				} else {
					logger.debug("Adding non-numeric field for lookup [name={}, type={}, isNumeric={}]", fieldName, fieldType, isNumeric);
					// No boost value
					termMatchingContext.andField(searchableField.getName());
				}
				
				// Guard against null values
				if(searchableField.getBoost() != null) {
					// Boost up
					termMatchingContext.boostedTo(searchableField.getBoost());
				}
			}
		}

		return termMatchingContext;
	}

	<T extends Entity> Map<String, String> getFacetsMetadata(Class<T> entityClass) throws SQLException {
		try {
			// Get all enabled fields
			final Set<SearchableField> set = getAllSearchableFields(entityClass, Optional.empty());

			final Map<String, String> facetsMetadata = Collections.newHashMap();
			set.forEach(item -> {
				if(item.isEnabled()) {
					final String[] facets = item.getFacets();
					for(int index = 0; index < facets.length; index++) {
						final String facetName = facets[index];
						final String facetRequestName = String.format("%s_group", facetName);	// Suffix with _request

						final String range = item.getRange();
						if(!Stringizer.isEmpty(range)) {
							// Attach developer provided range information for processing
							facetsMetadata.put(facetRequestName, String.format("%s:%s", facetName, range.toLowerCase()));
						} else {
							facetsMetadata.put(facetRequestName, facetName);
						}
					}
				}
			});
			
			return facetsMetadata;
		} catch(final Throwable t) {
			throw new SQLException("Unable to get registered facets", t);
		}
	}

	<T extends Entity> FullTextQuery getFullTextQuery(EntityManager entityManager, Class<T> entityClass, final Query luceneSearchQuery) {
		final FullTextQuery fullTextQuery = getFullTextEntityManager(entityManager).createFullTextQuery(luceneSearchQuery, entityClass)
				.setMaxResults(maxFacetResultsCount);
		return fullTextQuery;
	}

	SearchFactory getSearchFactory(EntityManager entityManager) {
		final SearchFactory searchFactory = getFullTextEntityManager(entityManager).getSearchFactory();
		return searchFactory;
	}

	<T extends Entity> QueryBuilder getQueryBuilder(EntityManager entityManager, Class<T> entityClass) {
		final SearchFactory searchFactory = getSearchFactory(entityManager);
		final QueryBuilder queryBuilder = searchFactory.buildQueryBuilder().forEntity(entityClass).get();

		return queryBuilder;
	}

	<T extends Entity> FacetManager getFacetManager(EntityManager entityManager, Class<T> entityClass, final Query luceneSearchQuery) {
		final FullTextQuery fullTextQuery = getFullTextQuery(entityManager, entityClass, luceneSearchQuery);
		final FacetManager facetManager = fullTextQuery.getFacetManager();
		return facetManager;
	}

	<T extends Entity> void registerFacets(
		FacetManager facetManager,
		QueryBuilder queryBuilder,
		Query luceneSearchQuery,
		Map<String, String> facetsMetadata
	) {
		/*
		 * Build and enable faceting request based on meta data
		 */
		facetsMetadata.forEach((facetRequestName, facetNamePattern) -> {
			// Parse facet name
			final FacetRangeParser parser = new FacetRangeParser(facetNamePattern);
			final String facetName = parser.getName();
			final List<String> ranges = parser.getRanges();

			final FacetingRequest facetingRequest;
			if(ranges != null && ranges.size() > 0) {
				final FacetRangeAboveBelowContext<Number> rangeFacet = queryBuilder.facet()
					.name(facetRequestName)
					.onField(facetName)
					.range();

				FacetRangeBelowContinuationContext<Number> facetBelow = null;
				FacetRangeAboveContext<Number> facetAbove = null;
				FacetRangeEndContext<Number> facetBetween = null;

				for(final String range : ranges) {
					final String rangeText = Stringizer.between(range, "(", ")");

					if(range.startsWith(RANGE_ABOVE)) {
						if(facetBetween == null && facetBelow == null) {
							facetAbove = rangeFacet.above(Double.valueOf(rangeText));
						} else if(facetBetween == null && facetBelow != null) {
							facetAbove = facetBelow.above(Double.valueOf(rangeText));
						} else {
							facetAbove = facetBetween.above(Double.valueOf(rangeText));
						}
					} else if(range.startsWith(RANGE_BELOW)) {
						facetBelow = rangeFacet.below(Double.valueOf(rangeText));
					} else {
						final String[] between = rangeText.split(",");
						if(between.length == 2) {
							if(facetBetween == null && facetBelow == null) {
								facetBetween = rangeFacet.from(Double.valueOf(between[0])).to(Double.valueOf(between[1]));
							} else if(facetBetween == null && facetBelow != null) {
								facetBetween = facetBelow.from(Double.valueOf(between[0])).to(Double.valueOf(between[1]));
							} else {
								facetBetween = facetBetween.from(Double.valueOf(between[0])).to(Double.valueOf(between[1]));
							} 
						}
					}
				};

				if(facetBelow == null) {
					throw new IllegalArgumentException("Range information must include below(value)");
				}

				if(facetAbove == null) {
					throw new IllegalArgumentException("Range information must include above(value)");
				}

				facetingRequest = facetAbove
					.orderedBy(FacetSortOrder.RANGE_DEFINITION_ORDER)
					.includeZeroCounts(false)
					.maxFacetCount(maxFacetCount)
					.createFacetingRequest();
			} else {
				facetingRequest = queryBuilder.facet()
					.name(facetRequestName)
					.onField(facetName)
					.discrete()
					.orderedBy(FacetSortOrder.COUNT_DESC)
					.includeZeroCounts(false)
					.maxFacetCount(maxFacetCount)
					.createFacetingRequest();
			}

			facetManager.enableFaceting(facetingRequest);
		});
	}
	
	<T extends Entity> Query createLuceneSearchQuery(
		Class<T> entityClass,
		TextSearchType textSearchType,
		String queryString,
		TextSearchType filterTextSearchType,
		Map<String, String> filters,
		Optional<Long> since,
		QueryBuilder queryBuilder
	) throws SQLException {
		try {
			// Sanitize URL
			queryString = Stringizer.decodeUrl(queryString).toLowerCase();
			
			// Factor default wild cards
			if(textSearchType == TextSearchType.WILDCARD && !hasWildcards(queryString)) {
				queryString = String.format("*%s*", queryString);
			}
			
			queryString = FrameworkHelper.escapeLuceneQuery(queryString);
			
			final Set<SearchableField> searchableFields = getAllSearchableFields(entityClass, Optional.of(true));
			final TermMatchingContext termMatchingContext = newTermMatchingContext(
				entityClass,
				textSearchType,
				queryBuilder,
				Collections.toArray(searchableFields, SearchableField.class)
			);
			
			// Make a primary query
			final Builder builder = new BooleanQuery.Builder();

			if(!Stringizer.isEmpty(queryString, false)) {
				logger.debug("Setting up query with provided text [value={}]", queryString);
				final Query query = termMatchingContext.matching(queryString).createQuery();
				builder.add(query, Occur.MUST);
			} else {
				logger.debug("Setting up query to return all records");
				final Query query = queryBuilder.all().createQuery();
				builder.add(query, Occur.MUST);
			}
			
			// Make since query (optional)
			if(since.isPresent()) {
				final Query sinceRangeQuery = createSinceRangeQuery(entityClass, textSearchType, since, queryBuilder);

				if(sinceRangeQuery != null) {
					logger.debug("Processing since filter [value={}]", since.get());
					builder.add(sinceRangeQuery, Occur.MUST);
					logger.debug("Added since filter [value={}]", since.get());
				}
			}
			
			// Construct filters
			if(filters.size() > 0) {
				logger.debug("Processing additional filters [values={}]", filters);
				final Map<String, Query> queries = FrameworkHelper.toQuery(entityClass, filters, queryBuilder);
				
				queries.forEach((filterKey, query) -> {
					final String filterValue = filters.get(filterKey);
					
					if(filterValue.startsWith("!")) {
						builder.add(query, Occur.MUST_NOT);
					} else {
						builder.add(query, Occur.MUST);
					}
				});

				logger.debug("Added additional filters [values={}]", filters);
			}
	
			// Finalize query
			final Query query = builder.build();
			return query;
		} catch (final Throwable t) {
			throw new SQLException(t);
		}
	}
}
