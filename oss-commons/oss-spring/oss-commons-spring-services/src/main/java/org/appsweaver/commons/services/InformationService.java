/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.services;

import java.io.File;
import java.util.List;

import org.appsweaver.commons.models.devops.ReleaseNote;
import org.appsweaver.commons.models.properties.BuildInformation;
import org.appsweaver.commons.utilities.Fileinator;
import org.appsweaver.commons.utilities.Jsonifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * 
 * @author UD
 * 
 */
@Service("informationService")
@ConditionalOnProperty(name = "webservice.build-information.enabled", havingValue = "true")
public class InformationService {
	@Value("${webservice.build-information.version}")
	private String version;

	@Value("${webservice.build-information.number}")
	private String number;

	@Value("${webservice.build-information.date}")
	private String date;
	
	@Value("${webservice.build-information.mode:UNKNOWN}")
	private String mode;
	
	@Value("${webservice.build-information.release-notes-file}")
	private String releaseNotesFile;

	public BuildInformation getBuildInformation() {
		final BuildInformation buildInformation = new BuildInformation();
		
		buildInformation.setDate(date);
		buildInformation.setVersion(version);
		buildInformation.setNumber(number);
		buildInformation.setMode(mode);
		
		return buildInformation;
	}
	
	public List<ReleaseNote> getReleaseNotes() throws Throwable {
		try {
			final String json = Fileinator.toString(new File(releaseNotesFile));
			final List<ReleaseNote> releaseNotes = Jsonifier.toList(json, ReleaseNote.class);
			return releaseNotes;
		} catch (Throwable t) {
			throw t;
		}
	}
}
