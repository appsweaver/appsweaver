/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.services;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

import org.appsweaver.commons.api.EntityRanker;
import org.appsweaver.commons.jpa.repositories.CustomPrimaryRepository;
import org.appsweaver.commons.jpa.types.Rank;
import org.appsweaver.commons.models.persistence.PrimaryEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 *
 * @author UD
 *
 * Algorithm design:
 * 	Use big decimal to store rank (Using 20 digit precision part and 20 digit scale part)
 *
 * 	4611686018427387904 is the maximum value of a positive number that can be store in a 20 digit precision (integer part) of a decimal value.
 *
 * 	Begin ranking from midpoint (0, max value)
 *
 * 	When rank to top is requested; rank by decrementing from the previous minimum value (first preference)
 *
 * 	When rank to bottom is requested; rank by incrementing from the previous maximum value (first preference)
 *
 *  When ranking is performed between already ranked rows; then use scale part and stepper to either increment or decrement based on operation requested.
 */
@Service
public class PrecisionBackedEntityRanker implements EntityRanker {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final BigDecimal TWO = new BigDecimal(2.0);
	private static final BigDecimal STEPPER_LOW = new BigDecimal(0.0000000000000000001);
	private static final BigDecimal STEPPER_HIGH = new BigDecimal(0.4600000000000000000);
	private static final BigDecimal MAX_PRECISION = BigDecimal.valueOf(4611686018427387904.0); // 20 digit scale -- 2^62

	BigDecimal seedValue() {
		final BigDecimal midPoint = midPoint(BigDecimal.ZERO, MAX_PRECISION);
		return midPoint.add(STEPPER_HIGH);
	}

	BigDecimal midPoint(BigDecimal min, BigDecimal max) {
		if(min == null) {
			min = BigDecimal.ZERO;
		}
		if(max == null) {
			max = MAX_PRECISION;
		}

		final BigDecimal difference = max.subtract(min);
		final BigDecimal midPoint = difference.divide(TWO);

		return midPoint;
	}

	<T extends PrimaryEntity> void rankTop(T input, BigDecimal currentMin) throws SQLException {
		if(input != null) {
			/*
			 * Start with midpoint
			 */
			if(currentMin == null) {
				final BigDecimal rank = seedValue();
				input.setRank(rank);
			} else {
				final BigDecimal rank = currentMin.subtract(BigDecimal.ONE);
				final int compareTo = rank.compareTo(BigDecimal.ZERO);
				if(compareTo >= 0) {
					final BigDecimal summedRank = rank.setScale(0, BigDecimal.ROUND_DOWN).add(STEPPER_LOW);
					input.setRank(summedRank);
				} else {
					throw new SQLException("Rank cannot be < 0");
				}
			}

			logger.debug("Ranked entity to top of list [entityClass={}, id={}, rank={}]",
				input.getClass().getSimpleName(),
				input.getId(),
				input.getRank()
			);
		}
	}

	<T extends PrimaryEntity> void rankBottom(T input, BigDecimal currentMax) throws SQLException {
		if(input != null) {
			/*
			 * Start with midpoint
			 */
			if(currentMax == null) {
				final BigDecimal rank = seedValue();
				input.setRank(rank);
			} else {
				final BigDecimal rank = currentMax.add(BigDecimal.ONE);
				final int compareTo = rank.compareTo(MAX_PRECISION);
				if(compareTo < 0) {
					input.setRank(rank.add(STEPPER_HIGH));
				} else {
					throw new SQLException("Rank cannot be >= max value (4611686018427387904)");
				}
			}

			logger.debug("Ranked entity to bottom of list [entityClass={}, id={}, rank={}]",
				input.getClass().getSimpleName(),
				input.getId(),
				input.getRank()
			);
		}
	}

	/*
	 * Compute rank using scale part
	 *
	 * Obtain scale from top and bottom
	 * Find a midpoint
	 */
	BigDecimal computeRank(BigDecimal top, BigDecimal bottom) {
		final BigDecimal midPoint = midPoint(top, bottom);
		return midPoint;
	}

	<T extends PrimaryEntity> void rankBetween(T input, T top, T bottom) {
		if(input != null && top != null && bottom != null) {
			final BigDecimal tr = top.getRank();
			final BigDecimal br = bottom.getRank();

			final BigDecimal computedRank = computeRank(tr, br);
			final BigDecimal rank = tr.add(computedRank);
			input.setRank(rank);

			logger.debug("Ranked entity between [entityClass={}, id={}, rank={}]; top[id={}, rank={}]; bottom[id={}, rank={}]",
				input.getClass().getSimpleName(),
				input.getId(),
				input.getRank(),

				top.getId(),
				top.getRank(),

				bottom.getId(),
				bottom.getRank()
			);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.appsweaver.commons.services.EntityRanker#rank(org.appsweaver.commons.jpa.repositories.CustomRepository, org.appsweaver.commons.jpa.models.Entity, org.appsweaver.commons.jpa.types.Rank, java.util.List)
	 */
	@Override
	public <T extends PrimaryEntity> T rank(
		CustomPrimaryRepository<T> repository,
		T input,
		Rank rank,
		List<T> references
	) throws SQLException {
		if(input == null) {
			throw new SQLException("Requires input row for ranking");
		}

		final BigDecimal minRank = repository.getMinRank();
		final BigDecimal maxRank = repository.getMaxRank();

		switch(rank) {
		case TOP: {
			// Find row (top) with lowest rank and move input before it
			rankTop(input, minRank);
		}
		break;

		case BETWEEN: {
			if(references.size() < 2) throw new SQLException("Requires two reference ids to rank between");

			// User must provide reference of top and bottom row ids
			final T top = references.get(0);
			final T bottom = references.get(1);

			rankBetween(input, top, bottom);
		}
		break;

		default: {
			/*
			 * By default move rows to bottom
			 * Find row (bottom) with highest rank and move input after it
			 */
			rankBottom(input, maxRank);
		}
		break;

		}

		return input;
	}
}
