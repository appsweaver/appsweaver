/*
 *
 * Copyright (c) 2016 appsweaver.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.appsweaver.commons.services;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

import org.appsweaver.commons.api.DataExporter;
import org.appsweaver.commons.api.EntityRanker;
import org.appsweaver.commons.api.PrimaryDataService;
import org.appsweaver.commons.api.RecordProcessor;
import org.appsweaver.commons.jpa.helpers.PageableHelper;
import org.appsweaver.commons.jpa.models.criteria.SortOrder;
import org.appsweaver.commons.jpa.models.custom.CustomDataType;
import org.appsweaver.commons.jpa.models.custom.ExtendableEntity;
import org.appsweaver.commons.jpa.models.custom.TextMultiSelectValue;
import org.appsweaver.commons.jpa.models.custom.Value;
import org.appsweaver.commons.jpa.models.search.TextSearchType;
import org.appsweaver.commons.jpa.repositories.CustomPrimaryRepository;
import org.appsweaver.commons.jpa.types.FilterConjunction;
import org.appsweaver.commons.jpa.types.Rank;
import org.appsweaver.commons.json.views.SelectedFacet;
import org.appsweaver.commons.models.persistence.PrimaryEntity;
import org.appsweaver.commons.services.exception.ServiceException;
import org.appsweaver.commons.spring.utilities.FrameworkHelper;
import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Ruminator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author UD
 *
 */
public abstract class AbstractDataService<T extends PrimaryEntity> extends AbstractEntityDataService<T> implements PrimaryDataService<T> {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Qualifier("primaryRepositoryService")
	@Autowired(required = false) PrimaryRepositoryService primaryRepositoryService;

	@Qualifier("primarySearchService")
	@Autowired(required = false) PrimarySearchService primarySearchService;

	@Qualifier("customDataTypeService")
	@Autowired(required = false) CustomDataTypeService customDataTypeService;

	@Autowired EntityRanker entityRanker;

	@Autowired DataExporter<T> dataExporter;

	public PrimaryRepositoryService getRepositoryService() {
		return primaryRepositoryService;
	}

	public PrimarySearchService getSearchService() {
		return primarySearchService;
	}

	public EntityRanker getEntityRanker() {
		return entityRanker;
	}

	public DataExporter<T> getDataExporter() {
		return dataExporter;
	}

	final void validateForRankable(T input, Optional<Rank> optionalRank, List<T> references) throws SQLException {
		final Class<? extends PrimaryEntity> entityClass = input.getClass();

		if(FrameworkHelper.isRankable(entityClass) && optionalRank.isPresent()) {
			final Rank rank = optionalRank.get();
			input = getEntityRanker().rank(getRepository(), input, rank, references);
		}
	}

	final void validateForExtendable(T input, Optional<Rank> optionalRank, List<T> references) throws SQLException {
		final Class<? extends PrimaryEntity> entityClass = input.getClass();
		final UUID id = input.getId();

		if(FrameworkHelper.isExtendable(entityClass)) {
			final ExtendableEntity extendableEntity = (ExtendableEntity)input;
			final Set<Value<?>> values = extendableEntity.getValues();

			final Set<Value<?>> validValues = Collections.newHashSet();
			/*
			 * Resolve field names and make sure the field references are set
			 * Ignore invalid or unregistered custom fields and values
			 */
			values.forEach(value -> {
				final String fieldName = value.getFieldName();
				final CustomDataType fieldType = value.getDataType();

				if(fieldName == null && fieldType == null) {
					logger.warn("Ignoring invalid field name [entityId={}, class={}]", id, entityClass);
					return;
				}

				if(fieldType == null) {
					final Optional<CustomDataType> optionalDataType = customDataTypeService.findByName(fieldName);
					if(optionalDataType.isPresent()) {
						final CustomDataType customDataType = optionalDataType.get();
						value.setDataType(customDataType);

						if(value instanceof TextMultiSelectValue) {
							/*
							 * Multiple Selection (a.k.a list of values should be validated against the data type
							 */
							final TextMultiSelectValue instance = (TextMultiSelectValue)value;
							final List<String> validListOfValues = Collections.newList();

							final List<String> allowedListOfValues = instance.getDataType().getListOfValues();
							instance.getValue().forEach(selectedValue -> {
								if(allowedListOfValues.contains(selectedValue)) {
									validListOfValues.add(selectedValue);
								} else {
									logger.warn("Ignoring unknown selection [entityId={}, class={}, fieldName={}, selectedValue={}", id, entityClass, fieldName, selectedValue);
								}
							});

							/*
							 * Mark as valid
							 */
							instance.setValue(validListOfValues);
							validValues.add(value);
						} else {
							/*
							 * Mark as valid
							 */
							validValues.add(value);
						}
					} else {
						logger.warn("Ignoring unregistered field name [entityId={}, class={}, fieldName={}", id, entityClass, fieldName);
					}
				}
			});

			extendableEntity.setValues(validValues);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.appsweaver.commons.services.DataService#findById(java.util.UUID, org.appsweaver.commons.jpa.repositories.CustomRepository)
	 */
	@Override
	public Optional<T> findById(UUID id, CustomPrimaryRepository<T> repository) {
		if(repository.existsById(id)) {
			final Optional<T> entity = repository.findById(id);
			return entity;
		} else {
			return Optional.empty();
		}
	}


	/*
	 * (non-Javadoc)
	 * @see org.appsweaver.commons.services.DataService#findByIdList(java.util.List, org.appsweaver.commons.jpa.repositories.CustomRepository)
	 */
	@Override
	public List<T> findByIdList(List<UUID> ids, CustomPrimaryRepository<T> repository) {
		final List<T> entities = Collections.newList();
		if(ids != null && ids.size() > 0) {
			ids.forEach(id -> {
				final Optional<T> optionalReferenceEntity = findById(id, repository);
				if(optionalReferenceEntity.isPresent()) {
					final T referenceEntity = optionalReferenceEntity.get();
					entities.add(referenceEntity);
				}
			});
		}

		return entities;
	}

	/*
	 * (non-Javadoc)
	 * @see org.appsweaver.commons.services.DataService#save(org.appsweaver.commons.jpa.models.Entity, java.util.Optional, java.util.List)
	 */
	@Override
	public T save(T input, Optional<Rank> optionalRank, List<T> references) throws SQLException {
		if(input == null) throw new SQLException("Empty input cannot be processed");

		validateForRankable(input, optionalRank, references);	// Validate for Ranking
		validateForExtendable(input, optionalRank, references);	// Validate for Custom Data Fields and Values

		final T saved = getRepository().save(input);
		return saved;
	}

	@Override
	public Page<T> find(
		Class<T> entityClass, TextSearchType type,
		Map<String, String> filterMap, FilterConjunction filterConjunction,
		List<SortOrder> sortOrders,
		List<SelectedFacet> selectedFacets,
		Integer page, Integer size
	) throws Throwable {
		return find(entityClass, type, filterMap, filterConjunction, sortOrders, selectedFacets, page, size, /* since */null);
	}

	@Override
	public Page<T> find(
		Class<T> entityClass, TextSearchType type,
		Map<String, String> filterMap, FilterConjunction filterConjunction,
		List<SortOrder> sortOrders,
		List<SelectedFacet> selectedFacets,
		Integer page, Integer size,
		Long since
	) throws Throwable {
		// Search requested
		if (filterMap.containsKey("q") || selectedFacets != null && selectedFacets.size() > 0) {
			final String query = filterMap.containsKey("q") ? filterMap.remove("q") : "*";

			final AtomicLong total = new AtomicLong(0);
			final Collection<T> collection = getSearchService().search(entityClass, type, query, filterMap, sortOrders, selectedFacets, page, size, since, total);
			final Page<T> pages = PageableHelper.toPage(collection, page, size, total.longValue());
			return pages;
		} else {
			final Page<T> pages = getRepositoryService().find(getRepository(), entityClass, filterMap, sortOrders, page, size, since, filterConjunction);
			return pages;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.appsweaver.commons.services.DataService#findById(java.util.UUID)
	 */
	@Override
	public Optional<T> findById(UUID id) {
		return findById(id, getRepository());
	}

	/*
	 * (non-Javadoc)
	 * @see org.appsweaver.commons.services.DataService#findByIdList(java.util.List)
	 */
	@Override
	public List<T> findByIdList(List<UUID> ids) {
		return findByIdList(ids, getRepository());
	}

	/*
	 * (non-Javadoc)
	 * @see org.appsweaver.commons.services.DataService#saveAll(java.util.List)
	 */
	@Override
	public List<T> saveAll(List<T> input) {
		return getRepository().saveAll(input);
	}

	/*
	 * (non-Javadoc)
	 * @see org.appsweaver.commons.services.DataService#delete(org.appsweaver.commons.jpa.models.Entity)
	 */
	@Override
	public void delete(T input) {
		getRepository().delete(input);
	}

	/*
	 * (non-Javadoc)
	 * @see org.appsweaver.commons.services.DataService#mergeAndSave(org.appsweaver.commons.jpa.models.Entity, org.appsweaver.commons.jpa.models.Entity)
	 */
	@Override
	public T mergeAndSave(T source, T target) {
		Ruminator.mergeNonNullValues(source, target);
		return getRepository().save(target);
	}

	/*
	 * (non-Javadoc)
	 * @see org.appsweaver.commons.services.DataService#mergeAndSave(org.appsweaver.commons.jpa.models.Entity, org.appsweaver.commons.jpa.models.Entity)
	 */
	@Override
	public T mergeAndSave(T source, T target, Optional<Set<String>> optionalIgnoreProperties) {
		Ruminator.mergeNonNullValues(source, target, optionalIgnoreProperties);
		return getRepository().save(target);
	}

	public long count() {
		return getRepository().count();
	}

	public T update(T input) throws SQLException {
		if(input.getId() == null) throw new SQLException("Update not possible on a non-existent instance!");

		final Optional<T> optionalPerson = findById(input.getId());
		if(optionalPerson.isPresent()) {
			final T existing = optionalPerson.get();

			// Update item id
			input.setId(existing.getId());

			return mergeAndSave(input, existing);
		}

		throw new SQLException("Update not possible on an instance that was not found or was deleted recently!");
	}
	
	public List<T> findAll() {
		return getRepository().findAll();
	}

	public Page<T> findAll(Pageable pageable) {
		return getRepository().findAll(pageable);
	}
	
	@Override
	public void forEachRecord(RecordProcessor<T> recordProcessor, Pageable pageable) throws ServiceException {
		final Page<T> page = findAll(pageable);
		forEachRecord(recordProcessor, page);
	}
	
	public void forEachRecord(RecordProcessor<T> recordProcessor, Page<T> page) throws ServiceException {
		final long totalElements = page.getTotalElements();
		
		// Track count of elements
		long count = 0;
		
		do {
			// Must use iterators, otherwise, things can get into loop 
			// as content in DB changes
			final Iterator<T> iterator = page.iterator();
			while(iterator.hasNext()) {
				final T item = iterator.next();
				recordProcessor.process(item);
			}
			
			// accumulate items processed
			count += page.getNumberOfElements();
			
			// Advance to next page
			if(page.hasNext()) {
				page = this.findAll(page.nextPageable());
			}
		} while(count < totalElements);
	}
}
