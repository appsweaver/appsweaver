/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.services;

import java.util.List;
import java.util.Map;

import org.appsweaver.commons.jpa.models.criteria.SortOrder;
import org.appsweaver.commons.jpa.repositories.query.CustomQueryRepository;
import org.appsweaver.commons.jpa.types.FilterConjunction;
import org.appsweaver.commons.models.persistence.QueryEntity;
import org.appsweaver.commons.spring.utilities.FrameworkHelper;
import org.appsweaver.commons.spring.utilities.IDHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author UD
 *
 */
@Service("queryRepositoryService")
public class QueryRepositoryService {
	final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * Apply filters and find relevant records
	 *
	 * @param repository - handle to repository
	 * @param clazz - specialized class
	 * @param filters - map of filter name, value pairs
	 * @param sortValues - list of sort filed name and orders
	 * @param pageNumber - starting page number
	 * @param pageSize - count of items
	 * @param since - time since records were created or updated
	 * @param conjunction - AND, OR
	 * @param <T> This is the type parameter
	 * @return page of items
	 * @throws Throwable - generic
	 */
	@Transactional(readOnly = true)
	public <T extends QueryEntity> Page<T> find(
		CustomQueryRepository<T> repository,
		Class<T> clazz, Map<String, String> filters,
		List<SortOrder> sortValues,
		Integer pageNumber, Integer pageSize,
		Long since,
		FilterConjunction conjunction
	) throws Throwable {
		try {
			// Must be done first
			IDHelper.resolveObfuscatedId(filters, sortValues);

			final int localPageNumber = (pageNumber == null) ? 0 : pageNumber;
			final int localPageSize = (pageSize == null) ? 25 : pageSize;
			final List<Specification<T>> specifications = FrameworkHelper.toCriteria(clazz, filters);
			final List<Order> sortOrders = FrameworkHelper.toSortOrders(sortValues);
			final PageRequest pageRequest = PageRequest.of(localPageNumber, localPageSize, Sort.by(sortOrders));

			final Page<T> pages;
			if(specifications != null && specifications.size() > 0) {
				pages = repository.findAll(specifications, pageRequest, conjunction, since);
			} else {
				pages = repository.findAll(pageRequest, since);
			}

			return pages;
		} catch (Throwable t) {
			logger.error("Error fetching paged records", t);
			throw t;
		}
	}
	
	/**
	 * Apply filters and find relevant records
	 *
	 * @param repository - handle to repository
	 * @param clazz - specialized class
	 * @param filters - map of filter name, value pairs
	 * @param sortValues - list of sort filed name and orders
	 * @param pageNumber - starting page number
	 * @param pageSize - count of items
	 * @param conjunction - AND, OR
	 * @param <T> This is the type parameter
	 * @return page of items
	 * @throws Throwable - generic
	 */
	@Transactional(readOnly = true)
	public <T extends QueryEntity> Page<T> find(
		CustomQueryRepository<T> repository,
		Class<T> clazz, Map<String, String> filters,
		List<SortOrder> sortValues,
		Integer pageNumber, Integer pageSize,
		FilterConjunction conjunction
	) throws Throwable {
		return find(repository, clazz, filters, sortValues, pageNumber, pageSize, /* since */null, conjunction);
	}
}
