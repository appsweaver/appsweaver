/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.api;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import org.appsweaver.commons.jpa.models.criteria.SortOrder;
import org.appsweaver.commons.jpa.models.search.FacetMenuItem;
import org.appsweaver.commons.jpa.models.search.TextSearchType;
import org.appsweaver.commons.json.views.SelectedFacet;
import org.appsweaver.commons.models.criteria.SearchableField;
import org.appsweaver.commons.models.persistence.Entity;
import org.appsweaver.commons.spring.utilities.FrameworkHelper;

/**
 * 
 * @author UD
 * 
 */
public interface SearchService {
	/**
	 * Obtain entity specific attributes (fields) that drive fuzzy search
	 *
	 * @param <T> This is the type parameter
	 * @param entityClass - specialization type
	 * @param lookup true if fields only searchable fields must be factored
	 * @return list of fields that is part of search
	 * @throws SQLException - when no search fields are listed
	 */
	default <T extends Entity> Set<SearchableField> getAllSearchableFields(Class<T> entityClass, Optional<Boolean> lookup) throws SQLException {
		return FrameworkHelper.getAllSearchableFields(entityClass, lookup);
	}

	/**
	 * 
	 * @param <T> entity type
	 * @param entityClass entity class
	 * @param textSearchType text search type
	 * @param query user query to execute
	 * @param filterMap set of filters
	 * @param sortOptions list of sort options
	 * @param selectedFacets list of selected facets
	 * @param pageNumber starting page number
	 * @param pageSize number of items to return
	 * @param since time since records were updated or created
	 * @param total place holder to store total value
	 * @return collection of records
	 * @throws SQLException on any errors
	 */
	<T extends Entity> Collection<T> search(
		Class<T> entityClass, 
		TextSearchType textSearchType,
		String query,
		Map<String, String> filterMap,
		List<SortOrder> sortOptions, 
		List<SelectedFacet> selectedFacets,
		Integer pageNumber,
		Integer pageSize,
		Long since,
		AtomicLong total
	) throws SQLException;
	
	/**
	 * 
	 * @param <T> entity type
	 * @param entityClass entity class
	 * @param type text search type
	 * @param query user query to execute
	 * @param filterMap set of filters
	 * @param sortOptions list of sort options
	 * @param selectedFacets list of selected facets
	 * @param pageNumber starting page number
	 * @param pageSize number of items to return
	 * @param total place holder to store total value
	 * @return collection of records
	 * @throws SQLException on any errors
	 */
	<T extends Entity> Collection<T> search(
		Class<T> entityClass, 
		TextSearchType type, String query,
		Map<String, String> filterMap,
		List<SortOrder> sortOptions, 
		List<SelectedFacet> selectedFacets,
		Integer pageNumber, Integer pageSize,
		AtomicLong total
	) throws SQLException;

	/**
	 * Initialize service parameters
	 */
	void initialize();

	/**
	 * List facets
	 * 
	 * @param <T> entity type
	 * @param entityClass specialization type
	 * @param queryText query text
	 * @param filterMap additional filters
	 * @return list of facet as menu items
	 * @throws SQLException when no search fields are listed
	 */
	<T extends Entity> Map<String, List<FacetMenuItem>> getFacets(Class<T> entityClass, String queryText, Map<String, String> filterMap) throws SQLException;
}
