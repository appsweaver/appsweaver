/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.api;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.appsweaver.commons.jpa.repositories.CustomPrimaryRepository;
import org.appsweaver.commons.models.persistence.PrimaryEntity;

/**
 *
 * @author UD
 *
 */
public interface PrimaryDataService<T extends PrimaryEntity> extends DataService<T>{
	/**
	 * Provide a repository instance
	 *
	 * @param <S> entity type
	 * @return repository instance
	 */
	<S extends T> CustomPrimaryRepository<S> getRepository();
	
	/**
	 * Find by id (UUID) from a provided repository
	 *
	 * @param id object id
	 * @param repository repository instance
	 * @return matching object
	 */
	Optional<T> findById(UUID id, CustomPrimaryRepository<T> repository);
	
	/**
	 * Find by id list (resolve numeric and UUID) from a provided repository
	 *
	 * @param ids object ids
	 * @param repository repository instance
	 * @return matching objects
	 */
	List<T> findByIdList(List<UUID> ids, CustomPrimaryRepository<T> repository);
}
