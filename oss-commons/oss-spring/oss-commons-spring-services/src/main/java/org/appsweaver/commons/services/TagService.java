/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.services;

import java.util.Optional;

import org.appsweaver.commons.jpa.models.tag.Tag;
import org.appsweaver.commons.jpa.repositories.CustomPrimaryRepository;
import org.appsweaver.commons.jpa.repositories.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

/**
 * 
 * @author UD
 * 
 */
@Service("tagService")
@ConditionalOnBean(name = "tagRepository")
public class TagService extends AbstractDataService<Tag> {
	@Autowired TagRepository tagRepository;

	@Override
	public CustomPrimaryRepository<Tag> getRepository() {
		return tagRepository;
	}
	
	public Optional<Tag> findByName(String name) {
		return tagRepository.findByName(name);
	}
}
