/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.services;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;

import org.appsweaver.commons.api.DataExporter;
import org.appsweaver.commons.api.EntityWriter;
import org.appsweaver.commons.models.FieldMetadata;
import org.appsweaver.commons.models.persistence.Entity;
import org.appsweaver.commons.spring.utilities.FrameworkHelper;
import org.appsweaver.commons.types.FileType;
import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Randomizer;
import org.appsweaver.commons.utilities.Stringizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * 
 * @author UD
 * 
 */
@Service
public class DefaultDataExporter<T extends Entity> implements DataExporter<T> {
	@Value("${webservice.data-exports-dir:/tmp}") String dataExportsDir;
	
	@Autowired EntityWriter<T> csvEntityWriter;
	
	@Autowired EntityWriter<T> xlsxEntityWriter;
	
	@Autowired EntityWriter<T> tsvEntityWriter;
	
	@Override
	public Map<String, Object> export(
		Class<T> entityClass,
		FileType fileType,
		Stream<T> stream,
		Map<String, String> columnMapping
	) throws Throwable {
		
		final UUID uuid = Randomizer.uuid();
		final String filename = String.format("%s.%s", uuid, String.valueOf(fileType).toLowerCase());
		final String filePath = String.format("%s/%s/%s",
			dataExportsDir,
			Stringizer.upperCamelToLowerCaseUnderscored(entityClass.getSimpleName()),
			filename
		);
		final File file = new File(filePath);
		
		final Map<String, Object> result = Collections.newHashMap();
		result.put("handle", uuid);
		result.put("type", fileType);
		result.put("filename", filename);
		result.put("path", file.getAbsolutePath());
		
		// Create directories as necessary
		file.getParentFile().mkdirs();
		
		// Define mapping
		if(columnMapping.size() == 0) {
			final List<FieldMetadata> properties = FrameworkHelper.getProperties(entityClass);
			properties.forEach(property -> {
				// INFO: COLUMN_NAME, JAVA_PROPERTY_NAME
				final String columnName = Stringizer.lowerCamelToUpperCaseUnderscored(property.name);
				columnMapping.put(columnName, property.name);
			});
		}
		
		switch(fileType) {
			case XLSX: {
				xlsxEntityWriter.write(entityClass, file, stream, columnMapping);
			}
			break;
			
			case CSV: {
				csvEntityWriter.write(entityClass, file, stream, columnMapping);
			}
			break;
			
			case TSV: {
				tsvEntityWriter.write(entityClass, file, stream, columnMapping);
			}
			break;
			
			default: {
				throw new Throwable(String.format("Data export capability not available for file type '%s'", fileType));
			}
		}
		
		return result;
	}
	
}
