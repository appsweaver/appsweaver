/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.services.components;

import static org.appsweaver.commons.SystemConstants.STOMP_MESSAGE_BROKER_MESSAGES_ENDPOINT;

import org.appsweaver.commons.jpa.models.messages.Message;
import org.appsweaver.commons.jpa.models.messages.Recepient;
import org.appsweaver.commons.jpa.models.messages.Sender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author AC
 *
 */
@Component
@ConditionalOnProperty(name="webservice.websocket.messaging.enabled")
public class WebSocketMessagingHelper {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Value("${webservice.websocket.messaging.user.email}")
	String senderEmail;

	@Value("${webservice.websocket.messaging.user.name}")
	String senderFullname;

	@Autowired SimpMessagingTemplate simpMessagingTemplate;

	public void sendMessage(String from, String to, String subject, String body, String topic) {
		Sender<String> sender = new Sender<>();
		sender.setEmail(from);

		Recepient<String> recepient = new Recepient<>();
		recepient.setEmail(to);

		Message<String> message = new Message<>();
		message.setFrom(sender);
		message.setTo(recepient);
		message.setSubject(subject);
		message.setBody(body);
		message.setTopic(topic);

		logger.debug("Sending message {}", message.toString());
		simpMessagingTemplate.convertAndSend(STOMP_MESSAGE_BROKER_MESSAGES_ENDPOINT, message);
	}

	public String getSenderEmail() {
		return senderEmail;
	}

	public void setSenderEmail(String senderEmail) {
		this.senderEmail = senderEmail;
	}

	public String getSenderFullname() {
		return senderFullname;
	}

	public void setSenderFullname(String senderFullname) {
		this.senderFullname = senderFullname;
	}
}
