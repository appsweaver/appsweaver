/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.services;

import java.util.Collection;
import java.util.UUID;

import org.appsweaver.commons.SystemConstants;
import org.appsweaver.commons.api.SecurityService;
import org.appsweaver.commons.models.security.SecurityFacade;
import org.appsweaver.commons.utilities.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

/**
 * 
 * @author UD
 * 
 */
@Service
@ConditionalOnBean(name = "securityFacade")
public class DefaultSecurityService implements SecurityService {
	@Autowired protected SecurityFacade securityFacade;

	@Override
	public String getLoginId() {
		if(securityFacade != null) {
			return securityFacade.getLoginId();
		}
		
		return SystemConstants.ANONYMOUS_USER;
	}
	
	@Override
	public UUID getUserId() {
		if(securityFacade != null) {
			return securityFacade.getUserId();
		}
		
		return null;
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getGrantedAuthorities() {
		if(securityFacade != null) {
			return securityFacade.getAuthorities();
		}
		
		return Collections.newList();
	}
}