# See http://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html
# See https://docs.spring.io/spring-boot/docs/current/reference/html/production-ready-endpoints.html for actuator endpoints

# Application specific configuration
webservice: 
  project-name: tests
  base-package-name: org.appsweaver.apps
  
  server-env-name: local-tests
  server-name: ${HOSTNAME}
  server-port: ${random.int(5000,5100)}
  instance-id: ${random.uuid}
  data-exports-dir: /tmp/${webservice.project-name}/data_exports_dir/${webservice.instance-id}
  work-dir: /tmp/${webservice.project-name}/work_dir/${webservice.instance-id}
  search-index-dir: /tmp/${webservice.project-name}/search/${webservice.instance-id}
  release-notes-file: ${webservice.work-dir}/data/release-notes.json
  
  admin-server-enabled: false
  eureka-enabled: false
  
  search:
    max-clause-count: 4096

  websocket:
    messaging:
      enabled: false
      
      user:
        email: ${NOTIFIER_EMAIL:appsweaver-team@googlegroups.com}
        name: ${NOTIFIER_NAME:Appsweaver Team}
  
  build-information:
    version: ${BUILD_VERSION:BETA}
    number: ${BUILD_NUMBER:vBETA}
    date: ${BUILD_DATE:${random.long(1576859108,1576938327)}}

  repository:
    enabled: true
    clean-on-start: false
    migration-enabled: false
    
    hostname: ${AWE_POSTGRESQL_HOSTNAME:127.0.0.1}
    port: ${AWE_POSTGRESQL_PORT:5432}
    schema: service_test_db
    username: sa
    password: 
    
  query-repository:
    enabled: ${AWE_CQRS_TESTS_ENABLED:false}
    
    hostname: ${AWE_MONGODB_HOSTNAME:127.0.0.1}
    port: ${AWE_MONGODB_PORT:27017}
    schema: ${AWE_CQRS_TESTS_MONGO_SCHEMA:query_service_test_db}
    ################################################################
    # Uncomment the following lines when similar configuraion is 
    # required for operationalizing OGM
    ################################################################
    # auth-schema: ${AWE_CQRS_TESTS_MONGO_AUTH_SCHEMA:admin}
    # username: ${AWE_CQRS_TESTS_MONGO_USERNAME:service_tester}
    # password: ${AWE_CQRS_TESTS_MONGO_PASSWORD:P0we4fu1PhrAsE}

    jpa:
      properties:
        hibernate.transaction.jta.platform: org.hibernate.service.jta.platform.internal.JBossStandAloneJtaPlatform
        hibernate.ogm.datastore.provider: mongodb
        hibernate.ogm.datastore.create_database: true
        hibernate.ogm.datastore.host: ${webservice.query-repository.hostname}
        hibernate.ogm.datastore.port: ${webservice.query-repository.port}
        hibernate.ogm.datastore.database: ${webservice.query-repository.schema}
        ################################################################
        # Uncomment the following lines when similar configuraion is 
        # required for operationalizing OGM
        ################################################################
        # hibernate.ogm.datastore.username: ${webservice.query-repository.username}
        # hibernate.ogm.datastore.password: ${webservice.query-repository.password}
        # hibernate.ogm.mongodb.authentication_database: ${webservice.query-repository.auth-schema}
        hibernate.ogm.mongodb.authentication_mechanism: BEST
        hibernate.ogm.mongodb.connection_timeout: 18000
        hibernate.ogm.mongodb.associations.store: IN_ENTITY
        hibernate.ogm.mongodb.writeconcern: ACKNOWLEDGED
        # Use spring.jpa.properties.* for Hibernate native properties (the prefix is stripped before adding them to the entity manager)
        # The SQL dialect makes Hibernate generate better SQL for the chosen database
        hibernate.search.default.exclusive_index_use: false
        hibernate.search.default.directory_provider: filesystem
        hibernate.search.default.indexBase: ${webservice.search-index-dir}/QUERY
  
  messaging:
    properties:
      host: ${AWE_RABBITMQ_HOSTNAME:127.0.0.1}
      port: 5672
      username: ${AWE_RMQ_USER}
      password: ${AWE_RMQ_PASSWORD}

      acknowledge-mode: CLIENT_ACKNOWLEDGE

      concurrency: 1-1
      recovery-interval: 60000
      receive-timeout: 60000
      mqservice-enabled: false
      
spring:
  application:
    name: ${webservice.project-name}

  flyway:
    enabled: false
    baseline-on-migrate: true
    locations: filesystem:${webservice.config-dir}/repository/migration/h2db
    url: ${spring.datasource.url}
    user: ${spring.datasource.username}
    password: ${spring.datasource.password}
    table: awe_repository_versions
    schemas: ${webservice.repository.schema}
  
  # Required for spring-boot admin
  rabbitmq:
    host: ${webservice.messaging.properties.host}
    port: ${webservice.messaging.properties.port}
    username: ${webservice.messaging.properties.username}
    password: ${webservice.messaging.properties.password}

  datasource:
    url: jdbc:h2:mem:${webservice.repository.schema};MODE=PostgreSQL;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE;INIT=CREATE SCHEMA IF NOT EXISTS ${webservice.repository.schema}
    username: ${webservice.repository.username}
    password: ${webservice.repository.password}
    
    # Hikari properties must be set at this level for customized DataSourceConfig
    connectionTimeout: 20000
    minimumIdle: 5
    maximumPoolSize: 32
    idleTimeout: 300000
    maxLifetime: 1200000
    autoCommit: true
    leakDetectionThreshold: 18000

    # Keep the connection alive if idle for a long time (needed in production)
    dbcp2:
      test-while-idle: true
      validation-query: SELECT 1

  # JPA Properties
  jpa:
    # Use spring.jpa.properties.* for Hibernate native properties (the prefix is stripped before adding them to the entity manager)
    # The SQL dialect makes Hibernate generate better SQL for the chosen database
    properties:
      hibernate.show-sql: true
      hibernate.format-sql: true

      # Hibernate ddl auto (create, create-drop, update, validate)
      hibernate.hbm2ddl.auto: create
      
      hibernate.dialect: org.hibernate.dialect.PostgreSQLDialect
      hibernate.enable_lazy_load_no_trans: true
      hibernate.default_schema: ${webservice.repository.schema}
      hibernate.use-new-id-generator-mappings: true
      hibernate.physical_naming_strategy: ${webservice.base-package-name}.${webservice.project-name}.domain.strategies.MyPhysicalTableNamingStrategy
      
      # Use spring.jpa.properties.* for Hibernate native properties (the prefix is stripped before adding them to the entity manager)
      # The SQL dialect makes Hibernate generate better SQL for the chosen database
      hibernate.search.default.exclusive_index_use: false
      hibernate.search.default.directory_provider: filesystem
      hibernate.search.default.indexBase: ${webservice.search-index-dir}/PRIMARY
        
  boot:
    admin:
      client:
        enabled: ${webservice.admin-server-enabled}
        
        url: https://${webservice.admin-server-name}:${webservice.admin-server-port}
        username: ${webservice.admin-server-username}
        password: ${webservice.admin-server-password}
        
        instance:
          health-url: https://${webservice.server-name}:${webservice.server-port}/${webservice.project-name}/actuator/health
          management-url: https://${webservice.server-name}:${webservice.server-port}/${webservice.project-name}/actuator
          service-url: https://${webservice.server-name}:${webservice.server-port}/${webservice.project-name}/actuator
          management-base-url: https://${webservice.server-name}:${webservice.server-port}/${webservice.project-name}/actuator

          metadata:
            user:
              name: ${webservice.admin-server-username}
              password: ${webservice.admin-server-password}
              
  main:
    banner-mode: console

  mvc:
    dispatch-options-request: true
    servlet:
      path: /

  cloud:
    discovery:
      enabled: false
      
server:
  compression:
    enabled: true
    mime-types:
      - application/json
      - application/xml
      - text/html
      - text/xml
      - text/plain
      - application/javascript
      - text/css

  port: ${webservice.server-port}
  servlet.context-path: /${webservice.project-name}

  ssl:
    enabled: false

eureka:
  instance:
    lease-renewal-interval-in-seconds: 10
    hostname: ${webservice.eureka-server-name}
    secure-port: ${webservice.eureka-server-port}
    secure-port-enabled: true
    
    metadata-map:
      user.name: ${webservice.eureka-server-username}
      user.password: ${webservice.eureka-server-password}
      management.context-path: ${server.servlet.context-path}/actuator
  
  client:
    enabled: ${webservice.eureka-enabled}
    
    registry-fetch-interval-seconds: 5
    register-with-eureka: ${webservice.eureka-enabled}
    fetch-registry: false
    
    service-url:
      defaultZone: https://${webservice.eureka-server-username}:${webservice.eureka-server-password}@${webservice.eureka-server-name}:${webservice.eureka-server-port}/eureka/

management:
  server:
    servlet:
      context-path: ${server.servlet.context-path}
      
  endpoints:
    web:
      exposure:
        include: "*"
        
  endpoint:
    health:
      show-details: ALWAYS

info:
  app:
    name: ${spring.application.name}
    description: ${spring.application.name}
    version: ${webservice.build-information.version}

logging:
  level:
    org.springframework.web: INFO
    org.springframework.ldap: DEBUG
    org.springframework.security: DEBUG
    org.springframework.beans: DEBUG

    org.appsweaver: DEBUG

    org.hibernate: INFO
    # org.hibernate.SQL: DEBUG
    # org.hibernate.type: TRACE
    org.hibernate.tool.hbm2ddl: DEBUG
    
    jdbc: INFO
