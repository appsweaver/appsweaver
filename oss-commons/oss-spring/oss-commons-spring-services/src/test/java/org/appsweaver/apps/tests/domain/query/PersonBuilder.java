/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.tests.domain.query;

import org.appsweaver.apps.tests.domain.Gender;
import org.appsweaver.commons.utilities.Dates;

/**
 * 
 * @author UD
 *
 */
public class PersonBuilder {
	final static String DOB_TEXT_FORMAT="yyyy/MM/dd";

	String name;
	Long dateOfBirth;
	Gender gender;
	Address address;
	Boolean employed;
	
	Long toTimestamp(String dateValue) throws Throwable {
		return Dates.toTime(dateValue, DOB_TEXT_FORMAT);
	}
	public PersonBuilder() {}
	
	public PersonBuilder name(String name) {
		this.name = name;
		return this;
	}
	
	public PersonBuilder dateOfBirth(String dateOfBirth) throws Throwable {
		this.dateOfBirth = toTimestamp(dateOfBirth);
		return this;
	}
	
	public PersonBuilder gender(Gender gender) {
		this.gender = gender;
		return this;
	}
	
	public PersonBuilder employed(Boolean employed) {
		this.employed = employed;
		return this;
	}
	
	public PersonBuilder address(Address address) {
		this.address = address;
		return this;
	}
	
	public Person build() {
		final Person person = new Person();
		
		person.setName(name);
		person.setDateOfBirth(dateOfBirth);
		person.setGender(gender);
		person.setAddress(address);
		person.setEmployed(employed);
		
		return person;
	}
}
