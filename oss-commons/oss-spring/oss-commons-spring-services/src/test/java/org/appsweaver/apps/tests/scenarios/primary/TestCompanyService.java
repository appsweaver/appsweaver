/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.tests.scenarios.primary;

import java.util.List;
import java.util.Optional;

import org.appsweaver.apps.tests.components.primary.PrimaryTestDataProvider;
import org.appsweaver.apps.tests.domain.primary.Company;
import org.appsweaver.apps.tests.scenarios.AbstractPrimaryServiceTest;
import org.appsweaver.apps.tests.services.primary.CompanyService;
import org.appsweaver.commons.models.web.WebRequest;
import org.appsweaver.commons.models.web.WebRequest.WebRequestBuilder;
import org.appsweaver.commons.utilities.Dates;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;

/**
 * 
 * @author UD
 *
 */
public class TestCompanyService extends AbstractPrimaryServiceTest {
	@Autowired(required = false) PrimaryTestDataProvider primaryTestDataProvider;
	
	@Autowired(required = false) CompanyService companyService;
	
	@Override
	public void setUp() {
		super.setUp();
		
		Assume.assumeNotNull(primaryTestDataProvider);
		Assume.assumeNotNull(companyService);
	}
	
	@Test
	public void testCompanyCreatedDates() throws Throwable {
		final List<Company> companies = companyService.findAll();
		
		companies.forEach(company -> {
			final Long createdOn = company.getCreatedOn();
			Assert.assertNotNull(createdOn);
		});
	}
	
	@Test
	public void testCompanyUpdatedDates() throws Throwable {
		final Optional<Company> optionalCompany = companyService.findByName("Merck & Co., Inc.");
		
		Assert.assertTrue(optionalCompany.isPresent());
		
		final Company company = optionalCompany.get();
		
		company.setEstablishedOn(1891);	// Real Date
		
		final Long checkpoint = Dates.toTime();
		
		final Company updated = companyService.save(company);
		
		final Long updatedOn = updated.getUpdatedOn();
		Assert.assertNotNull(updatedOn);
		
		Assert.assertTrue(updated.getUpdatedOn() >= checkpoint);
	}
	
	@Test
	public void testCompanyCount() throws Throwable {
		Assert.assertEquals(primaryTestDataProvider.getCompanies().size(), companyService.count());
	}
	
	@Test
	public void testAllCompanyListingWithSortingInAscendingOrder() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("q", "*");
		webRequestBuilder.sortOrder("establishedOn", Direction.ASC);
		webRequestBuilder.pageSize(100);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Company> pages = companyService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Company> items = pages.getContent();
		final int total = items.size();
		
		items.forEach(item -> {
			System.out.println(item);
		});
		
		Assert.assertEquals(28, total);
		
		Assert.assertTrue(1872 == items.get(0).getEstablishedOn());
		Assert.assertTrue(2005 == items.get(items.size() - 1).getEstablishedOn());
	}
	
	@Test
	public void testAllCompanyListingWithSortingInDescendingOrder() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("q", "*");
		webRequestBuilder.sortOrder("establishedOn", Direction.DESC);
		webRequestBuilder.pageSize(100);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Company> pages = companyService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Company> items = pages.getContent();
		final int total = items.size();
		
		items.forEach(item -> {
			System.out.println(item);
		});
		
		Assert.assertEquals(28, total);
		
		Assert.assertTrue(2005 == items.get(0).getEstablishedOn());
		Assert.assertTrue(1872 == items.get(items.size() - 1).getEstablishedOn());
	}
	
	@Test
	public void testFindAllCompaniesWhoseEstablishedYearEquals2001() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("q", "*");
		webRequestBuilder.filter("establishedOn", "2001");
		webRequestBuilder.sortOrder("establishedOn", Direction.ASC);
		webRequestBuilder.pageSize(100);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Company> pages = companyService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Company> items = pages.getContent();
		final int total = items.size();
		
		items.forEach(item -> {
			System.out.println(item);
		});
		
		Assert.assertEquals(2, total);
		
		Assert.assertEquals("La-Z-Boy Inc.", items.get(0).getName());
		Assert.assertEquals("B & H Ocean Carriers Ltd.", items.get(items.size() - 1).getName());
	}
	
	@Test
	public void testFindCoporationsWhoseEstablishedYearEquals1992() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("q", "*orp*");
		webRequestBuilder.filter("establishedOn", "1992");
		webRequestBuilder.sortOrder("establishedOn", Direction.ASC);
		webRequestBuilder.pageSize(100);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Company> pages = companyService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Company> items = pages.getContent();
		final int total = items.size();
		
		items.forEach(item -> {
			System.out.println(item);
		});
		
		Assert.assertEquals(1, total);
		
		Assert.assertEquals("Kohl's Corp.", items.get(0).getName());
	}
	
	@Test
	public void testFindCompaniesEstablishedYearEquals1995() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("q", "*");
		webRequestBuilder.filter("establishedOn", "1995");
		webRequestBuilder.sortOrder("establishedOn", Direction.DESC);
		webRequestBuilder.pageSize(100);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Company> pages = companyService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Company> items = pages.getContent();
		final int total = items.size();
		
		items.forEach(item -> {
			System.out.println(item);
		});
		
		Assert.assertEquals(1, total);
		
		Assert.assertEquals(
			"Barak I.T.C. (1995) - International Telecommunications Services Corp.", 
			items.get(0).getName()
		);
	}
	
	@Test
	public void testFindCompaniesEstablishedYearNotEquals1995() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("q", "*");
		webRequestBuilder.filter("establishedOn", "!eq(1995)");
		webRequestBuilder.sortOrder("establishedOn", Direction.ASC);
		webRequestBuilder.pageSize(100);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Company> pages = companyService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Company> items = pages.getContent();
		final int total = items.size();
		
		items.forEach(item -> {
			System.out.println(item);
		});
		
		Assert.assertEquals(27, total);
		
		Assert.assertEquals(
			"Johnson & Johnson", 
			items.get(0).getName()
		);
		Assert.assertEquals(
				"Level 3 Communications Inc.", 
				items.get(items.size() - 1).getName()
			);
	}
	
	@Test
	public void testFindCompaniesEstablishedYearIn1995() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("q", "*");
		webRequestBuilder.filter("establishedOn", "in(1995)");
		webRequestBuilder.sortOrder("establishedOn", Direction.DESC);
		webRequestBuilder.pageSize(100);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Company> pages = companyService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Company> items = pages.getContent();
		final int total = items.size();
		
		items.forEach(item -> {
			System.out.println(item);
		});
		
		Assert.assertEquals(1, total);
		
		Assert.assertEquals(
			"Barak I.T.C. (1995) - International Telecommunications Services Corp.", 
			items.get(0).getName()
		);
	}
	
	@Test
	public void testFindCompaniesEstablishedYearIn1995or2000or2001or2003() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("q", "*");
		webRequestBuilder.filter("establishedOn", "in(1995, 2000, 2001, 2003)");
		webRequestBuilder.sortOrder("establishedOn", Direction.DESC);
		webRequestBuilder.pageSize(100);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Company> pages = companyService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Company> items = pages.getContent();
		final int total = items.size();
		
		items.forEach(item -> {
			System.out.println(item);
		});
		
		Assert.assertEquals(5, total);
		
		Assert.assertEquals(
			"L-3 Communications Holdings Inc.", 
			items.get(0).getName()
		);
		Assert.assertEquals(
			"Barak I.T.C. (1995) - International Telecommunications Services Corp.", 
			items.get(items.size() - 1).getName()
		);
	}
	
	@Test
	public void testFindCompaniesEstablishedYearNotIn1995or2000or2001or2003() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("q", "*");
		webRequestBuilder.filter("establishedOn", "!in(1995, 2000, 2001, 2003)");
		webRequestBuilder.sortOrder("establishedOn", Direction.DESC);
		webRequestBuilder.pageSize(100);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Company> pages = companyService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Company> items = pages.getContent();
		final int total = items.size();
		
		items.forEach(item -> {
			System.out.println(item);
		});
		
		Assert.assertEquals(23, total);
		
		Assert.assertEquals(
			"Level 3 Communications Inc.", 
			items.get(0).getName()
		);
		
		Assert.assertEquals(
			"Johnson & Johnson", 
			items.get(items.size() - 1).getName()
		);
	}
	
	@Test
	public void testFindCompaniesEstablishedYearBetween1999And2002() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("q", "*");
		webRequestBuilder.filter("establishedOn", "between(1999, 2002)");
		webRequestBuilder.sortOrder("establishedOn", Direction.ASC);
		webRequestBuilder.pageSize(100);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Company> pages = companyService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Company> items = pages.getContent();
		final int total = items.size();
		
		items.forEach(item -> {
			System.out.println(item);
		});
		
		Assert.assertEquals(4, total);
		
		Assert.assertEquals("Gemstar-TV Guide International Inc.", items.get(0).getName());
		Assert.assertEquals("B & H Ocean Carriers Ltd.", items.get(items.size() - 1).getName());
	}
	
	@Test
	public void testFindCompaniesEstablishedYearNotBetween1999And2002() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("q", "*");
		webRequestBuilder.filter("establishedOn", "!between(1999, 2002)");
		webRequestBuilder.sortOrder("establishedOn", Direction.ASC);
		webRequestBuilder.pageSize(100);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Company> pages = companyService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Company> items = pages.getContent();
		final int total = items.size();
		
		items.forEach(item -> {
			System.out.println(item);
		});
		
		Assert.assertEquals(24, total);
		
		Assert.assertEquals("Johnson & Johnson", items.get(0).getName());
		Assert.assertEquals("Level 3 Communications Inc.", items.get(items.size() - 1).getName());
	}
	
	@Test
	public void testFindCompaniesWithNameLikeCorp() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("q", "*");
		webRequestBuilder.filter("name", "like(corp)");
		webRequestBuilder.sortOrder("establishedOn", Direction.ASC);
		webRequestBuilder.pageSize(100);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Company> pages = companyService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Company> items = pages.getContent();
		final int total = items.size();
		
		items.forEach(item -> {
			System.out.println(item);
		});
		
		Assert.assertEquals(6, total);
		
		Assert.assertEquals("Bausch & Lomb Incorporated", items.get(0).getName());
		Assert.assertEquals("Kerr-McGee Corporation", items.get(items.size() - 1).getName());
	}
	
	@Test
	public void testFindCompaniesWithNameNotLikeInc() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("q", "*");
		webRequestBuilder.filter("name", "!like(inc)");
		webRequestBuilder.sortOrder("establishedOn", Direction.ASC);
		webRequestBuilder.pageSize(100);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Company> pages = companyService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Company> items = pages.getContent();
		final int total = items.size();
		
		items.forEach(item -> {
			System.out.println(item);
		});
		
		Assert.assertEquals(12, total);
		
		Assert.assertEquals("Johnson & Johnson", items.get(0).getName());
		Assert.assertEquals("Kerr-McGee Corporation", items.get(items.size() - 1).getName());
	}
	
	@Test
	public void testFindCompaniesEstablisedAfter1999() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("q", "*");
		webRequestBuilder.filter("establishedOn", "gt(1999)");
		webRequestBuilder.sortOrder("establishedOn", Direction.ASC);
		webRequestBuilder.pageSize(100);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Company> pages = companyService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Company> items = pages.getContent();
		final int total = items.size();
		
		items.forEach(item -> {
			System.out.println(item);
		});
		
		Assert.assertEquals(6, total);
		
		Assert.assertEquals("Banco Totta & Acores S.A.", items.get(0).getName());
		Assert.assertEquals("Level 3 Communications Inc.", items.get(items.size() - 1).getName());
	}
	
	@Test
	public void testFindCompaniesEstablisedOnOrAfter1999() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("q", "*");
		webRequestBuilder.filter("establishedOn", "ge(1999)");
		webRequestBuilder.sortOrder("establishedOn", Direction.ASC);
		webRequestBuilder.pageSize(100);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Company> pages = companyService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Company> items = pages.getContent();
		final int total = items.size();
		
		items.forEach(item -> {
			System.out.println(item);
		});
		
		Assert.assertEquals(7, total);
		
		Assert.assertEquals("Gemstar-TV Guide International Inc.", items.get(0).getName());
		Assert.assertEquals("Level 3 Communications Inc.", items.get(items.size() - 1).getName());
	}
	
	@Test
	public void testFindCompaniesEstablisedOnOrBefore1921() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("q", "*");
		webRequestBuilder.filter("establishedOn", "le(1921)");
		webRequestBuilder.sortOrder("establishedOn", Direction.ASC);
		webRequestBuilder.pageSize(100);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Company> pages = companyService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Company> items = pages.getContent();
		final int total = items.size();
		
		items.forEach(item -> {
			System.out.println(item);
		});
		
		Assert.assertEquals(5, total);
		
		Assert.assertEquals("Johnson & Johnson", items.get(0).getName());
		Assert.assertEquals("McDonald's Corporation", items.get(items.size() - 1).getName());
	}
	
	@Test
	public void testFindCompaniesEstablisedBefore1921() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("q", "*");
		webRequestBuilder.filter("establishedOn", "lt(1921)");
		webRequestBuilder.sortOrder("establishedOn", Direction.ASC);
		webRequestBuilder.pageSize(100);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Company> pages = companyService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Company> items = pages.getContent();
		final int total = items.size();
		
		items.forEach(item -> {
			System.out.println(item);
		});
		
		Assert.assertEquals(4, total);
		
		Assert.assertEquals("Johnson & Johnson", items.get(0).getName());
		Assert.assertEquals("Merck & Co., Inc.", items.get(items.size() - 1).getName());
	}
	
	@Test
	public void testFindIncorporationsEstablisedBefore1945() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("q", "inc");
		webRequestBuilder.filter("establishedOn", "lt(1945)");
		webRequestBuilder.sortOrder("establishedOn", Direction.ASC);
		webRequestBuilder.pageSize(100);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Company> pages = companyService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Company> items = pages.getContent();
		final int total = items.size();
		
		items.forEach(item -> {
			System.out.println(item);
		});
		
		Assert.assertEquals(4, total);
		
		Assert.assertEquals("M.D.C. Holdings Inc.", items.get(0).getName());
		Assert.assertEquals("Albertson's, Inc.", items.get(items.size() - 1).getName());
	}
	
	@Test
	public void testFindCompaniesWithHypenatedNames() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("q", "-");
		webRequestBuilder.sortOrder("establishedOn", Direction.ASC);
		webRequestBuilder.pageSize(100);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Company> pages = companyService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Company> items = pages.getContent();
		final int total = items.size();
		
		items.forEach(item -> {
			System.out.println(item);
		});
		
		Assert.assertEquals(7, total);
		
		Assert.assertEquals("Georgia-Pacific Corporation", items.get(0).getName());
		Assert.assertEquals("Kerr-McGee Corporation", items.get(items.size() - 1).getName());
	}
	
	@Test
	public void testFindCompaniesWithHypenAndSpacesInNames() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("q", " - ");
		webRequestBuilder.sortOrder("establishedOn", Direction.ASC);
		webRequestBuilder.pageSize(100);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Company> pages = companyService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Company> items = pages.getContent();
		final int total = items.size();
		
		items.forEach(item -> {
			System.out.println(item);
		});
		
		Assert.assertEquals(1, total);
		
		Assert.assertEquals("Barak I.T.C. (1995) - International Telecommunications Services Corp.", items.get(0).getName());
	}
	
	@Test
	public void testFindCompaniesWithAmpersandAndSpacesInNames() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("q", " & ");
		webRequestBuilder.sortOrder("establishedOn", Direction.ASC);
		webRequestBuilder.pageSize(100);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Company> pages = companyService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Company> items = pages.getContent();
		final int total = items.size();
		
		items.forEach(item -> {
			System.out.println(item);
		});
		
		Assert.assertEquals(6, total);
		
		Assert.assertEquals("Johnson & Johnson", items.get(0).getName());
		Assert.assertEquals("B & H Ocean Carriers Ltd.", items.get(items.size() - 1).getName());
	}
	
	@Test
	public void testFindCompaniesWithDotAndSpaceInNames() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("q", "C. H");
		webRequestBuilder.sortOrder("establishedOn", Direction.ASC);
		webRequestBuilder.pageSize(100);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Company> pages = companyService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Company> items = pages.getContent();
		final int total = items.size();
		
		items.forEach(item -> {
			System.out.println(item);
		});
		
		Assert.assertEquals(2, total);
		
		Assert.assertEquals("M.D.C. Holdings Inc.", items.get(0).getName());
		Assert.assertEquals("C. H. Robinson Worldwide Inc.", items.get(items.size() - 1).getName());
	}
	
	@Test
	public void testFindCompaniesWithApostropheInNames() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("q", "'s");
		webRequestBuilder.sortOrder("establishedOn", Direction.ASC);
		webRequestBuilder.pageSize(100);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Company> pages = companyService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Company> items = pages.getContent();
		final int total = items.size();
		
		items.forEach(item -> {
			System.out.println(item);
		});
		
		Assert.assertEquals(7, total);
		
		Assert.assertEquals("McDonald's Corporation", items.get(0).getName());
		Assert.assertEquals("Kohl's Corp.", items.get(items.size() - 1).getName());
	}
}
