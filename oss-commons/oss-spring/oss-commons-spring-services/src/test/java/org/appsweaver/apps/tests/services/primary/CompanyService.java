/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.tests.services.primary;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.appsweaver.apps.tests.domain.primary.Company;
import org.appsweaver.apps.tests.repositories.primary.CompanyRepository;
import org.appsweaver.commons.jpa.models.criteria.SortOrder;
import org.appsweaver.commons.jpa.models.search.TextSearchType;
import org.appsweaver.commons.jpa.types.FilterConjunction;
import org.appsweaver.commons.json.views.SelectedFacet;
import org.appsweaver.commons.services.AbstractDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

/**
 * 
 * @author UD
 *
 */
@Service
@ConditionalOnBean(name = "companyRepository")
public class CompanyService extends AbstractDataService<Company> {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired CompanyRepository companyRepository;

	@Override
	public CompanyRepository getRepository() {
		return companyRepository;
	}

	public boolean existsByName(String name) {
		return companyRepository.existsByName(name);
	}

	public Optional<Company> findByName(String name) {
		return companyRepository.findByName(name);
	}

	public Page<Company> find(
		TextSearchType textSearchType, 
		Map<String, String> filterMap, 
		FilterConjunction filterConjunction, 
		List<SortOrder> sortOrders, 
		List<SelectedFacet> selectedFacets,
		Integer pageNumber, Integer pageSize
	) throws Throwable {
		return super.find(
			Company.class,
			textSearchType,
			filterMap,
			filterConjunction,
			sortOrders,
			selectedFacets,
			pageNumber,
			pageSize
		);
	}
	
	public Page<Company> find(
		Map<String, String> filterMap, 
		FilterConjunction filterConjunction, 
		List<SortOrder> sortOrders, 
		List<SelectedFacet> selectedFacets,
		Integer pageNumber, Integer pageSize
	) throws Throwable {
		return find(
			Company.class,
			TextSearchType.WILDCARD,
			filterMap,
			filterConjunction,
			sortOrders,
			selectedFacets,
			pageNumber,
			pageSize
		);
	}
}