/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.tests.scenarios.query;

import org.appsweaver.commons.models.security.SecurityFacade;
import org.appsweaver.commons.services.config.FlywayMigrationConfig;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration;
import org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.FilterType;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;

/**
 * 
 * @author UD
 *
 */
@TestConfiguration
@ComponentScan(basePackages = {
	// Must load first - Begin
	"org.appsweaver.apps.tests.config",
	"org.appsweaver.apps.tests.components.generic",
	"org.appsweaver.apps.tests.components.query",
	// Must load first - End
		
	"org.appsweaver.commons.**.components",
	"org.appsweaver.commons.**.config",
	"org.appsweaver.commons.**.services",
	"org.appsweaver.commons.**.utilities",
	
	// Must load last - Begin
	"org.appsweaver.apps.tests.repositories.query",
	"org.appsweaver.apps.tests.services.query"
	// Must load last - End
},
excludeFilters = {
	@Filter(type = FilterType.ASSIGNABLE_TYPE, value = FlywayMigrationConfig.class),
})
@EnableAutoConfiguration(exclude = {
	HibernateJpaAutoConfiguration.class,
	FlywayAutoConfiguration.class,
	RabbitAutoConfiguration.class,
	SecurityAutoConfiguration.class
})
@EnableConfigurationProperties
@EnableEncryptableProperties
public class QueryServiceTestConfig {
	@MockBean SecurityFacade securityFacade;
}