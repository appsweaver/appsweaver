/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.tests.domain;

import org.appsweaver.commons.utilities.Stringizer;
import org.hibernate.search.bridge.builtin.EnumBridge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author UD
 *
 */
public class GenderFieldBridge extends EnumBridge {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	@SuppressWarnings("rawtypes") // Only called for an Enum
	public Enum<? extends Enum> stringToObject(String stringValue) {
		if (Stringizer.isEmpty(stringValue)) {
			return null;
		}

		return Gender.valueOf(stringValue);
	}
	
	@Override
	public String objectToString(Object object) {
		if(object != null) {
			return String.valueOf(object);
		}
		
		return null;
	}
	
	@Override
	public void setAppliedOnType(Class<?> returnType) {
		super.setAppliedOnType(Gender.class);
	}
}