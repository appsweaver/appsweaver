/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.tests.scenarios.primary;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.appsweaver.apps.tests.components.primary.PrimaryTestDataProvider;
import org.appsweaver.apps.tests.domain.primary.Address;
import org.appsweaver.apps.tests.domain.primary.Person;
import org.appsweaver.apps.tests.scenarios.AbstractPrimaryServiceTest;
import org.appsweaver.apps.tests.services.primary.CompanyService;
import org.appsweaver.apps.tests.services.primary.PersonService;
import org.appsweaver.commons.jpa.models.custom.Value;
import org.appsweaver.commons.jpa.models.search.FacetMenuItem;
import org.appsweaver.commons.jpa.types.FieldType;
import org.appsweaver.commons.json.views.SelectedFacet;
import org.appsweaver.commons.models.web.WebRequest;
import org.appsweaver.commons.models.web.WebRequest.WebRequestBuilder;
import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Dates;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;

/**
 * 
 * @author UD
 *
 */
public class TestPersonService extends AbstractPrimaryServiceTest {
	final static String DOB_TEXT_FORMAT="yyyy/MM/dd";
	
	final static int FACET_MAX_COUNT = 10000;
	
	@Autowired(required = false) PrimaryTestDataProvider primaryTestDataProvider;
	
	@Autowired(required = false) PersonService personService;
	
	@Autowired(required = false) CompanyService companyService;
	
	Date hireDate(String hireDate) throws Throwable {
		return Dates.toDate(hireDate, DOB_TEXT_FORMAT);
	}
	
	@Override
	public void setUp() {
		super.setUp();
		
		Assume.assumeNotNull(primaryTestDataProvider);
		Assume.assumeNotNull(personService);
	}
	
	@Test
	public void testIdConsistency() {
		final List<Person> people = primaryTestDataProvider.getPersons();
		
		Assert.assertEquals("0843ff4e-1dcc-35f7-9ebd-04d73dd91683", people.get(0).getId().toString());
		Assert.assertEquals("aeab2acd-5e41-332b-be1b-471b364c4c0b", people.get(1).getId().toString());
		Assert.assertEquals("07e5f019-4527-3cd9-9138-38bf6fdc97df", people.get(2).getId().toString());
		Assert.assertEquals("28355d39-900c-30b8-b30b-7f9fb8f3a2a0", people.get(3).getId().toString());
		Assert.assertEquals("43bc565e-9b91-3f26-839a-9a4ea652c3ec", people.get(4).getId().toString());
		Assert.assertEquals("282ac5a9-65a2-36e3-8b2c-d0aab8135246", people.get(5).getId().toString());
		Assert.assertEquals("ecc698db-b832-373e-9a7e-c9a4f3de0ffc", people.get(6).getId().toString());
		Assert.assertEquals("716393c8-cfae-3408-ade8-54658c540546", people.get(7).getId().toString());
		Assert.assertEquals("9c0d7f1d-975f-3bf5-b5f0-d477c4be980e", people.get(8).getId().toString());
		Assert.assertEquals("226793b6-ea55-355f-9673-2488b5d3f3a2", people.get(9).getId().toString());
		Assert.assertEquals("1105c691-7e72-3386-8b7e-d166deb395b5", people.get(10).getId().toString());
		
		final List<Address> addresses = primaryTestDataProvider.getAddresses();
		Assert.assertEquals("1bbfc5b4-a1fa-4959-8ccb-e7bb30bcc4d2", addresses.get(0).getId().toString());
		Assert.assertEquals("74355a84-f327-4e70-a000-d7e6599c6889", addresses.get(1).getId().toString());
		Assert.assertEquals("34d0143c-2d3e-4347-95fd-2e13dcfb4253", addresses.get(2).getId().toString());
		Assert.assertEquals("0c9d2885-ea42-45bf-89eb-6560a0c72914", addresses.get(3).getId().toString());
		Assert.assertEquals("dd4df92c-8e75-414b-87a6-4123421d5a0d", addresses.get(4).getId().toString());
		Assert.assertEquals("7548314a-af21-4de2-bb28-a79d4f350f1b", addresses.get(5).getId().toString());
		Assert.assertEquals("ad912a15-5d81-40dd-b89a-cff395ab4145", addresses.get(6).getId().toString());
		Assert.assertEquals("d9917261-a0f7-4757-9219-55d437906974", addresses.get(7).getId().toString());
	}
	
	@Test
	public void testMultipleQueries() throws Throwable {
		Assert.assertEquals(primaryTestDataProvider.getPersons().size(), personService.count());
		
		final Person randomPerson = primaryTestDataProvider.getRandomPerson();
		final Optional<Person> optionalRandomPerson = personService.findByName(randomPerson.getName());
		
		Assert.assertTrue(optionalRandomPerson.isPresent());
		final Person randomPersonByName = optionalRandomPerson.get();
		
		randomPersonByName.set(PrimaryTestDataProvider.CF_DEPARTMENT_CODE, 100L);
		randomPersonByName.set(PrimaryTestDataProvider.CF_BONUS_PERCENTAGE, 15.00);
		randomPersonByName.set(PrimaryTestDataProvider.CF_NOTES, "Welcome to AppsWeaver!");
		randomPersonByName.set(PrimaryTestDataProvider.CF_REVIEW_SUMMARY, "This must go into simpe normal text field", FieldType.TEXT);
		randomPersonByName.set(PrimaryTestDataProvider.CF_HIRE_DATE, hireDate("2018/08/27"));
		randomPersonByName.set(PrimaryTestDataProvider.CF_SALARY, 134909.00);
		randomPersonByName.set(PrimaryTestDataProvider.CF_CAN_TRAVEL, true);
		randomPersonByName.set(PrimaryTestDataProvider.CF_PREFERRED_TRAVEL_LOCATIONS, 
			Collections.newList("AUSTIN, TX, USA", "DUBLIN, IRELAND", "THIS MUST BE IGNORED")
		);
		
		final int customFieldCount = 8;	// Set this to # of fields that got set for asserts

		// Try to set an unregistered field
		randomPersonByName.set("bad_department_code", 1002L);	// Should be ignored
		
		// Should update custom fields and values
		try {
			final Person updatedRandomPersonByName = personService.save(randomPersonByName);
			final Set<Value<?>> values = updatedRandomPersonByName.getValues();
			Assert.assertEquals(customFieldCount, values.size());
			
			values.forEach(value -> {
				switch(value.getFieldName()) {
				case PrimaryTestDataProvider.CF_DEPARTMENT_CODE:
					Assert.assertEquals(FieldType.INTEGER, value.getFieldType());
					Assert.assertEquals(PrimaryTestDataProvider.CF_DEPARTMENT_CODE, value.getFieldName());
					Assert.assertEquals(100L, value.getValue());
					break;
					
				case PrimaryTestDataProvider.CF_BONUS_PERCENTAGE:
					Assert.assertEquals(FieldType.DECIMAL, value.getFieldType());
					Assert.assertEquals(PrimaryTestDataProvider.CF_BONUS_PERCENTAGE, value.getFieldName());
					Assert.assertEquals(15.00, value.getValue());
					break;
					
				case PrimaryTestDataProvider.CF_NOTES:
					Assert.assertEquals(FieldType.TINY_TEXT, value.getFieldType());
					Assert.assertEquals(PrimaryTestDataProvider.CF_NOTES, value.getFieldName());
					Assert.assertEquals("Welcome to AppsWeaver!", value.getValue());
					break;
				
				case PrimaryTestDataProvider.CF_REVIEW_SUMMARY:
					Assert.assertEquals(FieldType.TEXT, value.getFieldType());
					Assert.assertEquals(PrimaryTestDataProvider.CF_REVIEW_SUMMARY, value.getFieldName());
					Assert.assertEquals("This must go into simpe normal text field", value.getValue());
					break;
					
				case PrimaryTestDataProvider.CF_HIRE_DATE:
					try {
						Assert.assertEquals(FieldType.DATE, value.getFieldType());
						Assert.assertEquals(PrimaryTestDataProvider.CF_HIRE_DATE, value.getFieldName());
						
						final long timestamp = Dates.toTime(hireDate("2018/08/27"));
						Assert.assertEquals(timestamp, value.getValue());
					} catch(Throwable t) {
					}
					break;
					
				case PrimaryTestDataProvider.CF_SALARY:
					Assert.assertEquals(FieldType.DECIMAL, value.getFieldType());
					Assert.assertEquals(PrimaryTestDataProvider.CF_SALARY, value.getFieldName());
					Assert.assertEquals(134909.00, value.getValue());
					break;
					
				case PrimaryTestDataProvider.CF_CAN_TRAVEL:
					Assert.assertEquals(FieldType.BOOLEAN, value.getFieldType());
					Assert.assertEquals(PrimaryTestDataProvider.CF_CAN_TRAVEL, value.getFieldName());
					Assert.assertEquals(true, value.getValue());
					break;
					
				case PrimaryTestDataProvider.CF_PREFERRED_TRAVEL_LOCATIONS:
					Assert.assertEquals(FieldType.LIST_OF_VALUES, value.getFieldType());
					Assert.assertEquals(PrimaryTestDataProvider.CF_PREFERRED_TRAVEL_LOCATIONS, value.getFieldName());
					Assert.assertEquals(Collections.newList("AUSTIN, TX, USA", "DUBLIN, IRELAND"), value.getValue());
					break;
				}
			});
		} catch(Throwable t) {
		}
		
		// object equals with lombok does not seem to work right
		Assert.assertEquals(randomPerson.getId(), randomPersonByName.getId());
		
		// Test pagination
		final int pageSize = 4;
		final Page<Person> page1 = personService.findAll(PageRequest.of(0, pageSize));
		Assert.assertEquals(4, page1.getNumberOfElements());
		
		final Page<Person> page2 = personService.findAll(PageRequest.of(1, pageSize));
		Assert.assertEquals(4, page2.getNumberOfElements());
		
		final Page<Person> page3 = personService.findAll(PageRequest.of(2, pageSize));
		Assert.assertEquals(4, page3.getNumberOfElements());
		
		final Page<Person> page4 = personService.findAll(PageRequest.of(3, pageSize));
		Assert.assertEquals(2, page4.getNumberOfElements());
		
		// Test Sorting - ASC
		final Page<Person> ascSortedPage = personService.findAll(PageRequest.of(0, 25, Sort.by(Order.asc("name"))));
		final Person ascPerson1 = ascSortedPage.getContent().get(0);
		final Person ascPersonN = ascSortedPage.getContent().get(ascSortedPage.getNumberOfElements() - 1);
		Assert.assertEquals("Amelia Jackson", ascPerson1.getName());
		Assert.assertEquals("Vers Zurich", ascPersonN.getName());
		
		// Test Sorting - DESC
		final Page<Person> descSortedPage = personService.findAll(PageRequest.of(0, 25, Sort.by(Order.desc("name"))));
		final Person descPerson1 = descSortedPage.getContent().get(0);
		final Person descPersonN = descSortedPage.getContent().get(descSortedPage.getNumberOfElements() - 1);
		Assert.assertEquals("Vers Zurich", descPerson1.getName());
		Assert.assertEquals("Amelia Jackson", descPersonN.getName());
		
		// Test Updates
		final Optional<Person> optionalCullenJames = personService.findByName("Cullen James");
		Assert.assertTrue(optionalCullenJames.isPresent());

		final Person cullenJames = optionalCullenJames.get();
		cullenJames.setName("Raven James");
		final Person ravenJames = personService.update(cullenJames);
		Assert.assertEquals("Raven James", ravenJames.getName());
		
		final Optional<Person> optionalRavenJames = personService.findByName("Raven James");
		Assert.assertTrue(optionalRavenJames.isPresent());
	}
	
	@Test
	public void testQueryWithNullFilter() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("employed", "null()");
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Person> pages = personService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Person> people = pages.getContent();
		final int total = people.size();
		
		people.forEach(person -> {
			System.out.println(person);
		});
		
		Assert.assertEquals(9, total);
	}
	
	@Test
	public void testQueryWithNotNullFilter() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("employed", "!null()");
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Person> pages = personService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Person> people = pages.getContent();
		final int total = people.size();
		
		Assert.assertEquals(5, total);
	}
	
	@Test
	public void testFullTextQueryForLastName() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		
		webRequestBuilder.filter("q", "thomas");
		webRequestBuilder.sortOrder("name", Direction.ASC);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Person> pages = personService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Person> people = pages.getContent();
		final int total = people.size();
		
		people.forEach(person -> {
			System.out.println(person);
		});
		
		Assert.assertEquals(1, total);
	}
	
	@Test
	public void testFullTextQueryForFirstName() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		
		webRequestBuilder.filter("q", "han");
		webRequestBuilder.sortOrder("name", Direction.ASC);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Person> pages = personService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Person> people = pages.getContent();
		final int total = people.size();
		
		people.forEach(person -> {
			System.out.println(person);
		});
		
		Assert.assertEquals(1, total);
	}
	
	@Test
	public void testFullTextQueryWithFilters() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		
		webRequestBuilder.filter("q", "A*");
		webRequestBuilder.filter("gender", "FEMALE");
		webRequestBuilder.sortOrder("name", Direction.ASC);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Person> pages = personService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Person> people = pages.getContent();
		final int total = people.size();
		
		people.forEach(person -> {
			System.out.println(person);
		});
		
		Assert.assertEquals(4, total);
	}
	
	@Test
	public void testFullTextQueryWithNotNullFilter() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		
		webRequestBuilder.filter("q", "A*");
		webRequestBuilder.filter("gender", "FEMALE");
		webRequestBuilder.filter("employed", "!null()");
		webRequestBuilder.sortOrder("name", Direction.ASC);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Person> pages = personService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Person> people = pages.getContent();
		final int total = people.size();
		
		people.forEach(person -> {
			System.out.println(person);
		});
		
		Assert.assertEquals(1, total);
		
		final Person first = pages.getContent().get(0);
		Assert.assertEquals("Amelia Jackson", first.getName());
	}
	
	@Test
	public void testFullTextQueryWithNullFilter() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		
		webRequestBuilder.filter("q", "A*");
		webRequestBuilder.filter("gender", "FEMALE");
		webRequestBuilder.filter("employed", "null()");
		webRequestBuilder.sortOrder("name", Direction.ASC);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Person> pages = personService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Person> people = pages.getContent();
		final int total = people.size();
		
		people.forEach(person -> {
			System.out.println(person);
		});
		
		Assert.assertEquals(3, total);
	}
	
	@Test
	public void testFullTextQueryFacets() throws Throwable {
		final Map<String, String> filterMap = Collections.newHashMap();
		final Map<String, List<FacetMenuItem>> allFacets = personService.getFacets(Person.class, "*", filterMap);
		
		Assert.assertEquals(12, allFacets.size());
		
		// Use Independent filters to test
		filterMap.put("gender", "FEMALE");
		final Map<String, List<FacetMenuItem>> filteredFacets = personService.getFacets(Person.class, "*", filterMap);
		
		final List<FacetMenuItem> genderFacets = filteredFacets.get("gender_facet");
		final FacetMenuItem genderFacet = genderFacets.get(0);
		Assert.assertEquals(6, genderFacet.getCount());
		
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.sortOrder("name", Direction.ASC);
		
		final SelectedFacet selectedGenderFacet = new SelectedFacet();
		selectedGenderFacet.setName("gender_facet");
		selectedGenderFacet.setValues(Collections.newList("FEMALE"));
		
		final SelectedFacet selectedCityFacet = new SelectedFacet();
		selectedCityFacet.setName("city_facet");
		selectedCityFacet.setValues(Collections.newList("San Ramon"));
		
		final WebRequest webRequest = webRequestBuilder.selectedFacet(selectedGenderFacet).selectedFacet(selectedCityFacet).build();
		
		// find using facets
		final Page<Person> pages = personService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		Assert.assertEquals(2, pages.getTotalElements());
	}
	
	@Test
	public void testStringCollectionFacets() throws Throwable {
		
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.sortOrder("name", Direction.ASC);
		
		final Map<String, String> filterMap = Collections.newHashMap();
		final Map<String, List<FacetMenuItem>> filteredFacets = personService.getFacets(Person.class, "*", filterMap);
		final List<FacetMenuItem> colorFacets = filteredFacets.get("favorite_color_facet");
		
		final SelectedFacet selectedColorFacet = new SelectedFacet();
		selectedColorFacet.setName("favorite_color_facet");
		selectedColorFacet.setValues(Collections.newList("RED"));
		
		final WebRequest webRequest = webRequestBuilder.selectedFacet(selectedColorFacet).build();
		
		// find using facets
		final Page<Person> pages = personService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		Assert.assertEquals(1, pages.getTotalElements());
	}
	
	@Test(expected = SQLException.class)
	public void testFullTextQueryWithInvalidOperator() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		
		webRequestBuilder.filter("q", "A*");
		webRequestBuilder.filter("gender", "bring(FEMALE)");
		webRequestBuilder.filter("employed", "!null()");
		webRequestBuilder.sortOrder("name", Direction.ASC);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		personService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
	}
	
	@Test
	public void testQueryWithTagsFilterInOperator() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("tags", "in(Married, Single)");
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Person> pages = personService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Person> people = pages.getContent();
		final int total = people.size();
		
		people.forEach(person -> {
			System.out.println(person);
		});
		
		Assert.assertEquals(8, total);
	}
	
	@Test
	public void testQueryWithTagsFilterEQOperator() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("tags", "Single");
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Person> pages = personService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Person> people = pages.getContent();
		final int total = people.size();
		
		people.forEach(person -> {
			System.out.println(person);
		});
		
		Assert.assertEquals(4, total);
	}
	
	@Test
	public void testFullTextSearchWithTagsFilterEQOperator() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("tags", "Single");
		webRequestBuilder.sortOrder("name", Direction.ASC);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Person> pages = personService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Person> people = pages.getContent();
		final int total = people.size();
		
		people.forEach(person -> {
			System.out.println(person);
		});
		
		Assert.assertEquals(4, total);
	}
	
	@Test
	public void testMaxFacetCount() throws Throwable {
		final Map<String, String> filterMap = Collections.newHashMap();
		List<Person> peopleList = personService.findAll();
		final int numOfClones = FACET_MAX_COUNT - peopleList.size();
		
		final List<Person> clones = primaryTestDataProvider.createHumanClonesForFacet(numOfClones);
		Map<String, List<FacetMenuItem>> allFacets = personService.getFacets(Person.class, "*", filterMap);
		
		final List<FacetMenuItem> nameFacetMenuItems = allFacets.get("name_facet");
		final List<FacetMenuItem> genderFacetMenuItems = allFacets.get("gender_facet");
		
		int totalNameCount = 0;
		int totalGenderCount = 0;
		
		for (FacetMenuItem item: nameFacetMenuItems) {
			totalNameCount += item.getCount();
		}
		
		for (FacetMenuItem item: genderFacetMenuItems) {
			totalGenderCount += item.getCount();
		}
		
		Assert.assertEquals(totalGenderCount, FACET_MAX_COUNT);
		Assert.assertEquals(totalNameCount, FACET_MAX_COUNT);
		
		/*
		 * Add one extra human clone above the max facet count
		 */
		final Person defectClone = primaryTestDataProvider.createHumanCloneForFacet("Clone Defect");
		peopleList = personService.findAll();
		
		allFacets = personService.getFacets(Person.class, "*", filterMap);
		
		totalNameCount = 0;
		totalGenderCount = 0;
		
		for (FacetMenuItem item: nameFacetMenuItems) {
			totalNameCount += item.getCount();
		}
		
		for (FacetMenuItem item: genderFacetMenuItems) {
			totalGenderCount += item.getCount();
		}
		
		Assert.assertEquals(totalGenderCount, FACET_MAX_COUNT);
		Assert.assertEquals(totalNameCount, FACET_MAX_COUNT);
		Assert.assertNotEquals(peopleList.size(), totalGenderCount);
		Assert.assertNotEquals(peopleList.size(), totalNameCount);
		
		/*
		 * Remove clone entities to keep the rest of 
		 * unit tests independent
		 */
		clones.add(defectClone);
		for (Person clone : clones) {
			personService.delete(clone);
		}
	}
	
	@Test
	public void testQueryForCommaIncludedValues() throws Throwable {
		final String urlDecodedValue = "in(Malcom%2C Vender)";
		final String urlRawValue = "in(Malcom, Vender)";
		
		/*
		 * Scenario #1 
		 *  - test on RelationalDatabaseFilter layer
		 * 	- use decoded value
		 */
		WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("name", urlDecodedValue);
		
		WebRequest webRequest = webRequestBuilder.build();
		
		Page<Person> pages = personService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		List<Person> people = pages.getContent();
		int total = people.size();
		
		Assert.assertEquals(1, total);
		
		/*
		 * Scenario #2
		 *  - test on RelationalDatabaseFilter layer
		 * 	- use raw value
		 */
		webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("name", urlRawValue);
		
		webRequest = webRequestBuilder.build();
		
		pages = personService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		people = pages.getContent();
		total = people.size();
		
		Assert.assertEquals(0, total);
		
		/*
		 * Scenario #1 
		 *  - test on LuceneFilter layer
		 * 	- use decoded value
		 */
		final Map<String, String> filterMap = Collections.newHashMap();
		filterMap.put("name", urlDecodedValue);
		Map<String, List<FacetMenuItem>> filteredFacets = personService.getFacets(Person.class, "*", filterMap);
		
		List<FacetMenuItem> ageFacets = filteredFacets.get("age_facet");
		List<FacetMenuItem> genderFacets = filteredFacets.get("gender_facet");
		List<FacetMenuItem> nameFacets = filteredFacets.get("name_facet");
		
		Assert.assertTrue(ageFacets.size() == 1);
		Assert.assertEquals(ageFacets.get(0).getCount(), 1);
		Assert.assertTrue(genderFacets.size() == 1);
		Assert.assertEquals(genderFacets.get(0).getCount(), 1);
		Assert.assertTrue(nameFacets.size() == 1);
		Assert.assertEquals(nameFacets.get(0).getCount(), 1);
		
		/*
		 * Scenario #4 
		 *  - test on LuceneFilter layer
		 * 	- use raw value
		 */
		filterMap.put("name", urlRawValue);
		filteredFacets = personService.getFacets(Person.class, "*", filterMap);
		
		ageFacets = filteredFacets.get("age_facet");
		genderFacets = filteredFacets.get("gender_facet");
		nameFacets = filteredFacets.get("name_facet");
		
		Assert.assertTrue(ageFacets.size() == 0);
		Assert.assertTrue(genderFacets.size() == 0);
		Assert.assertTrue(nameFacets.size() == 0);
	}
}
