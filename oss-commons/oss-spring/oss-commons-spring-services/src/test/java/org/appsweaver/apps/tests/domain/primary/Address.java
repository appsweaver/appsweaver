/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.tests.domain.primary;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.appsweaver.commons.HibernateSearch;
import org.appsweaver.commons.jpa.models.BaseEntity;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.SortableField;
import org.hibernate.search.annotations.Store;

import lombok.EqualsAndHashCode;

/**
 * 
 * @author UD
 *
 */
@Entity
@Indexed(index = "ADDRESSES")
@Table(name = "ADDRESSES")
@EqualsAndHashCode(callSuper = true)
public class Address extends BaseEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8563825589038835857L;

	@Field(store = Store.YES)
	@SortableField
	@Column(name = "STREET", length = 128, nullable = false)
	private String street;
	
	@Field(store = Store.YES, analyzer = @Analyzer(definition = HibernateSearch.KEYWORD_ANALYZER))
	@SortableField
	@Column(name = "CITY", length = 128, nullable = false)
	private String city;
	
	@Field(store = Store.YES, analyzer = @Analyzer(definition = HibernateSearch.KEYWORD_ANALYZER))
	@SortableField
	@Column(name = "STATE", length = 128, nullable = false)
	private String state;
	
	@Field(store = Store.YES, analyzer = @Analyzer(definition = HibernateSearch.KEYWORD_ANALYZER))
	@SortableField
	@Column(name = "COUNTRY", length = 64, nullable = false)
	private String country;
	
	@Field(store = Store.YES, analyzer = @Analyzer(definition = HibernateSearch.KEYWORD_ANALYZER))
	@SortableField
	@Column(name = "ZIPCODE", length = 32, nullable = false)
	private String zipcode;
	
	public Address() {
		super();
	}
	
	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
}
