/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.tests.domain.query;

/**
 * 
 * @author UD
 *
 */
public class AddressBuilder {
	String street;
	String city;
	String state;
	String country;
	String zipcode;
	
	public AddressBuilder() {}
	
	public AddressBuilder street(String street) {
		this.street = street;
		return this;
	}
	
	public AddressBuilder city(String city) {
		this.city = city;
		return this;
	}
	
	public AddressBuilder state(String state) {
		this.state = state;
		return this;
	}
	
	public AddressBuilder country(String country) {
		this.country = country;
		return this;
	}
	
	public AddressBuilder zipcode(String zipcode) {
		this.zipcode = zipcode;
		return this;
	}

	public Address build() {
		final Address address = new Address();
		address.setStreet(street);
		address.setCity(city);
		address.setState(state);
		address.setCountry(country);
		address.setZipcode(zipcode);
		
		return address;
	}
}
