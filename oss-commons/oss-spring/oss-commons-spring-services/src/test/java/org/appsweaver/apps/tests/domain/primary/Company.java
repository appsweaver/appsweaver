/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.tests.domain.primary;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.appsweaver.commons.annotations.Filterable;
import org.appsweaver.commons.annotations.Searchable;
import org.appsweaver.commons.jpa.models.ExtendableBaseEntity;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.SortableField;
import org.hibernate.search.annotations.Store;

import lombok.EqualsAndHashCode;

/**
 * 
 * @author UD
 *
 */
@Entity
@Indexed(index = "COMPANIES")
@Table(name = "COMPANIES")
@EqualsAndHashCode(callSuper = true)
public class Company extends ExtendableBaseEntity {
	private static final long serialVersionUID = 8563825589038835857L;

	@Field(store = Store.YES)
	@Searchable
	@Filterable
	@SortableField
	@Column(name = "NAME", length = 1024, nullable = false)
	private String name;
	
	@Field(store = Store.YES)
	@Searchable
	@SortableField
	@Filterable(valueClass = Integer.class)
	@Column(name = "ESTABLISHED_ON")
	private Integer establishedOn;
	
	public Company() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getEstablishedOn() {
		return establishedOn;
	}

	public void setEstablishedOn(Integer establishedOn) {
		this.establishedOn = establishedOn;
	}

	public String toString() {
		return String.format("[name=%s, establishedOn=%d]", name, establishedOn);
	}
}
