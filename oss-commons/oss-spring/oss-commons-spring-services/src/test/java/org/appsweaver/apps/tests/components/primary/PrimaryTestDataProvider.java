/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.tests.components.primary;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.appsweaver.apps.tests.domain.Color;
import org.appsweaver.apps.tests.domain.Gender;
import org.appsweaver.apps.tests.domain.primary.Address;
import org.appsweaver.apps.tests.domain.primary.AddressBuilder;
import org.appsweaver.apps.tests.domain.primary.Company;
import org.appsweaver.apps.tests.domain.primary.CompanyBuilder;
import org.appsweaver.apps.tests.domain.primary.Person;
import org.appsweaver.apps.tests.domain.primary.PersonBuilder;
import org.appsweaver.apps.tests.repositories.primary.AddressRepository;
import org.appsweaver.apps.tests.repositories.primary.CompanyRepository;
import org.appsweaver.apps.tests.repositories.primary.PersonRepository;
import org.appsweaver.commons.jpa.models.tag.Tag;
import org.appsweaver.commons.jpa.types.FieldType;
import org.appsweaver.commons.services.CustomDataTypeService;
import org.appsweaver.commons.services.TagService;
import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Randomizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.test.context.TestComponent;

/**
 * 
 * @author UD
 *
 */
@TestComponent
@ConditionalOnProperty(prefix = "webservice.repository", name = "enabled", havingValue = "true")
public class PrimaryTestDataProvider {
	final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public static final String CF_DEPARTMENT_CODE = "department_code";
	public static final String CF_BONUS_PERCENTAGE = "bonus_percentage";
	public static final String CF_NOTES = "notes";
	public static final String CF_REVIEW_SUMMARY = "review_summary";
	public static final String CF_HIRE_DATE = "hire_date";
	public static final String CF_SALARY = "salary";
	public static final String CF_CAN_TRAVEL = "can_travel";
	public static final String CF_PREFERRED_TRAVEL_LOCATIONS = "preferred_travel_locations";

	final List<Address> addresses = Collections.newList();
	
	final List<Person> persons = Collections.newList();
	
	final List<Company> companies = Collections.newList();

	@Autowired PersonRepository personRepository;
	
	@Autowired AddressRepository addressRepository;
	
	@Autowired CompanyRepository companyRepository;
	
	@Autowired TagService tagService;
	
	@Autowired CustomDataTypeService customDataTypeService;

	@PostConstruct
	public void initialize() throws Throwable {
		/*
		 * Register few value types
		 */
		customDataTypeService.register(CF_DEPARTMENT_CODE, FieldType.INTEGER);
		customDataTypeService.register(CF_BONUS_PERCENTAGE, FieldType.DECIMAL);
		customDataTypeService.register(CF_NOTES, FieldType.TINY_TEXT);
		customDataTypeService.register(CF_REVIEW_SUMMARY, FieldType.TEXT);
		customDataTypeService.register(CF_HIRE_DATE, FieldType.DATE);
		customDataTypeService.register(CF_SALARY, FieldType.DECIMAL);
		customDataTypeService.register(CF_CAN_TRAVEL, FieldType.BOOLEAN);
		customDataTypeService.register(CF_PREFERRED_TRAVEL_LOCATIONS, FieldType.LIST_OF_VALUES,
			"SF BAY AREA, CA, USA",	// Default Value
			Collections.newList(
				"SF BAY AREA, CA, USA", 
				"AUSTIN, TX, USA", 
				"DUBLIN, IRELAND", 
				"BANGALORE, INDIA"
			)
		);

		// Should be ignored with warning
		customDataTypeService.register(CF_HIRE_DATE, FieldType.TEXT);
		
		/*
		 * Register couple tags
		 */
		final Tag tagMarried = tagService.save(new Tag("Married"));
		final Tag tagSingle = tagService.save(new Tag("Single"));
		
		/*
		 *  Create Addresses
		 *  
		 *  Addresses must have the ID set in this test case.
		 *  This ID value MUST NOT BE over written by the database provider
		 */
		addresses.add(addressRepository.save(new AddressBuilder().uuid("1bbfc5b4-a1fa-4959-8ccb-e7bb30bcc4d2").city("San Jose").state("CA").country("USA").street("1234 O'Brien Dr").zipcode("44908").build()));			// 1
		addresses.add(addressRepository.save(new AddressBuilder().uuid("74355a84-f327-4e70-a000-d7e6599c6889").city("San Ramon").state("CA").country("USA").street("925 Henry St").zipcode("54908").build()));			// 2
		addresses.add(addressRepository.save(new AddressBuilder().uuid("34d0143c-2d3e-4347-95fd-2e13dcfb4253").city("Fremont").state("CA").country("USA").street("480 Ford Way").zipcode("99080").build()));				// 3
		addresses.add(addressRepository.save(new AddressBuilder().uuid("0c9d2885-ea42-45bf-89eb-6560a0c72914").city("Newark").state("CA").country("USA").street("28808 Abington Dr").zipcode("44908").build()));			// 4
		addresses.add(addressRepository.save(new AddressBuilder().uuid("dd4df92c-8e75-414b-87a6-4123421d5a0d").city("Menlo Park").state("CA").country("USA").street("9880 Henderson Dr").zipcode("44908").build()));		// 5
		addresses.add(addressRepository.save(new AddressBuilder().uuid("7548314a-af21-4de2-bb28-a79d4f350f1b").city("Phoenix").state("AZ").country("USA").street("8990 Skywalker Way").zipcode("88900").build()));		// 6
		addresses.add(addressRepository.save(new AddressBuilder().uuid("ad912a15-5d81-40dd-b89a-cff395ab4145").city("Milpitas").state("NJ").country("USA").street("7777 Crazy Crokpot Rd").zipcode("11109").build()));	// 7
		addresses.add(addressRepository.save(new AddressBuilder().uuid("d9917261-a0f7-4757-9219-55d437906974").city("Bangalore").state("Karnataka").country("India").street("Palaya").zipcode("560008").build()));		// 8
		
		// Create Companies
		companies.add(companyRepository.save(new CompanyBuilder().name("Bausch & Lomb Incorporated").establisedOn(1890).build()));
		companies.add(companyRepository.save(new CompanyBuilder().name("Bed Bath & Beyond Inc.").establisedOn(1976).build()));
		companies.add(companyRepository.save(new CompanyBuilder().name("C. H. Robinson Worldwide Inc.").establisedOn(1977).build()));
		companies.add(companyRepository.save(new CompanyBuilder().name("Citigroup, Inc").establisedOn(1989).build()));
		companies.add(companyRepository.save(new CompanyBuilder().name("Gemstar-TV Guide International Inc.").establisedOn(1999).build()));
		companies.add(companyRepository.save(new CompanyBuilder().name("Georgia-Pacific Corporation").establisedOn(1957).build()));
		companies.add(companyRepository.save(new CompanyBuilder().name("Group 1 Automotive Inc.").establisedOn(1978).build()));
		companies.add(companyRepository.save(new CompanyBuilder().name("Johnson & Johnson").establisedOn(1872).build()));
		companies.add(companyRepository.save(new CompanyBuilder().name("Kerr-McGee Corporation").establisedOn(2004).build()));
		companies.add(companyRepository.save(new CompanyBuilder().name("L-3 Communications Holdings Inc.").establisedOn(2003).build()));
		companies.add(companyRepository.save(new CompanyBuilder().name("La-Z-Boy Inc.").establisedOn(2001).build()));
		companies.add(companyRepository.save(new CompanyBuilder().name("Level 3 Communications Inc.").establisedOn(2005).build()));
		companies.add(companyRepository.save(new CompanyBuilder().name("M.D.C. Holdings Inc.").establisedOn(1875).build()));
		companies.add(companyRepository.save(new CompanyBuilder().name("Wendy's International Inc").establisedOn(1975).build()));
		companies.add(companyRepository.save(new CompanyBuilder().name("Albertson's, Inc.").establisedOn(1931).build()));
		companies.add(companyRepository.save(new CompanyBuilder().name("Domino's Pizza LLC").establisedOn(1972).build()));
		companies.add(companyRepository.save(new CompanyBuilder().name("Dreyer's Grand Ice Cream, Inc.").establisedOn(1970).build()));
		companies.add(companyRepository.save(new CompanyBuilder().name("Kohl's Corp.").establisedOn(1992).build()));
		companies.add(companyRepository.save(new CompanyBuilder().name("Lowe's Companies Inc.").establisedOn(1980).build()));
		companies.add(companyRepository.save(new CompanyBuilder().name("McDonald's Corporation").establisedOn(1921).build()));
		companies.add(companyRepository.save(new CompanyBuilder().name("Merck & Co., Inc.").establisedOn(1949).build()));
		companies.add(companyRepository.save(new CompanyBuilder().name("Swift Transportation, Co., Inc").establisedOn(1969).build()));
		companies.add(companyRepository.save(new CompanyBuilder().name("B & H Ocean Carriers Ltd.").establisedOn(2001).build()));
		companies.add(companyRepository.save(new CompanyBuilder().name("Banco Totta & Acores S.A.").establisedOn(2000).build()));
		companies.add(companyRepository.save(new CompanyBuilder().name("Barak I.T.C. (1995) - International Telecommunications Services Corp.").establisedOn(1995).build()));
		companies.add(companyRepository.save(new CompanyBuilder().name("Formula Systems (1985) Ltd.").establisedOn(1985).build()));
		companies.add(companyRepository.save(new CompanyBuilder().name("Chipmos Technologies (Bermuda) Ltd.").establisedOn(1975).build()));
		companies.add(companyRepository.save(new CompanyBuilder().name("Noga Electro-Mechanical Industries (1986) Ltd.").establisedOn(1986).build()));

		/*
		 *  Create Persons
		 *  
		 *  Person's MUST have the ID generated from names.
		 *  This ID value MUST be consistently hashed, and MUST be same each time this test runs
		 */
		persons.add(personRepository.save(new PersonBuilder().name("Emmett Brown").gender(Gender.MALE).dateOfBirth("1988/10/10").address(getAddressAt(0)).tag(tagMarried).build()));				    // 1
		persons.add(personRepository.save(new PersonBuilder().name("Arya Thomas").gender(Gender.FEMALE).dateOfBirth("1977/07/15").address(getAddressAt(1)).tag(tagMarried).build()));					// 2
		persons.add(personRepository.save(new PersonBuilder().name("Ben Kenobi").gender(Gender.MALE).dateOfBirth("1990/05/05").address(getAddressAt(2)).tag(tagMarried).build()));						// 3
		persons.add(personRepository.save(new PersonBuilder().name("Aquila Fowl").gender(Gender.FEMALE).dateOfBirth("2009/08/28").address(getAddressAt(3)).tag(tagMarried).build()));					// 4
		persons.add(personRepository.save(new PersonBuilder().name("Cullen James").gender(Gender.MALE).dateOfBirth("1965/06/23").address(getAddressAt(4)).tag(tagSingle).build()));				   		// 5
		persons.add(personRepository.save(new PersonBuilder().name("Anakin Skywalker").gender(Gender.MALE).dateOfBirth("2010/10/29").address(getAddressAt(5)).tag(tagSingle).build()));			   		// 6
		persons.add(personRepository.save(new PersonBuilder().name("Rowan Ramsey").gender( Gender.FEMALE).dateOfBirth("1998/03/08").address(getAddressAt(6)).tag(tagSingle).build()));					// 7
		persons.add(personRepository.save(new PersonBuilder().name("Aurra Bux").gender(Gender.FEMALE).dateOfBirth("1989/06/03").address(getAddressAt(7)).tag(tagSingle).build()));						// 8
		persons.add(personRepository.save(new PersonBuilder().name("Han Solo").gender(Gender.MALE).dateOfBirth("1995/12/01").address(getAddressAt(0)).build()));					            		// 9
		persons.add(personRepository.save(new PersonBuilder().name("Amelia Jackson").gender(Gender.FEMALE).dateOfBirth("1998/03/08").address(getAddressAt(1)).employed(true).build()));		    		// 10
		// Person without an address
		persons.add(personRepository.save(new PersonBuilder().name("Vers Zurich").gender(Gender.FEMALE).dateOfBirth("1992/04/18").employed(true).build()));							            		// 11
		// Person with comma included name
		persons.add(personRepository.save(new PersonBuilder().name("Malcom, Vender").gender(Gender.MALE).dateOfBirth("1993/04/18").employed(true).build()));							                // 12
		// Person with favorite colors
		persons.add(personRepository.save(new PersonBuilder().name("Nick, Carter").gender(Gender.MALE).dateOfBirth("1993/04/18").employed(true).color(Color.GREEN).color(Color.YELLOW).build()));              // 13
		persons.add(personRepository.save(new PersonBuilder().name("Edward, Casaubon").gender(Gender.MALE).dateOfBirth("1993/04/18").employed(true).color(Color.GREEN).color(Color.RED).build()));              // 14
	}
	
	public List<Person> createHumanClonesForFacet(int numOfPeople) throws Throwable {
		final String baseName = "Clone #";
		final List<Person> humanClones = new ArrayList<Person>();
		for (int i = 0; i < numOfPeople; i++) {
			String uniqueName = baseName + String.valueOf(i);
			humanClones.add((new PersonBuilder().name(uniqueName).gender(Gender.FEMALE).dateOfBirth("2020/07/04").build()));
		}
		return personRepository.saveAll(humanClones);
		
	}
	
	public Person createHumanCloneForFacet(String name) throws Throwable {
		return personRepository.save((new PersonBuilder().name(name).gender(Gender.MALE).dateOfBirth("2020/07/04")).build());
	}

	public Address getRandomAddress() {
		if(addresses.size() > 0) {
			return addresses.get(Randomizer.randomBetween(0, addresses.size() - 1));
		}
		
		return null;
	}
	
	public Address getAddressAt(int index) {
		if(index >= 0 && index < addresses.size()) {
			return addresses.get(index);
		}
		
		return null;
	}
	
	public Person getRandomPerson() {
		if(persons.size() > 0) {
			return persons.get(Randomizer.randomBetween(0, persons.size() - 1));
		}
		
		return null;
	}

	public List<Address> getAddresses() {
		return addresses;
	}
		
	public List<Person> getPersons() {
		return persons;
	}
	
	public List<Company> getCompanies() {
		return companies;
	}
}