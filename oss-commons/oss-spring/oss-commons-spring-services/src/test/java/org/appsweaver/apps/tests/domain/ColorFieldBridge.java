/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.tests.domain;

import java.util.Collection;

import org.apache.lucene.document.Document;
import org.hibernate.search.bridge.FieldBridge;
import org.hibernate.search.bridge.LuceneOptions;
import org.hibernate.search.bridge.StringBridge;

public class ColorFieldBridge implements FieldBridge, StringBridge {
	@Override
	public String objectToString(Object object) {
		if (object == null) return null;
		
		if (!(object instanceof String || object instanceof Color)) {
			throw new IllegalArgumentException(String.format("%s only support Color!", getClass().getSimpleName()));
		}
		return String.valueOf(object);
	}

	@Override
	public void set(String name, Object value, Document document, LuceneOptions luceneOptions) {
		if (value == null) return;
		
		if (!(value instanceof Collection)) {
			throw new IllegalArgumentException(String.format("%s only supports Collection of Enum properties!", getClass().getSimpleName()));
		}
		
		final Collection<?> objects = (Collection<?>) value;
		for (Object object : objects) {
			luceneOptions.addFieldToDocument(name, objectToString(object), document);
		}
	}
}