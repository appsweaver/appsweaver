/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.tests.repositories.query;

import org.appsweaver.apps.tests.domain.query.Address;
import org.appsweaver.commons.jpa.repositories.query.AbstractQueryRepository;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Component;

/**
 * 
 * @author UD
 *
 */
@Component
@ConditionalOnBean(name = "queryHibernateHelper")
public class AddressQueryRepository extends AbstractQueryRepository<Address> {
	@Override
	public Class<Address> getEntityClass() {
		return Address.class;
	}
}