/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.tests.services.query;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.appsweaver.apps.tests.domain.query.Person;
import org.appsweaver.apps.tests.repositories.query.PersonQueryRepository;
import org.appsweaver.commons.api.RecordProcessor;
import org.appsweaver.commons.jpa.models.criteria.SortOrder;
import org.appsweaver.commons.jpa.models.search.TextSearchType;
import org.appsweaver.commons.jpa.repositories.query.CustomQueryRepository;
import org.appsweaver.commons.jpa.types.FilterConjunction;
import org.appsweaver.commons.json.views.SelectedFacet;
import org.appsweaver.commons.services.AbstractQueryDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * 
 * @author UD
 *
 */
@Service
@ConditionalOnBean(name = "personQueryRepository")
public class PersonQueryService extends AbstractQueryDataService<Person> {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired(required = false) PersonQueryRepository personQueryRepository;

	@Override
	public CustomQueryRepository<Person> getRepository() {
		return personQueryRepository;
	}

	public boolean existsByName(String name) throws SQLException {
		return personQueryRepository.existsByName(name);
	}

	public Optional<Person> findByName(String name) throws SQLException {
		return personQueryRepository.findByName(name);
	}

	public Iterable<Person> findAll() {
		return personQueryRepository.findAll();
	}

	public Iterable<Person> findAll(int pageNumber, int pageSize) throws SQLException {
		return personQueryRepository.findAll(PageRequest.of(pageNumber, pageSize));
	}
	
	public Page<Person> findAll(Pageable pageable) throws SQLException {
		return personQueryRepository.findAll(pageable, null);
	}
	
	public Page<Person> find(
		TextSearchType textSearchType, 
		Map<String, String> filterMap, 
		FilterConjunction filterConjunction, 
		List<SortOrder> sortOrders, 
		List<SelectedFacet> selectedFacets,
		Integer pageNumber, Integer pageSize
	) throws Throwable {
		return super.find(
			Person.class,
			textSearchType,
			filterMap,
			filterConjunction,
			sortOrders,
			selectedFacets,
			pageNumber,
			pageSize
		);
	}

	public Page<Person> find(
		Map<String, String> filterMap, 
		FilterConjunction filterConjunction, 
		List<SortOrder> sortOrders, 
		List<SelectedFacet> selectedFacets,
		Integer pageNumber, Integer pageSize
	) throws Throwable {
		return find(
			Person.class,
			TextSearchType.WILDCARD,
			filterMap,
			filterConjunction,
			sortOrders,
			selectedFacets,
			pageNumber,
			pageSize
		);
	}

	@Override
	public void forEachRecord(RecordProcessor<Person> recordProcessor, Pageable pageable) throws SQLException {
		throw new SQLException("Iterating over records is not supported for query services");
	}
}