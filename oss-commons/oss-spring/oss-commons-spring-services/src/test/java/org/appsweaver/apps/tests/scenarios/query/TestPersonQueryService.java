/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.tests.scenarios.query;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.appsweaver.apps.tests.components.query.QueryTestDataProvider;
import org.appsweaver.apps.tests.domain.query.Person;
import org.appsweaver.apps.tests.scenarios.AbstractQueryServiceTest;
import org.appsweaver.apps.tests.services.query.PersonQueryService;
import org.appsweaver.commons.jpa.models.search.FacetMenuItem;
import org.appsweaver.commons.json.views.SelectedFacet;
import org.appsweaver.commons.models.web.WebRequest;
import org.appsweaver.commons.models.web.WebRequest.WebRequestBuilder;
import org.appsweaver.commons.utilities.Collections;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;

/**
 * 
 * @author UD
 *
 */
public class TestPersonQueryService extends AbstractQueryServiceTest {
	@Qualifier("queryTestDataProvider")
	@Autowired(required = false) QueryTestDataProvider queryTestDataProvider;

	@Qualifier("personQueryService")
	@Autowired(required = false) PersonQueryService personQueryService;

	@Test
	public void testAutowiring() throws SQLException {
		if(personQueryService == null || queryTestDataProvider == null) return;
		
		Assert.assertNotNull(queryTestDataProvider);
		Assert.assertNotNull(personQueryService);
	}
	
	@Test
	public void testVariousPersonQueries() throws SQLException {
		if(personQueryService == null || queryTestDataProvider == null) return;
		
		Assert.assertEquals(queryTestDataProvider.getPersons().size(), personQueryService.count());
		
		final Person randomPerson = queryTestDataProvider.getRandomPerson();
		final Optional<Person> optionalRandomPerson = personQueryService.findByName(randomPerson.getName());
		
		Assert.assertTrue(optionalRandomPerson.isPresent());
		final Person randomPersonByName = optionalRandomPerson.get();
		
		// object equals with lombok does not seem to work right
		Assert.assertEquals(randomPerson.getId(), randomPersonByName.getId());
		
		// Test pagination
		final int pageSize = 4;
		final Page<Person> page1 = personQueryService.findAll(PageRequest.of(0, pageSize));
		Assert.assertEquals(4, page1.getNumberOfElements());
		
		final Page<Person> page2 = personQueryService.findAll(PageRequest.of(1, pageSize));
		Assert.assertEquals(4, page2.getNumberOfElements());
		
		final Page<Person> page3 = personQueryService.findAll(PageRequest.of(2, pageSize));
		Assert.assertEquals(3, page3.getNumberOfElements());
		
		final Page<Person> page4 = personQueryService.findAll(PageRequest.of(3, pageSize));
		Assert.assertEquals(0, page4.getNumberOfElements());
		
		// Test Sorting - ASC
		final Page<Person> ascSortedPage = personQueryService.findAll(PageRequest.of(0, 25, Sort.by(Order.asc("name"))));
		final Person ascPerson1 = ascSortedPage.getContent().get(0);
		final Person ascPersonN = ascSortedPage.getContent().get(ascSortedPage.getNumberOfElements() - 1);
		Assert.assertEquals("Amelia Jackson", ascPerson1.getName());
		Assert.assertEquals("Vers Zurich", ascPersonN.getName());
		
		// Test Sorting - DESC
		final Page<Person> descSortedPage = personQueryService.findAll(PageRequest.of(0, 25, Sort.by(Order.desc("name"))));
		final Person descPerson1 = descSortedPage.getContent().get(0);
		final Person descPersonN = descSortedPage.getContent().get(descSortedPage.getNumberOfElements() - 1);
		Assert.assertEquals("Vers Zurich", descPerson1.getName());
		Assert.assertEquals("Amelia Jackson", descPersonN.getName());
		
		// Test Updates
		final Optional<Person> optionalCullenJames = personQueryService.findByName("Cullen James");
		Assert.assertTrue(optionalCullenJames.isPresent());

		final Person cullenJames = optionalCullenJames.get();
		cullenJames.setName("Raven James");
		final Person ravenJames = personQueryService.update(cullenJames);
		Assert.assertEquals("Raven James", ravenJames.getName());
		
		final Optional<Person> optionalRavenJames = personQueryService.findByName("Raven James");
		Assert.assertTrue(optionalRavenJames.isPresent());
	}
	
	@Test
	public void testPersonQueryWithNullFilter() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("employed", "null()");
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Person> persons = personQueryService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Person> personList = persons.getContent();
		final int totalPersons = personList.size();
		
		personList.forEach(person -> {
			System.out.println(person);
		});
		
		Assert.assertEquals(9, totalPersons);
	}
	
	@Test
	public void testPersonQueryWithNonExistantName() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("name", "Arnold");
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Person> persons = personQueryService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Person> personList = persons.getContent();
		final int totalPersons = personList.size();
		
		personList.forEach(person -> {
			System.out.println(person);
		});
		
		Assert.assertEquals(0, totalPersons);
	}
	
	@Test
	public void testPersonQueryWithNotNullFilter() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.filter("employed", "!null()");
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Person> persons = personQueryService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Person> personList = persons.getContent();
		final int totalPersons = personList.size();
		
		Assert.assertEquals(2, totalPersons);
	}
	
	@Test
	public void testPersonFullTextQueryWithFilters() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		
		webRequestBuilder.filter("q", "A*");
		webRequestBuilder.filter("gender", "FEMALE");
		webRequestBuilder.sortOrder("name", Direction.ASC);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Person> persons = personQueryService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Person> personList = persons.getContent();
		final int totalPersons = personList.size();
		
		personList.forEach(person -> {
			System.out.println(person);
		});
		
		Assert.assertEquals(4, totalPersons);
	}
	
	@Test
	public void testPersonFullTextQueryWithNotNullFilter() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		
		webRequestBuilder.filter("q", "A*");
		webRequestBuilder.filter("gender", "FEMALE");
		webRequestBuilder.filter("employed", "!null()");
		webRequestBuilder.sortOrder("name", Direction.ASC);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Person> persons = personQueryService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Person> personList = persons.getContent();
		final int totalPersons = personList.size();
		
		personList.forEach(person -> {
			System.out.println(person);
		});
		
		Assert.assertEquals(1, totalPersons);
		
		final Person first = persons.getContent().get(0);
		Assert.assertEquals("Amelia Jackson", first.getName());
	}
	
	@Test
	public void testPersonFullTextQueryWithNullFilter() throws Throwable {
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		
		webRequestBuilder.filter("q", "A*");
		webRequestBuilder.filter("gender", "FEMALE");
		webRequestBuilder.filter("employed", "null()");
		webRequestBuilder.sortOrder("name", Direction.ASC);
		
		final WebRequest webRequest = webRequestBuilder.build();
		
		final Page<Person> persons = personQueryService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		final List<Person> personList = persons.getContent();
		final int totalPersons = personList.size();
		
		personList.forEach(person -> {
			System.out.println(person);
		});
		
		Assert.assertEquals(3, totalPersons);
	}
	
	@Test
	public void testPersonQueryFactes() throws Throwable {
		final Map<String, String> filterMap = Collections.newHashMap();
		final Map<String, List<FacetMenuItem>> allFacets = personQueryService.getFacets(Person.class, "*", filterMap);
		
		Assert.assertEquals(5, allFacets.size());
		
		// Use Independent filters to test
		filterMap.put("gender", "FEMALE");
		final Map<String, List<FacetMenuItem>> filteredFacets = personQueryService.getFacets(Person.class, "*", filterMap);
		
		final List<FacetMenuItem> genderFacets = filteredFacets.get("gender_facet");
		final FacetMenuItem genderFacet = genderFacets.get(0);
		Assert.assertEquals(6, genderFacet.getCount());
		
		final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
		webRequestBuilder.sortOrder("name", Direction.ASC);
		
		final SelectedFacet selectedGenderFacet = new SelectedFacet();
		selectedGenderFacet.setName("gender_facet");
		selectedGenderFacet.setValues(Collections.newList("FEMALE"));
		
		final SelectedFacet selectedCityFacet = new SelectedFacet();
		selectedCityFacet.setName("city_facet");
		selectedCityFacet.setValues(Collections.newList("San Ramon"));
		
		final WebRequest webRequest = webRequestBuilder.selectedFacet(selectedGenderFacet).selectedFacet(selectedCityFacet).build();
		
		// find using facets
		final Page<Person> persons = personQueryService.find(
			webRequest.getFilterMap(), 
			webRequest.getFilterConjunction(), 
			webRequest.getSortOrders(), 
			webRequest.getSelectedFacets(), 
			webRequest.getPageNumber(),
			webRequest.getPageSize()
		);
		
		Assert.assertEquals(2, persons.getTotalElements());
	}
}
