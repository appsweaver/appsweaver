/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.tests.domain.primary;

/**
 * 
 * @author UD
 *
 */
public class CompanyBuilder {
	String name;
	Integer establisedOn;
	
	public CompanyBuilder() {}
	
	public CompanyBuilder name(String name) {
		this.name = name;
		return this;
	}
	
	public CompanyBuilder establisedOn(Integer establisedOn) {
		this.establisedOn = establisedOn;
		return this;
	}
	
	public Company build() {
		final Company company = new Company();
		
		company.setName(name);
		company.setEstablishedOn(establisedOn);
		
		return company;
	}
}
