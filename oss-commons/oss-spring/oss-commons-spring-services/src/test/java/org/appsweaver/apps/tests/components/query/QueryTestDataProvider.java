/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.tests.components.query;

import java.util.List;

import javax.annotation.PostConstruct;

import org.appsweaver.apps.tests.domain.Gender;
import org.appsweaver.apps.tests.domain.query.Address;
import org.appsweaver.apps.tests.domain.query.AddressBuilder;
import org.appsweaver.apps.tests.domain.query.Person;
import org.appsweaver.apps.tests.domain.query.PersonBuilder;
import org.appsweaver.apps.tests.repositories.query.AddressQueryRepository;
import org.appsweaver.apps.tests.repositories.query.PersonQueryRepository;
import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Randomizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.test.context.TestComponent;

/**
 * 
 * @author UD
 *
 */
@TestComponent("queryTestDataProvider")
@ConditionalOnBean(
	name = { 
		"addressQueryRepository",
		"personQueryRepository"
	}
)
public class QueryTestDataProvider {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	final List<Address> addresses = Collections.newList();
	
	final List<Person> persons = Collections.newList();

	@Autowired(required = false) AddressQueryRepository addressQueryRepository;

	@Autowired(required = false) PersonQueryRepository personQueryRepository;

	@PostConstruct
	public void initialize() throws Throwable {
		if(addressQueryRepository == null || personQueryRepository == null) return;
		
		// Delete objects if they exist
		personQueryRepository.deleteAll();
		addressQueryRepository.deleteAll();
		
		// Create Addresses
		addresses.add(addressQueryRepository.save(new AddressBuilder().city("San Jose").state("CA").country("USA").street("1234 O'Brien Dr").zipcode("44908").build()));			// 1
		addresses.add(addressQueryRepository.save(new AddressBuilder().city("San Ramon").state("CA").country("USA").street("925 Henry St").zipcode("54908").build()));				// 2
		addresses.add(addressQueryRepository.save(new AddressBuilder().city("Fremont").state("CA").country("USA").street("480 Ford Way").zipcode("99080").build()));				// 3
		addresses.add(addressQueryRepository.save(new AddressBuilder().city("Newark").state("CA").country("USA").street("28808 Abington Dr").zipcode("44908").build()));			// 4
		addresses.add(addressQueryRepository.save(new AddressBuilder().city("Menlo Park").state("CA").country("USA").street("9880 Henderson Dr").zipcode("44908").build()));		// 5
		addresses.add(addressQueryRepository.save(new AddressBuilder().city("Phoenix").state("AZ").country("USA").street("8990 Skywalker Way").zipcode("88900").build()));			// 6
		addresses.add(addressQueryRepository.save(new AddressBuilder().city("Milpitas").state("NJ").country("USA").street("7777 Crazy Crokpot Rd").zipcode("11109").build()));		// 7
		addresses.add(addressQueryRepository.save(new AddressBuilder().city("Bangalore").state("Karnataka").country("India").street("Palaya").zipcode("560008").build()));			// 8
		
		// Create Persons
		persons.add(personQueryRepository.save(new PersonBuilder().name("Emmett Brown").gender(Gender.MALE).dateOfBirth("1988/10/10").address(getAddressAt(0)).build()));							// 1
		persons.add(personQueryRepository.save(new PersonBuilder().name("Arya Thomas").gender(Gender.FEMALE).dateOfBirth("1977/07/15").address(getAddressAt(1)).build()));							// 2
		persons.add(personQueryRepository.save(new PersonBuilder().name("Ben Kenobi").gender(Gender.MALE).dateOfBirth("1990/05/05").address(getAddressAt(2)).build()));								// 3
		persons.add(personQueryRepository.save(new PersonBuilder().name("Aquila Fowl").gender(Gender.FEMALE).dateOfBirth("2009/08/28").address(getAddressAt(3)).build()));							// 4
		persons.add(personQueryRepository.save(new PersonBuilder().name("Cullen James").gender(Gender.MALE).dateOfBirth("1965/06/23").address(getAddressAt(4)).build()));							// 5
		persons.add(personQueryRepository.save(new PersonBuilder().name("Anakin Skywalker").gender(Gender.MALE).dateOfBirth("2010/10/29").address(getAddressAt(5)).build()));						// 6
		persons.add(personQueryRepository.save(new PersonBuilder().name("Rowan Ramsey").gender( Gender.FEMALE).dateOfBirth("1998/03/08").address(getAddressAt(6)).build()));						// 7
		persons.add(personQueryRepository.save(new PersonBuilder().name("Aurra Bux").gender(Gender.FEMALE).dateOfBirth("1989/06/03").address(getAddressAt(7)).build()));							// 8
		persons.add(personQueryRepository.save(new PersonBuilder().name("Han Solo").gender(Gender.MALE).dateOfBirth("1995/12/01").address(getAddressAt(0)).build()));								// 9
		persons.add(personQueryRepository.save(new PersonBuilder().name("Amelia Jackson").gender(Gender.FEMALE).dateOfBirth("1998/03/08").address(getAddressAt(1)).employed(true).build()));		// 10
		// Person without an address
		persons.add(personQueryRepository.save(new PersonBuilder().name("Vers Zurich").gender(Gender.FEMALE).dateOfBirth("1992/04/18").employed(true).build()));									// 11

	}


	public Address getRandomAddress() {
		if(addresses.size() > 0) {
			return addresses.get(Randomizer.randomBetween(0, addresses.size() - 1));
		}
		
		return null;
	}
	
	public Address getAddressAt(int index) {
		if(index >= 0 && index < addresses.size()) {
			return addresses.get(index);
		}
		
		return null;
	}
	
	public Person getRandomPerson() {
		if(persons.size() > 0) {
			return persons.get(Randomizer.randomBetween(0, persons.size() - 1));
		}
		
		return null;
	}

	public List<Address> getAddresses() {
		return addresses;
	}
		
	public List<Person> getPersons() {
		return persons;
	}
}