/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.tests.domain.primary;

import java.util.Calendar;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.appsweaver.apps.tests.domain.Color;
import org.appsweaver.apps.tests.domain.ColorFieldBridge;
import org.appsweaver.apps.tests.domain.Gender;
import org.appsweaver.apps.tests.domain.GenderFieldBridge;
import org.appsweaver.commons.HibernateSearch;
import org.appsweaver.commons.annotations.Filterable;
import org.appsweaver.commons.annotations.Filterables;
import org.appsweaver.commons.annotations.IdGenerationPolicy;
import org.appsweaver.commons.annotations.Searchable;
import org.appsweaver.commons.jpa.models.ExtendableBaseEntity;
import org.appsweaver.commons.utilities.Collections;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Facet;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.FieldBridge;
import org.hibernate.search.annotations.Fields;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.hibernate.search.annotations.SortableField;
import org.hibernate.search.annotations.Store;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.EqualsAndHashCode;

/**
 * 
 * @author UD
 *
 */
@Entity
@Indexed(index = "PERSONS")
@Table(name = "PERSONS")
@EqualsAndHashCode(callSuper = true)
@IdGenerationPolicy("name")
public class Person extends ExtendableBaseEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8563825589038835857L;

	// Facet Names
	private static final String AGE_FACET = "age_facet";
	private static final String GENDER_FACET = "gender_facet";
	private static final String CITY_FACET = "city_facet";
	private static final String STATE_FACET = "state_facet";
	private static final String COUNTRY_FACET = "country_facet";
	private static final String NAME_FACET = "name_facet";
	private static final String FAVORITE_COLOR_FACET = "favorite_color_facet";
	// Facet Names
	
	@Searchable
	@Filterable
	@SortableField
	@Facet(forField = NAME_FACET)
	@Fields(value = { 
			@Field(store = Store.YES), 
			@Field(name = NAME_FACET, analyze = Analyze.NO) 
			})
	@Column(name = "NAME", length = 512, nullable = false)
	private String name;
	
	@Filterable(valueClass = Long.class)
	@Column(name = "DATE_OF_BIRTH", nullable = false)
	protected Long dateOfBirth;
	
	@Filterable(valueClass = Boolean.class)
	@Field(store = Store.YES,analyze = Analyze.NO, indexNullAs = HibernateSearch.NULL_VALUE)
	@Searchable
	@Column(name = "IS_EMPLOYED")
	protected Boolean employed = null;
	
	@Field(store = Store.YES, analyzer = @Analyzer(definition = HibernateSearch.KEYWORD_ANALYZER), bridge = @FieldBridge(impl = GenderFieldBridge.class))
	@Filterable(valueClass = Gender.class)
	@SortableField
	@Column(name = "GENDER", length = 8, nullable = false)
	@Enumerated(EnumType.STRING)
	private Gender gender;
	
	@ManyToOne
	@Filterables(value = {
		@Filterable(name = "city", path = "address.city"),
		@Filterable(name = "state", path = "address.state"),
		@Filterable(name = "country", path = "address.country")
	})
	@Searchable
	@IndexedEmbedded(includePaths = {
		"city", "state", "country", "zipcode"
	})
	private Address address;
	
	@Field(store = Store.YES, bridge = @FieldBridge(impl = ColorFieldBridge.class), analyze = Analyze.NO)
	@ElementCollection
	protected Set<Color> favoriteColors;
	
	@JsonIgnore
	@Searchable(lookup = false)
	@Facet(forField = FAVORITE_COLOR_FACET)
	@Field(name = FAVORITE_COLOR_FACET, analyze = Analyze.NO, bridge = @FieldBridge(impl = ColorFieldBridge.class))
	public Set<String> getFavoriteColorNames() {
		final Set<String> favoriteColorsList = Collections.newHashSet();
		
		if(favoriteColors != null && favoriteColors.size() > 0) {
			favoriteColors.forEach(color -> {
				favoriteColorsList.add(color.toString());
			});
		}
		
		return favoriteColorsList;
	}
	
	@JsonIgnore
	@Searchable(lookup = false)
	@Facet(forField = GENDER_FACET)
	@Field(store = Store.YES, name = GENDER_FACET, analyze = Analyze.NO)
	public String getGenderName() {
		return (gender != null) ? gender.name() : null;
	}
	
	@JsonIgnore
	@Searchable(lookup = false)
	@Facet(forField = STATE_FACET)
	@Field(store = Store.YES, name = STATE_FACET, analyze = Analyze.NO)
	public String getState() {
		return (address != null) ? address.getState() : null;
	}
	
	@JsonIgnore
	@Searchable(lookup = false)
	@Facet(forField = CITY_FACET)
	@Field(name = CITY_FACET, analyze = Analyze.NO)
	public String getCity() {
		return (address != null) ? address.getCity() : null;
	}
	
	@JsonIgnore
	@Searchable(lookup = false)
	@Facet(forField = COUNTRY_FACET)
	@Field(store = Store.YES, name = COUNTRY_FACET, analyze = Analyze.NO)
	public String getCountry() {
		return (address != null) ? address.getCountry() : null;
	}
	
	@JsonIgnore
	@Searchable(lookup = false)
	@Facet(forField = AGE_FACET)
	@Field(store = Store.YES, name = AGE_FACET, analyze = Analyze.NO)
	public String getAge() {	
		final Calendar calendar = Calendar.getInstance();
		final int currentYear = calendar.get(Calendar.YEAR);
		
		calendar.setTimeInMillis(dateOfBirth);
		final int birthYear = calendar.get(Calendar.YEAR);
		
		return String.valueOf(currentYear - birthYear);
	}
	
	public Person() {
		super();
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Long getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Long dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	public Boolean getEmployed() {
		return employed;
	}

	public void setEmployed(Boolean employed) {
		this.employed = employed;
	}

	public void setFavoriteColors(Set<Color> favoriteColors) {
		this.favoriteColors = favoriteColors;
	}
	
	public Set<Color> getFavoriteColors() {
		return this.favoriteColors;
	}

	public String toString() {
		return String.format("[name=%s, gender=%s, employed=%s]", name, gender, (employed != null) ? employed : "N/A");
	}
}
