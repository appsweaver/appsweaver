/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.models.web.exceptions;

/**
 * 
 * @author AC
 *
 * Exception class for the handling the Authentication Exception
 * 
 */
public class AuthenticationException extends RuntimeException {
	private static final long serialVersionUID = 6840005005529407962L;

	private String reason;

	public AuthenticationException(String reason) {
		this.setReason(reason);
	}
	
	public AuthenticationException(Throwable cause) {
		super(cause);
	}

	public AuthenticationException(String message, String reason) {
		super(message);
		this.setReason(reason);
	}

	public AuthenticationException(String message, Throwable cause, String reason) {
		super(message, cause);
		this.setReason(reason);
	}

	public AuthenticationException(Throwable cause, String reason) {
		super(cause);
		this.setReason(reason);
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
}
