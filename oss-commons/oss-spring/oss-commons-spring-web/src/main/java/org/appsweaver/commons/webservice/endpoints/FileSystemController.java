/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.webservice.endpoints;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.appsweaver.commons.annotations.Loggable;
import org.appsweaver.commons.models.web.ApplicationErrorCode;
import org.appsweaver.commons.models.web.WebResponse;
import org.appsweaver.commons.models.web.exceptions.ApplicationException;
import org.appsweaver.commons.utilities.Fileinator;
import org.appsweaver.commons.utilities.PagedFileReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 
 * @author UD
 * 
 */
@Loggable
@RestController
@RequestMapping(value = "/api/v1/filesystem", produces = { MediaType.APPLICATION_JSON_VALUE })
@Api(value = "File system controler")
public class FileSystemController {
	final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@GetMapping(value = "/read")
	@ApiOperation(value = "Stream file content")
	public @ResponseBody WebResponse<Page<String>> read(
			@ApiParam @RequestParam(value = "path") String path,
			@ApiParam @RequestParam(value = "start", required = false, defaultValue = "1") Integer start,
			@ApiParam @RequestParam(value = "count", required = false, defaultValue = "25") Integer count,
			@ApiParam @RequestParam(value = "numbered", required = false, defaultValue = "true") boolean numbered
	) {
		try {
			final PagedFileReader pagedFileReader = new PagedFileReader(path, numbered);
			if(start <= pagedFileReader.getSize()) {
				pagedFileReader.readLines(start, count);
			}
			
			final List<String> lines = pagedFileReader.getLines();
			final Long total = pagedFileReader.getSize();
			return WebResponse.newWebResponse(lines, start, count, total, false);
		} catch(Throwable t) {
			logger.error("Reading file content failed!", t);
			throw new ApplicationException("Read File", HttpStatus.INTERNAL_SERVER_ERROR, ApplicationErrorCode.FAILED_TO_READ_LINES_FROM_FILE);
		}
	}
	
	@GetMapping("/download")
	@ApiOperation(value = "Download a file")
	public ResponseEntity<Resource> download(
		@ApiParam @RequestParam(value = "file") String file, HttpServletRequest request
	) {
		try {
			// Load file as Resource
			final Resource resource = Fileinator.toResource(file);

			String contentType;
			try {
				// Try to determine file's content type
				contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
			} catch (Throwable t) {
				contentType = MediaType.APPLICATION_OCTET_STREAM_VALUE;
			}
			
			return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=\"%s\"", resource.getFilename()))
				.body(resource);
			
		} catch (Throwable t) {
			logger.error("File download failed!", t);
			throw new ApplicationException("Download File", HttpStatus.INTERNAL_SERVER_ERROR, ApplicationErrorCode.FAILED_TO_DOWNLOAD_FILE);
		}
	}
}
