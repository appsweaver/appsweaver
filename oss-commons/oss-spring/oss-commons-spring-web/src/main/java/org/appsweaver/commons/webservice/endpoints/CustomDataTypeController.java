/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.webservice.endpoints;

import org.appsweaver.commons.api.PrimaryDataService;
import org.appsweaver.commons.jpa.models.custom.CustomDataType;
import org.appsweaver.commons.services.CustomDataTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;

/**
 * 
 * @author UD
 * 
 */
@RestController
@ConditionalOnBean(name = "customDataTypeService")
@RequestMapping(value = "/api/v1/custom-data-types", produces = { MediaType.APPLICATION_JSON_VALUE })
@Api(value = "Custom Data Type Controller")
public class CustomDataTypeController extends AbstractPrimaryController<CustomDataType> {
	@Autowired CustomDataTypeService customDataTypeService;

	@Override
	public Class<CustomDataType> getEntityClass() {
		return CustomDataType.class;
	}

	@Override
	public PrimaryDataService<CustomDataType> getDataService() {
		return customDataTypeService;
	}
}