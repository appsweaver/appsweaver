/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.models.web;

import java.util.Arrays;
import java.util.Collection;

import org.appsweaver.commons.jpa.helpers.PageableHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * 
 * @author UD
 * 
 * Web response wrapper to produce consistent REST responses
 *
 */
public class WebResponse<T> extends ResponseEntity<T> {
	static final Logger logger = LoggerFactory.getLogger(WebResponse.class);
	
	private WebResponse(HttpStatus status) {
		super(status);
	}
	
	private WebResponse(T data, HttpStatus httpStatus) {
		super(data, httpStatus);
	}
	
	public static <T> WebResponse<T> newWebResponse(HttpStatus httpStatus) {
		return new WebResponse<>(httpStatus);
	}
	
	public static <T> WebResponse<T> newWebResponse(T data, HttpStatus httpStatus) {
		return new WebResponse<>(data, httpStatus);
	}
	
	public static <T> WebResponse<T> newWebResponse(T data) {
		return newWebResponse(data, HttpStatus.OK);
	}
	
	public static <T> WebResponse<Page<T>> newWebResponse(Collection<T> data, Integer start, Integer size, Long total) {
		return newWebResponse(data, start, size, total, true);
	}
	
	public static <T> WebResponse<Page<T>> newWebResponse(T[] data, Integer start, Integer size, Long total) {
		return newWebResponse(Arrays.asList(data), start, size, total, true);
	}
	
	public static <T> WebResponse<Page<T>> newWebResponse(Collection<T> data, Integer start, Integer size, Long total, boolean makeSubList) {
		final Page<T> page = PageableHelper.toPage(data, start, size, total, makeSubList);
		final WebResponse<Page<T>> webResponse = new WebResponse<>(page, HttpStatus.OK);
		return webResponse;
	}
}
