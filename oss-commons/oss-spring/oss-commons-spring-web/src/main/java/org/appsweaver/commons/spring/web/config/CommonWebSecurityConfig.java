/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.spring.web.config;

import static org.appsweaver.commons.SystemConstants.API_VERSIONED_PATH_WILD;
import static org.appsweaver.commons.SystemConstants.AUTHENTICATION_ENDPOINT;
import static org.appsweaver.commons.SystemConstants.STOMP_WHITELISTED_ENDPOINTS;

import java.util.List;

import javax.servlet.Filter;

import org.appsweaver.commons.models.properties.SecuredResource;
import org.appsweaver.commons.models.properties.Security;
import org.appsweaver.commons.spring.components.WebSecurityHelper;
import org.appsweaver.commons.spring.components.WebServiceProperties;
import org.appsweaver.commons.spring.config.WebServiceConfig;
import org.appsweaver.commons.spring.web.filters.CommonAuthorizationFilter;
import org.appsweaver.commons.spring.web.services.UAMClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;

/**
 *
 * @author UD
 * 
 */
@EnableWebSecurity(debug = false)
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Configuration("webSecurityConfig")
@ConditionalOnProperty(name = "webservice.security.enable-common-security", havingValue = "true")
public class CommonWebSecurityConfig extends WebSecurityConfigurerAdapter {
	/**
	 * The Logger.
	 */
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * The Web service config.
	 */
	@Autowired WebServiceConfig webServiceConfig;

	/**
	 * The Application properties.
	 */
	@Autowired WebServiceProperties webServiceProperties;
	
	/**
	 * Handle to web security helper
	 */
	@Autowired WebSecurityHelper webSecurityHelper;

	/**
	 * The UAAM client.
	 */
	@Autowired UAMClient uamClient;
	
	@Override
	public void configure(WebSecurity webSecurity) throws Exception {
		logger.debug("Configuring CommonWebSecurityConfig::WebSecurity");
		// @formatter:off
		// Allow unauthenticated access to swagger and spring management console
		webSecurity.ignoring()
			.antMatchers(HttpMethod.OPTIONS, webServiceConfig.getWildRootPath())
			.antMatchers(HttpMethod.POST, AUTHENTICATION_ENDPOINT)
			.antMatchers(webServiceConfig.getActuatorPath())
			.antMatchers(webServiceConfig.getSwaggerIndexPagePath())
			.antMatchers(webServiceConfig.getSwaggerApiDocsPath())
			.antMatchers(webServiceConfig.getSwaggerResourcesPath())
			.antMatchers(webServiceConfig.getSwaggerUIConfigPath())
			.antMatchers(webServiceConfig.getSwaggerSecurityConfigPath())
			.antMatchers(webServiceConfig.getSwaggerWebJarsPath())
			;
		// @formatter:on
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		logger.debug("Configuring CommonWebSecurityConfig::HttpSecurity");
		http.exceptionHandling().and().anonymous().and().servletApi().and().headers().cacheControl();

		final ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry 
			expressionInterceptUrlRegistry = http.cors().and().csrf().disable().authorizeRequests();

		expressionInterceptUrlRegistry
			.antMatchers(HttpMethod.OPTIONS, API_VERSIONED_PATH_WILD).permitAll()
			.antMatchers(HttpMethod.OPTIONS, webServiceConfig.getWildRootPath()).permitAll()
			.antMatchers(webServiceConfig.getActuatorPath()).permitAll()
			.antMatchers(webServiceConfig.getSwaggerIndexPagePath()).permitAll()
			.antMatchers(webServiceConfig.getSwaggerApiDocsPath()).permitAll()
			.antMatchers(webServiceConfig.getSwaggerResourcesPath()).permitAll()
			.antMatchers(webServiceConfig.getSwaggerUIConfigPath()).permitAll()
			.antMatchers(webServiceConfig.getSwaggerSecurityConfigPath()).permitAll()
			.antMatchers(webServiceConfig.getSwaggerWebJarsPath()).permitAll()
		
			// Permit access to pre-flight requests for all
			.antMatchers(HttpMethod.OPTIONS, API_VERSIONED_PATH_WILD).permitAll()
			
			// Permit access to authentication end-point for all
			.antMatchers(HttpMethod.POST, AUTHENTICATION_ENDPOINT).permitAll()

			// Notifications web socket end-point
			.antMatchers(STOMP_WHITELISTED_ENDPOINTS).permitAll();
		
		final Security securityProperties = webServiceProperties.getSecurity();
		final List<SecuredResource> securedResources = securityProperties.getSecuredResources();
		webSecurityHelper.addSecuredRoutes(expressionInterceptUrlRegistry, securedResources);
				
		if (securityProperties.isEnableCommonTokenAuthorization()) {
			// Authenticate rest of the end-points (Secured)
			expressionInterceptUrlRegistry
				.antMatchers(API_VERSIONED_PATH_WILD).authenticated()
				.and()
				.addFilterBefore(authenticationTokenFilterBean(), RequestHeaderAuthenticationFilter.class)
				.sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		}
	}
	
	Filter authenticationTokenFilterBean() throws Exception {
		return new CommonAuthorizationFilter(authenticationManager(), uamClient);
	}
}
