/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.spring.web.components;

import java.io.Serializable;

import org.apache.commons.lang3.reflect.MethodUtils;
import org.appsweaver.commons.utilities.Stringizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

/**
 * 
 * @author UD
 * 
 */
@Component
public class CommonPermissionEvaluator implements PermissionEvaluator {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public boolean hasPermission(Authentication authentication, Object object, Object permission) {
		if (authentication != null && object instanceof MethodSecurityExpressionOperations) {
			final MethodSecurityExpressionOperations methodSecurityExpressionOperations = (MethodSecurityExpressionOperations)object;
			final Object target = methodSecurityExpressionOperations.getThis();
			try {
				final Object entityName = MethodUtils.invokeMethod(target, "entityName");
				if(entityName != null) {
					final String className = String.valueOf(entityName);
					final String converted = Stringizer.upperCamelToUpperUnderscored(className);

					final String permissionName = String.format("%s_%s", permission, converted);
					for(GrantedAuthority authority : authentication.getAuthorities()) {
						if(authority.getAuthority().equals(permissionName)) {
							return true;
						}
					}
				}
			} catch (Throwable t) {
				logger.error("Error evaluating permission", t);
			}
		}
		
		return false;
	}

	@Override
	public boolean hasPermission(Authentication authentication, Serializable serializable, String targetType, Object permission) {
		return false;
	}
}
