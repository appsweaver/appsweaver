/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.webservice.endpoints;

import java.util.List;

import org.appsweaver.commons.models.GenericErrorCode;
import org.appsweaver.commons.models.devops.ReleaseNote;
import org.appsweaver.commons.models.properties.BuildInformation;
import org.appsweaver.commons.models.web.WebResponse;
import org.appsweaver.commons.models.web.exceptions.ApplicationException;
import org.appsweaver.commons.services.InformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author UD
 * 
 */
@ConditionalOnBean(name = "informationService")
@RestController
@RequestMapping(value = "/api/v1/information", produces = { MediaType.APPLICATION_JSON_VALUE })
@Api(value = "Build information endpoint")
public class InformationController extends DefaultSimpleController {
	@Autowired InformationService informationService;

	/**
	 * Get build information
	 * 
	 * @return build information
	 */
	@GetMapping("/build")
	@ApiOperation(value = "Get Build Information")
	public @ResponseBody WebResponse<BuildInformation> getBuildInformation() {
		try {
			getLogger().info("START: Get build information");
			
			final BuildInformation buildInformation = informationService.getBuildInformation();
			
			return WebResponse.newWebResponse(buildInformation);
		} catch (Throwable t) {
			getLogger().error("Error fetching build information", t);
			throw new ApplicationException(
				controllerName(), 
				HttpStatus.INTERNAL_SERVER_ERROR, 
				GenericErrorCode.FAILED_TO_GET_BUILD_INFORMATION, 
				String.format("Error getting build information"),
				t.getMessage()
			);
		} finally {
			getLogger().info("END: Get build information");
		}
	}
	
	/**
	 * Get release notes
	 * 
	 * @return release notes
	 */
	@GetMapping("/release")
	@ApiOperation(value = "Get Release Notes")
	public @ResponseBody WebResponse<List<ReleaseNote>> getReleaseNotes() {
		try {
			getLogger().info("START: Get release notes");
			
			final List<ReleaseNote> releaseNotes = informationService.getReleaseNotes();
			
			return WebResponse.newWebResponse(releaseNotes);
		} catch (Throwable t) {
			getLogger().error("Error fetching release notes", t);
			throw new ApplicationException(
				controllerName(), 
				HttpStatus.INTERNAL_SERVER_ERROR, 
				GenericErrorCode.FAILED_TO_GET_RELEASE_NOTES, 
				String.format("Error getting release notes"),
				t.getMessage()
			);
		} finally {
			getLogger().info("END: Get release notes");
		}
	}
}
