/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.webservice.endpoints;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.appsweaver.commons.jpa.models.criteria.SortOrder;
import org.appsweaver.commons.jpa.models.search.TextSearchType;
import org.appsweaver.commons.jpa.types.FilterConjunction;
import org.appsweaver.commons.jpa.types.Rank;
import org.appsweaver.commons.models.GenericErrorCode;
import org.appsweaver.commons.models.persistence.PrimaryEntity;
import org.appsweaver.commons.models.web.WebResponse;
import org.appsweaver.commons.models.web.exceptions.ApplicationException;
import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Jsonifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 *
 * @author UD
 *
 */
public abstract class AbstractPrimaryController<T extends PrimaryEntity> extends AbstractPrimarySearchController<T> {

	/**
	 * Creates a new instance from given JSON pay load.  id filed must not be populated for this API.  When "id" field is present, <br>
	 * entity will be updated (this may cause undesired end-user behavior).
	 *
	 * @param input input object to be created
	 * @param rank rank
	 * @param references references
	 * @return created object
	 */
	@Transactional
	@PostMapping(consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ApiOperation(value = "Create Entity")
	public @ResponseBody WebResponse<T> create(
		@RequestBody T input,
		@ApiParam @RequestParam(value = "rank", required = false) Rank rank,
		@ApiParam @RequestParam(value = "references", required = false) List<UUID> references
	) {
		final String message = String.format("CREATE: [entityClass=%s]", entityName());

		try {
			getLogger().info("START: {}", message);

			final List<T> referenceEntities = getDataService().findByIdList(references);

			final T saved = getDataService().save(input, Optional.ofNullable(rank), referenceEntities);
			getLogger().info("CREATED [id={}, class={}, requestor={}]", saved.getId(), entityName(), getDataService().getLoginId());

			return WebResponse.newWebResponse(saved);
		} catch (Throwable t) {
			getLogger().error("Error creating {}", entityName(), t);
			throw new ApplicationException(
				controllerName(),
				HttpStatus.INTERNAL_SERVER_ERROR,
				GenericErrorCode.FAILED_TO_CREATE_ITEM,
				String.format("Error creating item [context=%s]", entityName()),
				t.getMessage()
			);
		} finally {
			getLogger().info("END: {}", message);
		}
	}

	/**
	 *
	 * @param id id
	 * @param rank rank
	 * @param references references
	 * @return the ranked object
	 */

	@Transactional
	@PostMapping(path = "/{id}/rank/{rank}", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ApiOperation(value = "Rank Entity")
	public @ResponseBody WebResponse<T> rank(
		@ApiParam @PathVariable(value = "id") UUID id,
		@ApiParam @PathVariable(value = "rank", required = false) Rank rank,
		@ApiParam @RequestParam(value = "references",  required = false) List<UUID> references
	) {
		final String message = String.format("CREATE: [entityClass=%s]", entityName());

		try {
			getLogger().info("START: {}", message);

			final Optional<T> optionalInput = getDataService().findById(id);
			if(optionalInput.isPresent()) {
				final T input = optionalInput.get();

				if(getDataService().isRankable(getEntityClass())) {
					final Optional<Rank> optionalRank = (rank != null) ? Optional.of(rank) : Optional.of(Rank.BOTTOM);
					final List<T> referenceEntities = getDataService().findByIdList(references);
					getDataService().save(input, optionalRank, referenceEntities);

					getLogger().info("RANKING [id={}, rank={}, class={}, requestor={}]", input.getId(), rank, entityName(), getDataService().getLoginId());
				} else {
					getLogger().warn("RANKING not allowed [id={}, class={}, requestor={}]", input.getId(), entityName(), getDataService().getLoginId());
				}

				return WebResponse.newWebResponse(input);
			} else {
				throw new ApplicationException(
					controllerName(),
					HttpStatus.PRECONDITION_FAILED,
					GenericErrorCode.FAILED_TO_RANK_ITEM,
					String.format("Error ranking item with invalid id [context=%s, id=%s]", entityName(), id)
				);
			}
		} catch (Throwable t) {
			getLogger().error("Error ranking {}", entityName(), t);
			throw new ApplicationException(
				controllerName(),
				HttpStatus.INTERNAL_SERVER_ERROR,
				GenericErrorCode.FAILED_TO_RANK_ITEM,
				String.format("Error ranking item [context=%s]", entityName()),
				t.getMessage()
			);
		} finally {
			getLogger().info("END: {}", message);
		}
	}

	/**
	 * Creates many new instance from given JSON pay load.  id filed must not be populated for this API.  When "id" field is present, <br>
	 * entity will be updated (this may cause undesired end-user behavior).
	 *
	 * @param input input
	 * @return list of created objects
	 */
	@Transactional
	@PostMapping(path = "/bulk", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ApiOperation(value = "Create Many Entity")
	public @ResponseBody WebResponse<Page<T>> createMany(
		@RequestBody List<T> input
	) {
		final String message = String.format("CREATE MANY: [entityClass=%s]", entityName());
		try {
			getLogger().info("START: {}", message);

			final List<T> savedList = getDataService().saveAll(input);
			savedList.forEach(saved -> {
				getLogger().info("CREATED [id={}, class={}, requestor={}]", saved.getId(), entityName(), getDataService().getLoginId());
			});

			final int savedListSize = savedList.size();
			return WebResponse.newWebResponse(savedList, 0, savedListSize, Long.valueOf(savedListSize));
		} catch (Throwable t) {
			getLogger().error("Error creating {}", entityName(), t);
			throw new ApplicationException(
				controllerName(),
				HttpStatus.INTERNAL_SERVER_ERROR,
				GenericErrorCode.FAILED_TO_CREATE_MANY_ITEMS,
				String.format("Error creating item [context=%s]", entityName()),
				t.getMessage()
			);
		} finally {
			getLogger().info("END: {}", message);
		}
	}

	/**
	 * Update existing object from given JSON pay load. Id of entity is provided as path parameter.  Consumer of API can provide either <br>
	 * obfuscated or DB id.  When obfuscated id is provided as input API will try to convert to DB id if possible.
	 *
	 * Where entity with given "id" is not already in DB, API will throw an exception
	 *
	 * API updates update time stamp implicitly.
	 *
	 * @param id id
	 * @param input input
	 * @return updated object
	 */
	@Transactional
	@PutMapping(path = "/{id}", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ApiOperation(value = "Update Entity")
	public @ResponseBody WebResponse<T> update(
		@ApiParam @PathVariable("id") UUID id,
		@RequestBody T input
	) {
		final String message = String.format("UPDATE: [entityClass=%s, id=%s]", entityName(), id);
		try {
			getLogger().info("START: {}", message);
			// Check if input is right
			validateInput(id, "PUT");

			getLogger().debug("Check if object exists [class={}, id={}]", entityName(), id);
			final Optional<T> entity = getDataService().findById(id);
			if (entity.isPresent()) {
				final T saved = getDataService().save(input);

				getLogger().info("UPDATED [id={}, class={}, requestor={}]", id, entityName(), getDataService().getLoginId());

				return WebResponse.newWebResponse(saved);
			} else {
				throw new ApplicationException(
					controllerName(),
					HttpStatus.NOT_MODIFIED,
					GenericErrorCode.NO_OPERATION,
					String.format("Item not modified [context=%s, id=%s]", entityName(), id)
				);
			}
		} catch (Throwable t) {
			getLogger().error("Error updating {} [id={}]", entityName(), id, t);
			throw new ApplicationException(
				controllerName(),
				HttpStatus.INTERNAL_SERVER_ERROR,
				GenericErrorCode.FAILED_TO_UPDATE_ITEM,
				String.format("Error updating item [context=%s, id=%s]", entityName(), id),
				t.getMessage()
			);
		} finally {
			getLogger().info("END: {}", message);
		}
	}

	/**
	 * Patch will partially update object from given JSON pay load. It will merge new values with existing values in the database.
	 * Id of entity is provided as path parameter.  Consumer of API can provide either <br>
	 * obfuscated or DB id.  When obfuscated id is provided as input API will try to convert to DB id if possible.
	 *
	 * Where entity with given "id" is not already in DB, API will throw an exception
	 *
	 * API updates update time stamp implicitly.
	 *
	 * @param id id
	 * @param input input
	 * @return updated object
	 */
	@Transactional
	@PatchMapping(path = "/{id}", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ApiOperation(value = "Patch Entity")
	public @ResponseBody WebResponse<T> patch(
		@ApiParam @PathVariable("id") UUID id,
		@RequestBody T input
	) {
		final String message = String.format("PATCH: [entityClass=%s, id=%s]", entityName(), id);
		try {
			getLogger().info("START: {}", message);
			// Check if input is right
			validateInput(id, "PATCH");

			getLogger().debug("Check if object exists [class={}, id={}]", entityName(), id);
			final Optional<T> optionalEntity = getDataService().findById(id);
			if (optionalEntity.isPresent()) {
				final T entity = optionalEntity.get();

				final T saved = getDataService().mergeAndSave(input, entity);

				getLogger().info("PATCH [id={}, class={}, requestor={}]", id, entityName(), getDataService().getLoginId());

				return WebResponse.newWebResponse(saved);
			} else {
				throw new ApplicationException(
					controllerName(),
					HttpStatus.NOT_MODIFIED,
					GenericErrorCode.NO_OPERATION,
					String.format("Item not modified [context=%s, id=%s]", entityName(), id)
				);
			}
		} catch (Throwable t) {
			getLogger().error("Error updating {} [id={}]", entityName(), id, t);
			throw new ApplicationException(
				controllerName(),
				HttpStatus.INTERNAL_SERVER_ERROR,
				GenericErrorCode.FAILED_TO_UPDATE_ITEM,
				String.format("Error patching item [context=%s, id=%s]", entityName(), id),
				t.getMessage()
			);
		} finally {
			getLogger().info("END: {}", message);
		}
	}

	/**
	 * Get instance for given id
	 *
	 * @param id object id
	 * @return object object
	 */
	@Transactional(readOnly = true)
	@GetMapping(path = "/{id}")
	@ApiOperation(value = "Get Entity By ID / UUID")
	public @ResponseBody WebResponse<T> getById(
		@ApiParam  @PathVariable("id") UUID id
	) {
		final String message = String.format("GET: [entityClass=%s, id=%s]", entityName(), id);
		try {
			getLogger().info("START: {}; [internalId={}]", message, id);
			// Check if input is right
			validateInput(id, "GET");

			getLogger().debug("Check if object exists [class={}, id={}]", entityName(), id);

			final Optional<T> entity = getDataService().findById(id);

			getLogger().info("GET [id={}, class={}, requestor={}]", id, entityName(), getDataService().getLoginId());

			return (entity.isPresent()) ? WebResponse.newWebResponse(entity.get()) : WebResponse.newWebResponse(HttpStatus.NO_CONTENT);
		} catch (Throwable t) {
			getLogger().error("Error getting one item {} [id={}]", entityName(), id, t);
			throw new ApplicationException(
				controllerName(),
				HttpStatus.INTERNAL_SERVER_ERROR,
				GenericErrorCode.FAILED_TO_GET_ITEM_BY_ID,
				String.format("Error getting one item [context=%s, id=%s]", entityName(), id),
				t.getMessage()
			);
		} finally {
			getLogger().info("END: {}", message);
		}
	}

	/**
	 * Delete object for given id
	 *
	 * @param id object id
	 * @return deleted object
	 */
	@Transactional
	@DeleteMapping(path = "/{id}")
	@ApiOperation(value = "Delete Entity By Id")
	public @ResponseBody WebResponse<T> deleteById(@ApiParam @PathVariable("id") UUID id) {

		final String message = String.format("DELETE: [entityClass=%s, id=%s]", entityName(), id);

		try {
			getLogger().info("START: {}", message);

			// Check if input is right
			validateInput(id, "DELETE");

			final Optional<T> optionalEntity = getDataService().findById(id);
			if(optionalEntity.isPresent()) {
				final T entity = optionalEntity.get();
				getDataService().delete(entity);
				getLogger().info("DELETED [id={}, class={}, requestor={}]", id, entityName(), getDataService().getLoginId());
				return WebResponse.newWebResponse(entity, HttpStatus.OK);
			}

			return WebResponse.newWebResponse(HttpStatus.NO_CONTENT);
		} catch (Throwable t) {
			getLogger().error("Error deleting {} [id={}]", entityName(), id, t);
			throw new ApplicationException(
				controllerName(),
				HttpStatus.INTERNAL_SERVER_ERROR,
				GenericErrorCode.FAILED_TO_DELETE_ITEM,
				String.format("Error deleting item [context=%s, id=%s]", entityName(), id),
				t.getMessage()
			);
		} finally {
			getLogger().info("END: {}", message);
		}
	}

	/**
	 * Delete many instances based on provided filter and sort criteria.  Optional page offset and size can be input for paginating.
	 * "eq" (equals) is default filter behavior.  API consumers can use like(value), between(1,10), in(1,2,3,4), lt, gt, ge, le operators
	 *
	 * Filter example:
	 * { "id" : "1" } - applies equals operator by default
	 * { "name" : "like(sam)" } - applies like operator implicitly converting sam to %sam% internally
	 * { "tags" : "in(TAG1, TAG2, TAG3)" } - applies in operator
	 * { "updatedOn" : "between(1531243047107, 1531243056005)" } - applies between operator, expects number range
	 *
	 * @param filters filter criteria
	 * @return page of search results
	 */
	@Transactional
	@DeleteMapping
	@ApiOperation(value = "Delete many entities using filters")
	public @ResponseBody WebResponse<Page<T>> deleteMany(
		@ApiParam @RequestParam(value = "filter", defaultValue = "{}") String filters
	) {
		final String message = String.format("DELETE MANY: [entityClass=%s]", entityName());
		try {
			getLogger().info("START: {}", message);

			final Map<String, String> filterMap = Jsonifier.toMap(filters);
			final List<SortOrder> sortValues = Collections.newList();
			sortValues.add(new SortOrder("id", Direction.ASC));

			final Page<T> entityPage = getDataService().find(
				getEntityClass(), TextSearchType.WILDCARD,
				filterMap, FilterConjunction.AND,
				sortValues,
				null, // No selected facets
				0, Integer.MAX_VALUE
			);

			entityPage.forEach(entity -> {
				try {
					getDataService().delete(entity);
				} catch(Throwable t) {
					getLogger().warn("Failed to delete entity [id={}]", entity.getId());
				}
			});

			getLogger().info("DELETE MANY [size={}, total={}, pages={}, class={}, requestor={}]", entityPage.getSize(), entityPage.getTotalElements(), entityPage.getTotalPages(), entityName(), getDataService().getLoginId());

			return WebResponse.newWebResponse(entityPage);
		} catch (Throwable t) {
			getLogger().error("Error listing {}(s)", entityName(), t);
			throw new ApplicationException(
				controllerName(),
				HttpStatus.INTERNAL_SERVER_ERROR,
				GenericErrorCode.FAILED_TO_DELETE_MANY_ITEMS,
				String.format("Error getting all items [context=%s]", entityName()),
				t.getMessage()
			);

		} finally {
			getLogger().info("END: {}", message);
		}
	}
}
