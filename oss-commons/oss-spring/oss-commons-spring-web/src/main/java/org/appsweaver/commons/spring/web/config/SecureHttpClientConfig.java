/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.spring.web.config;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;


/**
 * 
 * @author UD
 * 
 */
@Configuration
public class SecureHttpClientConfig {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Value("${webservice.http.connection-time-out:30000}")
	int connectionTimeOut;

	@Value("${webservice.http.connection-request-time-out:30000}")
	int connectionRequestTimeOut;
	
	public SecureHttpClientConfig() {
    	logger.info("Secure HTTP Client Configuration Created!");
	}
	
	@Bean
	public RestTemplate restTemplate() {
		final HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
		
		final CloseableHttpClient httpClient = HttpClients.custom()
	        .setSSLHostnameVerifier(new NoopHostnameVerifier())
	        .build();
		
		clientHttpRequestFactory.setHttpClient(httpClient);
		clientHttpRequestFactory.setConnectionRequestTimeout(connectionRequestTimeOut);
		clientHttpRequestFactory.setConnectTimeout(connectionTimeOut);

		return new RestTemplate(clientHttpRequestFactory);
	}
}
