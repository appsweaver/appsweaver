/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.models.web;

import java.util.Arrays;
import java.util.List;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.service.contexts.SecurityContext;

/**
 * 
 * @author UD
 * 
 */
public class SwaggerApiHelper {
	public static ApiInfo apiInfo(String title, String description, String version) {
		return new ApiInfoBuilder().title(title).description(description).version(version).build();
	}
	
	public static SecurityContext securityContext(String keyName, String scope) {
		return SecurityContext.builder().securityReferences(defaultAuth(keyName, scope)).forPaths(PathSelectors.any()).build();
	}
	
	private static List<SecurityReference> defaultAuth(String keyName, String scope) {
		final AuthorizationScope[] authorizationScopes = { new AuthorizationScope(scope, scope) };
		return Arrays.asList(new SecurityReference(keyName, authorizationScopes));
	}
}
