/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.webservice.endpoints;

import static org.appsweaver.commons.SystemConstants.STOMP_MESSAGE_BROKER_MESSAGES_ENDPOINT;

import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.http.HttpServletRequest;

import org.appsweaver.commons.jpa.models.messages.Message;
import org.appsweaver.commons.jpa.models.messages.Sender;
import org.appsweaver.commons.models.GenericErrorCode;
import org.appsweaver.commons.models.security.SecurityFacade;
import org.appsweaver.commons.models.web.WebResponse;
import org.appsweaver.commons.models.web.exceptions.ApplicationException;
import org.appsweaver.commons.utilities.Dates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 *
 * @author UD
 *
 */
@RestController
@ConditionalOnProperty(name="webservice.websocket.messaging.enabled", havingValue="true")
@RequestMapping(value = "/api/v1/websocket", produces = { MediaType.APPLICATION_JSON_VALUE })
@Api(value = "Websocket endpoint")
public class WebSocketController extends DefaultSimpleController {
	@Value("${webservice.websocket.messaging.user.email:no-reply@personalis.com}")
	@Autowired
	String senderEmail;
	
	@Value("${webservice.websocket.messaging.user.name:DO_NOT_REPLY}")
	@Autowired
	String senderFullname;
	
	@Autowired
	SecurityFacade securityFacade;
	
	@Autowired(required = false)
	SimpMessagingTemplate simpMessagingTemplate;
	
	final AtomicLong messageCount = new AtomicLong(0);
	
	void updateMessage(Message<Long> message, boolean system) {
		if(!system) {
			final Sender<Long> from = new Sender<>();
			from.setEmail(senderEmail);
			from.setName(senderFullname);
			from.setId(null);
			message.setFrom(from);
		} else {
			/*
			 * TODO: Find a way to lookup from user management service
			 */
		}

		message.setId(messageCount.incrementAndGet());
		message.setSentOn(Dates.toTime());
	}

	/*
	@PostMapping(value = "/message/user/{id}", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ApiOperation(value = "Send message to user", response = WebResponse.class)
	public @ResponseBody WebResponse sendToUser(
		@RequestBody Message<String> message,
		HttpServletRequest httpServletRequest
	) {
		try {
			updateMessage(message, false);
			simpMessagingTemplate.convertAndSend(STOMP_MESSAGE_BROKER_MESSAGES_ENDPOINT, message);
			return WebResponse.ok();
		} catch(Throwable t){
			logger.error("Authentication Error!", t);
			throw new ApplicationException(name(), HttpStatus.UNAUTHORIZED, GenericErrorCode.USER_IS_FORBIDEN_FROM_PEFORMING_REQUESTED_OPERATION, t.getMessage());
		}
	}
	*/

	@PostMapping(value = "/message/broadcast", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ApiOperation(value = "Broadcast message")
	public @ResponseBody WebResponse<Message<Long>> broadcast(
		@RequestParam(value = "system", defaultValue = "true", required = false) boolean system,
		@RequestBody Message<Long> message, HttpServletRequest httpServletRequest
	) {
		try {
			updateMessage(message, false);

			simpMessagingTemplate.convertAndSend(STOMP_MESSAGE_BROKER_MESSAGES_ENDPOINT, message);
			return WebResponse.newWebResponse(message);
		} catch(Throwable t){
			logger.error("Authentication Error!", t);
			throw new ApplicationException(controllerName(), HttpStatus.UNAUTHORIZED, GenericErrorCode.REQUESTED_OPERATION_IS_FORBIDDEN, t.getMessage());
		}
	}
}
