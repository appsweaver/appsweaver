/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.spring.web.filters;

import org.appsweaver.commons.models.exceptions.AuthenticationTokenExpiredException;
import org.appsweaver.commons.models.security.SecuredUser;
import org.appsweaver.commons.spring.web.services.UAMClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

/**
 * 
 * @author UD
 * 
 */
public class CommonAuthorizationFilter extends ExtendedAuthenticationFilter {
	final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	final UAMClient uamClient;
	
	public CommonAuthorizationFilter(AuthenticationManager authenticationManager, UAMClient uamClient) {
		super(authenticationManager);
		this.uamClient = uamClient;
	}

	@Override
	protected UsernamePasswordAuthenticationToken newAuthentication(String token) throws AuthenticationTokenExpiredException {
		final String subject = getSubject(token);

		/*
		 * Is valid token and has subject
		 */
		if (subject != null) {
			final SecuredUser user = uamClient.findUser(subject);

			if(user != null && user.isInActiveSession()) {
				logger.trace("A vaid subject was found in the authentication server [subject={}, subjectId={}, token={}]", subject, user.getId(), token);

				return newAuthenticationToken(user);
			} else {
				logger.trace("Requested subject was not found in the authentication server [subject={}, token={}]", subject, token);
			}
		}
		
		logger.trace("Subject information not available in authentication token [token={}]", token);
		return null;
	}
	
	protected UsernamePasswordAuthenticationToken newAuthenticationToken(SecuredUser securedUser) {
		final String loginId = securedUser.getLoginId();

		logger.trace("Making token [loginId={}, id={}]", loginId, securedUser.getId());
		final UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(loginId, null, securedUser.toGrantedAuthorities());
		usernamePasswordAuthenticationToken.setDetails(securedUser);

		return usernamePasswordAuthenticationToken;
	}
}
