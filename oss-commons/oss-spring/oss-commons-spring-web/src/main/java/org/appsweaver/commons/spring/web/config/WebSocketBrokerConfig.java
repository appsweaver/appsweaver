/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.spring.web.config;

import static org.appsweaver.commons.SystemConstants.STOMP_APPLICATION_DESTINATION_PREFIX;
import static org.appsweaver.commons.SystemConstants.STOMP_MESSAGE_BROKER_MESSAGES_ENDPOINT;
import static org.appsweaver.commons.SystemConstants.STOMP_WEBSOCKET_ENDPOINT;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

/**
 * 
 * @author UD
 * 
 */
@Configuration
@EnableWebSocketMessageBroker
@ConditionalOnProperty(name="webservice.websocket.messaging.enabled", havingValue="true")
public class WebSocketBrokerConfig implements WebSocketMessageBrokerConfigurer {

	@Override
	public void configureMessageBroker(MessageBrokerRegistry messageBrokerRegistry) {
		messageBrokerRegistry
			.setApplicationDestinationPrefixes(STOMP_APPLICATION_DESTINATION_PREFIX)
			.enableSimpleBroker(STOMP_MESSAGE_BROKER_MESSAGES_ENDPOINT);
		;
	}
	
	@Override
	public void registerStompEndpoints(StompEndpointRegistry stompEndpointRegistry) {
		stompEndpointRegistry
			.addEndpoint(STOMP_WEBSOCKET_ENDPOINT)
			.setAllowedOrigins("*")
			.withSockJS()
		;
	}
}
