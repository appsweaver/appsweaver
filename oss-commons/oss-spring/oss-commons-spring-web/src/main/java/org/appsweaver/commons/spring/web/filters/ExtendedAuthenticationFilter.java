/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.spring.web.filters;

import static org.appsweaver.commons.SystemConstants.AUTHORIZATION;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.appsweaver.commons.SystemConstants;
import org.appsweaver.commons.models.exceptions.AuthenticationTokenExpiredException;
import org.appsweaver.commons.spring.components.Cryptographer;
import org.appsweaver.commons.utilities.Stringizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;

/**
 * 
 * @author UD
 * 
 */
public abstract class ExtendedAuthenticationFilter extends RequestHeaderAuthenticationFilter {
	final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	static final Pattern authEndpointPattern = Pattern.compile(".*/api/.*v[0-9]+/authenticate");
	static final Pattern protectedEndpointPattern = Pattern.compile(".*/api/.*v[0-9]+.*");

	public ExtendedAuthenticationFilter(AuthenticationManager authenticationManager) {
		setAuthenticationManager(authenticationManager);
		setPrincipalRequestHeader(SystemConstants.AUTHORIZATION);
	}
	
	boolean isAuthenticateEndpoint(String text) {
		final Matcher matcher = authEndpointPattern.matcher(text);
		return matcher.matches();
	}
	
	boolean isProtectedEndpoint(String text) {
		final Matcher matcher = protectedEndpointPattern.matcher(text);
		return matcher.matches();
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
		final HttpServletRequest request = (HttpServletRequest)servletRequest;
		final HttpServletResponse response = (HttpServletResponse)servletResponse;
		
		final String requestURI = request.getRequestURI();
		final String token = request.getHeader(AUTHORIZATION);
		SecurityContextHolder.clearContext();

		if(!isAuthenticateEndpoint(requestURI) && isProtectedEndpoint(requestURI)) {
			// Is a protected resource
			
			if(!Stringizer.isEmpty(token) && token.startsWith(SystemConstants.TOKEN_PREFIX)) {
				// Has bearer token
				try {
					final String subject = getSubject(token);
					final Authentication authentication = newAuthentication(token);
	
					if (authentication != null) {
						SecurityContextHolder.getContext().setAuthentication(authentication);
	
						// Continue with business as usual
						chain.doFilter(request, response);
						return;
					} else {
						final String message = String.format("Subject could not be authenticated [subject=%s]", subject);
						response.sendError(HttpServletResponse.SC_UNAUTHORIZED, message);
						return;
					}
				} catch (AuthenticationTokenExpiredException e) {
					final String message = String.format("User access token has expired!");
					response.sendError(HttpServletResponse.SC_UNAUTHORIZED, message);
					return;
				}
			} else {
				// but is missing 'Authorization' header
				final String message = String.format("Protected endpoints require 'Authorization' header!");
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED, message);
				return;
			}
		} else {
			final Authentication authentication = new AnonymousAuthenticationToken(
				SystemConstants.ANONYMOUS_USER,
				SystemConstants.ANONYMOUS_USER,
				AuthorityUtils.createAuthorityList("ROLE_ANONYMOUS")
			);
			
			SecurityContextHolder.getContext().setAuthentication(authentication);
		}

		// Continue with business as usual
		chain.doFilter(request, response);
	}

	abstract protected UsernamePasswordAuthenticationToken newAuthentication(String token) throws AuthenticationTokenExpiredException;

	protected final String getSubject(String token) throws AuthenticationTokenExpiredException {
		final String subject = Cryptographer.getSubjectFromJwtsToken(token);
		logger.debug("Obtained username from token [subject={}]", subject);
		return subject;
	}
}
