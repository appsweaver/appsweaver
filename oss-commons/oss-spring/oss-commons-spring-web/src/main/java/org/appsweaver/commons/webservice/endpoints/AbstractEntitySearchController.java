/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.webservice.endpoints;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ForkJoinPool;

import org.appsweaver.commons.api.ServiceBackedController;
import org.appsweaver.commons.jpa.models.criteria.SortOrder;
import org.appsweaver.commons.jpa.models.search.FacetMenuItem;
import org.appsweaver.commons.jpa.models.search.TextSearchType;
import org.appsweaver.commons.jpa.types.FilterConjunction;
import org.appsweaver.commons.json.views.FacetMenuItemView;
import org.appsweaver.commons.json.views.SelectedFacet;
import org.appsweaver.commons.models.GenericErrorCode;
import org.appsweaver.commons.models.persistence.Entity;
import org.appsweaver.commons.models.web.WebRequest;
import org.appsweaver.commons.models.web.WebRequest.WebRequestBuilder;
import org.appsweaver.commons.models.web.WebResponse;
import org.appsweaver.commons.models.web.exceptions.ApplicationException;
import org.appsweaver.commons.spring.utilities.FrameworkHelper;
import org.appsweaver.commons.types.FileType;
import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Jsonifier;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.async.DeferredResult;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
/**
 *
 * @author UD
 *
 */
public abstract class AbstractEntitySearchController<T extends Entity> 
	extends DefaultSimpleController 
	implements ServiceBackedController<T, UUID> 
{
	// @formatter:off
	/**
	 * Get many instances based on provided filter and sort criteria. Optional page
	 * offset and size can be input for paginating. "eq" (equals) is default filter
	 * behavior. API consumers can use like(value), between(1,10), in(1,2,3,4), lt,
	 * gt, ge, le operators
	 * 
	 * Perform fuzzy text search using when a filter name "q" is present using its
	 * value as search term.
	 * 
	 * Filter example: 
	 * { "id" : "1" } - applies equals operator by default 
	 * { "name" : "like(sam)" } - applies like operator implicitly converting 
	 *     sam to %sam% internally 
	 * { "tags" : "in(TAG1, TAG2, TAG3)" } - applies in operator
	 * { "updatedOn" : "between(1531243047107, 1531243056005)" } - applies between 
	 * operator, expects number range
	 * 
	 * sort example: [{"field":"id","order":"DESC"}] - applies default sort by id in
	 * descending order
	 *
	 * @param pageNumber the page number
	 * @param pageSize the page size
	 * @param filterOptions the filter options
	 * @param textSearchType the text search type
	 * @param sortOptions the sort options
	 * @param facetOptions the selected facets
	 * @param filterConjunction the filter join option
	 * @param since time since the records where created or updated
	 * @return the page of records
	 */
	@Transactional(readOnly = true)
	@GetMapping
	@ApiOperation(value = "Filter, Sort And Get Rows With Paging Options")
	public @ResponseBody WebResponse<Page<T>> getMany(
		@ApiParam @RequestParam(value = "page", defaultValue = "0", required = false) Integer pageNumber,
		@ApiParam @RequestParam(value = "size", defaultValue = "25", required = false) Integer pageSize,
		@ApiParam @RequestParam(value = "filter", defaultValue = "{}", required = false) String filterOptions,
		@ApiParam @RequestParam(value = "type", defaultValue = "WILDCARD", required = false) TextSearchType textSearchType,
		@ApiParam @RequestParam(value = "sort", defaultValue = "[]", required = false) String sortOptions,
		@ApiParam @RequestParam(value = "facets", defaultValue = "[]", required = false) String facetOptions,
		@ApiParam @RequestParam(value = "conjunction", defaultValue = "AND", required = false) FilterConjunction filterConjunction,
		@ApiParam @RequestParam(value = "since", defaultValue = "0", required = false) Long since
	) {
		final String message = String.format("GET MANY: [entityClass=%s]", entityName());
		try {
			getLogger().info("START: {}", message);

			final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
			webRequestBuilder.pageNumber(pageNumber);
			webRequestBuilder.pageSize(pageSize);
			webRequestBuilder.filter(filterOptions);
			webRequestBuilder.textSearchType(textSearchType);
			webRequestBuilder.sortOrder(sortOptions);
			webRequestBuilder.selectedFacet(facetOptions);
			webRequestBuilder.filterConjunction(filterConjunction);
			webRequestBuilder.since(since);
			
			final WebRequest webRequest = webRequestBuilder.build();
			
			final Map<String, String> filterMap = webRequest.getFilterMap();
			final List<SortOrder> sortValues = webRequest.getSortOrders();
			final List<SelectedFacet> selectedFacets = webRequest.getSelectedFacets();
			
			final Page<T> entityPage = getDataService().find(getEntityClass(), textSearchType, filterMap, filterConjunction, sortValues, selectedFacets, pageNumber, pageSize, since);
			getLogger().info("GET MANY [size={}, total={}, pages={}, class={}, requestor={}]", 
				entityPage.getNumberOfElements(), entityPage.getTotalElements(), 
				entityPage.getTotalPages(),
				entityName(),
				getDataService().getLoginId()
			);
			
			return WebResponse.newWebResponse(entityPage);
		} catch (final Throwable t) {
			getLogger().error("Error listing {}(s)", entityName(), t);
			throw new ApplicationException(controllerName(), HttpStatus.INTERNAL_SERVER_ERROR, GenericErrorCode.FAILED_TO_GET_MANY_ITEMS,
				String.format("Error getting all items [context=%s]", entityName()), t.getMessage());

		} finally {
			getLogger().info("END: {}", message);
		}
	}
	
	/**
	 * Export the data to a file
	 * 
	 * @param pageNumber the page number
	 * @param pageSize the page size
	 * @param filterOptions the filter options
	 * @param textSearchType the text search type
	 * @param sortOptions the sort options
	 * @param facetOptions the selected facets
	 * @param filterConjunction the filter join option
	 * @param since time since the records where created or updated
	 * @param fileType output file
	 * @param mapping the column attribute mapping
	 * @return handle to exported content
	 */
	@Transactional(readOnly = true)
	@GetMapping("/export")
	@ApiOperation(value = "Export rows to a file")
	public @ResponseBody DeferredResult<WebResponse<?>> export(
		@ApiParam @RequestParam(value = "page", defaultValue = "0", required = false) Integer pageNumber,
		@ApiParam @RequestParam(value = "size", defaultValue = "-1", required = false) Integer pageSize,
		@ApiParam @RequestParam(value = "filter", defaultValue = "{}", required = false) String filterOptions,
		@ApiParam @RequestParam(value = "type", defaultValue = "WILDCARD", required = false) TextSearchType textSearchType,
		@ApiParam @RequestParam(value = "sort", defaultValue = "[]", required = false) String sortOptions,
		@ApiParam @RequestParam(value = "facets", defaultValue = "[]", required = false) String facetOptions,
		@ApiParam @RequestParam(value = "conjunction", defaultValue = "AND", required = false) FilterConjunction filterConjunction,
		@ApiParam @RequestParam(value = "fileType", defaultValue = "CSV", required = false) FileType fileType,
		@ApiParam @RequestParam(value = "mapping", defaultValue = "{}", required = false) String mapping,
		@ApiParam @RequestParam(value = "since", defaultValue = "0", required = false) Long since
	) 
	{
		final String message = String.format("EXPORT: [entityClass=%s]", entityName());
		final String loginId = getDataService().getLoginId();

		try {
			getLogger().info("START: {}", message);

			final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
			webRequestBuilder.filter(filterOptions);
			webRequestBuilder.textSearchType(textSearchType);
			webRequestBuilder.sortOrder(sortOptions);
			webRequestBuilder.selectedFacet(facetOptions);
			webRequestBuilder.filterConjunction(filterConjunction);
			webRequestBuilder.pageNumber(pageNumber);
			webRequestBuilder.since(since);
			webRequestBuilder.pageSize(pageSize < 0 ? Integer.MAX_VALUE : pageSize);
			
			final WebRequest webRequest = webRequestBuilder.build();
			
			final Map<String, String> filterMap = webRequest.getFilterMap();
			final List<SortOrder> sortOrders = webRequest.getSortOrders();
			final List<SelectedFacet> selectedFacets = webRequest.getSelectedFacets();
			final Map<String, String> columnMapping = Jsonifier.toMap(mapping);
			
			final DeferredResult<WebResponse<?>> deferredResult = new DeferredResult<>();
			ForkJoinPool.commonPool().submit(() -> {
		        try {
		        	final Map<String, Object> result = getDataService().export(
		        		getEntityClass(), textSearchType, filterMap, filterConjunction, sortOrders, selectedFacets, 
		        		webRequest.getPageNumber(), webRequest.getPageSize(), fileType, columnMapping
		        	);
		        	
					getLogger().info("EXPORT [fileType={}, handle={}, class={}, requestor={}]", 
						fileType,
						result,
						entityName(),
						loginId
					);
					
					deferredResult.setResult(WebResponse.newWebResponse(result));
		        } catch (final Throwable t) {
		        	getLogger().error("EXPORT Error [fileType={}, class={}, requestor={}]", 
						fileType,
						entityName(),
						loginId,
						t
					);
							
		        	final Map<String, Object> result = Collections.newHashMap();
		        	
		        	result.put("error", String.format("Error exporting data to file [context=%s]", entityName()));
		        	result.put("message", t.getMessage());
		        	
		        	deferredResult.setErrorResult(result);
		        }
		    });
			
			return deferredResult;
		} catch (final Throwable t) {
			getLogger().error("Error exporting {}(s)", entityName(), t);
			throw new ApplicationException(controllerName(), HttpStatus.INTERNAL_SERVER_ERROR, GenericErrorCode.FAILED_TO_EXPORT_DATA,
				String.format("Error exporting data [context=%s]", entityName()), t.getMessage());
		} finally {
			getLogger().info("END: {}", message);
		}
	}
	
	/**
	 * Get facets
	 * 
	 * @param filterOptions the filter options
	 * @param textSearchType the text search type
	 * @return the list of available facets
	 */
	@Transactional(readOnly = true)
	@GetMapping(path = "/facets")
	@ApiOperation(value = "Get Facets")
	public @ResponseBody WebResponse<Page<FacetMenuItemView>> getFacets(
		@ApiParam @RequestParam(value = "filter", defaultValue = "{}") String filterOptions,
		@ApiParam @RequestParam(value = "type", defaultValue = "WILDCARD", required = false) TextSearchType textSearchType
	) {
		final String message = String.format("GET FACETS: [entityClass=%s]", entityName());
		try {
			getLogger().info("START: {}", message);
			
			final WebRequestBuilder webRequestBuilder = new WebRequest.WebRequestBuilder();
			webRequestBuilder.pageNumber(0);
			webRequestBuilder.pageSize(Integer.MAX_VALUE);
			webRequestBuilder.filter(filterOptions);
			webRequestBuilder.textSearchType(textSearchType);
			
			final WebRequest webRequest = webRequestBuilder.build();
			
			final Map<String, String> filterMap = webRequest.getFilterMap();

			final Map<String, List<FacetMenuItem>> facetMenuMap = getDataService().getFacets(getEntityClass(), "*", filterMap);
			final List<FacetMenuItemView> collection = Collections.newList();
			
			facetMenuMap.forEach((k, v) -> {
				final FacetMenuItemView item = new FacetMenuItemView();
				item.setName(k);
				item.setItems(v);
				
				collection.add(item);
			});

			getLogger().info("GET FACETS [size={}, class={}, requestor={}]", collection.size(), entityName(), getDataService().getLoginId());

			return WebResponse.newWebResponse(collection, 0, collection.size(), Long.valueOf(collection.size()));
		} catch (final Throwable t) {
			getLogger().error("Error listing facets {}(s)", entityName(), t);
			throw new ApplicationException(controllerName(), HttpStatus.INTERNAL_SERVER_ERROR, GenericErrorCode.FAILED_TO_GET_FACETS,
					String.format("Error listing facets [context=%s]", entityName()), t.getMessage());
		} finally {
			getLogger().info("END: {}", message);
		}
	}
	
	/**
	 * Get entity class attributes (this is introspective API useful for REST API consumers to know what fields available and can be filtered / searched)
	 *
	 * @return map of object attributes
	 */
	@GetMapping(path = "/metadata")
	@ApiOperation(value = "Get Metadata")
	public @ResponseBody WebResponse<Map<String, Object>> getMetadata() {
		final String message = String.format("GET METADATA [entityClass=%s]", entityName());

		try {
			getLogger().info("START: {}", message);

			final Map<String, Object> metadata = FrameworkHelper.getMetadata(getEntityClass());
			if(metadata != null && metadata.size() > 0) {
				getLogger().info("GET METADATA [size={}, class={}, requestor={}]", metadata.size(), entityName(), getDataService().getLoginId());

				return WebResponse.newWebResponse(metadata);
			} else {
				throw new ApplicationException(controllerName(), HttpStatus.PRECONDITION_FAILED, GenericErrorCode.FAILED_TO_GET_ENTITY_ATTRIBUTES, "Could not get entity class attributes");
			}
		} catch (final Throwable t) {
			getLogger().error("Error fetching metadata {}", entityName(), t);
			throw new ApplicationException(
				controllerName(),
				HttpStatus.INTERNAL_SERVER_ERROR,
				GenericErrorCode.FAILED_TO_GET_ENTITY_ATTRIBUTES,
				"Could not get entity class metadata",
				t.getMessage()
			);
		} finally {
			getLogger().info("END: {}", message);
		}
	}
}
