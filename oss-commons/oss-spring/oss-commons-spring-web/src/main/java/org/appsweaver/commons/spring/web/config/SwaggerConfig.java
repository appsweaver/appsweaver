/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.spring.web.config;

import static org.appsweaver.commons.SystemConstants.API_KEY_NAME;
import static org.appsweaver.commons.SystemConstants.API_SCOPE;
import static org.appsweaver.commons.SystemConstants.API_VERSIONED_PATH_WILD;
import static org.appsweaver.commons.SystemConstants.AUTHORIZATION;
import static org.appsweaver.commons.SystemConstants.HEADER;

import java.util.Arrays;

import org.appsweaver.commons.models.web.SwaggerApiHelper;
import org.appsweaver.commons.spring.config.AbstractSwaggerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.web.bind.annotation.RestController;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiKey;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * 
 * @author UD
 *
 */
@Configuration
public class SwaggerConfig extends AbstractSwaggerConfig {
	final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Value("${webservice.security.enabled:false}")
	Boolean securityEnabled;
	
	public SwaggerConfig() {
    	logger.info("Swagger Configuration Created!");
	}
	
	@Bean
	public Docket api() {
		final Docket docket = new Docket(DocumentationType.SWAGGER_2)
			.apiInfo(
				SwaggerApiHelper.apiInfo(
					getName(), 
					getDescription(), 
					getVersion()
				)
			)
			.enable(true)
			.useDefaultResponseMessages(false)
			.ignoredParameterTypes(PagedResourcesAssembler.class, Pageable.class)
			.select()
			.apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
			.paths(PathSelectors.ant(API_VERSIONED_PATH_WILD))
			.build();
		
		if(securityEnabled) {
	    	logger.info("Enabling authorization tokens support for swagger!");

			docket.securitySchemes(
				Arrays.asList(
					new ApiKey(API_KEY_NAME, AUTHORIZATION, HEADER)
				)
			).securityContexts(
				Arrays.asList(
					SwaggerApiHelper.securityContext(API_KEY_NAME, API_SCOPE)
				)
			);
		}
		
		return docket;
	}
}
