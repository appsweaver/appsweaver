/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.models.web.exceptions;

import org.appsweaver.commons.api.ErrorCode;
import org.appsweaver.commons.services.exception.ServiceException;
import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @author UD
 *
 */
@JsonIgnoreProperties({"suppressed"})
public class ApplicationException extends RuntimeException {
	private static final long serialVersionUID = 247991585545845883L;

	private HttpStatus httpStatus;
	private ErrorCode errorCode;

	private String controller;
	private String reason;

	public ApplicationException(String controller, HttpStatus httpStatus, ErrorCode errorCode, String message, String reason) {
		super(message);

		setController(controller);
		setHttpStatus(httpStatus);
		setErrorCode(errorCode);
		setReason(reason);
	}

	public ApplicationException(String controller, HttpStatus httpStatus, ErrorCode errorCode, String message) {
		this(controller, httpStatus, errorCode, message, null);
	}

	public ApplicationException(String controller, HttpStatus httpStatus, ErrorCode errorCode) {
		this(controller, httpStatus, errorCode, null);
	}

	public ApplicationException(String controller, ServiceException serviceException, String message, String reason) {
		this(controller, HttpStatus.INTERNAL_SERVER_ERROR, serviceException.getErrorCode(), message, reason);
	}
	
	public String getController() {
		return controller;
	}

	public void setController(String controller) {
		this.controller = controller;
	}

	public ErrorCode getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(ErrorCode errorCode) {
		this.errorCode = errorCode;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@JsonIgnore
	@Override
	public String getLocalizedMessage() {
		return super.getLocalizedMessage();
	}

	@JsonIgnore
	@Override
	public StackTraceElement[] getStackTrace() {
		return super.getStackTrace();
	}

	@JsonIgnore
	@Override
	public synchronized Throwable getCause() {
		return super.getCause();
	}
}
