/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.api;

import java.util.UUID;

import org.appsweaver.commons.models.persistence.Entity;

/**
 *
 * @author UD
 *
 */
public interface ServiceBackedController<T extends Entity, ID> extends SimpleController {
	/**
	 * Provide data service implementation
	 * 
	 * @param <S> the entity type
	 * @return data service instance
	 */
	<S extends T> DataService<S> getDataService();
	
	/**
	 * Provide entity class
	 *
	 * @param <S> the entity type
	 * @return class name
	 */
	<S extends T> Class<S> getEntityClass();

	/**
	 * Provide simple entity class name
	 *
	 * @return simple class name
	 */
	default String entityName() {
		return getEntityClass().getSimpleName();
	}
	
	/**
	 * Validate id to make sure it is non-null
	 *
	 * @param uuid id
	 * @param operation operation
	 * @throws IllegalArgumentException if the arguments are invalid
	 */
	default void validateInput(UUID uuid, String operation) throws IllegalArgumentException {
		if(uuid == null) {
			final String errorMessage = String.format("Insufficient data to fulfill request [context=%s, operation=%s, id=%s]", controllerName(), operation, uuid);
			getLogger().error(errorMessage);

			throw new IllegalArgumentException(errorMessage);
		}
	}
}
