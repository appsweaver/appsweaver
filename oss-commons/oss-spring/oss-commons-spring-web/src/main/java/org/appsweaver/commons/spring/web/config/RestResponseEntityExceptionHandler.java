/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.spring.web.config;

import org.appsweaver.commons.models.web.exceptions.ApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * 
 * @author UD
 * 
 */
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	@ExceptionHandler(value = { ApplicationException.class })
	protected ResponseEntity<Object> handleApplicationException(ApplicationException applicationException, WebRequest request) {
		logger.error("Error sending response", applicationException);
		return handleExceptionInternal(applicationException, applicationException, new HttpHeaders(), applicationException.getHttpStatus(), request);
	}

	@ExceptionHandler(value = { Exception.class })
	protected ResponseEntity<Object> handleGenericException(Exception exception, WebRequest request) {
		logger.error("Unhandled application exception", exception);
		return handleExceptionInternal(exception, exception, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
	}


	@ExceptionHandler(value = { UsernameNotFoundException.class })
	protected ResponseEntity<Object> handleUserNotFoundException(UsernameNotFoundException usernameNotFoundException, WebRequest request) {
		logger.error("User not found exception", usernameNotFoundException);
		return handleExceptionInternal(usernameNotFoundException, usernameNotFoundException, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
	}
}
