/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.spring.web.services;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.appsweaver.commons.SystemConstants;
import org.appsweaver.commons.annotations.Loggable;
import org.appsweaver.commons.models.security.BasicCredential;
import org.appsweaver.commons.models.security.SecuredUser;
import org.appsweaver.commons.models.web.exceptions.AuthenticationException;
import org.appsweaver.commons.utilities.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author UD
 *
 */
@Loggable
@Service
@ConditionalOnProperty(name = "webservice.security.enable-common-security", havingValue = "true")
public class UAMClient {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Value("${webservice.security.common-auth-server-base-url}")
	private String authServerBaseUrl;
	
	@Value("${webservice.security.uam-client.max-retries:1}")
	private int maxRetries;

	@Autowired
	private RestTemplate restTemplate;

	public UAMClient() {
	}

	public UAMClient(String authServerBaseUrl, RestTemplate restTemplate) {
		this.authServerBaseUrl = authServerBaseUrl;
		this.restTemplate = restTemplate;
	}
	
	/**
	 * 
	 * @param username
	 * 			the email or login id or secondary login id
	 * @param password
	 * 			the base64 encoded password
	 * @return
	 * 			user object if authentication is successful, null otherwise
	 */
	public SecuredUser authenticate(String username, String password) {
		final String requestUrl = String.format("%s", getAuthenticationUrl());
		
		final HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", "application/json");
		headers.add("Content-Type", "application/json");
		
		final HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
		SecuredUser user = null;
		
		try {
			/*
			 * User must provide base64 encoded password
			 */
			final ResponseEntity<SecuredUser> responseEntity = restTemplate.postForEntity(requestUrl, new BasicCredential(username, password), SecuredUser.class, entity);
			if(HttpStatus.OK == responseEntity.getStatusCode()) {
				user = responseEntity.getBody();
	
				logger.info("Got user [id={}, firstName={}, lastName={}]", user.getId(), user.getFirstName(), user.getLastName());
			}
		} catch(Throwable t) {
			throw new AuthenticationException(t, t.getMessage());
		}

		return user;
		
	}

	/**
	 * Logout user session for given token
	 * 
	 * @param token
	 * 		a valid access token
	 */
	public Optional<SecuredUser> logout(String token) {
		final String requestUrl = String.format("%s", getLogoutUrl());
		
		final HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", "application/json");
		headers.add("Content-Type", "application/json");
		headers.add(SystemConstants.AUTHORIZATION, String.format("%s%s", SystemConstants.TOKEN_PREFIX, token));
		
		final HttpEntity<?> httpEntity = new HttpEntity<>("parameters", headers);
		try {
			/*
			 * User must provide base64 encoded password
			 */
			final ResponseEntity<SecuredUser> responseEntity = restTemplate.exchange(
				requestUrl,
				HttpMethod.POST,
				httpEntity,
				SecuredUser.class
			);
			
			if(HttpStatus.OK == responseEntity.getStatusCode()) {
				final SecuredUser securedUser = responseEntity.getBody();
				
				logger.trace("Logged out user! [loginid={}]", securedUser.getLoginId());
				return Optional.ofNullable(responseEntity.getBody());
			}
		} catch(Throwable t) {
			throw new AuthenticationException(t, t.getMessage());
		}
		
		return Optional.empty();
	}
	
	/**
	 * Find user by value
	 * 
	 * @param value
	 * 		the user lookup value that is any of id, login id, email id or a short name
	 * @return
	 * 		the handle to user
	 */
	public SecuredUser findUser(String value) {
		final String requestUrl = String.format("%s?value=%s", getTrustedClientUsersApiUrl(), value);
		
		final HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", "application/json");
		headers.add("Content-Type", "application/json");
		
		final HttpEntity<String> httpEntity = new HttpEntity<>("parameters", headers);

		boolean retry = true;
		int count = 0;

		do {
			try {
				final ResponseEntity<SecuredUser> responseEntity = restTemplate.getForEntity(requestUrl, SecuredUser.class, httpEntity);
				if(HttpStatus.OK == responseEntity.getStatusCode()) {
					final SecuredUser user = responseEntity.getBody();
					logger.info("Got user [id={}, firstName={}, lastName={}, attemptCount={}]", user.getId(), user.getFirstName(), user.getLastName(), count);
					return user;
				} else if(HttpStatus.NOT_FOUND == responseEntity.getStatusCode()) {
					logger.error("User not found {} ", value);
					return null;
				}
			} catch(Throwable t) {
				logger.debug("Error while finding user [value={}]", value, t);
			}
		} while(retry && count++ < maxRetries);

		logger.warn("Failed to find user [value={}], after {} attempts", value, maxRetries);
		return null;
	}

	String getAuthenticationUrl() {
		return String.format("%s/api/v1/authenticate", authServerBaseUrl);
	}
	
	String getLogoutUrl() {
		return String.format("%s/api/v1/logout", authServerBaseUrl);
	}

	String getTrustedClientApiBaseUrl() {
		return String.format("%s/tc-api/v1", authServerBaseUrl);
	}

	String getTrustedClientUsersApiUrl() {
		return getTrustedClientApiBaseUrl() + "/users";
	}
	
	SecuredUser toSecuredUser(String userId) {
		final SecuredUser securedUser = findUser(userId);
		return securedUser;
	}

	UUID loginIdToId(String loginId) {
		final SecuredUser securedUser = toSecuredUser(loginId);
		return securedUser.getId();
	}
	
	UUID[] loginIdToId(String... loginIds) {
		final List<UUID> userIdList = Collections.newList();
		
		Arrays.asList(loginIds).forEach(loginId -> {
			userIdList.add(loginIdToId(loginId));
		});
		
		return Collections.toArray(userIdList, UUID.class);
	}
}
