/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.models;

import java.math.BigDecimal;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import org.appsweaver.commons.annotations.Filterable;
import org.appsweaver.commons.annotations.Searchable;
import org.appsweaver.commons.jpa.listeners.CustomEntityListener;
import org.appsweaver.commons.models.persistence.PrimaryEntity;
import org.appsweaver.commons.utilities.Dates;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.DocumentId;
import org.hibernate.search.annotations.Facet;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.FieldBridge;
import org.hibernate.search.annotations.SortableField;
import org.hibernate.search.bridge.builtin.IntegerBridge;
import org.hibernate.search.bridge.builtin.UUIDBridge;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.EqualsAndHashCode;

/**
 *
 * @author UD
 *
 * Abstract entity class which every implementing class will inherit
 *
 * EntityListeners(AuditingEntityListener.class) enables Auditor Awareness
 * PrePersist and PreUpdate calls are made by Spring to inject audit information
 */
@EqualsAndHashCode(callSuper = true)
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@EntityListeners({
	/*
	 * Auditing entity listener from Spring injects user id (from security facade) and
	 * set created and updated time stamps
	 */
	AuditingEntityListener.class,

	/*
	 * Custom entity listener addresses the following aspects
	 *
	 * 1. Time dimension injection to convert time stamp values into time grain
	 * month number, year, quarter number, biannual (half-year).  This is used to facilitate
	 * time faceted filter
	 *
	 * 2. Entity id enhancements for UUID field
	 */
	CustomEntityListener.class
})
@MappedSuperclass
public abstract class AbstractEntity extends AbstractSearchableEntity implements PrimaryEntity {
	private static final long serialVersionUID = 1340573817191513850L;

	// Facet Names
	private static final String YEAR_CREATED_FACET = "year_created_facet";
	private static final String MONTH_CREATED_FACET = "month_created_facet";
	private static final String QUARTER_CREATED_FACET = "quarter_created_facet";
	private static final String BIANNUAL_CREATED_FACET = "biannual_created_facet";
	// Facet Names
		
	/**
	 * Entity id for DB layer, this must be hidden from serializers in REST API
	 * Obfuscated human id is provided as id instead.
	 *
	 * UUID can be set by clients
	 */
	@DocumentId
	@Filterable(valueClass = UUID.class)
	@FieldBridge(impl = UUIDBridge.class)
	@Id
	@Column(name = "ID", nullable = false, unique = true)
	protected UUID id;
	
	/**
	 * Version number for this entity
	 */
	@Version
	@Column(name = "VERSION")
	protected long version;

	/**
	 * Has this entity logically archived
	 */
	@Filterable(valueClass = Boolean.class)
	@Column(name = "ARCHIVED")
	protected boolean archived = false;

	/**
	 * Has this entity been logically deleted
	 */
	@Filterable(valueClass = Boolean.class)
	@Column(name = "DELETED")
	@Field
	protected boolean deleted = false;

	/**
	 * Timestamp when entity was created
	 */
	@Field
	@SortableField
	@Filterable(valueClass = Long.class)
	@Column(name = "CREATED_ON", nullable = false)
	@CreatedDate
	protected Long createdOn = Dates.toTime();

	/**
	 * Timestamp when entity was updated
	 */
	@Field
	@SortableField
	@Filterable(valueClass = Long.class)
	@Column(name = "UPDATED_ON")
	@LastModifiedDate
	protected Long updatedOn;

	/**
	 * User who created this object
	 */
	@Searchable
	@Field(bridge = @FieldBridge(impl = UUIDBridge.class))
	@Filterable(valueClass = UUID.class)
	@Column(name = "CREATED_BY")
	@CreatedBy
	protected UUID createdBy;

	/**
	 * User who updated this object
	 */
	@Searchable
	@Field(bridge = @FieldBridge(impl = UUIDBridge.class))
	@Filterable(valueClass = UUID.class)
	@Column(name = "UPDATED_BY")
	@LastModifiedBy
	protected UUID updatedBy;

	/**
	 * Name of the month when object was updated (set by Entity Listener)
	 */
	@JsonIgnore
	@Filterable(valueClass = Integer.class)
	@Column(name = "MONTH_UPDATED_ON")
	protected Integer monthUpdatedOn;

	/**
	 * Year with object was updated (set by Entity Listener)
	 */
	@JsonIgnore
	@Filterable(valueClass = Integer.class)
	@Column(name = "YEAR_UPDATED_ON")
	protected Integer yearUpdatedOn;

	/**
	 * Quarter name when object was updated (set by Entity Listener)
	 */
	@JsonIgnore
	@Filterable(valueClass = Integer.class)
	@Column(name = "QUARTER_UPDATED_ON")
	protected Integer quarterUpdatedOn;

	/**
	 * Binannual name when object was updated (set by Entity Listener)
	 */
	@JsonIgnore
	@Filterable(valueClass = Integer.class)
	@Column(name = "BIANNUAL_UPDATED_ON")
	protected Integer biannualUpdatedOn;

	/**
	 * Name of the month when object was created (set by Entity Listener)
	 */
	@JsonIgnore
	@Filterable(valueClass = Integer.class)
	@Column(name = "MONTH_CREATED_ON")
	protected Integer monthCreatedOn;

	/**
	 * Year with object was created (set by Entity Listener)
	 */
	@JsonIgnore
	@Searchable(lookup = false, range = "[below(2010); between(2010, 2020); between(2020, 2030); between(2030, 2040); above(2040)]")
	@Facet(forField = YEAR_CREATED_FACET)
	@Field(name = YEAR_CREATED_FACET, analyze = Analyze.NO, bridge = @FieldBridge(impl = IntegerBridge.class))
	@Filterable(valueClass = Integer.class)
	@Column(name = "YEAR_CREATED_ON")
	protected Integer yearCreatedOn;

	/**
	 * Quarter name when object was created (set by Entity Listener)
	 */
	@JsonIgnore
	@Filterable(valueClass = Integer.class)
	@Column(name = "QUARTER_CREATED_ON")
	protected Integer quarterCreatedOn;

	/**
	 * Biannual name when object was created (set by Entity Listener)
	 */
	@JsonIgnore
	@Filterable(valueClass = Integer.class)
	@Column(name = "BIANNUAL_CREATED_ON")
	protected Integer biannualCreatedOn;

	/**
	 * Row rank
	 */
	@Column(name = "RANK", precision = 57, scale = 30)
	protected BigDecimal rank;

	// Hibernate Search Facet Helpers - BEGIN
	@JsonIgnore
	@Searchable(lookup = false)
	@Facet(forField = BIANNUAL_CREATED_FACET)
	@Field(name = BIANNUAL_CREATED_FACET, analyze = Analyze.NO)
	public String getCreatedOnBiannualFacetValue() {
		String biannualName = null;
		try {
			if(biannualCreatedOn != null) {
				biannualName = Dates.biannualName(biannualCreatedOn);
			}
		} catch(final Throwable t) {
		}
		
		return biannualName;
	}
	
	@JsonIgnore
	@Searchable(lookup = false)
	@Facet(forField = QUARTER_CREATED_FACET)
	@Field(name = QUARTER_CREATED_FACET, analyze = Analyze.NO)
	public String getCreatedOnQuarterFacetValue() {
		return (quarterCreatedOn != null) ? Dates.quarterName(quarterCreatedOn) : null;
	}
	
	@JsonIgnore
	@Searchable(lookup = false)
	@Facet(forField = MONTH_CREATED_FACET)
	@Field(name = MONTH_CREATED_FACET, analyze = Analyze.NO)
	public String getCreatedOnMonthFacetValue() {
		return (monthCreatedOn != null) ? Dates.monthName(monthCreatedOn) : null;
	}
	// Hibernate Search Facet Helpers - END
	
	/**
	 * Default constructor, must be provided for encode/decode
	 */
	public AbstractEntity() {
		super();
	}

	@Override
	public UUID getId() {
		return id;
	}

	@Override
	public void setId(UUID id) {
		this.id = id;
	}

	@Override
	public Long getCreatedOn() {
		return createdOn;
	}

	@Override
	public void setCreatedOn(Long createdOn) {
		this.createdOn = createdOn;
	}

	@Override
	public Long getUpdatedOn() {
		return updatedOn;
	}

	@Override
	public void setUpdatedOn(Long updatedOn) {
		this.updatedOn = updatedOn;
	}

	public boolean getArchived() {
		return archived;
	}

	public void setArchived(boolean archived) {
		this.archived = archived;
	}

	public boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	@Override
	public UUID getCreatedBy() {
		return createdBy;
	}

	@Override
	public void setCreatedBy(UUID createdBy) {
		this.createdBy = createdBy;
	}

	@Override
	public UUID getUpdatedBy() {
		return updatedBy;
	}

	@Override
	public void setUpdatedBy(UUID updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Override
	public Integer getMonthUpdatedOn() {
		return monthUpdatedOn;
	}

	@Override
	public void setMonthUpdatedOn(Integer monthUpdatedOn) {
		this.monthUpdatedOn = monthUpdatedOn;
	}

	@Override
	public Integer getYearUpdatedOn() {
		return yearUpdatedOn;
	}

	@Override
	public void setYearUpdatedOn(Integer yearUpdatedOn) {
		this.yearUpdatedOn = yearUpdatedOn;
	}

	@Override
	public Integer getQuarterUpdatedOn() {
		return quarterUpdatedOn;
	}

	@Override
	public void setQuarterUpdatedOn(Integer quarterUpdatedOn) {
		this.quarterUpdatedOn = quarterUpdatedOn;
	}

	@Override
	public Integer getMonthCreatedOn() {
		return monthCreatedOn;
	}

	@Override
	public void setMonthCreatedOn(Integer monthCreatedOn) {
		this.monthCreatedOn = monthCreatedOn;
	}

	@Override
	public Integer getYearCreatedOn() {
		return yearCreatedOn;
	}

	@Override
	public void setYearCreatedOn(Integer yearCreatedOn) {
		this.yearCreatedOn = yearCreatedOn;
	}

	@Override
	public Integer getQuarterCreatedOn() {
		return quarterCreatedOn;
	}

	@Override
	public void setQuarterCreatedOn(Integer quarterCreatedOn) {
		this.quarterCreatedOn = quarterCreatedOn;
	}

	@Override
	public Integer getBiannualUpdatedOn() {
		return biannualUpdatedOn;
	}

	@Override
	public void setBiannualUpdatedOn(Integer biannualUpdatedOn) {
		this.biannualUpdatedOn = biannualUpdatedOn;
	}

	@Override
	public Integer getBiannualCreatedOn() {
		return biannualCreatedOn;
	}

	@Override
	public void setBiannualCreatedOn(Integer biannualCreatedOn) {
		this.biannualCreatedOn = biannualCreatedOn;
	}

	@Override
	public BigDecimal getRank() {
		return rank;
	}

	@Override
	public void setRank(BigDecimal rank) {
		this.rank = rank;
	}
	
	@Override
	public void setVersion(long version) {
		this.version = version;
	}
	
	@Override
	public long getVersion() {
		return version;
	}
}
