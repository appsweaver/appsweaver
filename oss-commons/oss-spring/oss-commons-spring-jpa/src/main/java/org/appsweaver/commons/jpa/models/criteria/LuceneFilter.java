/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.models.criteria;

import static org.appsweaver.commons.utilities.TypeConverter.toList;
import static org.appsweaver.commons.utilities.TypeConverter.valueOf;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.lucene.search.Query;
import org.appsweaver.commons.HibernateSearch;
import org.appsweaver.commons.models.criteria.FilterableField;
import org.appsweaver.commons.models.persistence.Entity;
import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.FilterHelper;
import org.hibernate.search.query.dsl.BooleanJunction;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author UD
 *
 * Create Lucene filter queries from filter expressions
 *
 */
public class LuceneFilter<T extends Entity> extends AbstractFilter<Entity> {
	final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	final Class<T> entityClass;
	final QueryBuilder queryBuilder;
	
	public LuceneFilter(Class<T> entityClass, FilterableField filterableField, String value, QueryBuilder queryBuilder) {
		super(filterableField, value);
		
		this.queryBuilder = queryBuilder;
		this.entityClass = entityClass;
	}

	public Query toQuery() {
		final String fieldName = filterExpressionParser.getName();
		final Class<?> fieldType = this.getType();
		final String filterOperator = filterExpressionParser.getOperator();
		
		String filterValue = filterExpressionParser.getValue();
		
		// Obtain matching context for filters
		Query query = null;
		
		switch(filterOperator) {
		case "lt": {
			if(Number.class.isAssignableFrom(fieldType)) {
				final Object valueObject = valueOf(filterValue, fieldType);
				query = queryBuilder
					.range()
					.onField(fieldName)
					.below(valueObject)
					.excludeLimit()
					.createQuery();
			} else {
				throw new IllegalArgumentException(String.format("Operator [%s] does not supported non numeric values!", filterOperator));
			}
		} break;

		case "le": {
			if(Number.class.isAssignableFrom(fieldType)) {
				final Object valueObject = valueOf(filterValue, fieldType);
				query = queryBuilder
					.range()
					.onField(fieldName)
					.below(valueObject)
					.createQuery();
			} else {
				throw new IllegalArgumentException(String.format("Operator [%s] does not supported non numeric values!", filterOperator));
			}
		} break;
		
		case "gt": {
			if(Number.class.isAssignableFrom(fieldType)) {
				final Object valueObject = valueOf(filterValue, fieldType);
				query = queryBuilder
					.range()
					.onField(fieldName)
					.above(valueObject)
					.excludeLimit()
					.createQuery();
			} else {
				throw new IllegalArgumentException(String.format("Operator [%s] does not supported non numeric values!", filterOperator));
			}
		} break;
		
		case "ge": {
			if(Number.class.isAssignableFrom(fieldType)) {
				final Object valueObject = valueOf(filterValue, fieldType);
				query = queryBuilder
					.range()
					.onField(fieldName)
					.above(valueObject)
					.createQuery();
			} else {
				throw new IllegalArgumentException(String.format("Operator [%s] does not supported non numeric values!", filterOperator));
			}
		} break;
		
		case "in":
		case "!in": {
			final List<Query> queries = Collections.newList();
			final BooleanJunction<?> bool = queryBuilder.bool();
			if(Number.class.isAssignableFrom(fieldType)) {
				final List<?> numbers = toList(filterValue, fieldType, true);
				numbers.forEach(number -> {
					final Query subQuery = queryBuilder
						.range()
						.onField(fieldName)
						.from(number)
						.to(number)
						.createQuery();
					
					queries.add(subQuery);
				});
			} else {
				List<?> valueObjects = toList(filterValue, fieldType, true);
						
				if (String.class.isAssignableFrom(fieldType)) {
					valueObjects = valueObjects.stream().map(valueObject -> {
						return FilterHelper.sanitizeFilterValue(valueObject);
					}).collect(Collectors.toList());	
				}
				
				valueObjects.forEach(valueObject -> {
					final Query subQuery = queryBuilder
						.keyword()
						.onField(fieldName)
						.matching(valueObject)
						.createQuery();
					
					queries.add(subQuery);
				});
			}

			// Factor the queries into junction
			queries.forEach(subQuery -> {
				bool.should(subQuery);
			});
			
			query = bool.createQuery();
		} break;
		
		case "eq":
		case "!eq": {
			Object valueObject = valueOf(filterValue, fieldType);
			
			if(Number.class.isAssignableFrom(fieldType)) {
				query = queryBuilder
					.range()
					.onField(fieldName)
					.from(valueObject)
					.to(valueObject)
					.createQuery();
			} else {
				if (String.class.isAssignableFrom(fieldType)) {
					valueObject = FilterHelper.sanitizeFilterValue(valueObject);
				}
				query = queryBuilder
					.keyword()
					.onField(fieldName)
					.matching(valueObject)
					.createQuery();
			}
		} break;
		
		case "null":
		case "!null": {
			query = queryBuilder
				.keyword()
				.onField(fieldName)
				.matching(HibernateSearch.NULL_VALUE)
				.createQuery();
		} break;

		case "like":
		case "!like": {
			final Object valueObject = valueOf(filterValue, fieldType);
			
			if(String.class.isAssignableFrom(fieldType)) {
				final Object sanitizedObject = FilterHelper.sanitizeFilterValue(valueObject);
				final String stringValue = String.format("*%s*",String.valueOf(sanitizedObject).toLowerCase());
				
				query = queryBuilder
					.keyword().wildcard()
					.onField(fieldName)
					.matching(stringValue)
					.createQuery();
			} else {
				query = queryBuilder
					.keyword()
					.onField(fieldName)
					.matching(valueObject)
					.createQuery();
			}
		} break;
		
		case "between":
		case "!between": {
			if(Number.class.isAssignableFrom(fieldType)) {
				final List<?> numbers = toList(filterValue, fieldType, true);
				
				final Object min = numbers.get(0);
				final Object max = numbers.get(1);
				
				query = queryBuilder
					.range()
					.onField(fieldName)
					.from(min)
					.to(max)
					.createQuery();
			} else {
				throw new IllegalArgumentException(String.format("Operator [%s] does not supported non numeric values!", filterOperator));
			}
		} break;

		default:
			throw new UnsupportedOperationException(String.format("Operator [%s] not supported yet!", filterOperator));
		}
		
		return query;
	}
	
}
