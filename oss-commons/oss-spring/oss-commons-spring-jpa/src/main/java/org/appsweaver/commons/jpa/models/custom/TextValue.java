/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.models.custom;

import javax.persistence.Column;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.EqualsAndHashCode;

/**
 *
 * @author UD
 *
 */
@DynamicUpdate
@javax.persistence.Entity
@Table(name = "CUSTOM_TEXT_VALUES")
@EqualsAndHashCode(callSuper = true)
public class TextValue extends Value<String> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8711472321879808180L;
	
	public static final int LENGTH = 65535;

	protected String value;
	
	public TextValue() {
		super();
	}
	
	public TextValue(String fieldName, String value) {
		super();
		
		setFieldName(fieldName);
		setValue(value);
	}
	
	public TextValue(CustomDataType type, String value) {
		super(type);
		
		setValue(value);
	}

	@Override
	@Column(name = "VALUE", length = LENGTH)
	public String getValue() {
		return value;
	}

	@Override
	public void setValue(String value) {
		this.value = value;
	}
}
