/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.json.converters;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.appsweaver.commons.jpa.repositories.CustomPrimaryRepository;
import org.appsweaver.commons.models.persistence.PrimaryEntity;
import org.appsweaver.commons.utilities.Collections;

import com.fasterxml.jackson.databind.util.StdConverter;

/**
 * 
 * @author UD
 * 
 */
public abstract class IdToEntityCollectionConverter<T extends PrimaryEntity> extends StdConverter<Collection<UUID>, Collection<T>> {
	@Override
	public Collection<T> convert(Collection<UUID> collection) {
		final Set<T> entities = Collections.newHashSet();

		collection.forEach(id -> {
			final Optional<T> entity = getRepository().findById(id);
			if(entity.isPresent()) {
				// Add to set only it is present
				entities.add(entity.get());
			}
		});

		return entities;
	}

	public abstract Class<T> getEntityClass();

	public abstract CustomPrimaryRepository<T> getRepository();
}
