/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.models.search;

import org.hibernate.search.query.facet.Facet;
import org.hibernate.search.query.facet.RangeFacet;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 
 * @author UD
 * 
 */
public class FacetMenuItem {
	private boolean selected;
	private final int index;
	private final String facetingName;
	private final Facet facet;

	public FacetMenuItem(Facet facet, boolean selected, int index) {
		this.selected = selected;
		this.facet = facet;
		this.facetingName = facet.getFacetingName();
		this.index = index;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public int getCount() {
		return facet.getCount();
	}
	
	@JsonIgnore
	public Facet getFacet() {
		return facet;
	}

	public String getValue() {
		String value;
		if (facet instanceof RangeFacet) {
			@SuppressWarnings("unchecked") // Should be RangeFacet always
			final RangeFacet<Number> rangeFacet = ((RangeFacet<Number>) facet);
			value = String.format("between(%s, %s)", String.valueOf(rangeFacet.getMin()), String.valueOf(rangeFacet.getMax()));
		} else {
			value = facet.getValue();
		}
		return value;
	}

	public int getIndex() {
		return index;
	}

	@JsonIgnore
	public String getFacetingName() {
		return facetingName;
	}
	
	public String toString() {
		return String.format("[facetName=%s, fieldName=%s, value=%s, selected=%s, count=%s]",
			facet.getFacetingName(), facet.getFieldName(), facet.getValue(),
			selected, getCount()
		);
	}
}
