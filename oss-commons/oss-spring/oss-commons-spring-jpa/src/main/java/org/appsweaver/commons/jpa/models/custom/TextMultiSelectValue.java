/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.models.custom;

import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import org.appsweaver.commons.utilities.Collections;
import org.hibernate.annotations.DynamicUpdate;

import lombok.EqualsAndHashCode;

/**
 *
 * @author UD
 *
 */
@DynamicUpdate
@javax.persistence.Entity
@Table(name = "CUSTOM_TEXT_MULTI_SELECT_VALUES")
@EqualsAndHashCode(callSuper = true)
public class TextMultiSelectValue extends Value<List<String>> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6147737138756259683L;
	
	@ElementCollection
    @CollectionTable(name = "CUSTOM_SELECTED_TEXT_MULTI_VALUES", joinColumns = @JoinColumn(name = "MULTI_SELECT_ID"))
	private List<String> value = Collections.newList();
	
	public TextMultiSelectValue() {
		super();
	}
	
	public TextMultiSelectValue(String fieldName, List<String> value) {
		super();
		
		setFieldName(fieldName);
		setValue(value);
	}
	
	public TextMultiSelectValue(CustomDataType type, List<String> value) {
		super(type);
		
		setValue(value);
	}

	@Override
	public List<String> getValue() {
		return value;
	}

	@Override
	public void setValue(List<String> value) {
		this.value = value;
	}
}
