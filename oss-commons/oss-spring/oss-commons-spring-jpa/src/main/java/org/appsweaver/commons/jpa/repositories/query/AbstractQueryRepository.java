/*
 *
 * Copyright (c) 2016 appsweaver.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.appsweaver.commons.jpa.repositories.query;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Stream;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.appsweaver.commons.jpa.components.QueryHibernateHelper;
import org.appsweaver.commons.jpa.models.criteria.ObjectDatabaseFilter;
import org.appsweaver.commons.jpa.types.FilterConjunction;
import org.appsweaver.commons.models.persistence.QueryEntity;
import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Stringizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.NoRepositoryBean;

/**
 *
 * @author UD
 *
 */
@NoRepositoryBean
public abstract class AbstractQueryRepository<T extends QueryEntity> implements CustomQueryRepository<T> {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	final static Character SQL_QUOTE_CHARACTER = '\'';

	@Value("${webservice.query-repository.schema}")
	String schemaName;


	@Autowired(required = false)
	QueryHibernateHelper queryHibernateHelper;

	@Override
	public QueryHibernateHelper getHibernateHelper() {
		return queryHibernateHelper;
	}

	public String getSchemaName() {
		return schemaName;
	}

	abstract public Class<T> getEntityClass();

	String getEntityName() {
		final String simpleName = getEntityClass().getSimpleName();
		return simpleName;
	}

	@Override
	public long count() throws SQLException {
		return getHibernateHelper().count(getEntityClass(), Optional.empty());
	}

	@Override
	public void flush() throws SQLException {
		getHibernateHelper().flush();
	}

	@Override
	public T getOne(UUID id) {
		try {
			final T saved = getHibernateHelper().findById(getEntityClass(), id);
			return saved;
		} catch (final Throwable t) {
			logger.error("Failed to fetch record [class={}, id={}]", getEntityClass(), id, t);
			return null;
		}
	}

	@Override
	public void deleteById(UUID id) {
		try {
			final T entity = getOne(id);
			delete(entity);
		} catch (final Throwable t) {
			logger.error("Failed to delete record [class={}, id{}]", getEntityClass(), id, t);
		}
	}

	@Override
	public void delete(T entity) {
		try {
			getHibernateHelper().delete(entity);
		} catch (final Throwable t) {
			logger.error("Failed to delete record [class={}, id{}]", getEntityClass(), entity.getId(), t);
		}
	}
	
	@Override
	public void delete(UUID id) {
		try {
			getHibernateHelper().delete(getEntityClass(), id);
		} catch (final Throwable t) {
			logger.error("Failed to delete record [class={}, id{}]", getEntityClass(), id, t);
		}
	}


	@Override
	public T save(T entity) {
		try {
			final T saved = getHibernateHelper().save(entity);
			return saved;
		} catch (final Throwable t) {
			logger.error("Failed to save record [class={}]", getEntityClass(), t);
			return null;
		}
	}

	@Deprecated
	public Stream<T> streamAll() throws SQLException {
		/*
		 * Scrolling and Streaming depends on Criteria API
		 * Both are not supported yet by OGM
		 */
		return getHibernateHelper().streamAll(getEntityClass());
	}

	@Override
	public List<T> findAll() {
		try {
			return getHibernateHelper().findAll(getEntityClass());
		} catch (final Throwable t) {
			logger.error("Failed to fetch all records [class={}]", getEntityClass(), t);
		}

		return Collections.newList();
	}

	@Override
	public BigDecimal getMinRank() {
		final EntityManager entityManager = getHibernateHelper().createEntityManager();
		try {
			final String qlString = String.format("SELECT MIN(rank) FROM %s", getEntityName());
			final Query query = entityManager.createQuery(qlString);

			final BigDecimal rank = (BigDecimal)query.getSingleResult();
			return rank;
		} finally {
			// Close Entity Manager
			entityManager.close();
		}
	}

	@Override
	public BigDecimal getMaxRank() {
		final EntityManager entityManager = getHibernateHelper().createEntityManager();
		try {
			final String qlString = String.format("SELECT MAX(rank) FROM %s", getEntityName());
			final Query query = entityManager.createQuery(qlString);

			final BigDecimal rank = (BigDecimal)query.getSingleResult();
			return rank;
		} finally {
			// Close Entity Manager
			entityManager.close();
		}
	}

	@Override
	public boolean existsById(UUID id) {
		try {
			final T entity = getOne(id);
			return entity != null && id == entity.getId();
		} catch(final Throwable t) {
		}

		return false;
	}

	@Override
	public T saveAndFlush(T entity) {
		try {
			final T saved = getHibernateHelper().save(entity);
			getHibernateHelper().flush();

			return saved;
		} catch (final Throwable t) {
			logger.error("Failed to save and flush record [class={}]", getEntityClass(), t);
			return null;
		}
	}

	@Override
	public Optional<T> findById(UUID id) {
		final T entity = getOne(id);
		return Optional.ofNullable(entity);
	}

	@Override
	public void deleteAll() {
		final EntityManager entityManager = getHibernateHelper().createEntityManager();
		try {
			// Begin Transaction
			entityManager.getTransaction().begin();

			/*
			 * Expensive; but this is what works for now.  As OGM evolves, perhaps such operations
			 * will work efficiently.
			 */
			findAll().forEach(item -> {
				try {
					final T existing = entityManager.find(getEntityClass(), item.getId());
					entityManager.remove(existing);
				} catch(final Throwable t) {
				}
			});

			entityManager.getTransaction().commit();
			// End Transaction

		} catch(final Throwable t){
			// Roll Back Transaction
			entityManager.getTransaction().rollback();
		} finally {
			// Close Entity Manager
			entityManager.close();
		}

		/*
		final EntityManager entityManager = getHibernateHelper().createEntityManager();
		try {
			// Begin Transaction
			entityManager.getTransaction().begin();

			final String qlString = String.format("db.%s.deleteMany('{}');", getSchemaName(), getEntityName().toUpperCase());
			final Query query = entityManager.createNativeQuery(qlString);

			// final String qlString = String.format("DELETE FROM %s", getEntityName());
			// final Query query = entityManager.createQuery(qlString);
			query.setFlushMode(FlushModeType.AUTO);

			query.executeUpdate();
			entityManager.getTransaction().commit();
			// End Transaction

		} catch(Throwable t){
			// Roll Back Transaction
			entityManager.getTransaction().rollback();
		} finally {
			// Close Entity Manager
			entityManager.close();
		}
		// */
	}

	@Override
	public void deleteAll(Iterable<? extends T> entities) {
		final EntityManager entityManager = getHibernateHelper().createEntityManager();
		try {
			// Begin Transaction
			entityManager.getTransaction().begin();
			entities.forEach(entity -> {
				entityManager.remove(entity);
			});
			entityManager.getTransaction().commit();
			// End Transaction

		} catch(final Throwable t){
			// Roll Back Transaction
			entityManager.getTransaction().rollback();
		} finally {
			// Close Entity Manager
			entityManager.close();
		}
	}

	@Override
	public Iterable<T> saveAll(Iterable<T> entities) {
		final Set<T> collection = Collections.newHashSet();

		final EntityManager entityManager = getHibernateHelper().createEntityManager();
		try {
			entities.forEach(entity -> {
				final T savedEntity = save(entity);
				collection.add(savedEntity);
			});

		} finally {
			// Close Entity Manager
			entityManager.close();
		}

		// Returned saved entities
		return collection;
	}

	@Override
	public Iterable<T> findAllById(Iterable<UUID> ids) {
		final EntityManager entityManager = getHibernateHelper().createEntityManager();
		try {
			final String idsText = Stringizer.toCSVString(ids, Optional.of(SQL_QUOTE_CHARACTER));

			final String qlString = String.format("FROM %s WHERE id IN (%s)", getEntityName(), idsText);
			final Query query = entityManager.createQuery(qlString);

			@SuppressWarnings("unchecked")
			final List<T> resultList = query.getResultList();

			return resultList;
		}  catch(final NoResultException e) {
			/*
			 *  NO-OP: Results were not found for the given conditions
			 */
			return Collections.newList();
		} finally {
			// Close Entity Manager
			entityManager.close();
		}
	}

	@Override
	public void deleteInBatch(Iterable<T> entities) {
		deleteAll(entities);
	}

	@Override
	public Iterable<T> findAll(Sort sort) {
		final EntityManager entityManager = getHibernateHelper().createEntityManager();
		try {
		final ObjectDatabaseFilter<T> databaseFilter = new ObjectDatabaseFilter<>(
				getEntityClass(),			// Entity Class
				Optional.empty(),			// Specifications
				Optional.ofNullable(sort),	// Sort
				Optional.empty(),			// Page Number
				Optional.empty(),			// Page Size
				Optional.empty(),			// Conjunction
				Optional.empty()			// Since
			);
				
			final Query query = databaseFilter.toQuery(
				entityManager, 
				Optional.empty()	// SQL Statement
			);
				
			@SuppressWarnings("unchecked")
			final List<T> resultList = query.getResultList();

			return resultList;
		}  catch(final NoResultException e) {
			/*
			 *  NO-OP: Results were not found for the given conditions
			 */
			return Collections.newList();
		} finally {
			// Close Entity Manager
			entityManager.close();
		}
	}

	public List<T> findAll(Pageable pageable) throws SQLException {
		return getHibernateHelper().findAll(
			getEntityClass(),								// Entity class
			Optional.empty(),								// Specification
			Optional.empty(),								// Sort
			Optional.of(pageable.getPageNumber()),			// Page Number
			Optional.of(pageable.getPageSize()),			// Page Size
			Optional.ofNullable(FilterConjunction.AND),		// Conjunction
			Optional.empty()								// Since creation or update epoch
		);
	}

	@Override
	public Page<T> findAll(List<Specification<T>> specifications, Pageable pageable, FilterConjunction conjunction, Long since) throws SQLException{
		return getHibernateHelper().findAll(
			getEntityClass(), 								// Entity class
			Optional.ofNullable(specifications),			// Specification
			Optional.ofNullable(pageable),					// Pageable
			Optional.empty(),								// Sort
			Optional.ofNullable(conjunction),				// Conjunction
			Optional.empty()								// Since creation or update epoch
		);
	}

	@Override
	public Page<T> findAll(Pageable pageable, Long since) throws SQLException {
		return getHibernateHelper().findAll(
			getEntityClass(), 								// Entity class
			Optional.empty(),								// Specification
			Optional.ofNullable(pageable),					// Pageable
			Optional.empty(),								// Sort
			Optional.ofNullable(FilterConjunction.AND),		// Conjunction
			Optional.ofNullable(since)						// Since creation or update epoch
		);
	}

	public Page<T> findAll(List<Specification<T>> specifications, Pageable pageable, FilterConjunction conjunction) throws SQLException {
		return getHibernateHelper().findAll(
			getEntityClass(), 								// Entity class
			Optional.ofNullable(specifications), 			// Specification
			Optional.ofNullable(pageable),					// Pageable
			Optional.empty(),								// Sort
			Optional.ofNullable(conjunction),				// Conjunction
			Optional.empty()								// Since creation or update epoch
		);
	}
}
