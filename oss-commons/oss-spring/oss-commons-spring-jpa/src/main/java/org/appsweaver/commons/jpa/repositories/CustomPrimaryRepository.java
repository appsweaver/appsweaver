/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.repositories;

import java.util.List;
import java.util.UUID;

import org.appsweaver.commons.jpa.types.FilterConjunction;
import org.appsweaver.commons.models.persistence.PrimaryEntity;
import org.appsweaver.commons.spring.utilities.FrameworkHelper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * 
 * @author UD
 * 
 * @param <T> - entity class type for specialization
 * 
 */
@NoRepositoryBean
public interface CustomPrimaryRepository<T extends PrimaryEntity> extends 
	JpaRepository<T, UUID>, 
	JpaSpecificationExecutor<T>, 
	RankableRepository<T>
{
	default Page<T> findAll(List<Specification<T>> specifications, Pageable pageable, FilterConjunction conjunction, Long since) {
		Specification<T> specification = null;
		if(specifications != null && specifications.size() >= 1) {
			specification = specifications.get(0);

			for (int index = 1; index < specifications.size(); index++) {
				if (conjunction == FilterConjunction.OR) {
					specification = Specification.where(specification).or(specifications.get(index));
				} else if(conjunction == FilterConjunction.AND) {
					specification = Specification.where(specification).and(specifications.get(index));
				}
			}
		}
		
		if(since != null) {
			final Specification<T> sinceSpecification = FrameworkHelper.sinceSpecification(since);
			if(specification != null) {
				specification = Specification.where(specification).and(sinceSpecification);
			} else {
				specification = sinceSpecification;
			}
		}
		
		return findAll(specification, pageable);
	}
	
	default Page<T> findAll(Pageable pageable, Long since) {
		if(since != null) {
			final Specification<T> specification = FrameworkHelper.sinceSpecification(since);
			return findAll(specification, pageable);
		}
		
		return findAll(pageable);
	}
}
