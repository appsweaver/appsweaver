/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.models.web;

import java.util.List;
import java.util.Map;

import org.appsweaver.commons.jpa.models.criteria.SortOrder;
import org.appsweaver.commons.jpa.models.search.TextSearchType;
import org.appsweaver.commons.jpa.types.FilterConjunction;
import org.appsweaver.commons.json.views.SelectedFacet;
import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Jsonifier;
import org.appsweaver.commons.utilities.Stringizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort.Direction;

/**
 * 
 * @author UD
 *
 */
public class WebRequest {
	final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	final Long since;
	final String body;
	final Integer pageNumber;
	final Integer pageSize;

	final TextSearchType textSearchType;
	final FilterConjunction filterConjunction;

	final List<SelectedFacet> selectedFacets;
	final List<SortOrder> sortOrders;
	final Map<String, String> filterMap;
	
	private WebRequest(
		Long since,
		String body,
		Integer pageNumber,
		Integer pageSize,

		TextSearchType textSearchType,
		FilterConjunction filterConjunction,

		List<SelectedFacet> selectedFacets,
		List<SortOrder> sortOrders,
		Map<String, String> filterMap
	) {
		this.since = since;
		this.body = body;
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
		
		this.textSearchType = textSearchType;
		this.filterConjunction = filterConjunction;
		
		this.selectedFacets = selectedFacets;
		this.sortOrders = sortOrders;
		this.filterMap = filterMap;
	}
	
	public Logger getLogger() {
		return logger;
	}

	public Long getSince() {
		return since;
	}

	public String getBody() {
		return body;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public TextSearchType getTextSearchType() {
		return textSearchType;
	}

	public FilterConjunction getFilterConjunction() {
		return filterConjunction;
	}

	public List<SelectedFacet> getSelectedFacets() {
		return selectedFacets;
	}

	public List<SortOrder> getSortOrders() {
		return sortOrders;
	}

	public Map<String, String> getFilterMap() {
		return filterMap;
	}

	public static class WebRequestBuilder {
		final Logger logger = LoggerFactory.getLogger(this.getClass());
		
		TextSearchType textSearchType = TextSearchType.WILDCARD;
		Integer pageNumber = 0;
		Integer pageSize = 25;
		String body;
		Long since;

		List<SelectedFacet> selectedFacets = Collections.newList();
		List<SortOrder> sortOrders = Collections.newList();
		FilterConjunction filterConjunction = FilterConjunction.AND;
		Map<String, String> filterMap = Collections.newHashMap();
		
		public WebRequestBuilder() {
		}
		
		public WebRequest build() {
			final WebRequest webRequest = new WebRequest(
				since,
				body,
				pageNumber,
				pageSize,

				textSearchType,
				filterConjunction,

				selectedFacets,
				sortOrders,
				filterMap
			);
			
			return webRequest;
		}
		
		public WebRequestBuilder pageSize(Integer pageSize) {
			this.pageSize = pageSize;
			return this;
		}
		
		public WebRequestBuilder pageNumber(Integer pageNumber) {
			this.pageNumber = pageNumber;
			return this;
		}
		
		public WebRequestBuilder body(String body) {
			this.body = body;
			return this;
		}
		
		public WebRequestBuilder body(Long since) {
			this.since = since;
			return this;
		}
		
		public WebRequestBuilder textSearchType(TextSearchType textSearchType) {
			this.textSearchType = textSearchType;
			return this;
		}
		
		public WebRequestBuilder selectedFacet(String selectedFacets) {
			if(!Stringizer.isEmpty(selectedFacets)) {
				final List<SelectedFacet> selectedFacetList = Jsonifier.toList(selectedFacets, SelectedFacet.class);
				this.selectedFacets.addAll(selectedFacetList);
			}
			
			return this;
		}
		
		public WebRequestBuilder selectedFacet(SelectedFacet selectedFacet) {
			selectedFacets.add(selectedFacet);
			return this;
		}
		
		public WebRequestBuilder filter(String filters) {
			final Map<String, String> filterMap = Jsonifier.toMap(filters);
			this.filterMap.putAll(filterMap);
			return this;
		}
		
		public WebRequestBuilder filter(String fieldName, String expression) {
			filterMap.put(fieldName, expression);
			return this;
		}
		
		public WebRequestBuilder sortOrder(String fieldName, Direction direction) {
			sortOrders.add(new SortOrder(fieldName, direction));
			return this;
		}
		
		public WebRequestBuilder sortOrder(String sortOrders) {
			if(!Stringizer.isEmpty(sortOrders)) {
				final List<SortOrder> sortOrderList = Jsonifier.toList(sortOrders, SortOrder.class);
				this.sortOrders.addAll(sortOrderList);
			}
			
			return this;
		}
		
		public WebRequestBuilder filterConjunction(FilterConjunction filterConjunction) {
			this.filterConjunction = filterConjunction;
			return this;
		}

		public WebRequestBuilder since(Long since) {
			this.since = since;
			return this;
		}
	}
}
