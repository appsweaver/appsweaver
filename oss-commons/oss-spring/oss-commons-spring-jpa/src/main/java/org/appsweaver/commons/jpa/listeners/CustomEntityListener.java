/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.listeners;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.appsweaver.commons.models.persistence.PrimaryEntity;
import org.appsweaver.commons.spring.utilities.IDHelper;
import org.appsweaver.commons.utilities.Dates;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author UD
 *
 */
public class CustomEntityListener {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	public CustomEntityListener() {
		super();
	}

	@PrePersist
	public void onPreCreate(final Object target) {
		// Make entity related operations
		if (target instanceof PrimaryEntity) {
			final PrimaryEntity entity = (PrimaryEntity) target;

			updateCreatedTimeFacets(entity); // updated created time facets
			updateModifiedTimeFacets(entity); // updated modified time facets
			
			IDHelper.resolveId(entity); // ID must be resolved for create
		}
	}

	@PreUpdate
	public void onPreUpdate(final Object target) {
		// Make entity related operations
		if (target instanceof PrimaryEntity) {
			final PrimaryEntity entity = (PrimaryEntity) target;

			updateModifiedTimeFacets(entity); // updated modified time facets
		}
	}

	void updateCreatedTimeFacets(final PrimaryEntity entity) {
		final Long timestamp = entity != null ? entity.getCreatedOn() : null;
		
		if(timestamp != null) {
			entity.setMonthCreatedOn(Dates.month(timestamp).getValue());
			entity.setYearCreatedOn(Dates.yearValue(timestamp));
			entity.setQuarterCreatedOn(Dates.quarterValue(timestamp));
			entity.setBiannualCreatedOn(Dates.biannualValue(timestamp));
		}
	}

	void updateModifiedTimeFacets(final PrimaryEntity entity) {
		final Long timestamp = entity != null ?  entity.getUpdatedOn() : null;
		
		if(timestamp != null) {
			entity.setMonthUpdatedOn(Dates.month(timestamp).getValue());
			entity.setYearUpdatedOn(Dates.yearValue(timestamp));
			entity.setQuarterUpdatedOn(Dates.quarterValue(timestamp));
			entity.setBiannualUpdatedOn(Dates.biannualValue(timestamp));
		}
	}
}
