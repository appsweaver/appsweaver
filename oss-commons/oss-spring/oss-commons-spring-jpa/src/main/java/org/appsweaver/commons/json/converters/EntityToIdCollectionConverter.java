/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.json.converters;

import java.util.Collection;
import java.util.Set;
import java.util.UUID;

import org.appsweaver.commons.models.persistence.Entity;
import org.appsweaver.commons.utilities.Collections;

import com.fasterxml.jackson.databind.util.StdConverter;

/**
 * 
 * @author UD
 * 
 */
public class EntityToIdCollectionConverter<T extends Entity> extends StdConverter<Collection<T>, Collection<UUID>> {
	@Override
	public Collection<UUID> convert(Collection<T> collection) {
		final Set<UUID> ids = Collections.newHashSet();
		
		collection.forEach(item -> {
			ids.add(item.getId());
		});
		
		return ids;
	}
}
