/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.spring.utilities;

import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.lucene.search.Query;
import org.appsweaver.commons.annotations.Extendable;
import org.appsweaver.commons.annotations.Rankable;
import org.appsweaver.commons.annotations.processors.FilterableAnnotationProcessor;
import org.appsweaver.commons.annotations.processors.SearchableAnnotationProcessor;
import org.appsweaver.commons.jpa.models.AbstractEntity;
import org.appsweaver.commons.jpa.models.criteria.LuceneFilter;
import org.appsweaver.commons.jpa.models.criteria.RelationalDatabaseFilter;
import org.appsweaver.commons.jpa.models.criteria.SortOrder;
import org.appsweaver.commons.jpa.models.criteria.SortSpecification;
import org.appsweaver.commons.models.FieldMetadata;
import org.appsweaver.commons.models.criteria.FilterableField;
import org.appsweaver.commons.models.criteria.SearchableField;
import org.appsweaver.commons.models.persistence.Entity;
import org.appsweaver.commons.models.persistence.PrimaryEntity;
import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Ruminator;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.jpa.domain.Specification;

/**
 * 
 * @author UD
 * 
 */
public class FrameworkHelper {
	final static Logger logger = LoggerFactory.getLogger(FrameworkHelper.class);
	
	static final String AWE_ASTERIX = "__AWE_ASTERIX__";
	static final String AWE_QUESTION = "__AWE_QUESTION__";

	/**
	 * Get properties of a class
	 * 
	 * @param <T> type of object
	 * @param entityClass name of entity class
	 * @return list of field metadata
	 */
	public static <T extends Entity> List<FieldMetadata> getProperties(Class<T> entityClass) {
		final Collection<Field> fields = Ruminator.getAllNonStaticFields(entityClass);
		if(fields != null && fields.size() > 0) {
			final List<FieldMetadata> properties = Collections.newList();
			
			for(Field field : fields) {
				final FieldMetadata fieldMetadata = new FieldMetadata();
				fieldMetadata.name = field.getName();
				fieldMetadata.type = field.getType().getCanonicalName();
				properties.add(fieldMetadata);
			}
			
			return properties;
		}

		return null;
	}
	
	/**
	 * Determine if input entity class is annotated for ranking
	 *
	 * @param <T> type of object
	 * @param entityClass entity class
	 * @return true if ranking is allowed else false
	 */
	public static <T extends Entity> boolean isRankable(Class<T> entityClass) {
		try {
			if(Ruminator.isAnnotatedWith(entityClass, Rankable.class)) {
				final Rankable rankable = entityClass.getAnnotation(Rankable.class);
				if(rankable.enabled()) {
					return true;
				}
			}
		} catch (final Throwable t) {
		}

		return false;
	}
	
	/**
	 * Determine if input entity class is annotated to support custom fields and values
	 *
	 * @param <T> type of object
	 * @param entityClass entity class
	 * @return true class supports custom fields and values, false otherwise
	 */
	public static <T extends Entity> boolean isExtendable(Class<T> entityClass) {
		try {
			if(Ruminator.isAnnotatedWith(entityClass, Extendable.class)) {
				final Extendable extendable = entityClass.getAnnotation(Extendable.class);
				if(extendable.enabled()) {
					return true;
				}
			}
		} catch (final Throwable t) {
		}

		return false;
	}
	
	/**
	 * Obtain a list of searchable fields
	 * 
	 * @param <T> type of object
	 * @param entityClass entity class name
	 * @param lookup flag to include lookup fields (used in onField)
	 * @return list of unique fields
	 * @throws SQLException on any errors
	 */
	public static <T extends Entity> Set<SearchableField> getAllSearchableFields(Class<T> entityClass, Optional<Boolean> lookup) throws SQLException {
		try {
			/*
			 * Process annotations
			 */
			final SearchableAnnotationProcessor searchableAnnotationProcessor = new SearchableAnnotationProcessor();
			searchableAnnotationProcessor.process(entityClass);

			final Set<SearchableField> set = searchableAnnotationProcessor.getFields(entityClass);
			
			if(lookup.isPresent()) {
				final Boolean flag = lookup.get();
				final Set<SearchableField> filtered = set.stream().filter(
					item -> (item.isEnabled() && item.isLookup() == flag)
				).collect(Collectors.toSet());
				
				return filtered;
			}
			
			// return all but only enabled
			final Set<SearchableField> filtered = set.stream().filter(
					item -> item.isEnabled()
				).collect(Collectors.toSet());
			return filtered;
		} catch(final Throwable t) {
			throw new SQLException(String.format("Unable to get searchable attributes [class=%s]", entityClass.getName()), t);
		}
	}
	
	
	public static <T extends Entity> Map<String, Object> getMetadata(Class<T> entityClass) throws SQLException {
		final Map<String, Object> metadata = Collections.newHashMap();

		final List<FieldMetadata> properties = FrameworkHelper.getProperties(entityClass);
		metadata.put("properties", properties);

		final Map<String, FilterableField> filterableFields = getFilterableFields(entityClass);
		metadata.put("filterable", filterableFields.values());
		
		// Show only fields allowed to be looked up during query
		final Set<SearchableField> searchableFields = getAllSearchableFields(entityClass, Optional.of(true));
		final Set<SearchableField> filteredSearchableFields = searchableFields.stream().filter(searchableField -> searchableField.isLookup()).collect(Collectors.toSet());
		metadata.put("searchable", Arrays.asList(filteredSearchableFields));

		return metadata;
	}
	
	/**
	 * Obtain entity specific attributes that drive filtering of records
	 *
	 * @param clazz - specialized type
	 * @param <T> This is the type parameter
	 * @return map of field name and field definition
	 * @throws SQLException if SQL exception occurs
	 */
	public static <T extends Entity> Map<String, FilterableField> getFilterableFields(Class<T> clazz) throws SQLException {
		try {
			/*
			 * Process annotations
			 */
			final FilterableAnnotationProcessor filterableAnnotationProcessor = new FilterableAnnotationProcessor();
			filterableAnnotationProcessor.process(clazz);

			final Map<String, FilterableField> map = filterableAnnotationProcessor.getFields(clazz);
			return map;
		} catch(Throwable t) {
			throw new SQLException(String.format("Unable to get filterable attributes [class=%s]", clazz.getName()), t);
		}
	}
	
	/**
	 * Create specification from filters and sorts
	 *
	 * @param <T> This is the type parameter
	 * @param clazz - type specialization
	 * @param filters - map of filter name, value pairs
	 * @return list of specifications
	 * @throws Throwable on any errors
	 */
	public static <T extends Entity> List<Specification<T>> toCriteria(Class<T> clazz, Map<String, String> filters) throws Throwable {
		final List<Specification<T>> specifications = Collections.newList();

		if(filters != null && !filters.isEmpty()) {
			final Map<String, FilterableField> fields = FrameworkHelper.getFilterableFields(clazz);

			filters.forEach((attribute, value) -> {
				final FilterableField filterableField = fields.get(attribute);
				
				if(filterableField != null) {
					final RelationalDatabaseFilter<T> databaseFilter = new RelationalDatabaseFilter<>(filterableField, value);
					specifications.add(databaseFilter);
					logger.debug("Enabling filter on field [attribute={}, value={}, class={}]", attribute, value, clazz);
				}
				else {
					logger.warn("Ignoring filter on unsupported field [attribute={}, value={}, class={}]", attribute, value, clazz);
				}
			});
		}

		return specifications;
	}
	
	/**
	 * Create Lucene Query from filters and sorts
	 *
	 * @param <T> This is the type parameter
	 * @param clazz - type specialization
	 * @param filters - map of filter name, value pairs
	 * @return list of specifications
	 * @throws Throwable on any errors
	 */
	public static <T extends Entity> Map<String, Query> toQuery(Class<T> clazz, Map<String, String> filters, QueryBuilder queryBuilder) throws Throwable {
		final Map<String, Query> queries = Collections.newHashMap();

		if(filters != null && !filters.isEmpty()) {
			final Map<String, FilterableField> fields = FrameworkHelper.getFilterableFields(clazz);

			filters.forEach((attribute, value) -> {
				final FilterableField filterableField = fields.get(attribute);
				
				if(filterableField != null) {
					final LuceneFilter<T> luceneFilter = new LuceneFilter<>(clazz, filterableField, value, queryBuilder);
					final Query query = luceneFilter.toQuery();
					
					if(query != null) {
						logger.debug("Enabling filter on field [attribute={}, value={}, class={}]", attribute, value, clazz);
						queries.put(attribute, query);
					}
				}
				else {
					logger.warn("Ignoring filter on unsupported field [attribute={}, value={}, class={}]", attribute, value, clazz);
				}
			});
		}

		return queries;
	}
	
	/**
	 * Convert given sort values into JPA sort order
	 *
	 * @param <T> type of object
	 * @param sortValues - list of sort field name and orders
	 * @return list of orders
	 */
	public static <T extends AbstractEntity> List<Order> toSortOrders(List<SortOrder> sortValues) {
		final  List<Order> sortOrders = Collections.newList();

		if(sortValues != null && sortValues.size() > 0) {
			for (SortOrder sortValue : sortValues) {
				final SortSpecification<T> specification = new SortSpecification<>(sortValue);
				sortOrders.add(specification.toOrder());
			}
		} else {
			// Sort by created on time stamp
			sortOrders.add(new org.springframework.data.domain.Sort.Order(Direction.DESC, "createdOn"));
		}

		return sortOrders;
	}

	public static <T extends PrimaryEntity> Specification<T> sinceSpecification(Long since) {
		final String expression = String.format("ge(%d)", since);
		RelationalDatabaseFilter<T> createdOnSpecification = RelationalDatabaseFilter.newDatabaseFilter("createdOn", "createdOn", expression, Long.class); 
		RelationalDatabaseFilter<T> updatedOnSpecification = RelationalDatabaseFilter.newDatabaseFilter("updatedOn", "updatedOn", expression, Long.class); 

		final Specification<T> specification = Specification.where(createdOnSpecification).or(updatedOnSpecification);
		return specification;
	}
	
	public static String escapeLuceneQuery(String text) {
		return escapeLuceneQuery(text, true);
	}
	
	public static String escapeLuceneQuery(String text, boolean preserveWild) {
		if(preserveWild) {
			text = text.replaceAll("\\*", AWE_ASTERIX).replaceAll("\\?", AWE_QUESTION);
		}
		
		final StringBuilder stringBuilder = new StringBuilder();
		for (int index = 0; index < text.length(); index++) {
			final char charAt = text.charAt(index);
			
			// These characters are part of the query syntax and must be escaped
			if (
				charAt == '\\' || charAt == '+' || charAt == '-' || charAt == '!' || charAt == '(' || charAt == ')' || charAt == ':'
				|| charAt == '^' || charAt == '[' || charAt == ']' || charAt == '\"' || charAt == '{' || charAt == '}' || charAt == '~'
				|| charAt == '|' || charAt == '&' || charAt == '/'
			) {
				stringBuilder.append('\\');
			}
			
			if(!preserveWild && charAt == '*' || charAt == '?') {
				stringBuilder.append('\\');
			}
			
			stringBuilder.append(charAt);
		}
		
		String escaped = stringBuilder.toString();
		if(preserveWild) {
			escaped = escaped.replaceAll(AWE_ASTERIX, "*").replaceAll(AWE_QUESTION, "?");
		}
		
		return escaped;
	}
}
