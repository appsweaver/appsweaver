/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.spring.utilities;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.appsweaver.commons.SystemConstants;
import org.appsweaver.commons.annotations.IdGenerationPolicy;
import org.appsweaver.commons.jpa.models.criteria.SortOrder;
import org.appsweaver.commons.models.persistence.Entity;
import org.appsweaver.commons.utilities.Randomizer;
import org.appsweaver.commons.utilities.Ruminator;
import org.appsweaver.commons.utilities.Stringizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author UD
 *
 */
public class IDHelper {
	final static Logger logger = LoggerFactory.getLogger(IDHelper.class);

	/**
	 * Revolve obfuscated id wherever applicable
	 *
	 * @param filters - map of filter name, value pairs
	 * @param sortOrders - list of sort orders
	 */
	public static void resolveObfuscatedId(Map<String, String> filters, List<SortOrder> sortOrders) {
		// Begin: map humanId as id - human id is transient
		if(filters != null && !filters.isEmpty()) {
			if(filters.containsKey(SystemConstants.OBFUSCATED_ID)) {
				final String value = filters.remove(SystemConstants.OBFUSCATED_ID);
				filters.put(SystemConstants.ENTITY_ID, value);
			}

			if(logger.isTraceEnabled()) {
				filters.forEach((k, v) -> {
					logger.trace("Query filter [key={}, value={}]", k, v);
				});
			}
		}

		if(sortOrders != null && !sortOrders.isEmpty()) {
			sortOrders.forEach(sortValue -> {
				if(sortValue.getField().equals(SystemConstants.OBFUSCATED_ID)) {
					sortValue.setField(SystemConstants.ENTITY_ID);
				}

				logger.trace("Sort filter [field={}, order={}]",sortValue.getField(), sortValue.getOrder());
			});
		}
		// End: map humanId as id - human id is transient
	}
	
	
	static String[] getIdGenerationPolicyFieldNames(Class<? extends Entity> entityClass) {
		try {
			final Class<? extends Annotation> annotationClass = IdGenerationPolicy.class;

			if (entityClass.isAnnotationPresent(annotationClass)) {
				final Annotation annotation = entityClass.getAnnotation(annotationClass);
				final IdGenerationPolicy idGenerationPolicy = (IdGenerationPolicy) annotation;

				final String[] fieldNames = idGenerationPolicy.value();
				if (fieldNames != null && fieldNames.length > 0) {
					return fieldNames;
				}
			}
		} catch (final Throwable t) {
		}

		return null;
	}
	
	static String idValue(final Entity entity) {
		// Buffer to store concatenated values from fields
		final StringBuilder builder = new StringBuilder();

		final String[] fieldNames = getIdGenerationPolicyFieldNames(entity.getClass());

		if (fieldNames != null && fieldNames.length > 0) {
			logger.trace("Found annotation IdGenerationPolicy [type={}, existingId={}, fieldNames={}]",
					entity.getClass(), entity.getId(), fieldNames);

			for (int index = 0; index < fieldNames.length; index++) { // Begin field name processing loop
				final String fieldName = fieldNames[index];

				logger.trace(
						"Processing field to apply IdGenerationPolicy [type={}, existingId={}, fieldName={}]",
						entity.getClass(), entity.getId(), fieldName);

				if (!Stringizer.isEmpty(fieldName)) { // Begin field name guard
					final Object value = Ruminator.getProperty(entity, fieldName);
					logger.trace("Processed [fieldName={}, valueType={}]", fieldName, value.getClass());

					if (value != null) { // Begin field value guard
						final String stringValue = String.valueOf(value);
						logger.trace("Buffering value [type={}, value={}, newId={}]", entity.getClass(),
								stringValue, entity.getId());

						if (index > 0) { // Begin add value separator
							builder.append("-");
						} // End add value separator

						builder.append(stringValue); // Add value to buffer
					} // End field value guard
				} // End field name guard
			} // End field name processing loop
		}
		
		return builder.toString();
	}
	
	public static void resolveId(final Entity entity) {
		final String idValue = idValue(entity);
		
		if (entity.getId() != null) {
			// User has already set the ID field
			logger.trace("ID Already Provided [type={}, existingId={}]", entity.getClass(), entity.getId());
		} else if(!Stringizer.isEmpty(idValue)) {
			// Apply generated ID
			final UUID uuid = Randomizer.nameUUID(idValue);
			entity.setId(uuid);
			
			logger.trace("ID generated and set from values [type={}, idValue={}, newId={}]", entity.getClass(), idValue, uuid);
		} else {
			// Generate a default
			final UUID uuid = Randomizer.uuid();
			entity.setId(uuid);
			
			logger.trace("A system generated ID is set [type={}, existingId={}]", entity.getClass(), uuid);
		}
	}
}
