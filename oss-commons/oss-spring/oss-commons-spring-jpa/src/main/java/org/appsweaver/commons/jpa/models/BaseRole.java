/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.models;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.appsweaver.commons.HibernateSearch;
import org.appsweaver.commons.annotations.Searchable;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.SortableField;

import lombok.EqualsAndHashCode;

/**
 * 
 * @author UD
 * 
 */
@MappedSuperclass
@EqualsAndHashCode(callSuper = true)
public abstract class BaseRole extends BaseEntity {
	private static final long serialVersionUID = 2196571017776227607L;

	@Field(analyzer = @Analyzer(definition = HibernateSearch.KEYWORD_ANALYZER))
	@SortableField
	@Searchable(boost = "1500")
	@Column(name = "NAME", length = 255, unique = true)
	protected String name;

	public BaseRole() {
		super();
	}

	public BaseRole(String name) throws Throwable {
		this();

		setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) throws Throwable {
		this.name = name;
	}
}
