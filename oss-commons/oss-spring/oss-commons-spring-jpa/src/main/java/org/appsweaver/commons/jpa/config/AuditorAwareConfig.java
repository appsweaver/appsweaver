/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.config;

import java.util.UUID;

import org.appsweaver.commons.jpa.models.auditor.DefaultAuditorAware;
import org.appsweaver.commons.models.security.SecurityFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * 
 * @author UD
 * 
 * 
 */
@Configuration
@ConditionalOnProperty(name="webservice.repository.auditing.enabled", havingValue = "true", matchIfMissing = true)
@EnableJpaAuditing(auditorAwareRef = "auditorAware")
public class AuditorAwareConfig {
	@Autowired(required = false) SecurityFacade securityFacade;
	
	@Bean
	public AuditorAware<UUID> auditorAware() {
		return new DefaultAuditorAware(securityFacade);
	}
}
