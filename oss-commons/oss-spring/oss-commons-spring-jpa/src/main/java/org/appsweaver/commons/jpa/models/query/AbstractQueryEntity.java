/*
 *
 * Copyright (c) 2016 appsweaver.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.appsweaver.commons.jpa.models.query;

import java.math.BigDecimal;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.appsweaver.commons.annotations.Filterable;
import org.appsweaver.commons.jpa.models.AbstractSearchableEntity;
import org.appsweaver.commons.models.persistence.QueryEntity;
import org.appsweaver.commons.utilities.Dates;
import org.appsweaver.commons.utilities.Randomizer;
import org.hibernate.search.annotations.DocumentId;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.FieldBridge;
import org.hibernate.search.annotations.SortableField;
import org.hibernate.search.bridge.builtin.UUIDBridge;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.EqualsAndHashCode;

/**
 *
 * @author UD
 *
 */
@EqualsAndHashCode(callSuper = true)
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@EntityListeners({
	/*
	 * Auditing entity listener from Spring injects user id (from security facade) and
	 * set created and updated time stamps
	 */
	AuditingEntityListener.class,
})
@MappedSuperclass
public abstract class AbstractQueryEntity extends AbstractSearchableEntity implements QueryEntity {
	/**
	 *
	 */
	private static final long serialVersionUID = 4469222780095544425L;

	@DocumentId
	@Id
	@Filterable(valueClass = UUID.class)
	@FieldBridge(impl = UUIDBridge.class)
	@Column(name = "ID", nullable = false, unique = true)
    protected UUID id = Randomizer.uuid();

	/**
	 * Version number for this entity
	 */
	//@Version
	//TODO: discuss about how @Version will work in CQRS and bring it back
	@Column(name = "VERSION")
	protected long version;

	/**
	 * Row rank
	 */
	@Column(name = "RANK")
	protected BigDecimal rank;

	/**
	 * Has this entity logically archived
	 */
	@Column(name = "ARCHIVED")
	@Filterable(valueClass = Boolean.class)
	protected boolean archived = false;

	/**
	 * Has this entity been logically deleted
	 */
	@Column(name = "DELETED")
	@Filterable(valueClass = Boolean.class)
	protected boolean deleted = false;

	/**
	 * Created On
	 */
	@Field
	@SortableField
	@Filterable(valueClass = Long.class)
	@Column(name = "CREATED_ON", nullable = false)
	@CreatedDate
	protected Long createdOn = Dates.toTime();

	/**
	 * Updated On
	 */
	@Field
	@SortableField
	@Filterable(valueClass = Long.class)
	@Column(name = "UPDATED_ON")
	@LastModifiedDate
	protected Long updatedOn;

	/**
	 * User who created this object
	 */
	@Filterable(valueClass = UUID.class)
	@Column(name = "CREATED_BY")
	@CreatedBy
	protected UUID createdBy;

	/**
	 * User who updated this object
	 */
	@Filterable(valueClass = UUID.class)
	@Column(name = "UPDATED_BY")
	@LastModifiedBy
	protected UUID updatedBy;

	public AbstractQueryEntity() {
		super();
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public BigDecimal getRank() {
		return rank;
	}

	public void setRank(BigDecimal rank) {
		this.rank = rank;
	}

	public boolean isArchived() {
		return archived;
	}

	public void setArchived(boolean archived) {
		this.archived = archived;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public Long getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Long createdOn) {
		this.createdOn = createdOn;
	}

	public Long getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Long updatedOn) {
		this.updatedOn = updatedOn;
	}

	public UUID getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(UUID createdBy) {
		this.createdBy = createdBy;
	}

	public UUID getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(UUID updatedBy) {
		this.updatedBy = updatedBy;
	}
}
