/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.models.search;

/**
 * 
 * @author UD
 * 
 */
public class SearchField {
	final String name;
	final Float boost;

	public SearchField(String value) {
		final String[] tokens = value.split("\\^");
		if(tokens.length >=2) {
			name = tokens[0];
			boost = Float.valueOf(tokens[1]);
		} else {
			name = tokens[0];
			boost = null;
		}
	}

	public String getName() {
		return name;
	}

	public Float getBoost() {
		return boost;
	}
}
