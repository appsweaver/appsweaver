/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.annotations.processors;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Set;

import org.appsweaver.commons.annotations.Searchable;
import org.appsweaver.commons.models.criteria.SearchableField;
import org.appsweaver.commons.models.persistence.Entity;
import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Stringizer;
import org.hibernate.search.annotations.Facet;
import org.hibernate.search.annotations.Facets;
import org.hibernate.search.annotations.IndexedEmbedded;

/**
 * 
 * @author UD
 *
 * Process entity class annotations for SearchableField
 * Uses prototype scope to provide new instance each time it is needed,
 * required for managing data structure used for annotation processing
 */
public class SearchableAnnotationProcessor extends AbstractFieldAnnotationProcessor {
	final protected Set<SearchableField> fields = Collections.newHashSet();

	public <T extends Entity> Set<SearchableField> getFields(Class<T> entityClass) {
		return fields;
	}

	@Override
	public Set<Class<? extends Annotation>> getAnnotationClasses() {
		return Collections.newHashSet(Searchable.class);
	}

	private String getFacetName(Facet facet) {
		String facetName = facet.forField();
		if(Stringizer.isEmpty(facetName)) {
			facetName = facet.name();
		}
		return facetName;
	}
	
	private Set<String> getFacetNames(Facets facets, Facet facet) {
		final Set<String> facetNames = Collections.newHashSet();
		
		// Look for Facets annotation
		if(facets != null) {
			final Facet[] facetArray = facets.value();
			if(facetArray != null && facetArray.length > 0) {
				for(int index = 0 ; index < facetArray.length; index++) {
					final Facet facetItem = facetArray[index];
					final String facetName = getFacetName(facetItem);
					if(!facetNames.contains(facetName)) {
						facetNames.add(facetName);
					}
				}
			}
		}

		// Look for Facet annotation
		if(facet != null) {
			final String facetName = getFacetName(facet);
			if(!facetNames.contains(facetName)) {
				facetNames.add(facetName);
			}
		}
		
		return facetNames;
	}
	
	private Set<String> getFacetNames(Member member) {
		final Facets facets = (member instanceof Method) ? ((Method)member).getAnnotation(Facets.class) : ((Field)member).getAnnotation(Facets.class);
		final Facet facet = (member instanceof Method) ? ((Method)member).getAnnotation(Facet.class) : ((Field)member).getAnnotation(Facet.class);

		final Set<String> facetNames = getFacetNames(facets, facet);

		return facetNames;
	}
	
	@Override
	public void processAnnotation(Annotation annotation, Member member) {
		final Searchable searchable = (Searchable)annotation;
		final Float boostValue = (Stringizer.isEmpty(searchable.boost())) ? null : Float.valueOf(searchable.boost());
		final boolean enabled = searchable.enabled();
		final boolean lookup = searchable.lookup();
		final String range = searchable.range();

		if(searchable.enabled()) {
			final String fieldName = member.getName();

			final IndexedEmbedded indexedEmbedded = (member instanceof Method) ? ((Method)member).getAnnotation(IndexedEmbedded.class) : ((Field)member).getAnnotation(IndexedEmbedded.class);
			final String[] embeddedFieldNames = (indexedEmbedded != null) ? indexedEmbedded.includePaths() : null;
			
			final Set<String> facetNames = getFacetNames(member);

			if(embeddedFieldNames != null && embeddedFieldNames.length > 0) {
				/*
				 * Only provide paths that must be searched
				 */
				Arrays.asList(embeddedFieldNames).forEach(embeddedFieldName -> {
					fields.add(new SearchableField(
						enabled,
						lookup,
						String.format("%s.%s", fieldName, embeddedFieldName), 	// field name
						boostValue, 											// Defaults to empty string, must be guarded
						Collections.toArray(facetNames, String.class),			// facet names
						range
					));
				});
			} else {
				/*
				 * Add field name as default search field
				 */
				fields.add(new SearchableField(
					enabled,
					lookup,
					fieldName, 													// field name
					boostValue, 												// Defaults to empty string, must be guarded
					Collections.toArray(facetNames, String.class),				// facet names
					range
				));
			}
		}
	}

	@Override
	public void initialize() {
		fields.clear();
	}
}
