/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.models;

import org.apache.lucene.analysis.core.KeywordTokenizerFactory;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.ngram.EdgeNGramFilterFactory;
import org.apache.lucene.analysis.standard.ClassicTokenizerFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.appsweaver.commons.HibernateSearch;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.AnalyzerDefs;
import org.hibernate.search.annotations.Parameter;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.EqualsAndHashCode;

/**
 * 
 * @author UD
 * 
 * Entity with support for full text query searches
 * 
 * Custom analyzer makes sure that all tokens are converted to lower case first
 * followed by N-Gram tokenization to push highly hit documents to the top
 */
@EqualsAndHashCode
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@AnalyzerDefs({
	@AnalyzerDef(name = HibernateSearch.CLASSIC_ANALYZER,
		tokenizer = @TokenizerDef(
			factory = ClassicTokenizerFactory.class, 
			params = {
				@Parameter(name = "maxTokenLength", value = "1024")
			}
		),
		filters = {
			@TokenFilterDef(factory = LowerCaseFilterFactory.class)
		}
	),
	
	@AnalyzerDef(name = HibernateSearch.STANDARD_ANALYZER,
		tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
		filters = {
			@TokenFilterDef(factory = LowerCaseFilterFactory.class)
		}
	),
	
	@AnalyzerDef(name = HibernateSearch.KEYWORD_ANALYZER,
		tokenizer = @TokenizerDef(factory = KeywordTokenizerFactory.class),
		filters = {
			@TokenFilterDef(factory = LowerCaseFilterFactory.class)
		}
	),
	
	@AnalyzerDef(name = HibernateSearch.AUTO_COMPLETE_ANALYZER,
		tokenizer = @TokenizerDef(factory = KeywordTokenizerFactory.class),
		filters = {
			@TokenFilterDef(factory = LowerCaseFilterFactory.class),
			@TokenFilterDef(
				factory = EdgeNGramFilterFactory.class, 
				params = { 
					@Parameter(name = "minGramSize", value = "3"), 
					@Parameter(name = "maxGramSize", value = "16") 
				}
			)
		}
	)
})
@Analyzer(definition = HibernateSearch.KEYWORD_ANALYZER)
public abstract class AbstractSearchableEntity {
}
