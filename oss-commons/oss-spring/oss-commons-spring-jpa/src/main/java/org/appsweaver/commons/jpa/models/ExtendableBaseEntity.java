/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.models;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.ManyToMany;
import javax.persistence.MappedSuperclass;

import org.appsweaver.commons.annotations.Extendable;
import org.appsweaver.commons.jpa.models.custom.BooleanValue;
import org.appsweaver.commons.jpa.models.custom.DateValue;
import org.appsweaver.commons.jpa.models.custom.DecimalValue;
import org.appsweaver.commons.jpa.models.custom.ExtendableEntity;
import org.appsweaver.commons.jpa.models.custom.IntegerValue;
import org.appsweaver.commons.jpa.models.custom.LargeTextValue;
import org.appsweaver.commons.jpa.models.custom.MediumTextValue;
import org.appsweaver.commons.jpa.models.custom.TextMultiSelectValue;
import org.appsweaver.commons.jpa.models.custom.TextValue;
import org.appsweaver.commons.jpa.models.custom.TinyTextValue;
import org.appsweaver.commons.jpa.models.custom.Value;
import org.appsweaver.commons.jpa.types.FieldType;
import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Stringizer;

import lombok.EqualsAndHashCode;

/**
 * 
 * @author UD
 * 
 */
@Extendable
@MappedSuperclass
@EqualsAndHashCode(callSuper = true)
public abstract class ExtendableBaseEntity extends BaseEntity implements ExtendableEntity {
	private static final long serialVersionUID = -6803397651320907042L;
	
	/*
	@JsonSerialize(converter = TagCollectionConverter.class)
	@Searchable
	@IndexedEmbedded(includePaths = { "value" })
	@Filterables(value = {
		@Filterable(name = "fieldName", path = "values[].dataType.name")
	})
	*/
	@ManyToMany(cascade = CascadeType.ALL)
	protected Set<Value<?>> values = Collections.newHashSet();

	public ExtendableBaseEntity() {
		super();
	}
	
	public ExtendableBaseEntity(Set<Value<?>> values) {
		super();
		
		setValues(values);
	}

	@Override
	public Set<Value<?>> getValues() {
		return values;
	}

	@Override
	public void setValues(Set<Value<?>> values) {
		this.values = values;
	}
	
	public void set(String fieldName, Integer value) {
		final IntegerValue instance = new IntegerValue(fieldName, Long.valueOf(value));
		values.add(instance);
	}
	
	public void set(String fieldName, Long value) {
		final IntegerValue instance = new IntegerValue(fieldName, value);
		values.add(instance);
	}
	
	public void set(String fieldName, Float value) {
		final DecimalValue instance = new DecimalValue(fieldName, Double.valueOf(value));
		values.add(instance);
	}
	
	public void set(String fieldName, Double value) {
		final DecimalValue instance = new DecimalValue(fieldName, value);
		values.add(instance);
	}
	
	public void set(String fieldName, Date value) {
		final DateValue dateValue = new DateValue(fieldName, value.toInstant().toEpochMilli());
		values.add(dateValue);
	}
	
	public void set(String fieldName, List<String> value) {
		final TextMultiSelectValue instance = new TextMultiSelectValue(fieldName, value);
		values.add(instance);
	}
	
	public void set(String fieldName, Boolean value) {
		final BooleanValue instance = new BooleanValue(fieldName, value);
		values.add(instance);
	}
	
	public void set(String fieldName, String value, FieldType fieldType) {
		if(fieldType == FieldType.LARGE_TEXT) {
			final LargeTextValue instance = new LargeTextValue(fieldName, value);
			values.add(instance);
		} else if(fieldType == FieldType.MEDIUM_TEXT) {
			final MediumTextValue instance = new MediumTextValue(fieldName, value);
			values.add(instance);
		}  else if(fieldType == FieldType.TEXT) {
			final TextValue instance = new TextValue(fieldName, value);
			values.add(instance);
		} else {
			final TinyTextValue instance = new TinyTextValue(fieldName, value);
			values.add(instance);
		}
	}
	
	public void set(String fieldName, String value) {
		set(fieldName, value, getFieldType(value));
	}
	
	FieldType getFieldType(String value) {
		if(!Stringizer.isEmpty(value)) {
			final int length = value.length();
			
			if(length >= TinyTextValue.LENGTH && length < TextValue.LENGTH) {
				return FieldType.TEXT;
			} else if(length >= TextValue.LENGTH && length < MediumTextValue.LENGTH) {
				return FieldType.MEDIUM_TEXT;
			} else if(length >= MediumTextValue.LENGTH && length < LargeTextValue.LENGTH) {
				return FieldType.LARGE_TEXT;
			}
		}
		
		return FieldType.TINY_TEXT;
	}
}
