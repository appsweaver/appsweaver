/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.models.criteria;

import static org.appsweaver.commons.utilities.TypeConverter.toList;
import static org.appsweaver.commons.utilities.TypeConverter.valueOf;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.appsweaver.commons.models.criteria.FilterableField;
import org.appsweaver.commons.models.persistence.Entity;
import org.appsweaver.commons.utilities.FilterHelper;
import org.appsweaver.commons.utilities.Stringizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author UD
 *
 * Create JPA criteria from filter expressions
 *
 */
public class RelationalDatabaseFilter<T extends Entity> extends AbstractFilter<Entity> implements Specification<T> {
	private static final long serialVersionUID = -7527231983370180153L;

	final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public RelationalDatabaseFilter(FilterableField filterableField, String value) {
		super(filterableField, value);
	}

	static class Pathway<Type> {
		final int length;
		final String name;
		final Path<Type> path;

		public Pathway(int length, String name, Path<Type> path) {
			this.length = length;
			this.name = name;
			this.path = path;
		}

		public int getLength() {
			return length;
		}

		public String getName() {
			return name;
		}

		public Path<Type> getPath() {
			return path;
		}
	}

	final <Type> Path<Type> path(Root<T> root, String field) {
		if(field.endsWith("[]")) {
			/*
			 * Assuming attribute names ending with [] are treated as list of values (any collection)
			 * Attribute will be treated as a join operation
			 */
			final String fieldName = field.replaceAll("\\[\\]", "");
			return root.join(fieldName);
		} else {
			return root.get(field);
		}
	}
	
	final <Type> Path<Type> path(Root<T> root, String[] paths) {
		if (paths.length > 1) {
			/*
			 * Assuming attribute names DO NOT end with [] but are splitted by '.' are treated as list of values (any collection)
			 * The first attribute will be treated as a join operation
			 */
			return root.join(paths[0]);
		} else {
			return root.get(paths[0]);
		}
	}

	final <Type> Path<Type> path(Root<T> root, FilterableField field) {
		final String []paths = field.getPath().split("\\.");

		Path<Type> path = path(root, paths);

		for(int index = 1; index < paths.length; index++) {
			path = path.get(paths[index]);
		}

		final Pathway<Type> pathway = new Pathway<Type>(paths.length, paths[paths.length - 1], path);
		return pathway.getPath();
	}
	
	@Override
	public Predicate toPredicate(Root<T> root, CriteriaQuery<?> criteria, CriteriaBuilder criteriaBuilder) {
		String filterValue = getValue();
		final String filterOperator = getOperator();
		final Class<?> fieldType = getType();
		
		switch (filterOperator.toLowerCase()) {
		case "lt":
			if(Number.class.isAssignableFrom(fieldType)) {
				final Path<Number> path = path(root, filterableField);
				return criteriaBuilder.ge(path, valueOf(filterValue, Number.class));
			} else {
				throw new IllegalArgumentException(String.format("Operator [%s] does not supported non numeric values!", filterOperator));
			}

		case "le":
			if(Number.class.isAssignableFrom(fieldType)) {
				final Path<Number> path = path(root, filterableField);
				return criteriaBuilder.le(path, valueOf(filterValue, Number.class));
			} else {
				throw new IllegalArgumentException(String.format("Operator [%s] does not supported non numeric values!", filterOperator));
			}

		case "gt":
			if(Number.class.isAssignableFrom(fieldType)) {
				final Path<Number> path = path(root, filterableField);
				return criteriaBuilder.gt(path, valueOf(filterValue, Number.class));
			} else {
				throw new IllegalArgumentException(String.format("Operator [%s] does not supported non numeric values!", filterOperator));
			}

		case "ge":
			if(Number.class.isAssignableFrom(fieldType)) {
				final Path<Number> path = path(root, filterableField);
				return criteriaBuilder.ge(path, valueOf(filterValue, Number.class));
			} else {
				throw new IllegalArgumentException(String.format("Operator [%s] does not supported non numeric values!", filterOperator));
			}

		case "in":
		case "!in": {
			final Path<Object> path = path(root, filterableField);
			List<?> values = toList(filterValue, fieldType, true);
			
			if (String.class.isAssignableFrom(fieldType)) {
				values = values.stream().map(valueObject -> {
					return FilterHelper.sanitizeFilterValue(valueObject);
				}).collect(Collectors.toList());	
			}

			// NOT IN
			if(filterOperator.startsWith("!")) {
				return criteriaBuilder.not(path.in(values));
			}

			// IN
			return path.in(values);
		}

		case "eq":
		case "!eq": {
			filterValue = String.valueOf(FilterHelper.sanitizeFilterValue(filterValue));
			if(Stringizer.isNumeric(filterValue)) {
				final Path<Number> path = path(root, filterableField);

				// NOT EQ
				if(filterOperator.startsWith("!")) {
					return criteriaBuilder.notEqual(path, valueOf(filterValue, Number.class));
				}

				// EQ
				return criteriaBuilder.equal(path, valueOf(filterValue, Number.class));
			} else {
				final Path<Object> path = path(root, filterableField);

				// NOT EQ
				if(filterOperator.startsWith("!")) {
					return criteriaBuilder.notEqual(path, valueOf(filterValue, fieldType));
				}

				// EQ
				return criteriaBuilder.equal(path, valueOf(filterValue, fieldType));
				
			}
		}
		
		case "null":
		case "!null": {
			final Path<Object> path = path(root, filterableField);
			
			// isNotNull
			if(filterOperator.startsWith("!")) {
				return criteriaBuilder.isNotNull(path);
			}

			// isNull
			return criteriaBuilder.isNull(path);
		}

		case "like":
		case "!like": {
			final String newValue = toLikeSyntax(String.valueOf(FilterHelper.sanitizeFilterValue(filterValue)));
			final Path<String> path = path(root, filterableField);

			// NOT LIKE
			if(filterOperator.startsWith("!")) {
				return criteriaBuilder.notLike(path, newValue);
			}

			// LIKE
			return criteriaBuilder.like(path, newValue);
		}

		case "between":
		case "!between": {
			// FIXME: Do not assume number only scenario; can be dates too
			if(Number.class.isAssignableFrom(fieldType)) {
				final List<String> values = toList(filterValue, String.class, true);
				final Path<Double> path = path(root, filterableField);
				
				// NOT BETWEEN
				if(filterOperator.startsWith("!")) {
					return criteriaBuilder.not(
						criteriaBuilder.between(path,
							valueOf(values.get(0), path.getJavaType()),
							valueOf(values.get(1), path.getJavaType())
						)
					);
				}
				
				// BETWEEN
				return criteriaBuilder.between(path,
					valueOf(values.get(0), path.getJavaType()),
					valueOf(values.get(1), path.getJavaType())
				);
			} else {
				throw new IllegalArgumentException(String.format("Operator [%s] does not supported non numeric values!", filterOperator));
			}
		}

		default:
			throw new UnsupportedOperationException(String.format("Operator [%s] not supported yet!", filterOperator.toLowerCase()));
		}
	}
	
	String toLikeSyntax(String value) {
		if(value.contains("*") || value.contains("%")) {
			return value.replaceAll("\\*", "%");
		} else {
			return String.format("%%%s%%", value);
		}
	}
	
	public static <T extends Entity> RelationalDatabaseFilter<T> newDatabaseFilter(String name, String path, String value, Class<?> type) {
		final FilterableField filterableField = new FilterableField(name, path, type);
		final RelationalDatabaseFilter<T> databaseFilter = new RelationalDatabaseFilter<>(filterableField, value);
		return databaseFilter;
	}
	
}
