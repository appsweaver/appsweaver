/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.models;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.appsweaver.commons.annotations.Filterable;
import org.appsweaver.commons.annotations.Searchable;
import org.appsweaver.commons.utilities.Stringizer;
import org.hibernate.annotations.NaturalId;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Facet;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Fields;
import org.hibernate.search.annotations.SortableField;

import lombok.EqualsAndHashCode;

/**
 * 
 * @author UD
 * 
 */
@MappedSuperclass
@EqualsAndHashCode(callSuper = true)
public abstract class BaseUser extends BaseEntity {
	private static final long serialVersionUID = -2758890234182477742L;

	// Facet Names
	private static final String USER_TITLE_FACET = "user_title_facet";
	// Facet Names
	
	@NaturalId
	@Field
	@SortableField
	@Searchable(boost = "5000")
	@Filterable
	@Column(name = "EMAIL", length = 255, unique = true)
	protected String email;

	@Field
	@SortableField
	@Searchable(boost = "1500")
	@Filterable
	@Column(name = "FIRST_NAME", length = 255)
	protected String firstName;

	@Field
	@SortableField
	@Searchable(boost = "1500")
	@Filterable
	@Column(name = "LAST_NAME", length = 255)
	protected String lastName;
	
	@Facet(forField = USER_TITLE_FACET)
	@Fields({
		@Field(name = USER_TITLE_FACET, analyze = Analyze.NO),
		@Field
	})
	@SortableField
	@Searchable(boost = "1000")
	@Filterable
	@Column(name = "TITLE", length = 255)
	protected String title;
	
	@Field
	@SortableField
	@Searchable
	@Filterable
	@Column(name = "MOBILE_NUMBER", length = 64)
	protected String mobileNumber;

	/**
	 * Is user enabled or disabled?
	 */
	@Filterable(valueClass = Boolean.class)
	@Column(name = "ENABLED")
	protected boolean enabled = true;
	
	/**
	 * Is user locked after invalid login attempts?
	 */
	@Filterable(valueClass = Boolean.class)
	@Column(name = "LOCKED")
	protected boolean locked = false;
	
	public BaseUser() {
		super();
	}

	public BaseUser(String email, String firstName, String lastName, String title, String mobileNumber) {
		this();

		setEmail(email);
		setFirstName(firstName);
		setLastName(lastName);
		setTitle(title);
		setMobileNumber(mobileNumber);
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		// email is sanitized to lower cased
		if(!Stringizer.isEmpty(email)) {
			this.email = email.toLowerCase();
		} else {
			email = null;
		}
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
