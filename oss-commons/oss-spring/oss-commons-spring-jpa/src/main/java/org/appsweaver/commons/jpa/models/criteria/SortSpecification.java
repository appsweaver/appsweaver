/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.models.criteria;

import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;

/**
 *
 * @author UD
 *
 * Create a sort specification<br>
 * <br>
 * Expected sort syntax: <b>[asc|desc]:&lt;attribute&gt;</b><br>
 * <br>
 * When sort direction is not provided; defaults to ascending<br>
 * <br>
 * attribute  	must be field name that can be translated to JPA layer; can think of obfuscation; will defer to later times<br>
 * <br>
 *
 *
 */
public class SortSpecification<T> {
	final SortOrder sortValue;

	public SortSpecification(SortOrder sortValue) {
		this.sortValue = sortValue;
	}

	public Order toOrder() {
		if (sortValue.getOrder() == Direction.DESC) {
			return new Order(Direction.DESC, sortValue.getField());
		} else {
			return new Order(Direction.ASC, sortValue.getField());
		}
	}
}
