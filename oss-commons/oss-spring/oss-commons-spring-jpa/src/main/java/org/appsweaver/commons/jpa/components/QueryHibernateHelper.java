/*
 *
 * Copyright (c) 2016 appsweaver.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.appsweaver.commons.jpa.components;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.appsweaver.commons.jpa.helpers.PageableHelper;
import org.appsweaver.commons.jpa.models.criteria.ObjectDatabaseFilter;
import org.appsweaver.commons.jpa.types.FilterConjunction;
import org.appsweaver.commons.models.persistence.QueryEntity;
import org.appsweaver.commons.utilities.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;


/**
 *
 * @author UD
 *
 */
@Component("queryHibernateHelper")
@ConditionalOnBean(name = "queryEntityManagerFactory")
public class QueryHibernateHelper implements HibernateHelper {
	@Qualifier("queryEntityManagerFactory")
	@Autowired EntityManagerFactory queryEntityManagerFactory;

	@Override
	public EntityManagerFactory getEntityManagerFactory() {
		return queryEntityManagerFactory;
	}

	public <T extends QueryEntity> T save(
		T item
	) throws SQLException {
		final EntityManager entityManager = createEntityManager();
		try {
			// Begin Transaction
			entityManager.getTransaction().begin();

			if(item.getId() == null) {
				// Object does not exist
				entityManager.persist(item);
			} else {
				// Assume object existence to be true
				entityManager.merge(item);
			}

			entityManager.getTransaction().commit();
			// End transaction

			return item;
		} catch (final Throwable t) {
			// Roll Back Transaction
			entityManager.getTransaction().rollback();
			throw t;
		} finally {
			// Close Entity Manager
			entityManager.close();
		}
	}

	public <T extends QueryEntity> T delete(
		T item
	) throws SQLException {
		final EntityManager entityManager = createEntityManager();
		try {
			// Begin Transaction
			entityManager.getTransaction().begin();
			entityManager.remove(item);
			entityManager.getTransaction().commit();
			// End transaction

			return item;
		} catch (final Throwable t) {
			// Roll Back Transaction
			entityManager.getTransaction().rollback();
			throw t;
		} finally {
			// Close Entity Manager
			entityManager.close();
		}
	}

	public <T extends QueryEntity> T delete(
			Class<T> entityClass,
			UUID id
		) throws SQLException {
			final EntityManager entityManager = createEntityManager();
			try {
				// Fetch entity
				final T item = entityManager.find(entityClass, id);
				// Begin Transaction
				entityManager.getTransaction().begin();
				entityManager.remove(item);
				entityManager.getTransaction().commit();
				// End transaction

				return item;
			} catch (final Throwable t) {
				// Roll Back Transaction
				entityManager.getTransaction().rollback();
				throw t;
			} finally {
				// Close Entity Manager
				entityManager.close();
			}
	}

	public <T extends QueryEntity> T findById(
		Class<T> entityClass,
		UUID id
	) throws SQLException {
		final EntityManager entityManager = createEntityManager();
		try {
			final T item = entityManager.find(entityClass, id);
			return item;
		}  catch(final NoResultException e) {
			/*
			 *  NO-OP: Results were not found for the given conditions
			 */
			return null;
		} finally {
			// Close Entity Manager
			entityManager.close();
		}
	}

	public <T extends QueryEntity> boolean existsById(
		Class<T> entityClass,
		UUID id
	) throws SQLException {
		final EntityManager entityManager = createEntityManager();
		try {
			final T item = entityManager.find(entityClass, id);
			return item != null;
		} catch(final NoResultException e) {
			/*
			 *  NO-OP: Results were not found for the given conditions
			 */
			return false;
		}  finally {
			// Close Entity Manager
			entityManager.close();
		}
	}

	public <T extends QueryEntity> List<T> findByAttributeNameAndValue(
		Class<T> entityClass,
		String attributeName,
		Object value
	) throws SQLException {
		final EntityManager entityManager = createEntityManager();
		try {
			final String qlString = String.format("FROM %s o WHERE o.%s = :%s", entityClass.getSimpleName(), attributeName, attributeName);
			final Query query = entityManager.createQuery(qlString);
			query.setParameter(attributeName, value);

			@SuppressWarnings("unchecked")
			final List<T> results = query.getResultList();
			return results;
		} catch(final NoResultException e) {
			/*
			 *  NO-OP: Results were not found for the given conditions
			 */
			return Collections.newList();
		} finally {
			// Close Entity Manager
			entityManager.close();
		}
	}

	public <T extends QueryEntity> T findOne(
		Class<T> entityClass,
		Optional<List<Specification<T>>> optionalSpecs
	) throws SQLException {
		final EntityManager entityManager = createEntityManager();
		try {
			final ObjectDatabaseFilter<T> databaseFilter = new ObjectDatabaseFilter<>(
				entityClass,		// Entity Class
				optionalSpecs,		// Specifications
				Optional.empty(),	// Sort
				Optional.empty(),	// Page Number
				Optional.empty(),	// Page Size
				Optional.empty(),	// Conjunction
				Optional.empty()	// Since
			);

			final Query query = databaseFilter.toQuery(
				entityManager,
				Optional.empty()	// SQL Statement
			);

			@SuppressWarnings("unchecked")
			final T result = (T) query.getSingleResult();
			return result;
		} catch(final NoResultException e) {
			/*
			 *  NO-OP: Results were not found for the given conditions
			 */
			return null;
		} finally {
			// Close Entity Manager
			entityManager.close();
		}
	}

	/*
	 * This is unsupported by OGM
	 */
	@Deprecated
	public <T extends QueryEntity> Stream<T> streamAll(Class<T> entityClass) throws SQLException {
		final EntityManager entityManager = createEntityManager();
		try {
			final String qlString = String.format("FROM %s", entityClass.getSimpleName());
			final Query query = entityManager.createQuery(qlString);

			@SuppressWarnings("unchecked")
			final Stream<T> results = query.getResultStream();
			return results;
		} catch(final NoResultException e) {
			/*
			 *  NO-OP: Results were not found for the given conditions
			 */
			return null;
		}  finally {
			// Close Entity Manager
			entityManager.close();
		}
	}

	public void flush() throws SQLException {
		final EntityManager entityManager = createEntityManager();
		try {
			entityManager.flush();
		} finally {
			// Close Entity Manager
			entityManager.close();
		}
	}

	public <T extends QueryEntity> long count(
		Class<T> entityClass,
		Optional<List<Specification<T>>> optionalSpecs
	) throws SQLException {

		final EntityManager entityManager = createEntityManager();
		try {

			final ObjectDatabaseFilter<T> databaseFilter = new ObjectDatabaseFilter<>(
				entityClass,		// Entity Class
				optionalSpecs,		// Specifications
				Optional.empty(),	// Sort
				Optional.empty(),	// Page Number
				Optional.empty(),	// Page Size
				Optional.empty(),	// Conjunction
				Optional.empty()	// Since
			);

			final Query query = databaseFilter.toQuery(
				entityManager, 									// Entity Manager
				Optional.of("SELECT COUNT(*) as TOTAL FROM")	// SQL Statement
			);

			final Integer count = (Integer) query.getSingleResult();
			return count;
		} catch(final NoResultException e) {
			/*
			 *  NO-OP: Results were not found for the given conditions
			 */
			return 0;
		} finally {
			// Close Entity Manager
			entityManager.close();
		}
	}

	public <T extends QueryEntity> List<T> findAll(Class<T> entityClass) throws SQLException {
		return findAll(
			entityClass,								// Entity class
			Optional.empty(),							// Specification
			Optional.empty(),							// Sort
			Optional.empty(),							// Page Number
			Optional.empty(),							// Page Size
			Optional.ofNullable(FilterConjunction.AND),	// Conjunction
			Optional.empty()							// Since creation or update epoch
		);
	}

	public <T extends QueryEntity> Page<T> findAll(
			Class<T> entityClass,
			Optional<List<Specification<T>>> optionalSpecs,
			Optional<Pageable> optionalPageable,
			Optional<Sort> optionalSort
		) throws SQLException {
		return findAll(
			entityClass, 								// Entity class
			optionalSpecs, 								// Specification
			optionalPageable,							// Pageable
			optionalSort,								// Sort
			Optional.ofNullable(FilterConjunction.AND),	// Conjunction
			Optional.empty()							// Since creation or update epoch
		);
	}

	public <T extends QueryEntity> Page<T> findAll(
		Class<T> entityClass,
		Optional<List<Specification<T>>> optionalSpecs,
		Optional<Pageable> optionalPageable,
		Optional<Sort> optionalSort,
		Optional<FilterConjunction> optionalConjunction,
		Optional<Long> optionalSince
	) throws SQLException {
		try {
			Integer pageNumber = null;
			Integer pageSize = null;
			Sort sort = null;

			if(optionalSort.isPresent()) {
				sort = optionalSort.get();
			}

			if(optionalPageable.isPresent()) {
				final Pageable pageable = optionalPageable.get();
				pageNumber = pageable.getPageNumber();
				pageSize = pageable.getPageSize();

				if(sort == null) {
					sort = pageable.getSort();
				}
			}

			final List<T> list = findAll(
				entityClass,						// Entity class
				optionalSpecs,						// Specification
				Optional.ofNullable(sort),			// Sort
				Optional.ofNullable(pageNumber), 	// Page Number
				Optional.ofNullable(pageSize),		// Page Size
				optionalConjunction,				// Conjunction
				optionalSince						// Since creation or update epoch
			);

			// Obtain total count
			final Long total = count(entityClass, optionalSpecs);

			// Return pages
			return PageableHelper.toPage(list, pageNumber, pageSize, total);
		} catch (SQLException | NoResultException e) {
			/*
			 *  NO-OP: Results were not found for the given conditions
			 */
			return PageableHelper.emptyPage();
		}
	}

	public <T extends QueryEntity> List<T> findAll(
		Class<T> entityClass,
		Optional<List<Specification<T>>> optionalSpecs,
		Optional<Sort> optionalSort,
		Optional<Integer> optionalPageNumber,
		Optional<Integer> optionalPageSize,
		Optional<FilterConjunction> optionalConjunction,
		Optional<Long> optionalSince
	) throws SQLException {
		final EntityManager entityManager = createEntityManager();
		try {
			final ObjectDatabaseFilter<T> databaseFilter = new ObjectDatabaseFilter<>(
				entityClass,
				optionalSpecs,
				optionalSort,
				optionalPageNumber,
				optionalPageSize,
				optionalConjunction,
				optionalSince
			);

			final Query query = databaseFilter.toQuery(
				entityManager,
				Optional.empty()
			);

			@SuppressWarnings("unchecked")
			final List<T> results = query.getResultList();
			return results;
		} catch(final NoResultException e) {
			/*
			 *  NO-OP: Results were not found for the given conditions
			 */
			return Collections.newList();
		} finally {
			// Close Entity Manager
			entityManager.close();
		}
	}
}
