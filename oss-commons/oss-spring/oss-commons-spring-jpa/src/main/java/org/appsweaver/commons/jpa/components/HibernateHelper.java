/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.components;

import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.metamodel.Metamodel;

import org.appsweaver.commons.models.persistence.Entity;
import org.appsweaver.commons.utilities.Collections;

/**
 * 
 * @author UD
 * 
 * http://hibernate.org/search/releases/#compatibility-matrix
 * 
 */
public interface HibernateHelper {
	EntityManagerFactory getEntityManagerFactory();
	
	default EntityManager createEntityManager() {
		return getEntityManagerFactory().createEntityManager();
	}

	default <T extends Entity> Set<Class<T>> getAllEntityClasses() {
		final Set<Class<T>> set = Collections.newHashSet();
		
		final EntityManager entityManager = createEntityManager();
		try {
			final EntityManagerFactory entityManagerFactory = entityManager.getEntityManagerFactory();
			final Metamodel metamodel = entityManagerFactory.getMetamodel();

			metamodel.getManagedTypes().forEach(managedType -> {
				@SuppressWarnings("unchecked")
				final Class<T> clazz = (Class<T>) managedType.getJavaType();
				set.add(clazz);
			});
		} finally {
			entityManager.close();
		}
		
		return set;
	}
}
