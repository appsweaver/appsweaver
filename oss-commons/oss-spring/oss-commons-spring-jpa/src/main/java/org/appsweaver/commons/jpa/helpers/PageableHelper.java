/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.helpers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.appsweaver.commons.utilities.Collections;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author UD
 *
 */
public class PageableHelper {
	/**
	 * Convert collection to page (pageable)
	 *
	 * @param collection collection
	 * @param offset offset
	 * @param size size
	 * @param total total
	 * @param <T> This is the type parameter
	 * @return page page
	 */
	public static <T> Page<T> toPage(Collection<T> collection, Integer offset, Integer size, Long total) {
		final boolean makeSublist = offset * size + size > total;
		return toPage(collection, offset, size, total, makeSublist);
	}

	/**
	 * Convert collection to page (pageable) by creating a sub list from a complete list.
	 *
	 * @param collection collection
	 * @param offset offset
	 * @param size size
	 * @param total total
	 * @param makeSubList makeSubList
	 * @param <T> This is the type parameter
	 * @return page
	 */
	public static <T> Page<T> toPage(Collection<T> collection, Integer offset, Integer size, Long total, boolean makeSubList) {

		/*
		 * Pages are zero indexed, thus providing 0 for page will return the first page.
		 * 
		 * Size must not be less than 1
		 */
		final Pageable pageable = PageRequest.of(offset, size <= 0 ? 1 : size);
		final List<T> list = new ArrayList<>(collection);
		final Page<T> page;

		if(makeSubList) {
			final int end = offset + size > collection.size() ? collection.size() : size;

			page = new PageImpl<>(list.subList(0, end), pageable, total);
		} else {
			page = new PageImpl<>(list, pageable, total);
		}

		return page;
	}

	public static <T> Page<T> emptyPage() {
		return new PageImpl<>(Collections.newList());
	}
}
