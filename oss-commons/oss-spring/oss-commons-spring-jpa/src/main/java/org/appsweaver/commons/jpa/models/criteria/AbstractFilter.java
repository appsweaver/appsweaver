/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.models.criteria;

import java.sql.SQLException;
import java.util.Map;

import org.appsweaver.commons.models.criteria.FilterableField;
import org.appsweaver.commons.models.persistence.Entity;
import org.appsweaver.commons.utilities.Stringizer;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * @author UD
 *
 * Create filter from expressions
 *
 *  Syntax: { 'name' : 'operator(value)' } <br>
 *  	<b>'name'</b> : name of the attribute (refer to entity class) <br>
 *  	<b>'operator'</b> : Optional [default: eq] <br>
 *
 * 		<b>Supported operators and example: </b><br>
 * 		<b>eq</b> Compare if value equals <br>
 * 		Example: { 'name' : 'eq(value)' } <br>
 *
 * 		<b>!eq</b> : Compare if value NOT equals <br>
 * 		Example: { 'name' : '!eq(value)' } <br>
 *
 * 		<b>lt</b> : Compare if value is less than <br>
 * 		Example: { 'name' : 'lt(numericValue)' } <br>
 *
 *		<b>gt</b> : Compare if value is greater than <br>
 *		Example: { 'name' : 'gt(numericValue)' } <br>
 *
 * 		<b>le</b> : Compare if value is less than or equals <br>
 * 		Example: { 'name' : 'le(numericValue)' } <br>
 *
 * 		<b>ge</b> : Compare if value is greater than or equals <br>
 * 		Example: { 'name' : 'ge(numericValue)' } <br>
 *
 * 		<b>in</b> : Check if value is in the given list <br>
 * 		Example: { 'name' : 'in(1, 2, 3, 4, 5)' } <br>
 *
 * 		<b>!in</b> : Check if value is NOT in the given list <br>
 * 		Example: { 'name' : '!in(1, 2, 3, 4, 5)' } <br>
 *
 * 		<b>like</b>: Compare if value is like (SQL operator) <br>
 * 		Example: { 'name' : 'like(value)' } <br>
 *
 * 		<b>!like</b>: Compare if value is NOT like (SQL operator) <br>
 * 		Example: { 'name' : '!like(value)' } <br>
 *
 * 		<b>between</b>: Compare if value is within given range <br>
 * 		Example: { 'name' : 'between(x, y)' }  <br>
 * 				 x &#38; y must be numeric values <br>
 *
 * 		<b>!between</b> : Compare if value is NOT within given range  <br>
 * 		Example: { 'name' : '!between(x, y)' } <br>
 * 				 x &#38; y must be numeric values <br>
 *
 *
 */
public class AbstractFilter<T extends Entity> {
	final FilterableField filterableField;
	final String value;
	final FilterExpressionParser filterExpressionParser;

	public AbstractFilter(FilterableField filterableField, String value) {
		this.filterableField = filterableField;
		this.value = value;
		
		// Parse and cache
		this.filterExpressionParser = new FilterExpressionParser(getPath(), value);
	}

	@JsonIgnore
	public Class<?> getType() {
		return filterableField.getType();
	}

	@JsonIgnore
	public String getName() {
		return filterableField.getName();
	}
	
	@JsonIgnore
	public String getPath() {
		return filterableField.getPath();
	}
	
	@JsonIgnore
	public String getValue() {
		return filterExpressionParser.getValue();
	}
	
	@JsonIgnore
	public String getOperator() {
		return filterExpressionParser.getOperator();
	}
	
	public void cacheAndValidate(Map<String, FilterableField> fields) throws SQLException {
		final String name = filterExpressionParser.getName();
		
		if(!fields.containsKey(name)) {
			throw new SQLException(String.format("Unsupported filterable attribute [name=%s]", name));
		}
	}

	final boolean isChild(String attribute) {
		final String []attribtuesAsArray = attribute.split("\\.");
		return attribtuesAsArray.length > 1;
	}
	
	public Object sanitizeValue(Object valueObject) {
		String stringValue = String.valueOf(valueObject);
		try {
			return Stringizer.decodeUrl(stringValue);
		} catch (Throwable e) {
			return stringValue;
		}
	}
	
}
