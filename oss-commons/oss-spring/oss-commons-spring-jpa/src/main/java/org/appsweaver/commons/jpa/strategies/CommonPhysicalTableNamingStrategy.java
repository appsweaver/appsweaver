/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.strategies;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;

/**
 * 
 * @author UD
 * 
 * Common naming strategy provider must be specialized by projects explicitly to prefix table names.
 * 
 * spring.jpa.hibernate.naming.physical-strategy must be added to application.yml file with class extending form this class.
 * 
 */
public abstract class CommonPhysicalTableNamingStrategy extends PhysicalNamingStrategyStandardImpl {
	private static final long serialVersionUID = -1131707523031497095L;

	final String prefix;

	public CommonPhysicalTableNamingStrategy(String prefix) {
		this.prefix = prefix.toLowerCase();
	}

	@Override
	public Identifier toPhysicalTableName(Identifier name, JdbcEnvironment context) {
		return new Identifier(prefix + addUnderscores(name.getText()), name.isQuoted());
	}
	
	@Override
    public Identifier toPhysicalSchemaName(Identifier name, JdbcEnvironment context) {
		return new Identifier(name.getText().toLowerCase(), name.isQuoted());
    }

	protected static String addUnderscores(String name) {
		final StringBuilder stringBuffer = new StringBuilder(name.replace('.', '_'));
		
		for (int i = 1; i < stringBuffer.length() - 1; i++) {
			if (Character.isLowerCase(stringBuffer.charAt(i - 1)) &&
					Character.isUpperCase(stringBuffer.charAt(i)) &&
					Character.isLowerCase(stringBuffer.charAt(i + 1))) {
				stringBuffer.insert(i++, '_');
			}
		}
		
		return stringBuffer.toString().toLowerCase();
	}
}
