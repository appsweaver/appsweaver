/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.model.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.appsweaver.commons.spring.components.Cryptographer;

/**
 * 
 * @author UD
 * 
 */
@Converter
public class PasswordAttributeConverter implements AttributeConverter<String, String> {
	@Override
	public String convertToDatabaseColumn(String rawText) {
		return Cryptographer.encryptPassword(rawText);
	}

	@Override
	public String convertToEntityAttribute(String encryptedText) {
		/*
		 * NOTE:
		 * 
		 * Passwords are encrypted using one-way encryption algorithm.  Password text cannot 
		 * be decrypted, instead check password is used to compare two strings that were 
		 * encrypted using one-way algorithm 
		 */
		return encryptedText;
	}
}
