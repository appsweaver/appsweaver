/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.models.custom;

import javax.persistence.Column;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.EqualsAndHashCode;

/**
 *
 * @author UD
 *
 */
@DynamicUpdate
@javax.persistence.Entity
@Table(name = "CUSTOM_INTEGER_VALUES")
@EqualsAndHashCode(callSuper = true)
public class IntegerValue extends Value<Long> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6147737138756259683L;

	@Column(name = "VALUE")
	private Long value;
	
	public IntegerValue() {
		super();
	}
	
	public IntegerValue(String fieldName, Long value) {
		super();
		
		setFieldName(fieldName);
		setValue(value);
	}
	
	public IntegerValue(CustomDataType type, Long value) {
		super(type);
		
		setValue(value);
	}

	@Override
	public Long getValue() {
		return value;
	}

	@Override
	public void setValue(Long value) {
		this.value = value;
	}
}
