/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.helpers;

import java.util.List;

import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Stringizer;

/**
 *
 * @author UD
 *
 */
public class FacetRangeParser {
	final String name;
	final List<String> ranges = Collections.newList();

	public FacetRangeParser(String facetNamePattern) {
		if (Stringizer.isEmpty(facetNamePattern)) {
			throw new IllegalArgumentException("Facet name pattern cannot be null or empty");
		}

		final String[] tokens = facetNamePattern.split(":");
		this.name = tokens[0];

		if (tokens.length >= 2) {
			final String rangeText = tokens[1];

			if (!Stringizer.isEmpty(rangeText)) {
				this.ranges.addAll(Stringizer.toList(rangeText, ";"));
			}
		}
	}

	public List<String> getRanges() {
		return ranges;
	}

	public String getName() {
		return name;
	}
}