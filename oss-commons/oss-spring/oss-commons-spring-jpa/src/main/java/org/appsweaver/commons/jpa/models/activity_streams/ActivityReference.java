/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.models.activity_streams;

import javax.persistence.Embeddable;

import org.appsweaver.commons.types.activity_streams.ActivityReferenceVO;

/**
 *
 * @author UD
 *
 */
@Embeddable
public class ActivityReference {
	private String id;
	
	private String name;

	private String origin;

	private String type;

	public ActivityReference() {
	}

	public ActivityReference(ActivityReferenceVO activityReferenceVO) {
		this.setId(activityReferenceVO.getId());
		this.setName(activityReferenceVO.getName());
		this.setOrigin(activityReferenceVO.getOrigin());
		this.setType(activityReferenceVO.getType());
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Get name of the origin system
	 *
	 * @return origin
	 */
	public String getOrigin() {
		return origin;
	}

	/**
	 * Set name of origin
	 *
	 * @param origin origin
	 */
	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type
	 *            the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
}
