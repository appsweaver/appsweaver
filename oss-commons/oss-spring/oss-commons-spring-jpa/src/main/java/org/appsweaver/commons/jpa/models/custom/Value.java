/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.models.custom;

import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.appsweaver.commons.jpa.models.AbstractEntity;
import org.appsweaver.commons.jpa.types.FieldType;
import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.EqualsAndHashCode;

/**
 *
 * @author UD
 *
 */
@DynamicUpdate
@javax.persistence.Entity
@Table(name = "CUSTOM_VALUES")
@Inheritance(strategy = InheritanceType.JOINED)
@EqualsAndHashCode(callSuper = true)
public abstract class Value<T> extends AbstractEntity implements CustomValue<T> {
	private static final long serialVersionUID = -2758890234182477742L;
	
	@ManyToOne(fetch = FetchType.EAGER)
	protected CustomDataType dataType;
	
	@Transient
	protected String fieldName;

	public Value() {
		super();
	}
	
	public Value(CustomDataType type) {
		super();
		
		setDataType(type);
	}
	
	@JsonIgnore
	@Override
	public CustomDataType getDataType() {
		return dataType;
	}

	@Override
	public void setDataType(CustomDataType dataType) {
		this.dataType = dataType;
		
		setFieldName(dataType.getName());
	}

	@Override
	public String getFieldName() {
		return (dataType != null) ? dataType.getName() : fieldName;
	}
	
	@Override
	public FieldType getFieldType() {
		return (dataType != null) ? dataType.getType() : null;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
}
