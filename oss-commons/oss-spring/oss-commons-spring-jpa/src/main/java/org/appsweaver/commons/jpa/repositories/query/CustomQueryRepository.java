/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.repositories.query;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import org.appsweaver.commons.jpa.components.QueryHibernateHelper;
import org.appsweaver.commons.jpa.types.FilterConjunction;
import org.appsweaver.commons.models.persistence.QueryEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * 
 * @author UD
 * 
 * @param <T> - entity class type
 * 
 */
@NoRepositoryBean
public interface CustomQueryRepository<T extends QueryEntity> extends QueryRepository<T, UUID> {
	/**
	 * Get handle to Hibernate helper implementation
	 * 
	 * @return instance of HibernateHelper
	 */
	QueryHibernateHelper getHibernateHelper();

	/**
	 * Fetch all records matching specifications, paged and sorted since epoch
	 * 
	 * @param specifications list of filter specifications
	 * @param pageable page request
	 * @param conjunction filter combination operator
	 * @param since epoch since record was updated or created
	 * @return page of rows
	 * @throws SQLException when errors are encountered
	 */
	Page<T> findAll(List<Specification<T>> specifications, Pageable pageable, FilterConjunction conjunction, Long since) throws SQLException;
	
	/**
	 * Deletes a given entity in one hibernate transaction session.
	 *
	 * @param id id of the given entity, must not be {@literal null}.
	 * @throws IllegalArgumentException in case the given entity is {@literal null}.
	 */
	void delete(UUID id);

}