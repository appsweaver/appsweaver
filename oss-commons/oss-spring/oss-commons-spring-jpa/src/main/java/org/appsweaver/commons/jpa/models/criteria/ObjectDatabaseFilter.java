/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.models.criteria;

import static org.appsweaver.commons.utilities.TypeConverter.valueOf;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.appsweaver.commons.jpa.types.FilterConjunction;
import org.appsweaver.commons.models.persistence.QueryEntity;
import org.appsweaver.commons.utilities.FilterHelper;
import org.appsweaver.commons.utilities.Stringizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author UD
 *
 * Create OGM query string from filter specifications
 *
 */
public class ObjectDatabaseFilter<T extends QueryEntity> {
	final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	final Class<T> entityClass;
	final Optional<List<Specification<T>>> optionalSpecs;
	final Optional<Sort> optionalSort;
	final Optional<Integer> optionalPageNumber;
	final Optional<Integer> optionalPageSize;
	final Optional<FilterConjunction> optionalConjunction;
	final Optional<Long> optionalSince;
	
	public ObjectDatabaseFilter(
		Class<T> entityClass,
		Optional<List<Specification<T>>> optionalSpecs,
		Optional<Sort> optionalSort,
		Optional<Integer> optionalPageNumber,
		Optional<Integer> optionalPageSize,
		Optional<FilterConjunction> optionalConjunction,
		Optional<Long> optionalSince
	) {
		this.entityClass = entityClass;
		this.optionalSpecs = optionalSpecs;
		this.optionalSort = optionalSort;
		this.optionalPageNumber = optionalPageNumber;
		this.optionalPageSize = optionalPageSize;
		this.optionalConjunction = optionalConjunction;
		this.optionalSince = optionalSince;
	}

	public Query toQuery(EntityManager entityManager, Optional<String> sqlStatement) {
		final StringBuffer stringBuffer = new StringBuffer();
		
		if(sqlStatement.isPresent()) {
			stringBuffer.append(sqlStatement.get());
		} else {
			stringBuffer.append("FROM");
		}
		
		stringBuffer.append(" ").append(entityClass.getSimpleName());
		
		// Where clause MUST BE first
		if(optionalSpecs.isPresent()) {
			final List<Specification<T>> specifications = optionalSpecs.get();
			final String whereClause = toWhereClause(
				specifications,
				(optionalConjunction.isPresent()) ? optionalConjunction.get() : null
			);
			stringBuffer.append(whereClause);
		}
		
		// Factor since option
		if(optionalSince.isPresent()) {
			final String subQuery;
			if(stringBuffer.toString().contains(" WHERE ")) {
				subQuery = " AND (createdOn >= :since OR updatedOn >= :since)";
			} else {
				subQuery = " WHERE (createdOn >= :since OR updatedOn >= :since)";
			}
			
			stringBuffer.append(subQuery);
		}
		
		// Order by clause MUST BE after where clause
		if(optionalSort.isPresent()) {
			final Sort sort = optionalSort.get();
			
			if(sort.isSorted()) {
				final String orderByClause = toOrderByClause(sort);
				stringBuffer.append(orderByClause);
			}
		}
		
		final String queryText = stringBuffer.toString();
		final Query query = entityManager.createQuery(queryText);
		
		// Perform parameter substitution without fail
		substituteParameters(query, optionalSpecs);
		
		// Factor since option
		if(optionalSince.isPresent()) {
			final Long since = optionalSince.get();
			query.setParameter("since", since);
		}

		if (optionalPageNumber.isPresent() && optionalPageSize.isPresent()) {
			final Integer pageNumber = optionalPageNumber.get();
			final Integer pageSize = optionalPageSize.get();
			
			// Compute offset
			final Integer offset = (pageNumber * pageSize);
			
			// Starting page number
			query.setFirstResult(offset);

			// Page size
			query.setMaxResults(pageSize);
		}
		
		return query;
	}
	
	
	/**
	 * 
	 * @param <T> type of object
	 * @param specifications the criteria
	 * @return the where clause text
	 */
	public String toWhereClause(List<Specification<T>> specifications) {
		return toWhereClause(specifications, FilterConjunction.AND);
	}

	/**
	 * Convert sort to order by clause text
	 * 
	 * @param sort sort values
	 * @return order by clause text
	 */
	public String toOrderByClause(Sort sort) {
		final StringBuffer stringBuffer = new StringBuffer();
		
		if(sort != null) {
			// Include ORDER BY clause
			stringBuffer.append(" ORDER BY ");

			final long count = count(sort);
			final Stream<Order> orders = sort.get();
			final Iterator<Order> iterator = orders.iterator();
			
			int index = 0;
			while(iterator.hasNext()) {
				final Order order = iterator.next();
				
				final String property = order.getProperty();
				final Direction direction = order.getDirection();
				
				if(index > 0 && index < count) {
					stringBuffer.append(", ");
				}
				
				if(!Stringizer.isEmpty(property)) {
					// Include property
					stringBuffer.append(property);
				}
				
				if(direction != null) {
					// Include direction
					stringBuffer.append(" ").append(direction.name());
				}
				
				index++;
			};
		}
		
		final String orderBy = stringBuffer.toString();
		return orderBy;
	}

	/**
	 * Convert list of specifications to where clause text
	 * 
	 * @param <T> type of object
	 * @param specifications the criteria
	 * @param conjunction the join option
	 * @return order by clause text
	 */
	public String toWhereClause(List<Specification<T>> specifications, FilterConjunction conjunction) {
		final StringBuffer stringBuffer = new StringBuffer();

		if(conjunction == null) {
			// Default to AND conjunction
			conjunction = FilterConjunction.AND;
		}
		
		if(specifications != null && !specifications.isEmpty()) {
			// Include WHERE clause
			stringBuffer.append(" WHERE ");
			
			int index = 0;
			final Iterator<Specification<T>> iterator = specifications.iterator();
			while(iterator.hasNext()) {
				final Specification<T> specification = iterator.next();
				
				if(index > 0 && index < specifications.size()) {
					stringBuffer.append(" ").append(conjunction.name()).append(" ");
				}
				
				if(specification instanceof RelationalDatabaseFilter) {
					final RelationalDatabaseFilter<T> databaseFilter = (RelationalDatabaseFilter<T>)specification;
					stringBuffer.append(toWhereClause(databaseFilter));
				}
				
				index++;
			}
		}
		
		final String queryText = stringBuffer.toString();
		logger.debug("WHERE Clause: {}", queryText);
		return queryText;
	}
	
	long count(Sort sort) {
		return (sort != null) ? sort.get().count() : 0L;
	}
	
	String toWhereClause(RelationalDatabaseFilter<T> databaseFilter) {
		final StringBuffer stringBuffer = new StringBuffer();

		final String name = databaseFilter.getName();
		final String path = databaseFilter.getPath();
		final String operator = databaseFilter.getOperator();
		final Class<?> type = databaseFilter.getType();
		String value = databaseFilter.getValue();
		
		stringBuffer.append(path);
		switch(operator) {
		case "lt":
			stringBuffer.append(" < ").append(":").append(name);
			break;
			
		case "le":
			stringBuffer.append(" <= ").append(":").append(name);
			break;
			
		case "gt":
			stringBuffer.append(" > ").append(":").append(name);
			break;
			
		case "ge":
			stringBuffer.append(" >= ").append(":").append(name);
			break;
			
		case "in":
		case "!in":
		case "!eq":
			if(operator.startsWith("!")) {
				stringBuffer.append(" NOT");
			}
			stringBuffer.append(" IN ");
			stringBuffer.append(":").append(name);
			break;

		case "eq":
			stringBuffer.append(" = ").append(":").append(name);
			break;
			
		case "null":
		case "!null":
			if(operator.startsWith("!")) {
				stringBuffer.append(" IS NOT NULL");
			} else {
				stringBuffer.append(" IS NULL");
			}
			break;
			
		case "like":
		case "!like":
			if(operator.startsWith("!")) {
				stringBuffer.append(" NOT");
			}
			if (String.class.isAssignableFrom(type)) { 
				value = String.valueOf(FilterHelper.sanitizeFilterValue(value));
			}
			stringBuffer.append(" LIKE '%").append(value).append("%'");
			break;
			
		case "between":
		case "!between":
			if(operator.startsWith("!")) {
				stringBuffer.append(" NOT");
			}
			stringBuffer
				.append(" BETWEEN ")
				.append(":").append(name).append("MinimumValue")
				.append(" AND ")
				.append(":").append(name).append("MaximumValue");
			break;
		}
		
		final String whereClause = stringBuffer.toString();
		logger.debug("WHERE Clause: {}", whereClause);
		return whereClause;
	}
	
	void substituteParameters(
		Query query,
		Optional<List<Specification<T>>> optionalSpecs
	) {
		if(optionalSpecs.isPresent()) {
			final List<Specification<T>> specifications = optionalSpecs.get();
			
			specifications.forEach(specification -> {
				if(specification instanceof RelationalDatabaseFilter) {
					final RelationalDatabaseFilter<T> filterSpecification = (RelationalDatabaseFilter<T>)specification;
					
					final String name = filterSpecification.getName();
					final String operator = filterSpecification.getOperator();
					final Class<?> type = filterSpecification.getType();
					String value = filterSpecification.getValue();
					
					switch(operator) {
					case "between":
					case "!between":
						final String[] tokens = value.split(",");
						assert(tokens.length == 2);
						final String start = tokens[0].trim();
						final String end = tokens[1].trim();
						
						query.setParameter(name + "MinimumValue", valueOf(start, type));
						query.setParameter(name + "MaximumValue", valueOf(end, type));
						
						break;
						
					case "like":
					case "!like":
					case "null":
					case "!null":
						break;
						
					default:
						if (String.class.isAssignableFrom(type)) {
							value = String.valueOf(FilterHelper.sanitizeFilterValue(value));
						}
						query.setParameter(name, valueOf(value, type));
						break;
					}
				}
			});
		}
	}

}
