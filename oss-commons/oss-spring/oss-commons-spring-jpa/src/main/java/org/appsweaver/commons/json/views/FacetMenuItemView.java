/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.json.views;

import java.util.List;

import org.appsweaver.commons.jpa.models.search.FacetMenuItem;

/**
 * 
 * @author UD
 * 
 */
public class FacetMenuItemView {
	private String name;
	private List<FacetMenuItem> items;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<FacetMenuItem> getItems() {
		return items;
	}

	public void setItems(List<FacetMenuItem> items) {
		this.items = items;
	}
}