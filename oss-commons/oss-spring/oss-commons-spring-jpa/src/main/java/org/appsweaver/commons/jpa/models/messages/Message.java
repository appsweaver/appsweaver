/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.models.messages;

import java.util.StringJoiner;

import org.appsweaver.commons.utilities.Dates;

/**
 * 
 * @author UD
 * 
 */
public class Message<T> {
	private Long id;
	
	private Sender<T> from;
	private Recepient<T> to;
	private String subject;
	private String body;
	private Long sentOn = Dates.toTime();
	private Long readOn;
	private String topic;

	public Sender<T> getFrom() {
		return from;
	}

	public void setFrom(Sender<T> from) {
		this.from = from;
	}

	public Recepient<T> getTo() {
		return to;
	}

	public void setTo(Recepient<T> to) {
		this.to = to;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Long getSentOn() {
		return sentOn;
	}

	public void setSentOn(Long sentOn) {
		this.sentOn = sentOn;
	}

	public Long getReadOn() {
		return readOn;
	}

	public void setReadOn(Long readOn) {
		this.readOn = readOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", Message.class.getSimpleName() + "[", "]").add("id=" + id).add("from=" + from).add("to=" + to)
				.add("subject='" + subject + "'").add("body='" + body + "'").add("sentOn=" + sentOn).add("readOn=" + readOn)
				.add("topic='" + topic + "'").toString();
	}
}
