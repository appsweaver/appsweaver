/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.models.criteria;

import static org.appsweaver.commons.utilities.TypeConverter.toSet;

import java.security.InvalidParameterException;
import java.util.List;

import org.appsweaver.commons.spring.criteria.AbstractExpressionParser;
import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Stringizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author BP
 *
 */
public class FilterExpressionParser extends AbstractExpressionParser {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	final String name;

	public FilterExpressionParser(String name, String value) {
		// Consider wild card string as like filter
		if(value.contains("*")) {
			value = String.format("like(%s)", value);
		}
		
		if (!isExpression(value)) {
			operator = "eq"; // Default to "eq"
			this.name = name;
			this.value = value;
		} else {
			operator = getOperator(value);
			this.name = name;
			this.value = getValue(value);
		}
	}

	public String getName() {
		return name;
	}

	@Override
	public String getValue() {
		switch(operator) {
		case "in":
		case "!in":
		case "between":
		case "!between": {
			if(value == null || value.isEmpty()) {
				throw new InvalidParameterException(String.format("Insufficient parameters / expression for '%s' operator", operator));
			}
			
			final List<String> values = Collections.newList(toSet(value, String.class));
			
			final String csvString = Stringizer.toCSVString(values);
			return csvString;
		}
		
		default:
			break;
		}
		
		return value;
	}
}
