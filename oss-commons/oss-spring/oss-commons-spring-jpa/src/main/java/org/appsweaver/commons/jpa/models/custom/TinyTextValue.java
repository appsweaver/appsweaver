/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.models.custom;

import javax.persistence.Column;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.EqualsAndHashCode;

/**
 *
 * @author UD
 *
 */
@DynamicUpdate
@javax.persistence.Entity
@Table(name = "CUSTOM_TINY_TEXT_VALUES")
@EqualsAndHashCode(callSuper = true)
public class TinyTextValue extends TextValue {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9052703735888124179L;
	
	public static final int LENGTH = 4096;

	public TinyTextValue() {
		super();
	}
	
	public TinyTextValue(String fieldName, String value) {
		super(fieldName, value);
	}
	
	public TinyTextValue(CustomDataType type, String value) {
		super(type, value);
	}

	@Column(name = "VALUE", length = LENGTH)
	@Override
	public String getValue() {
		return value;
	}
}
