/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.json.views;

import java.util.UUID;

import org.appsweaver.commons.jpa.models.BaseEntity;

/**
 * 
 * @author SN
 * 
 */
public class ExtendedView<T extends BaseEntity> extends DefaultView {
	protected Long createdOn;
	protected Long updatedOn;
	
	protected UUID createdBy;
	protected UUID updatedBy;

	public ExtendedView(T entity) {
		super(entity);
		
		this.createdOn = entity.getCreatedOn();
		this.updatedOn = entity.getUpdatedOn();
		this.createdBy = entity.getCreatedBy();
		this.updatedBy = entity.getUpdatedBy();
	}

	public Long getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Long createdOn) {
		this.createdOn = createdOn;
	}

	public Long getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Long updatedOn) {
		this.updatedOn = updatedOn;
	}

	public UUID getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(UUID createdBy) {
		this.createdBy = createdBy;
	}

	public UUID getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(UUID updatedBy) {
		this.updatedBy = updatedBy;
	}
}
