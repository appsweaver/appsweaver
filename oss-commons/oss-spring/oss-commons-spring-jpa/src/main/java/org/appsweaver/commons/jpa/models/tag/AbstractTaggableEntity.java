/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.models.tag;

import java.util.List;
import java.util.Set;

import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.MappedSuperclass;

import org.appsweaver.commons.annotations.Filterable;
import org.appsweaver.commons.annotations.Searchable;
import org.appsweaver.commons.jpa.models.AbstractEntity;
import org.appsweaver.commons.jpa.models.bridge.StringCollectionFieldBridge;
import org.appsweaver.commons.json.converters.TagCollectionConverter;
import org.appsweaver.commons.utilities.Collections;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Facet;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.FieldBridge;
import org.hibernate.search.annotations.IndexedEmbedded;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.EqualsAndHashCode;

/**
 * 
 * @author UD
 * 
 */
@MappedSuperclass
@EqualsAndHashCode(callSuper = true)
public abstract class AbstractTaggableEntity extends AbstractEntity implements TaggableEntity {
	private static final long serialVersionUID = -6803397651320907042L;
	
	// Facet Names
	private static final String TAG_NAME_FACET = "tag_name_facet";
	// Facet Names

	@JsonSerialize(converter = TagCollectionConverter.class)
	@Searchable
	@IndexedEmbedded(includePaths = { "name" })
	@Filterable(path = "tags.name")
	@ManyToMany(fetch = FetchType.EAGER)
	protected Set<Tag> tags = Collections.newHashSet();

	@JsonIgnore
	@Searchable(lookup = false)
	@Facet(forField = TAG_NAME_FACET)
	@Field(name = TAG_NAME_FACET, analyze = Analyze.NO, bridge = @FieldBridge(impl = StringCollectionFieldBridge.class))
	public List<String> getTagNames() {
		final List<String> tagNames = Collections.newList();
		
		if(tags != null && tags.size() > 0) {
			tags.forEach(tag -> {
				tagNames.add(tag.getName());
			});
		}
		
		return tagNames;
	}
	
	public AbstractTaggableEntity() {
		super();
	}

	@Override
	public Set<Tag> getTags() {
		return tags;
	}

	@Override
	public void setTags(Set<Tag> tags) {
		this.tags = tags;
	}
}
