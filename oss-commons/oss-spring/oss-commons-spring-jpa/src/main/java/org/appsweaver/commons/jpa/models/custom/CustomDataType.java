/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.jpa.models.custom;

import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import org.appsweaver.commons.annotations.Searchable;
import org.appsweaver.commons.jpa.models.BaseEntity;
import org.appsweaver.commons.jpa.types.FieldType;
import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Stringizer;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import lombok.EqualsAndHashCode;

/**
 * 
 * @author UD
 *
 */
@DynamicUpdate
@javax.persistence.Entity
@Table(name = "CUSTOM_DATA_TYPES")
@Indexed(index = "CUSTOM_DATA_TYPES")
@EqualsAndHashCode(callSuper = true)
public class CustomDataType extends BaseEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6739162666819399030L;
	
	@Field
	@Searchable
	@Column(name = "NAME", unique = true, nullable = false, length = 2048)
	private String name;
	
	@Column(name = "TYPE", nullable = false, length = 128)
	@Enumerated(EnumType.STRING)
	private FieldType type;
	
	@Column(name = "DEFAULT_VALUE", length = 2048)
	private String defaultValue;
	
	@Column(name = "INTEGER_MIN_VALUE")
	private Integer integerMinValue;
	
	@Column(name = "INTEGER_MAX_VALUE")
	private Integer integerMaxValue;
	
	@Column(name = "DECIMAL_MIN_VALUE")
	private Double decimalMinValue;
	
	@Column(name = "DECIMAL_MAX_VALUE")
	private Double decimalMaxValue;
	
	@Column(name = "DECIMAL_FORMAT", length = 128)
	private String decimalFormat;
	
	@ElementCollection
    @CollectionTable(name = "CUSTOM_DATA_TYPE_ALLOWED_LIST_OF_VALUES", joinColumns = @JoinColumn(name = "CUSTOM_DATA_TYPE_ID"))
	private List<String> listOfValues = Collections.newList();

	public CustomDataType() {
		super();
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public FieldType getType() {
		return type;
	}

	public void setType(FieldType type) {
		this.type = type;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public Integer getIntegerMinValue() {
		return integerMinValue;
	}

	public void setIntegerMinValue(Integer integerMinValue) {
		this.integerMinValue = integerMinValue;
	}

	public Integer getIntegerMaxValue() {
		return integerMaxValue;
	}

	public void setIntegerMaxValue(Integer integerMaxValue) {
		this.integerMaxValue = integerMaxValue;
	}

	public Double getDecimalMinValue() {
		return decimalMinValue;
	}

	public void setDecimalMinValue(Double decimalMinValue) {
		this.decimalMinValue = decimalMinValue;
	}

	public Double getDecimalMaxValue() {
		return decimalMaxValue;
	}

	public void setDecimalMaxValue(Double decimalMaxValue) {
		this.decimalMaxValue = decimalMaxValue;
	}

	public String getDecimalFormat() {
		return decimalFormat;
	}

	public void setDecimalFormat(String decimalFormat) {
		this.decimalFormat = decimalFormat;
	}

	public List<String> getListOfValues() {
		return listOfValues;
	}

	public void setListOfValues(List<String> listOfValues) {
		this.listOfValues = listOfValues;
	}
	
	@Override
	public String toString() {
		return String.format(
			"name=%s, type=%s, defaultValue=%s, integerMinValue=%d, integerMaxValue=%d, decimalMinValue=%f, decimalMaxValue=%f, decimalFormat=%s, listOfValues=%s",
			getName(),
			getType(),
			getDefaultValue(),
			getIntegerMinValue(),
			getIntegerMaxValue(),
			getDecimalMinValue(),
			getDecimalMaxValue(),
			getDecimalFormat(),
			Stringizer.toCSVString(listOfValues)
		);
	}
}
