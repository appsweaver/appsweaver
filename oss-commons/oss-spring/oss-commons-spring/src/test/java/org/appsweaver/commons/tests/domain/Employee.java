/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.tests.domain;

import java.util.UUID;

import org.appsweaver.commons.models.persistence.Entity;

/**
 *
 * @author bpolat
 * @since May 7, 2020
 *
 */
public class Employee implements Entity {

	private static final long serialVersionUID = 1L;

	private UUID id;

	private String firstName;

	private String lastName;

	private String email;

	private Long birthDate;

	private Long employmentDate;

	public Employee() {

	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Long birthDate) {
		this.birthDate = birthDate;
	}

	public Long getEmploymentDate() {
		return employmentDate;
	}

	public void setEmploymentDate(Long employmentDate) {
		this.employmentDate = employmentDate;
	}

	@Override
	public UUID getId() {
		return id;
	}

	@Override
	public void setId(UUID id) {
		this.id = id;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		
		builder.append("Employee [");
		if (id != null) {
			builder.append("id=");
			builder.append(id);
			builder.append(", ");
		}
		if (firstName != null) {
			builder.append("firstName=");
			builder.append(firstName);
			builder.append(", ");
		}
		if (lastName != null) {
			builder.append("lastName=");
			builder.append(lastName);
			builder.append(", ");
		}
		if (email != null) {
			builder.append("email=");
			builder.append(email);
			builder.append(", ");
		}
		if (birthDate != null) {
			builder.append("birthDate=");
			builder.append(birthDate);
			builder.append(", ");
		}
		if (employmentDate != null) {
			builder.append("employmentDate=");
			builder.append(employmentDate);
		}
		
		builder.append("]");
		return builder.toString();
	}

}
