/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.tests.spring;

import static org.appsweaver.commons.SystemConstants.DATE_FORMAT_MM_DD_YYYY;
import static org.appsweaver.commons.SystemConstants.DATE_FORMAT_YYYY_MM_DD;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.appsweaver.commons.spring.components.writers.AbstractEntityWriter;
import org.appsweaver.commons.spring.components.writers.XlsxEntityWriter;
import org.appsweaver.commons.tests.domain.Employee;
import org.appsweaver.commons.utilities.Dates;
import org.appsweaver.commons.utilities.Ruminator;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author bpolat
 * @since May 6, 2020
 */
public class TestEntityWriter extends AbstractSpringConfigTest {

	protected static ObjectMapper objectMapper = new ObjectMapper();

	private static List<Employee> employees = new ArrayList<>();

	private static List<String> columnOrder = new ArrayList<>(
			Arrays.asList("First Name", "Last Name", "Email", "Date Of Birth", "Employment Date"));

	private static Map<String, String> employeeMapping = new HashMap<>();

	private static String TestResourceDirectory = "src/test/resources/TestEntityWriter/";

	static {
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	@BeforeClass
	@SuppressWarnings("unchecked")
	public static void populateValues() throws Throwable {
		final List<Map<String, String>> employeeArray = getObjectByFileName(TestResourceDirectory + "employeeList.json", List.class);

		for (Map<String, String> employeeItem : employeeArray) {
			final Employee employee = new Employee();

			for (String fieldName : employeeItem.keySet()) {
				if (fieldName.endsWith("Date")) {
					final Long value = Dates.toTime(employeeItem.get(fieldName), DATE_FORMAT_MM_DD_YYYY);
					Ruminator.setProperty(employee, fieldName, value);
				} else {
					Ruminator.setProperty(employee, fieldName, employeeItem.get(fieldName));
				}
			}
			employees.add(employee);
		}

		employeeMapping = getObjectByFileName(TestResourceDirectory + "employeeColumnMapping.json",
				employeeMapping.getClass());
	}

	@AfterClass
	public static void deleteGeneratedResources() throws IOException {
		final File outputDir = new File(TestResourceDirectory + "output");
		if (outputDir.isDirectory()) {
			FileUtils.deleteDirectory(outputDir);
		}
	}

	@Test
	public void testToStringArray() throws Throwable {
		final AbstractEntityWriter<Employee> writer = new XlsxEntityWriter<Employee>();
		for (int index = 0; index < employees.size() - 4; index++) {
			final Employee employee = employees.get(index);
			final String[] exportedEmployee = writer.toStringArray(employee, employeeMapping, Optional.of(columnOrder));

			Assert.assertEquals(exportedEmployee[0], employee.getFirstName());
			Assert.assertEquals(exportedEmployee[1], employee.getLastName());
			Assert.assertEquals(exportedEmployee[2], employee.getEmail());
			Assert.assertEquals(Dates.toTime(exportedEmployee[3], DATE_FORMAT_YYYY_MM_DD), employee.getBirthDate());
			Assert.assertEquals(Dates.toTime(exportedEmployee[4], DATE_FORMAT_MM_DD_YYYY), employee.getEmploymentDate());
		}
	}

	@Test
	public void testToStringArrayDatesWithNullValues() throws Throwable {
		final AbstractEntityWriter<Employee> writer = new XlsxEntityWriter<Employee>();
		
		for (int index = 10; index < employees.size(); index++) {
			final Employee employee = employees.get(index);
			final String[] exportedEmployee = writer.toStringArray(employee, employeeMapping, Optional.of(columnOrder));

			Assert.assertEquals(exportedEmployee[0], employee.getFirstName());
			Assert.assertEquals(exportedEmployee[1], employee.getLastName());
			Assert.assertEquals(exportedEmployee[2], employee.getEmail());
			if (employee.getBirthDate() != null) {
				Assert.assertEquals(Dates.toTime(exportedEmployee[3], DATE_FORMAT_YYYY_MM_DD), employee.getBirthDate());
			} else {
				Assert.assertEquals(exportedEmployee[3], "");
			}
			if (employee.getEmploymentDate() != null) {
				Assert.assertEquals(Dates.toTime(exportedEmployee[4], DATE_FORMAT_MM_DD_YYYY),
						employee.getEmploymentDate());
			} else {
				Assert.assertEquals(exportedEmployee[4], "");
			}
		}
	}

	@Test
	public void testToStringArrayUnsupportedSyntax() {
		employeeMapping.put("Date Of Birth", "timestamp(birthDate, yyyy/MM/dd HH:MM:ss)");
		
		final AbstractEntityWriter<Employee> writer = new XlsxEntityWriter<Employee>();
		for (Employee employee : employees) {
			String[] exportedEmployee = writer.toStringArray(employee, employeeMapping, Optional.of(columnOrder));
			if (employee.getBirthDate() != null) {
				Assert.assertEquals(exportedEmployee[3], employee.getBirthDate().toString());
			} else {
				Assert.assertEquals(exportedEmployee[3], "");
			}
		}
		employeeMapping.put("Date Of Birth", "date(birthDate, yyyy/MM/dd)");
	}

	@Test
	public void testToStringArrayBadSyntax() throws Throwable {
		final AbstractEntityWriter<Employee> writer = new XlsxEntityWriter<Employee>();

		employeeMapping.put("Date Of Birth", "date(birthDate yyyy/MM/dd HH:MM:ss)");
		for (int index = 0; index < employees.size() - 4; index++) {
			String[] exportedSample = writer.toStringArray(employees.get(index), employeeMapping, Optional.of(columnOrder));
			Assert.assertEquals(exportedSample[3], "");
		}

		employeeMapping.put("Date Of Birth", "date(birthDates, yyyy/MM/dd HH:MM:ss)");
		for (int index = 0; index < employees.size() - 4; index++) {
			String[] exportedSample = writer.toStringArray(employees.get(index), employeeMapping, Optional.of(columnOrder));
			Assert.assertEquals(exportedSample[3], "");
		}

		employeeMapping.put("Date Of Birth", "date(birthDate, )");
		for (int index = 0; index < employees.size() - 4; index++) {
			Employee employee = employees.get(index);
			String[] exportedSample = writer.toStringArray(employee, employeeMapping, Optional.of(columnOrder));
			Assert.assertEquals(Dates.toTime(exportedSample[3], DATE_FORMAT_MM_DD_YYYY), employee.getBirthDate());
		}
		
		employeeMapping.put("Date Of Birth", "date(birthDate, yyyy/MM/dd)");
	}

	public static <T> T getObjectByFileName(String fileName, Class<T> clazz) {
		if (StringUtils.isNotBlank(fileName) && null != clazz) {
			return getObjectFromJson(fileName, clazz);
		}
		return null;
	}

	private static <T> T getObjectFromJson(String fileName, Class<T> clazz) {
		try {
			final File file = new File(fileName);
			return objectMapper.readValue(file, clazz);
		} catch (final Throwable e) {
			e.printStackTrace();
		}
		return null;
	}
}
