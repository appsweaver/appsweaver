/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.tests.spring;

import org.appsweaver.commons.spring.components.Cryptographer;
import org.appsweaver.commons.utilities.Randomizer;
import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Test;

/**
 * 
 * @author UD
 *
 */
public class TestCryptographer extends AbstractSpringConfigTest {
	@Test//(expected = EncryptionOperationNotPossibleException.class)
	public void testIsEncrypted() {
		final String encrypted1 = "ENC(NEoWxoTe9iIIY7NgZvkj/KVUWuOzc/Buac1N+96Pb4LRQrnXnnSocykxsCyjVDkQ6Y7vPY6sI349hAySgTqJyw==)";
		Assert.assertTrue(Cryptographer.isEncryptedValue(encrypted1));
		
		final String encrypted2 = "NEoWxoTe9iIIY7NgZvkj/KVUWuOzc/Buac1N+96Pb4LRQrnXnnSocykxsCyjVDkQ6Y7vPY6sI349hAySgTqJyw==";
		Assert.assertTrue(Cryptographer.isEncryptedValue(encrypted2));
		
		Assert.assertFalse(Cryptographer.isEncryptedValue(null));
		
		Assert.assertFalse(Cryptographer.isEncryptedValue(""));
		
		final String encrypted4 = "ENC(NEoWxoTe9iIIY7NgZvkj/KVUWuOzc/Buac1N+96Pb4LRQrnXnnSocykxsCyjVDkQ6Y7vPY6sI349hAySgTqJyw==";
		Assert.assertFalse(Cryptographer.isEncryptedValue(encrypted4));
		
		Assert.assertEquals("Pocoyo is a very nice person who teaches kids best!", Cryptographer.decrypt(encrypted1));

		try {
			final String encrypted5 = "This is a plain unencrypted text!";
			Cryptographer.decrypt(encrypted5);
		} catch(Throwable t) {
			Assert.assertTrue(t instanceof EncryptionOperationNotPossibleException);
		}
	}
	
	@Test
	public void testTextValueEncryptDecrypt() {
		final String text = 
				"During the experimental detonation of a gamma bomb, scientist Robert Bruce Banner saves teenager "
				+ "Rick Jones who has driven onto the testing field; Banner pushes Jones into a trench to save him, but is "
				+ "hit with the blast, absorbing massive amounts of gamma radiation.";
		final String encrypted = Cryptographer.encrypt(text);
		final String decrypted = Cryptographer.decrypt(encrypted);
		Assert.assertEquals(text, decrypted);
	}
	
	@Test
	public void testLongValueEncryptDecrypt() {
		final Long value = Randomizer.nextLong();
		final String encrypted = Cryptographer.encrypt(value);
		final String decrypted = Cryptographer.decrypt(encrypted);
		Assert.assertTrue(Long.compare(value, Long.valueOf(decrypted)) == 0);
	}
	
	@Test
	public void testEncryptDecryptHexedLong() {
		final boolean isHexed = true;
		final Long value = Randomizer.nextLong();
		final String encrypted = Cryptographer.encrypt(value, isHexed);
		final String decrypted = Cryptographer.decrypt(encrypted, isHexed);
		Assert.assertTrue(Long.compare(value, Long.valueOf(decrypted)) == 0);
	}
	
	@Test
	public void testUsernameFromJsonWebToken() {
		try {
			final String token = Cryptographer.createJwtsToken("someone@somedomain.com");
			final String username = Cryptographer.getSubjectFromJwtsToken(token);
			Assert.assertEquals("someone@somedomain.com", username);
		} catch(Throwable t) {
			Assume.assumeNoException(t);
		}
	}
	
	@Test
	public void testPasswordEncryptionAndValidation() {
		final String validPassword   = "YrXU6SZpcDd4cdCuYMcMxbfxau3ZevCHgFJGYe6bYNkYXYwsgJahpQrcSqmZe8M6jvkpeWJm";
		final String invalidPassword = "YrXU6SZpcDd4cdCuYMcMxbfxau4ZevEHgFJGYe6bYNkYXYwsGJahpQrcSqmAe8M0jvkpeWJm";
		
		final String encrypted = Cryptographer.encryptPassword(validPassword);
		Assert.assertTrue(Cryptographer.checkPassword(validPassword, encrypted));
		
		Assert.assertFalse(Cryptographer.checkPassword(invalidPassword, encrypted));
	}
}
