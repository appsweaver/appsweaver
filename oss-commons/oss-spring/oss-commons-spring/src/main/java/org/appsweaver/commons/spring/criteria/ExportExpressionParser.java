/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.spring.criteria;

import static org.appsweaver.commons.SystemConstants.DATE_FORMAT_MM_DD_YYYY;

import org.appsweaver.commons.models.persistence.Entity;
import org.appsweaver.commons.utilities.Dates;
import org.appsweaver.commons.utilities.Ruminator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author bpolat
 * @since May 7, 2020
 */
public class ExportExpressionParser extends AbstractExpressionParser {
	final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public ExportExpressionParser(String value) {
		
		if (!isExpression(value)) {
			operator = "none";
			this.value = value;
		} else {
			operator = getOperator(value);
			this.value = getValue(value);
		}
	}
	
	@Override
	public String getValue() {
		String[] valueMetadata = value.split(",");
		return valueMetadata.length != 0 ? valueMetadata[0] : null;
	}
	
	public String getFormatter() {
		String[] valueMetadata = value.split(",");
		switch(operator) {
		case "date":
			return (valueMetadata.length == 2 && valueMetadata[1].trim().length() != 0)
			? valueMetadata[1] : DATE_FORMAT_MM_DD_YYYY;
		default:
			return null;
		}
	}

	public <T extends Entity> Object getParsedProperty(T item) {
		String fieldName = getValue();
		String fieldFormat = getFormatter();
		switch(operator) {
		case "date":
			Long fieldValue = (Long) Ruminator.getProperty(item, fieldName);
			return (fieldValue != null) ? Dates.toString(fieldValue, fieldFormat) : null;
		default:
			 return Ruminator.getProperty(item, fieldName);
		}
	}
}
