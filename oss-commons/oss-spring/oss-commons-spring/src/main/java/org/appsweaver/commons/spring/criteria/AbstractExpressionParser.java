/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.spring.criteria;

import org.appsweaver.commons.utilities.Stringizer;

/**
 * @author bpolat
 * @since May 7, 2020
 */
public abstract class AbstractExpressionParser {

	static final String EXPR_PREFIX = "(";
	static final String EXPR_SUFFIX = ")";
	
	protected String operator;
	protected String value;
	
	public boolean isExpression(final String value) {
		if (value == null) {
			return false;
		}
		
		final String trimmedValue = value.trim();
		return trimmedValue.contains(EXPR_PREFIX) && trimmedValue.endsWith(EXPR_SUFFIX);
	}

	protected static String getValue(final String value) {
		return value.substring(value.indexOf(EXPR_PREFIX, 0) + 1, value.length() - EXPR_SUFFIX.length());
	}

	public static String getOperator(final String value) {
		final String operator = value.substring(0, value.indexOf(EXPR_PREFIX, 0));
		return (!Stringizer.isEmpty(operator)) ? operator.toLowerCase() : operator;
	}
	
	public String getOperator() {
		return operator;
	}

	public abstract String getValue();
}
