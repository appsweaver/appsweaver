/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.spring.components;

import java.util.List;

import org.appsweaver.commons.models.properties.SecuredResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.stereotype.Component;

/**
 * 
 * @author AC
 * 
 */
@Component
public class WebSecurityHelper {
	final Logger logger = LoggerFactory.getLogger(getClass());
	
	public void addSecuredRoutes(
		ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry expressionInterceptUrlRegistry,
		List<SecuredResource> securedResources
	) {
		if (null != securedResources && !securedResources.isEmpty()) {
			securedResources.forEach(securedResource -> {
				final String url = securedResource.getUrl();
			
				logger.debug("Adding url {} , method {} and permission {} to HttpSecurity", url, securedResource.getMethod(), securedResource.getType());

				final ExpressionUrlAuthorizationConfigurer<?>.AuthorizedUrl authorizedUrl;
				if (null != securedResource.getMethod()) {
					authorizedUrl = expressionInterceptUrlRegistry.antMatchers(securedResource.getMethod(), url);
				} else {
					authorizedUrl = expressionInterceptUrlRegistry.antMatchers(url);
				}

				addRoutePermission(authorizedUrl, securedResource.getType());
			});
		}
	}

	public void addRoutePermission(ExpressionUrlAuthorizationConfigurer<?>.AuthorizedUrl authorizedUrl,
			SecuredResource.AllowedType allowedType) {
		switch (allowedType) {
		case PERMIT:
			authorizedUrl.permitAll();
			break;
		case DENY:
			authorizedUrl.denyAll();
			break;
		default:
			logger.error("Unsupported secured resource permission {}", allowedType);
			break;
		}
	}
}
