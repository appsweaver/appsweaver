/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.spring.components.writers;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import org.appsweaver.commons.api.EntityWriter;
import org.appsweaver.commons.models.persistence.Entity;
import org.appsweaver.commons.spring.criteria.ExportExpressionParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author UD
 *
 */
public abstract class AbstractEntityWriter<T extends Entity> implements EntityWriter<T> {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public void write(Class<T> entityClass, File file, Stream<T> stream, Map<String, String> columnMapping, Optional<List<String>> optionalColumnOrder) {
		writeToFile(entityClass, file, stream, columnMapping, optionalColumnOrder);
	}
	
	public void write(Class<T> entityClass, File file, Stream<T> stream, Map<String, String> columnMapping) {
		write(entityClass, file, stream, columnMapping, Optional.empty());
	}

	public abstract void writeToFile(Class<T> entityClass, File file, Stream<T> stream, Map<String, String> columnMapping, Optional<List<String>> optionalColumnOrder);
	
	
	
	public String[] toStringArray(T item, Map<String, String> columnMapping, Optional<List<String>> optionalColumnOrder) {
		// Determine header length
		final int arrayLength = (optionalColumnOrder.isPresent()) ? optionalColumnOrder.get().size() : columnMapping.size();
		final String[] data = new String[arrayLength];
		
		// Determine header values 
		final Collection<String> headerValues = (optionalColumnOrder.isPresent()) ? optionalColumnOrder.get() : columnMapping.keySet();
		
		int index = 0;
		for(String headerValue : headerValues) {
			final Object object = getValue(item, columnMapping.get(headerValue));
			if(object != null) {
				// String value
				data[index] = String.valueOf(object);
			} else {
				// Empty value
				data[index] = "";
			}
			
			index++;
		};
		
		return data;
	}
	
	public String[] toStringArray(Map<String, String> columnMapping, Optional<List<String>> optionalColumnOrder) {
		// Determine header length
		final int arrayLength = (optionalColumnOrder.isPresent()) ? optionalColumnOrder.get().size() : columnMapping.size();
		final String[] data = new String[arrayLength];
		
		// Determine header values 
		final Collection<String> headerValues = (optionalColumnOrder.isPresent()) ? optionalColumnOrder.get() : columnMapping.keySet();
		
		int index = 0;
		for(String headerValue : headerValues) {
			// String value
			data[index] = headerValue;
			
			index++;
		};
		
		return data;
	}
	
	public Object getValue(T item, String propertyMetada) {
		ExportExpressionParser parser = new ExportExpressionParser(propertyMetada);
		return parser.getParsedProperty(item);
	}
}
