/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.spring.components;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.appsweaver.commons.SystemConstants;
import org.appsweaver.commons.models.exceptions.AuthenticationTokenExpiredException;
import org.appsweaver.commons.utilities.Dates;
import org.appsweaver.commons.utilities.Randomizer;
import org.appsweaver.commons.utilities.Stringizer;
import org.appsweaver.commons.utilities.TimeTextParser;
import org.jasypt.encryption.StringEncryptor;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.TokenExpiredException;


/**
 *
 * @author UD
 *
 */
@Component
public class Cryptographer {
	final static Logger logger = LoggerFactory.getLogger(Cryptographer.class.getClass());

	@Autowired StringEncryptor encryptor;

	@PostConstruct
	public void initializePostConstruct() {
		_encryptor = encryptor;

		logger.info("Cryptographer initialized!");
	}

	static StringEncryptor _encryptor;

	final static String ENCRYPTED_VALUE_PREFIX = "ENC(";
	final static String ENCRYPTED_VALUE_SUFFIX = ")";

	final static String APPLICATION_MASTER_KEY = "APPLICATION_MASTER_KEY";
	final static String APPLICATION_ACCESS_TOKEN_VALIDITY_DURATION = "APPLICATION_ACCESS_TOKEN_VALIDITY_DURATION";
	
	static boolean isEncryptedExpression(final String value) {
		return (value.startsWith(ENCRYPTED_VALUE_PREFIX) && value.endsWith(ENCRYPTED_VALUE_SUFFIX));
	}
	
	static String getInnerEncryptedValue(final String value) {
		return value.substring(ENCRYPTED_VALUE_PREFIX.length(), (value.length() - ENCRYPTED_VALUE_SUFFIX.length()));
	}
	
	public static String getMasterKey() {
		String value = System.getenv(APPLICATION_MASTER_KEY);
		if(Stringizer.isEmpty(value)) {
			/*
			 * Fallback to old name; eventually remove support for this
			 */
			value = System.getenv("WEBSERVICE_MASTER_KEY");
		}
		
		return value;
	}
	
	/**
	 * Get application access token validity duration, defaults to 8 hours
	 * Time text is set in environment variable APPLICATION_ACCESS_TOKEN_VALIDITY_DURATION
	 * <br>
	 * Supported Formats: Duration TimeUnit
	 * <br>
	 * Duration: Positive integer
	 * TimeUnit: d-days, h-hours, m-minutes, u-microseconds, s-seconds, n-nanoseconds; default: milliseconds)
	 *
	 * @return duration in milliseconds
	 */
	public static long getAccessTokenValidityDuration() {
		final String value = System.getenv(APPLICATION_ACCESS_TOKEN_VALIDITY_DURATION);
		
		if(Stringizer.isEmpty(value)) {
			/*
			 *  Defaults to hours specified in system constants
			 */
			final TimeTextParser timeTextParser = new TimeTextParser(SystemConstants.DEFAULT_APPLICATION_ACCESS_TOKEN_VALIDITY_DURATION);
			
			return timeTextParser.toMilliseconds();
		} else {
			/*
			 * Time unit as per acceptable values supported by time text parser.
			 * TODO: Standardize time unit short text
			 */
			final TimeTextParser timeTextParser = new TimeTextParser(value);

			final Integer duration = timeTextParser.getValue();
			final TimeUnit unit = timeTextParser.getUnit();

			return unit.toMillis(duration);
		}
	}
	
	/**
	 * Check if text is encrypted
	 *
	 * @param value the value
	 * @return true if encrypted, false otherwise
	 */
	public static boolean isEncryptedValue(final String value) {
		return isEncryptedValue(value, true);
	}
	
	/**
	 * Check if text is encrypted with brute force decryption to see if text was indeed encrypted
	 *
	 * @param value the value
	 * @param decryptToCheck toggle flag
	 * @return true if encrypted, false otherwise
	 */
	public static boolean isEncryptedValue(final String value, final boolean decryptToCheck) {
		if (value == null) {
			return false;
		}
		final String trimmedValue = value.trim();
		

		if(decryptToCheck) {
			final String encryptedValue = isEncryptedExpression(trimmedValue) ? getInnerEncryptedValue(trimmedValue) : trimmedValue;

			try {
				// Expensive, but can decisively say if the value can be decrypted or not
				decrypt(encryptedValue);
				return true;
			} catch(Throwable t) {
			}

			return false;
		} else {
			return isEncryptedExpression(trimmedValue);
		}
	}
	
	/**
	 * Encrypt password.
	 *
	 * @param password the password
	 * @return the string
	 */
	public static String encryptPassword(String password) {
		final StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
		final String encryptedPassword = passwordEncryptor.encryptPassword(password);
		final String toHex = Stringizer.toHex(encryptedPassword);
		return toHex;
	}

	/**
	 * Check password.
	 *
	 * @param actual the actual
	 * @param expected the expected
	 * @return true, if successful
	 */
	public static boolean checkPassword(String actual, String expected) {
		final StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
		final String fromHex = Stringizer.fromHex(expected);
		return passwordEncryptor.checkPassword(actual, fromHex);
	}

	/**
	 * Encrypt.
	 *
	 * @param value the value
	 * @return the string
	 */
	public static String encrypt(Long value) {
		return encrypt(String.valueOf(value), false);
	}

	/**
	 * Encrypt and hex.
	 *
	 * @param value the value
	 * @param hex toggle flag
	 * @return the string
	 */
	public static String encrypt(Long value, boolean hex) {
		return encrypt(String.valueOf(value), hex);
	}

	/**
	 * Encrypt.
	 *
	 * @param value the value
	 * @return the string
	 */
	public static String encrypt(String value) {
		return encrypt(value, false);
	}

	/**
	 * Encrypt and hex.
	 *
	 * @param value the value
	 * @param hex toggle flag
	 * @return the string
	 */
	public static String encrypt(String value, boolean hex) {
		final String encrypted = encryptor().encrypt(value);

		if(hex) {
			return Stringizer.toHex(encrypted);
		}

		return encrypted;
	}

	/**
	 * Decrypt.
	 *
	 * @param encryptedText the encrypted text
	 * @return the string
	 */
	public static String decrypt(String encryptedText) {
		return decrypt(encryptedText, false);
	}

	/**
	 * Decrypt with hex.
	 *
	 * @param encryptedText text to be decrypted
	 * @param hex enable or disable additional masking
	 * @return string plain text
	 */
	public static String decrypt(String encryptedText, boolean hex) {
		final String value;

		if(isEncryptedValue(encryptedText, false)) {
			encryptedText = getInnerEncryptedValue(encryptedText);
		}

		if(hex) {
			value = Stringizer.fromHex(encryptedText);
		} else {
			value = encryptedText;
		}

		final String plainText = encryptor().decrypt(value);
		return plainText;
	}
	
	/**
	 * Remove "Bearer" token prefix and return token value
	 * 
	 * @param token
	 * 		the raw token from authorization header
	 * @return
	 * 		the sanitized token value
	 */
	public static String getTokenValue(String token) {
		if(!Stringizer.isEmpty(token)) {
			return token.replace(SystemConstants.TOKEN_PREFIX, "");
		}
		return null;
	}

	/**
	 * Get subject from JWT token
	 *
	 * @param token JWT token
	 * @return string username
	 * @throws AuthenticationTokenExpiredException when token is expired
	 */
	public static String getSubjectFromJwtsToken(String token) throws AuthenticationTokenExpiredException {
		try {
			final String username = JWT.require(Algorithm.HMAC512(getMasterKey()))
				.build()
				.verify(getTokenValue(token))
				.getSubject();

			return username;
		} catch (TokenExpiredException e) {
			throw new AuthenticationTokenExpiredException(e);
		}
	}
	
	/**
	 * Obtain session id from JWT token
	 *
	 * @param token JWT token
	 * @return JWTS id as UUID (user generated during token creation)
	 * @throws AuthenticationTokenExpiredException when token is expired
	 */
	public static Optional<UUID> getJwtsIdFromToken(String token) {
		try {
			final String jwtId = JWT.require(Algorithm.HMAC512(getMasterKey()))
				.build()
				.verify(getTokenValue(token))
				.getId();

			return Optional.ofNullable(Randomizer.uuid(jwtId));
		} catch (Throwable t) {
			logger.trace("", t);
		}
		
		return Optional.empty();
	}
	
	/**
	 * Determine if JWT token is valid or not
	 * 
	 * @param token
	 * 		the token value from authorization header
	 * 
	 * @return
	 * 		true when token is valid, false otherwise
	 * 
	 * @throws AuthenticationTokenExpiredException
	 * 		when problems are encountered processing the token
	 */
	public static Boolean isJwtsTokenValid(String token) {
		try {
			final Date expiryDate = JWT.require(Algorithm.HMAC512(getMasterKey()))
				.build()
				.verify(getTokenValue(token))
				.getExpiresAt();

			if(expiryDate != null) {
				final long expiryTime = expiryDate.toInstant().toEpochMilli();
				final Long now = Dates.toTime();
				
				// Report time
				return now < expiryTime;
			}
		} catch (TokenExpiredException e) {
		}
		
		// Report it invalid
		return false;
	}

	/*
	 * Debug JWT token using https://jwt.io/#debugger-io
	 */
	public static String createJwtsToken(String username) throws Throwable {
		final Date expiryDate = Dates.toDate(Dates.toTime() + getAccessTokenValidityDuration());
		
		final String token = JWT.create()
	        .withSubject(username)
	        .withExpiresAt(expiryDate)
	        .withJWTId(Randomizer.uuid().toString())	// JWTS Id to track individual sessions
	        .sign(HMAC512(getMasterKey()));
		
		return token;
	}
	
	/**
	 * Handle to StringEncryptor
	 *
	 * @return the encryptor
	 */
	static StringEncryptor encryptor() {
		return _encryptor;
	}
}
