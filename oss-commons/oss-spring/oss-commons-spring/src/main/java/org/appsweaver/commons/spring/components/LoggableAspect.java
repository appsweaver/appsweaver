/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.spring.components;

import org.appsweaver.commons.annotations.Loggable;
import org.appsweaver.commons.utilities.Dates;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 
 * @author UD
 * 
 * Reference: https://docs.spring.io/spring/docs/current/spring-framework-reference/core.html#aop
 * 
 */
@Aspect
@Component
public class LoggableAspect {
	final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Around("@within(loggable) && execution(* *(..))")
	public Object around(ProceedingJoinPoint joinPoint, Loggable loggable) throws Throwable {
		if(loggable.enabled()) {
			final Signature signature = joinPoint.getSignature();
			final String className = signature.getDeclaringTypeName();
			final String methodName = signature.getName();

			final long start = Dates.toTime();
			logger.info("Started [class={}, method={}, startedOn={}]", className, methodName, start);

			final Object proceed = joinPoint.proceed();

			final long end = Dates.toTime();
			final long executionTime =  end - start;
			logger.info("Ended [class={}, method={}, startedOn={}, endedOn={}, time={}millis]", className, methodName, start, end, executionTime);
			
			return proceed;
		}
		
		// Proceed without logging
		return joinPoint.proceed();
	}
}
