/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.spring.components;

import java.io.File;
import java.security.KeyStore;

import javax.net.ssl.SSLContext;

import org.apache.http.ssl.SSLContexts;
import org.cryptacular.bean.KeyStoreFactoryBean;
import org.cryptacular.io.FileResource;
import org.cryptacular.io.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.stereotype.Component;

/**
 * 
 * @author UD
 * 
 */
@Component
@ConditionalOnProperty(prefix = "server.ssl", name = "enabled", havingValue = "true")
public class SslHelper {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	static final String FILE_RESOURCE_PREFIX = "filesystem:";

	static final String STORE_TYPE = "JKS";
	
	@Autowired ServerProperties serverProperties;
	
	public String storeType() {
		return STORE_TYPE;
	}
	
	public File toFile(final String path) {
		if (path.startsWith(FILE_RESOURCE_PREFIX)) {
			final String extractedPath = path.substring(FILE_RESOURCE_PREFIX.length());
			return new File(extractedPath);
		}

		return new File(path);
	}

	public String protocol() {
		return serverProperties.getSsl().getProtocol();
	}
	
	public Resource toResource(final String path) {
		return new FileResource(toFile(path));
	}
	
	public boolean isSslEnabled() {
		return serverProperties.getSsl().isEnabled();
	}
	
	public String keyStore() {
		return serverProperties.getSsl().getKeyStore();
	}
	
	public String trustStore() {
		return serverProperties.getSsl().getTrustStore();
	}
	
	public String keyAlias() {
		return serverProperties.getSsl().getKeyAlias();
	}
	
	public String keyStorePassword() {
		return serverProperties.getSsl().getKeyStorePassword();
	}
	
	public String trustStorePassword() {
		return serverProperties.getSsl().getTrustStorePassword();
	}
	
	public String[] ciphers() {
		return serverProperties.getSsl().getCiphers();
	}
	
	public String[] enabledProtocols() {
		return serverProperties.getSsl().getEnabledProtocols();
	}
	
	public int port() {
		return serverProperties.getPort();
	}
	
	public boolean isHttp2Enabled() {
		return serverProperties.getHttp2().isEnabled();
	}
	
	public SSLContext getSslContext() {
		final KeyStore keyStore = new KeyStoreFactoryBean(
			toResource(keyStore()), STORE_TYPE, keyStorePassword()
		).newInstance();

		final KeyStore trustStore = new KeyStoreFactoryBean(
			toResource(trustStore()), STORE_TYPE, trustStorePassword()
		).newInstance();

		try {
			final SSLContext sslContext = SSLContexts.custom()
				.setProtocol(protocol())
				.loadKeyMaterial(keyStore, trustStorePassword().toCharArray(), (aliases, socket) -> keyAlias())
				.loadTrustMaterial(trustStore, (x509Certificates, s) -> true)
				.build();

			return sslContext;
		} catch (Throwable t) {
			throw new IllegalStateException("Error loading key or trust material", t);
		}
	}
}
