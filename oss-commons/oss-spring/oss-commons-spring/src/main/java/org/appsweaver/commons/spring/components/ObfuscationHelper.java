/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.spring.components;

import java.lang.reflect.Method;

import org.appsweaver.commons.SystemConstants;
import org.appsweaver.commons.models.persistence.Entity;
import org.appsweaver.commons.utilities.Stringizer;
import org.hashids.Hashids;
import org.springframework.stereotype.Component;

/**
 *
 * @author UD
 *
 */
@Component
public class ObfuscationHelper {
	/**
	 * Obtain salt from entity class.  Each entity class must provide unique salt to create hash.
	 * This salt is critical to ensure id obfuscation.
	 *
	 * @param clazz
	 * @return salt
	 * @throws Throwable if exception occurs
	 */
	final <T extends Entity> String getSalt(Class<T> clazz) throws Throwable {
		try {
			// TODO: Use annotation processor
			// Obfuscatable.class get salt
			final Method method = clazz.getMethod("hashSalt");
			final String salt =  (String) method.invoke(null);
			return salt;
		} catch(Throwable t) {
			throw new Throwable(String.format("Unable to get salt for hashids [class=%s]", clazz.getName()), t);
		}
	}

	/**
	 * Instantiate hashids for entity using given salt
	 *
	 * @param clazz
	 * @return a new hashid
	 */
	final <T extends Entity> Hashids newHashids(Class<T> clazz) {
		try {
			return new Hashids(getSalt(clazz), SystemConstants.HASH_ID_LENGTH, SystemConstants.HASH_ID_CHARACTERS);
		} catch(Throwable t) {
			return null;
		}
	}

	/**
	 * Convert JPA id to obfuscated id
	 *
	 * @param clazz class
	 * @param id JPA id
	 * @param <T> type
	 * @return an obfuscated id
	 */
	public final <T extends Entity> String oid(Class<T> clazz, Long id) {
		if(id == null) return null;

		final Hashids hashids = newHashids(clazz);
		final String hash = hashids.encode(id);
		return hash;
	}

	/**
	 * Convert obfuscated id to JPA id
	 * @param clazz class
	 * @param obfuscatedId obfuscated id
	 * @param <T> type
	 * @return JPA id
	 */
	public final <T extends Entity> Long id(Class<T> clazz, String obfuscatedId) {
		if(obfuscatedId == null) return null;

		final Hashids hashids = newHashids(clazz);
		final long[] decoded = hashids.decode(obfuscatedId);
		return (decoded.length >= 1 ) ? decoded[0] : null;
	}

	/**
	 * Get implicit id value
	 *
	 * @param clazz class
	 * @param value value
	 * @param <T> type
	 * @return id string value
	 */
	public final <T extends Entity> String idString(Class<T> clazz, String value) {
		try {
			if(Stringizer.isNumeric(value)) {
				// Try to decode from hash
				final Long id = id(clazz, value);

				if(id != null) {
					return String.valueOf(id);
				}
			}
		} catch(Throwable t) {
		}

		// Return original value by default
		return value;
	}
}
