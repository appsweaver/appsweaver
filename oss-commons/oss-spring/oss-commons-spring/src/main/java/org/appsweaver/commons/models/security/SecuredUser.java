/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.models.security;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.appsweaver.commons.utilities.Collections;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 * 
 * @author UD
 * 
 */
public class SecuredUser {
	private UUID id;
	
	private String loginId;
	private String secondaryLoginId;
	private String email;

	private String firstName;
	private String lastName;
	
	private String title;
	private String departmentName;

	private String mobileNumber;
	
	private boolean enabled;
	private boolean locked;
	
	private String token;

	private String role;
	private List<String> grantedAuthorities;
	
	private AuthenticationProviderType authenticationProvider;
	
	private Map<String, String> additionalProperties;
	
	private boolean inActiveSession = false;
	
	
	public SecuredUser() {
	}
	
	public SecuredUser(
		UUID id, 
		String loginId, String secondaryLoginId, String email, 
		String firstName, String lastName, 
		String title, String departmentName,
		boolean enabled, boolean locked,
		String role,
		String token,
		Collection<String> grantAuthorities,
		boolean inActiveSession
	) {
		setId(id);

		setLoginId(loginId);
		setSecondaryLoginId(secondaryLoginId);
		setEmail(email);
		
		setFirstName(firstName);
		setLastName(lastName);
		
		setTitle(title);
		setDepartmentName(departmentName);
		
		setEnabled(enabled);
		setLocked(locked);

		setRole(role);

		setToken(token);

		setGrantedAuthorities(Collections.newList(grantAuthorities));
		
		setInActiveSession(inActiveSession);
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	public Collection<String> getGrantedAuthorities() {
		return grantedAuthorities;
	}

	public void setGrantedAuthorities(List<String> grantedAuthorities) {
		this.grantedAuthorities = grantedAuthorities;
	}
	
	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getSecondaryLoginId() {
		return secondaryLoginId;
	}
	
	public void setSecondaryLoginId(String secondaryLoginId) {
		this.secondaryLoginId = secondaryLoginId;
	}

	public String getDepartmentName() {
		return departmentName;
	}
	
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public List<GrantedAuthority> toGrantedAuthorities() {
		final List<GrantedAuthority> authorities = Collections.newList();
		
		if(grantedAuthorities != null) {
			grantedAuthorities.forEach(grantedAuthority -> {
				authorities.add(new SimpleGrantedAuthority(grantedAuthority));
			});
		}
		
		return authorities;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public boolean isInActiveSession() {
		return inActiveSession;
	}

	public void setInActiveSession(boolean inActiveSession) {
		this.inActiveSession = inActiveSession;
	}

	public AuthenticationProviderType getAuthenticationProvider() {
		return authenticationProvider;
	}

	public void setAuthenticationProvider(AuthenticationProviderType authenticationProvider) {
		this.authenticationProvider = authenticationProvider;
	}

	public Map<String, String> getAdditionalProperties() {
		return additionalProperties;
	}

	public void setAdditionalProperties(Map<String, String> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

}
