/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.annotations.processors;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Set;

import org.appsweaver.commons.models.exceptions.AnnotationProcessorException;
import org.appsweaver.commons.models.persistence.Entity;
import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Ruminator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author UD
 * 
 */
public abstract class AbstractFieldAnnotationProcessor {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	public abstract Set<Class<? extends Annotation>> getAnnotationClasses();
	
	public abstract void processAnnotation(Annotation Annotation, Member member);
	
	public abstract void initialize();
	
	public final <T extends Entity> void process(Class<T> entityClass) throws AnnotationProcessorException {
		try {
			final Set<Class<? extends Annotation>> annotationClasses = getAnnotationClasses();
			
			/*
			 *  Manage re-entry when component is invoked multiple times
			 */
			initialize();
			
			annotationClasses.forEach(annotationClass -> {
				// Process field annotations
				for (Field field: Ruminator.getAllFields(entityClass)) {
					field.setAccessible(true);

					if (field.isAnnotationPresent(annotationClass)) {
						logger.debug("Found annotation [class={}, field={}]", annotationClass, field.getName());

						final Annotation annotation = field.getAnnotation(annotationClass);
						processAnnotation(annotation, field);
					}
				}
				
				// Process method annotations
				final Map<String, Method> methods = Collections.newHashMap();
				Ruminator.getAllMethods(entityClass, methods);
				
				for (Method method : methods.values()) {
					method.setAccessible(true);

					if (method.isAnnotationPresent(annotationClass)) {
						logger.debug("Found annotation [class={}, method={}]", annotationClass, method.getName());

						final Annotation annotation = method.getAnnotation(annotationClass);
						processAnnotation(annotation, method);
					}
				}
			});
		} catch (Throwable t) {
			throw new AnnotationProcessorException(t);
		}
	}
}
