/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.spring.config;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

/**
 * 
 * @author AC
 * 
 * The Class SchedulerConfig.
 * 
 * Class is a conditional bean to enable the scheduler and configure the thread pool.
 * Pool size and pool name can be overridden by proving the properties {@code webservice.schedulers.poolsize} and {@code webservice.schedulers.poolname}
 */
@Configuration
@EnableScheduling
@ConditionalOnProperty(name = "webservice.schedulers.enabled", havingValue = "true")
public class SchedulerConfig implements SchedulingConfigurer {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

    /** The pool size. */
    @Value("${webservice.schedulers.poolsize:10}")
    private int poolSize;

    /** The pool name. */
    @Value("${webservice.schedulers.poolname:app-scheduler-pool-%d}")
    private String poolName;

    public SchedulerConfig() {
    	logger.info("Scheduler Configuration Created!");
    }
    
    /**
     * Scheduler task executor.
     *
     * @return the executor
     */
    @Bean(destroyMethod = "shutdown")
    public Executor schedulerTaskExecutor() {
        return Executors.newScheduledThreadPool(poolSize, new ThreadFactoryBuilder().setNameFormat(poolName).build());
    }

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.setScheduler(schedulerTaskExecutor());
    }

}
