/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.spring.config;

import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;

import org.appsweaver.commons.spring.components.SslHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.netflix.discovery.DiscoveryClient;
import com.netflix.discovery.shared.transport.jersey.EurekaJerseyClientImpl.EurekaJerseyClientBuilder;

/**
 * 
 * @author UD
 *
 */
@Configuration
@ConditionalOnProperty(prefix = "eureka.discovery", name = "secured", havingValue = "true")
public class SecuredDiscoveryClientConfig {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired SslHelper sslHelper;
	
	public SecuredDiscoveryClientConfig() {
    	logger.info("Secured Discovery Client Configuration Created!");
	}
	
	@Bean
	public DiscoveryClient.DiscoveryClientOptionalArgs discoveryClientOptionalArgs() throws NoSuchAlgorithmException {
	    final DiscoveryClient.DiscoveryClientOptionalArgs args = new DiscoveryClient.DiscoveryClientOptionalArgs();
	    
	    final SSLContext sslContext = sslHelper.getSslContext();
	    args.setSSLContext(sslContext);
	    
	    final EurekaJerseyClientBuilder builder = new EurekaJerseyClientBuilder();
	    builder.withClientName("secured-discovery-client");
	    builder.withSystemSSLConfiguration();
	    builder.withMaxTotalConnections(10);
	    builder.withMaxConnectionsPerHost(10);
	    args.setEurekaJerseyClient(builder.build());
	    return args;
	}
}
