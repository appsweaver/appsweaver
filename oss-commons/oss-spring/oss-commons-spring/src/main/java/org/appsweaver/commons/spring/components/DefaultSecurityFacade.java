/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.spring.components;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

import org.appsweaver.commons.models.security.SecuredUser;
import org.appsweaver.commons.models.security.SecurityFacade;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * 
 * @author UD
 *
 */
@Component("securityFacade")
public class DefaultSecurityFacade implements SecurityFacade {
    Optional<SecuredUser> getSecuredUser() {
		final SecurityContext securityContext = SecurityContextHolder.getContext();
		
		if (null != securityContext) {
			final Authentication authentication = securityContext.getAuthentication();
			if (null != authentication) {
				final Object details = authentication.getDetails();
				
				if(details instanceof SecuredUser) {
					final SecuredUser securedUser = (SecuredUser) authentication.getDetails();
					return Optional.ofNullable(securedUser);
				}
			}
		}

		return Optional.empty();
	}
    
    @Override
	public UUID getUserId() {
    	final Optional<SecuredUser> securedUser = getSecuredUser();
		return (securedUser.isPresent()) ? securedUser.get().getId() : null;
	}
    
	@Override
	public String getLoginId() {
		final Optional<SecuredUser> securedUser = getSecuredUser();
		return (securedUser != null) ? securedUser.get().getLoginId() : null;
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		final Optional<SecuredUser> securedUser = getSecuredUser();

		if(!securedUser.isPresent()) return null;
		
		final Collection<? extends GrantedAuthority> authorities = securedUser.get().toGrantedAuthorities();
		return authorities;
	}
}
