/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.api;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import org.appsweaver.commons.models.persistence.Entity;

/**
 *
 * @author UD
 *
 */
public interface EntityWriter<T extends Entity> {
	void write(Class<T> entityClass, File file, Stream<T> stream, Map<String, String> columnMapping);
	
	void write(Class<T> entityClass, File file, Stream<T> stream, Map<String, String> columnMapping, Optional<List<String>> optionalColumnOrder);
}
