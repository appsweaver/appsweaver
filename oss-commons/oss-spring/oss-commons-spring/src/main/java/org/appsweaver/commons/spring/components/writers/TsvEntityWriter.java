/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.spring.components.writers;

import java.io.FileWriter;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import org.appsweaver.commons.models.persistence.Entity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.opencsv.CSVWriter;

/**
 * 
 * @author udhansingh
 *
 * @param <T> entity class
 */
@Component
public class TsvEntityWriter<T extends Entity> extends CsvEntityWriter<T> {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	final static char DEFAULT_SEPARATOR = '\t';
	
	@Override
	void write(Class<T> entityClass, FileWriter fileWriter, Stream<T> stream, Map<String, String> columnMapping, Optional<List<String>> optionalColumnOrder) throws Throwable {
		try(final CSVWriter csvWriter = new CSVWriter(fileWriter, DEFAULT_SEPARATOR, CSVWriter.DEFAULT_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END)) {
			// Write Header
			final String[] headerLine = toStringArray(columnMapping, optionalColumnOrder);
			csvWriter.writeNext(headerLine, true);
			
			// Write Data
			stream.forEach(item -> {
				final String[] dataLine = toStringArray(item, columnMapping, optionalColumnOrder);
				csvWriter.writeNext(dataLine, true);
			});
		}
	}
}
