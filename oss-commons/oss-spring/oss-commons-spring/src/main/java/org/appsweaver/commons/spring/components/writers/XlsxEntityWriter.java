/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.spring.components.writers;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.appsweaver.commons.models.persistence.Entity;
import org.appsweaver.commons.utilities.Stringizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 
 * @author udhansingh
 *
 * @param <T> entity class
 */
@Component
public class XlsxEntityWriter<T extends Entity> extends AbstractEntityWriter<T> {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	void write(Class<T> entityClass, FileOutputStream fileOutputStream, Stream<T> stream, Map<String, String> columnMapping, Optional<List<String>> optionalColumnOrder) throws Throwable {
		try(final XSSFWorkbook workbook = new XSSFWorkbook()) {
			final Sheet sheet = workbook.createSheet();

			int columnCount = 0;
			int rowCount = 0;

			// Prepare Header Row
			final String[] headerLine = toStringArray(columnMapping, optionalColumnOrder);
			final Row headerRow = sheet.createRow(rowCount++);

			// Writer Header Row
			for (String headerValue : headerLine) {
				final Cell cell = headerRow.createCell(columnCount++);
				cell.setCellValue(headerValue);
			}

			// Write Data
			final Iterator<T> iterator = stream.iterator();
			while(iterator.hasNext()) {
				final T item = iterator.next();
				final String[] dataLine = toStringArray(item, columnMapping, optionalColumnOrder);

				// Prepare a new data row
				final Row dataRow = sheet.createRow(rowCount++);

				// Initialize Column Count
				columnCount = 0;

				// Write data row
				for(String dataValue : dataLine) {
					final Cell cell = dataRow.createCell(columnCount++);
					
					try {
						if(Stringizer.isNumeric(dataValue)) {
							if(dataValue.contains(".")) {
								// Parse as Double
								cell.setCellValue(Double.valueOf(dataValue));
							} else {
								// Parse as Long
								cell.setCellValue(Long.valueOf(dataValue));
							}
						} else {
							cell.setCellValue(dataValue);
						}
					} catch(Throwable t) {
						cell.setCellValue(dataValue);
					}
				}
			}

			// Write to file and flush
			workbook.write(fileOutputStream);
			fileOutputStream.flush();
		}
	}
	
	@Override
	public void writeToFile(Class<T> entityClass, File file, Stream<T> stream, Map<String, String> columnMapping, Optional<List<String>> optionalColumnOrder) {
		try(FileOutputStream fileOutputStream = new FileOutputStream(file)) {
			logger.info("Writing data to file [path={}]", file.getAbsoluteFile());

			write(entityClass, fileOutputStream, stream, columnMapping, optionalColumnOrder);
		} catch (Throwable t) {
			logger.error("Error writing data to file [path={}]", file.getAbsoluteFile(), t);
		}
	}
}
