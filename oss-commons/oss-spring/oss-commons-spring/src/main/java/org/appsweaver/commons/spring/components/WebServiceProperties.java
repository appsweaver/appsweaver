/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.appsweaver.commons.spring.components;

import java.util.Optional;

import org.appsweaver.commons.models.properties.Messaging;
import org.appsweaver.commons.models.properties.Repository;
import org.appsweaver.commons.models.properties.Security;
import org.appsweaver.commons.models.properties.attributes.AuthenticatonProviderDetails;
import org.appsweaver.commons.models.security.AuthenticationProviderType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

/**
 *
 * @author AC
 *
 */
@Component
@Validated
@ConfigurationProperties(prefix = "webservice")
public class WebServiceProperties {
	/**
	 * The Logger.
	 */
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	private String serverEnvName;
	private String serverName;
	private Integer serverPort;
	private Integer adminServerPort;
	private String releaseNotesFile;
	private Security security;
	private Repository repository;
	private Messaging messaging;

	/**
	 * Gets server env name.
	 *
	 * @return the server env name
	 */
	public String getServerEnvName() {
		return serverEnvName;
	}

	/**
	 * Sets server env name.
	 *
	 * @param serverEnvName
	 *            the server env name
	 */
	public void setServerEnvName(String serverEnvName) {
		this.serverEnvName = serverEnvName;
	}

	/**
	 * Gets server name.
	 *
	 * @return the server name
	 */
	public String getServerName() {
		return serverName;
	}

	/**
	 * Sets server name.
	 *
	 * @param serverName
	 *            the server name
	 */
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	/**
	 * Gets server port.
	 *
	 * @return the server port
	 */
	public Integer getServerPort() {
		return serverPort;
	}

	/**
	 * Sets server port.
	 *
	 * @param serverPort
	 *            the server port
	 */
	public void setServerPort(Integer serverPort) {
		this.serverPort = serverPort;
	}

	/**
	 * Gets admin server port.
	 *
	 * @return the admin server port
	 */
	public Integer getAdminServerPort() {
		return adminServerPort;
	}

	/**
	 * Sets admin server port.
	 *
	 * @param adminServerPort
	 *            the admin server port
	 */
	public void setAdminServerPort(Integer adminServerPort) {
		this.adminServerPort = adminServerPort;
	}

	/**
	 * Gets release notes file.
	 *
	 * @return the release notes file
	 */
	public String getReleaseNotesFile() {
		return releaseNotesFile;
	}

	/**
	 * Sets release notes file.
	 *
	 * @param releaseNotesFile
	 *            the release notes file
	 */
	public void setReleaseNotesFile(String releaseNotesFile) {
		this.releaseNotesFile = releaseNotesFile;
	}

	/**
	 * Gets security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets security.
	 *
	 * @param security
	 *            the security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets repository.
	 *
	 * @return the repository
	 */
	public Repository getRepository() {
		return repository;
	}

	/**
	 * Sets repository.
	 *
	 * @param repository
	 *            the repository
	 */
	public void setRepository(Repository repository) {
		this.repository = repository;
	}

	/**
	 * Gets messaging.
	 *
	 * @return the messaging
	 */
	public Messaging getMessaging() {
		return messaging;
	}

	/**
	 * Sets messaging.
	 *
	 * @param messaging
	 *            the messaging
	 */
	public void setMessaging(Messaging messaging) {
		this.messaging = messaging;
	}
	
	/**
	 * Obtains handle to authentication source attributes from security properties
	 * 
	 * @param authenticationProvider authentication source type
	 * @return handle to user property
	 */
	public AuthenticatonProviderDetails getAuthenticationProviderDetails(AuthenticationProviderType authenticationProvider) {
		final Optional<AuthenticatonProviderDetails> optional = getSecurity().getAuthenticationProviders().stream().filter(item -> 
			item.getAuthenticationProvider() == authenticationProvider
		).findFirst();
		
		if(optional.isPresent()) {
			return optional.get();
		}
		
		return null;
	}
}