/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.spring.config;

import static org.appsweaver.commons.SystemConstants.JASYPT_ALGORITHM;

import org.appsweaver.commons.spring.components.Cryptographer;
import org.jasypt.encryption.StringEncryptor;
import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.SimpleStringPBEConfig;
import org.jasypt.salt.RandomSaltGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * 
 * @author UD
 * 
 */
@Configuration
public class SecurityBeansConfig {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	public SecurityBeansConfig() {
		logger.info("Security Beans Configuration Created!");
	}
	
	@Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
	
	@Primary
	@Bean(name = "jasyptStringEncryptor")
	public StringEncryptor stringEncryptor() {
		final SimpleStringPBEConfig config = new SimpleStringPBEConfig();
		config.setPassword(Cryptographer.getMasterKey());
		config.setAlgorithm(JASYPT_ALGORITHM);
		config.setKeyObtentionIterations(20480);
		config.setPoolSize(8);
		config.setProviderName("SunJCE");
		config.setSaltGenerator(new RandomSaltGenerator());
		config.setStringOutputType("base64");
		
		final PooledPBEStringEncryptor encryptor = new PooledPBEStringEncryptor();
		encryptor.setConfig(config);
		
		return encryptor;
	}
}
