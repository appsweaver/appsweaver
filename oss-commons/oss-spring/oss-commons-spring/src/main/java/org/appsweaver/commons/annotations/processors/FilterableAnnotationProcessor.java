/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.annotations.processors;

import java.lang.annotation.Annotation;
import java.lang.reflect.Member;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

import org.appsweaver.commons.annotations.Filterable;
import org.appsweaver.commons.annotations.Filterables;
import org.appsweaver.commons.models.criteria.FilterableField;
import org.appsweaver.commons.models.persistence.Entity;
import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Stringizer;

/**
 * 
 * @author UD
 * 
 * Process entity class annotations for FilterableField
 * Uses prototype scope to provide new instance each time it is needed,
 * required for managing data structure used for annotation processing
 */
public class FilterableAnnotationProcessor extends AbstractFieldAnnotationProcessor {
	final Map<String, FilterableField> fields = Collections.newHashMap();
	
	public <T extends Entity> Map<String, FilterableField> getFields(Class<T> entityClass) {
		return fields;
	}

	@Override
	public Set<Class<? extends Annotation>> getAnnotationClasses() {
		return Collections.newHashSet(Filterable.class, Filterables.class);
	}

	private void processFilterable(Filterable filterable, String fieldName) {
		// Defaults to empty string, must be guarded
		final String name = (!Stringizer.isEmpty(filterable.name())) ? filterable.name() : fieldName;

		// Defaults to empty string, must be guarded
		final String path =  (!Stringizer.isEmpty(filterable.path())) ? filterable.path() : fieldName;

		if(filterable.enabled()) {
			fields.put(name, new FilterableField(
				name,
				path,
				filterable.valueClass()	// Defaults to String.class, can safely provide value class
			));
		}
	}
	
	@Override
	public void processAnnotation(Annotation annotation, Member member) {
		// Filterable (1 instance)
		if(annotation.annotationType().getName() == Filterable.class.getName()) {
			final Filterable filterable = (Filterable)annotation;

			processFilterable(filterable, member.getName());
		}
		// Filterables (n filterable instance)
		else if(annotation.annotationType().getName() == Filterables.class.getName()) {
			final Filterables filterables = (Filterables)annotation;

			final Filterable[] filters = filterables.value();
			Arrays.asList(filters).forEach(filterable -> {
				processFilterable(filterable, member.getName());
			});
		}
	}
	
	@Override
	public void initialize() {
		fields.clear();
	}
}
