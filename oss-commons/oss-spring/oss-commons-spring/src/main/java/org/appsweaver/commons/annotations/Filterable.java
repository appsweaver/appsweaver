/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * @author UD
 * 
 * Annotation class to mark entity attributes for Apache Lucene search.
 * 
 * name: apply boost factor at runtime (Optional; default: field name)
 * enabled: enable/disable field search (Optional; default: true)
 * path: provide translation to JPA path (default: field name)
 * valueClass: Type information about value (default: String.class)
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Repeatable(Filterables.class)
public @interface Filterable {
	/*
	 * External name that will be used in front end [default: field name from entity]
	 */
	String name() default "";
	
	/*
	 * Internal field mapping [default: field name from entity]
	 */
	String path() default "";
	
	/*
	 * Value class 
	 */
	Class<?> valueClass() default String.class;
	
	/*
	 * Field enabled for filtering
	 */
	boolean enabled() default true;
}
