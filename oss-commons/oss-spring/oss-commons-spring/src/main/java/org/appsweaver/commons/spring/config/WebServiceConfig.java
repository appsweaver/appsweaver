/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.spring.config;

import org.appsweaver.commons.SystemConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;

/**
 * 
 * @author UD
 * 
 */
@Configuration
@ConditionalOnProperty(prefix = "webservice", name = "enabled", havingValue = "true")
public class WebServiceConfig {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Value("${webservice.base-package-name}")
	private String basePackageName;
	
	@Value("${webservice.project-name}")
	private String projectName;
	
	public WebServiceConfig() {
    	logger.info("Web Service Configuration Created!");
	}

	public void setBasePackageName(String basePackageName) {
		this.basePackageName = basePackageName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getBasePackageName() {
		return basePackageName;
	}

	public String getProjectName() {
		return projectName;
	}

	public String getSwaggerIndexPagePath() {
		return String.format("/%s/swagger-ui.html", projectName);
	}

	public String getSwaggerApiDocsPath() {
		return String.format("/%s/v2/api-docs", projectName);
	}

	public String getSwaggerResourcesPath() {
		return String.format("/%s/swagger-resources", projectName);
	}

	public String getSwaggerUIConfigPath() {
		return String.format("%s/configuration/ui", getSwaggerResourcesPath());
	}

	public String getSwaggerSecurityConfigPath() {
		return String.format("%s/configuration/security", getSwaggerResourcesPath());
	}

	public String getSwaggerWebJarsPath() {
		return String.format("/%s/webjars/**", projectName);
	}

	public String getActuatorPath() {
		return String.format("/%s/actuator/**", projectName);
	}
	
	public String getWildRootPath() {
		return String.format("/%s/**", projectName);
	}
	
	public String getApiPath() {
		return String.format("%s", SystemConstants.API_VERSIONED_PATH_WILD);
	}
}
