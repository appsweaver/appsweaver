/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.commons.services;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.appsweaver.commons.jpa.models.criteria.SortOrder;
import org.appsweaver.commons.jpa.types.FilterConjunction;
import org.appsweaver.commons.models.criteria.FilterableField;
import org.appsweaver.commons.models.persistence.QueryEntity;
import org.appsweaver.commons.mongo.models.criteria.MongoFilterSpecification;
import org.appsweaver.commons.mongo.repositories.CustomMongoRepository;
import org.appsweaver.commons.spring.utilities.FrameworkHelper;
import org.appsweaver.commons.utilities.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.CriteriaDefinition;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author AN
 *
 */
@Service("mongoRepositoryService")
public class MongoRepositoryService {
	final Logger logger = LoggerFactory.getLogger(this.getClass());
	final static String TEXT_SEARCH_QUERY_ATTRIBUTE = "q";

	@Autowired
	MongoTemplate mongoTemplate;

	public <T extends QueryEntity> Page<T> findAllById(CustomMongoRepository<T> repository, Class<T> entityClass,
			List<UUID> collectionOfParentIds, List<SortOrder> sortValues, Integer pageNumber, Integer pageSize,
			Long since, long total) {
		final int localPageNumber = (pageNumber == null) ? 0 : pageNumber;
		final int localPageSize = (pageSize == null) ? 25 : pageSize;
		final List<Order> sortOrders = FrameworkHelper.toSortOrders(sortValues);
		//Iterable<T> iterable = repository.findAllById(collectionOfParentIds);
	    Query query = new Query();

		final PageRequest pageRequest = PageRequest.of(localPageNumber, localPageSize, Sort.by(sortOrders));
		query.with(pageRequest);

		return repository.findAllByIdIn(collectionOfParentIds, pageRequest);
	}

	@Transactional(readOnly = true)
	public <T extends QueryEntity> Page<T> find(CustomMongoRepository<T> repository, Class<T> entityClass,
			Map<String, String> filters, List<SortOrder> sortValues, Integer pageNumber, Integer pageSize, Long since,
			FilterConjunction conjunction) throws Throwable {
		try {

			final int localPageNumber = (pageNumber == null) ? 0 : pageNumber;
			final int localPageSize = (pageSize == null) ? 25 : pageSize;

			final List<Order> sortOrders = FrameworkHelper.toSortOrders(sortValues);

			final Optional<String> optionalQuery = Optional.ofNullable(filters.remove(TEXT_SEARCH_QUERY_ATTRIBUTE));

			final Map<String, String> likeFilters = Collections.newHashMap();
			if (optionalQuery.isPresent()) {
				final String query = optionalQuery.get();
				final Map<String, FilterableField> filterableFields = FrameworkHelper.getFilterableFields(entityClass);

				filterableFields.forEach((name, filterableField) -> {
					final Class<?> type = filterableField.getType();
					if (type.equals(String.class)) {
						likeFilters.put(name, String.format("like(%s)", query));
					}
				});
			}

			final List<CriteriaDefinition> criteriaList = toCriteria(entityClass, filters);
			final Criteria criteria = (!criteriaList.isEmpty()) ? (Criteria) criteriaList.remove(0) : new Criteria();

			final Criteria[] criteriaArray = new Criteria[criteriaList.size()];
			for (int index = 0; index < criteriaList.size(); index++) {
				criteriaArray[index] = (Criteria) criteriaList.get(index);
			}
			if (criteriaArray.length > 0) {

				if (FilterConjunction.AND.equals(conjunction)) {
					criteria.andOperator(criteriaArray);
				} else {
					criteria.orOperator(criteriaArray);
				}
			}

			if (likeFilters.size() > 0) {
				final List<CriteriaDefinition> likeCriteriaList = toCriteria(entityClass, likeFilters);

				final Criteria[] likeCriteriaArray = new Criteria[likeCriteriaList.size()];
				for (int index = 0; index < likeCriteriaList.size(); index++) {
					likeCriteriaArray[index] = (Criteria) likeCriteriaList.get(index);
				}

				// Use or to filter based on any field
				if (likeCriteriaArray.length > 0) {
					criteria.orOperator(likeCriteriaArray);
				}
			}

			// Do a MongoDB query (not text query)
			final Query query = Query.query(criteria);

			final PageRequest pageRequest = PageRequest.of(localPageNumber, localPageSize, Sort.by(sortOrders));
			query.with(pageRequest);

			final List<T> list = mongoTemplate.find(query, entityClass);
			final Page<T> paged = PageableExecutionUtils.getPage(list, pageRequest,
					() -> mongoTemplate.count(query, entityClass));

			return paged;
		} catch (Throwable t) {
			logger.error("Error fetching paged records", t);
			throw t;
		}
	}

	private <T extends QueryEntity> List<CriteriaDefinition> toCriteria(Class<T> clazz, Map<String, String> filters)
			throws Throwable {
		final List<CriteriaDefinition> criteriaDefinitions = Collections.newList();

		if (filters != null && !filters.isEmpty()) {
			filters.forEach((attribute, value) -> {
				try {
					final MongoFilterSpecification<T> specification = new MongoFilterSpecification<>(clazz, attribute,
							value);
					specification.validate();
					criteriaDefinitions.add(specification.toCriteria());
				} catch (SQLException e) {
					logger.warn("Error adding filter for [class={}, attribute={}, value={}]", clazz, attribute, value);
				}
			});
		}

		return criteriaDefinitions;
	}

}
