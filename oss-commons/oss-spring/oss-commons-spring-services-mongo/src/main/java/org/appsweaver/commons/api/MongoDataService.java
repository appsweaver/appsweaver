package org.appsweaver.commons.api;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.appsweaver.commons.models.persistence.QueryEntity;
import org.appsweaver.commons.mongo.repositories.CustomMongoRepository;

/**
 * @author AN
 *
 */
public interface MongoDataService<T extends QueryEntity> extends DataService<T> {

	<S extends T> CustomMongoRepository<S> getRepository();

	/**
	 * Find by id (UUID) from a provided repository
	 *
	 * @param id         object id
	 * @param repository repository instance
	 * @return matching object
	 */
	Optional<T> findById(UUID id, CustomMongoRepository<T> repository);

	/**
	 * Find by id list (resolve numeric and UUID) from a provided repository
	 *
	 * @param ids        object ids
	 * @param repository repository instance
	 * @return matching objects
	 */
	List<T> findByIdList(List<UUID> ids, CustomMongoRepository<T> repository);

}
