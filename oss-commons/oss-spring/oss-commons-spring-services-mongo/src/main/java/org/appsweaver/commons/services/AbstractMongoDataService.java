/*
 *
 * Copyright (c) 2016 appsweaver.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.appsweaver.commons.services;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

import org.appsweaver.commons.api.DataExporter;
import org.appsweaver.commons.api.EntityRanker;
import org.appsweaver.commons.api.MongoDataService;
import org.appsweaver.commons.api.RecordProcessor;
import org.appsweaver.commons.jpa.models.criteria.SortOrder;
import org.appsweaver.commons.jpa.models.search.TextSearchType;
import org.appsweaver.commons.jpa.types.FilterConjunction;
import org.appsweaver.commons.jpa.types.Rank;
import org.appsweaver.commons.json.views.SelectedFacet;
import org.appsweaver.commons.models.persistence.QueryEntity;
import org.appsweaver.commons.mongo.repositories.CustomMongoRepository;
import org.appsweaver.commons.spring.utilities.FrameworkHelper;
import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Ruminator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author AN
 *
 */
public abstract class AbstractMongoDataService<T extends QueryEntity, S extends QueryEntity> extends AbstractEntityDataService<T> implements MongoDataService<T> {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Qualifier("mongoRepositoryService")
	@Autowired(required = false) MongoRepositoryService mongoRepositoryService;

	@Qualifier("querySearchService")
	@Autowired(required = false)
	protected QuerySearchService querySearchService;

	@Autowired EntityRanker entityRanker;

	@Autowired DataExporter<T> dataExporter;
	
	protected abstract List<Class<S>> getSearchableClasses();
	
	protected abstract List<UUID> getParentUUIDs(Collection<S> collection);

	public MongoRepositoryService getRepositoryService() {
		return mongoRepositoryService;
	}

	public QuerySearchService getSearchService() {
		return querySearchService;
	}

	public EntityRanker getEntityRanker() {
		return entityRanker;
	}

	public DataExporter<T> getDataExporter() {
		return dataExporter;
	}
	/*
	 * (non-Javadoc)
	 * @see org.appsweaver.commons.services.DataService#findById(java.util.UUID, org.appsweaver.commons.jpa.repositories.CustomRepository)
	 */
	@Override
	public Optional<T> findById(UUID id, CustomMongoRepository<T> repository) {
		if(repository.existsById(id)) {
			final Optional<T> entity = repository.findById(id);
			return entity;
		} else {
			return Optional.empty();
		}
	}


	/*
	 * (non-Javadoc)
	 * @see org.appsweaver.commons.services.DataService#findByIdList(java.util.List, org.appsweaver.commons.jpa.repositories.CustomRepository)
	 */
	@Override
	public List<T> findByIdList(List<UUID> ids, CustomMongoRepository<T> repository) {
		final List<T> entities = Collections.newList();
		if(ids != null && ids.size() > 0) {
			ids.forEach(id -> {
				final Optional<T> optionalReferenceEntity = findById(id, repository);
				if(optionalReferenceEntity.isPresent()) {
					final T referenceEntity = optionalReferenceEntity.get();
					entities.add(referenceEntity);
				}
			});
		}

		return entities;
	}

	/*
	 * (non-Javadoc)
	 * @see org.appsweaver.commons.services.DataService#save(org.appsweaver.commons.jpa.models.Entity, java.util.Optional, java.util.List)
	 */
	@Override
	public T save(T input, Optional<Rank> optionalRank, List<T> references) throws SQLException {
		if(input != null && FrameworkHelper.isRankable(input.getClass()) && optionalRank.isPresent()) {
			// final Rank rank = optionalRank.get();
			// input = getEntityRanker().rank(getRepository(), input, rank, references);
			throw new SQLException("Ranking for Query Repository is not supported yet");
		}

		final T saved = getRepository().save(input);
		return saved;
	}

	@Override
	public Page<T> find(
		Class<T> entityClass, TextSearchType type,
		Map<String, String> filterMap, FilterConjunction filterConjunction,
		List<SortOrder> sortOrders,
		List<SelectedFacet> selectedFacets,
		Integer pageNumber, Integer pageSize,
		Long since
	) throws Throwable {
		// Search requested
		if (filterMap.containsKey("q") || selectedFacets != null && selectedFacets.size() > 0) {
			final String query = filterMap.containsKey("q") ? filterMap.remove("q") : "*";

			final AtomicLong total = new AtomicLong(0);
			//Search against the Query DataSource
			List<Class<S>> searchEntities = getSearchableClasses();
			Collection<S> collection = Collections.newList();
			for(Class<S> searchEntity : searchEntities) {
				collection.addAll(getSearchService().search(searchEntity, type, query, filterMap, sortOrders, selectedFacets, pageNumber, pageSize, since, total));
			}
			//Get the collection and find the parentId
			List<UUID> collectionOfParentIds = getParentUUIDs(collection);
			final Page<T> pages = getRepositoryService().findAllById(getRepository(), entityClass, collectionOfParentIds, sortOrders, pageNumber, pageSize, since, total.longValue());
			return pages;
		} else {
			final Page<T> pages = getRepositoryService().find(getRepository(), entityClass, filterMap, sortOrders, pageNumber, pageSize, since, filterConjunction);
			return pages;
		}
	}

	@Override
	public Page<T> find(
		Class<T> entityClass, TextSearchType type,
		Map<String, String> filterMap, FilterConjunction filterConjunction,
		List<SortOrder> sortOrders,
		List<SelectedFacet> selectedFacets,
		Integer pageNumber, Integer pageSize
	) throws Throwable {
		return find(entityClass, type, filterMap, filterConjunction, sortOrders, selectedFacets, pageNumber, pageSize, /* since */null);
	}

	/*
	 * (non-Javadoc)
	 * @see org.appsweaver.commons.services.DataService#findById(java.util.UUID)
	 */
	@Override
	public Optional<T> findById(UUID id) {
		return findById(id, getRepository());
	}

	/*
	 * (non-Javadoc)
	 * @see org.appsweaver.commons.services.DataService#findByIdList(java.util.List)
	 */
	@Override
	public List<T> findByIdList(List<UUID> ids) {
		return findByIdList(ids, getRepository());
	}

	/*
	 * (non-Javadoc)
	 * @see org.appsweaver.commons.services.DataService#saveAll(java.util.List)
	 */
	@Override
	public List<T> saveAll(List<T> input) {
		final Iterable<T> iterable = getRepository().saveAll(input);

		if(iterable != null) {

			final List<T> list = Collections.newList();
			iterable.forEach(item -> {
				list.add(item);
			});

			return list;
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.appsweaver.commons.services.DataService#delete(org.appsweaver.commons.jpa.models.Entity)
	 */
	@Override
	public void delete(T input) {
		getRepository().delete(input);
	}

	/*
	 * (non-Javadoc)
	 * @see org.appsweaver.commons.services.DataService#mergeAndSave(org.appsweaver.commons.jpa.models.Entity, org.appsweaver.commons.jpa.models.Entity)
	 */
	@Override
	public T mergeAndSave(T source, T target) {
		Ruminator.mergeNonNullValues(source, target);
		return getRepository().save(target);
	}

	/*
	 * (non-Javadoc)
	 * @see org.appsweaver.commons.services.DataService#mergeAndSave(org.appsweaver.commons.jpa.models.Entity, org.appsweaver.commons.jpa.models.Entity)
	 */
	@Override
	public T mergeAndSave(T source, T target, Optional<Set<String>> optionalIgnoreProperties) {
		Ruminator.mergeNonNullValues(source, target, optionalIgnoreProperties);
		return getRepository().save(target);
	}

	public long count() throws SQLException {
		return getRepository().count();
	}

	public T update(T input) throws SQLException {
		if(input.getId() == null) throw new SQLException("Update not possible on a non-existent instance!");

		final Optional<T> optionalPerson = findById(input.getId());
		if(optionalPerson.isPresent()) {
			final T existing = optionalPerson.get();

			// Update item id
			input.setId(existing.getId());

			return mergeAndSave(input, existing);
		}

		throw new SQLException("Update not possible on an instance that was not found or was deleted recently!");
	}

	@Override
	public void forEachRecord(RecordProcessor<T> recordProcessor, Pageable pageable) throws SQLException {
		throw new SQLException("Iterating over records is not supported for query services");
	}
}
