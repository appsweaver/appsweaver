/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.tests.domain.mongo;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.appsweaver.commons.HibernateSearch;
import org.appsweaver.commons.annotations.Filterable;
import org.appsweaver.commons.annotations.Searchable;
import org.appsweaver.commons.jpa.models.query.AbstractQueryEntity;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.SortableField;
import org.hibernate.search.annotations.Store;

import lombok.EqualsAndHashCode;

/**
 * 
 * @author AN
 *
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Indexed(index = "EMPLOYEE_CAR")
@Table(name = "EMPLOYEE_CAR")
public class EmployeeCar extends AbstractQueryEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8563825589038835857L;

	@Field(store = Store.YES)
	@SortableField
	@Column(name = "MODEL", length = 128, nullable = false)
	@Searchable
	@Filterable
	private String model;

	@Field(store = Store.YES, analyzer = @Analyzer(definition = HibernateSearch.KEYWORD_ANALYZER))
	@SortableField
	@Column(name = "NUMBER_PLATE", length = 128, nullable = false)
	@Searchable
	@Filterable
	private String numberPlate;

	@Field(store = Store.YES, analyzer = @Analyzer(definition = HibernateSearch.KEYWORD_ANALYZER))
	@SortableField
	@Column(name = "CAR_ID", length = 32, nullable = false)
	private UUID carId;

	@Field(store = Store.YES, analyzer = @Analyzer(definition = HibernateSearch.KEYWORD_ANALYZER))
	@SortableField
	@Column(name = "EMPLOYEE_ID", length = 32, nullable = false)
	private UUID employeeId;

	@Field(store = Store.YES, analyzer = @Analyzer(definition = HibernateSearch.KEYWORD_ANALYZER))
	@SortableField
	@Column(name = "COMPANY_ID", length = 32, nullable = false)
	private UUID companyId;

	public EmployeeCar() {
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getNumberPlate() {
		return numberPlate;
	}

	public void setNumberPlate(String numberPlate) {
		this.numberPlate = numberPlate;
	}

	public UUID getCarId() {
		return carId;
	}

	public void setCarId(UUID carId) {
		this.carId = carId;
	}

	public UUID getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(UUID employeeId) {
		this.employeeId = employeeId;
	}

	public UUID getCompanyId() {
		return companyId;
	}

	public void setCompanyId(UUID companyId) {
		this.companyId = companyId;
	}

}
