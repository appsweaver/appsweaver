package org.appsweaver.apps.tests.repositories.mongo;

import org.appsweaver.apps.tests.domain.mongo.EmployeeDetail;
import org.appsweaver.commons.mongo.repositories.CustomMongoRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;


/**
 * @author AN
 *
 */
@Component
@Repository
public interface EmployeeDetailQueryRepository extends CustomMongoRepository<EmployeeDetail>{

}
