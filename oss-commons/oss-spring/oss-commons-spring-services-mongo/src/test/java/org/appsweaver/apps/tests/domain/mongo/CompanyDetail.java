package org.appsweaver.apps.tests.domain.mongo;

import java.util.List;

import javax.persistence.Column;

import org.appsweaver.commons.utilities.Collections;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author AN
 *
 */
@Document(collection = "COMPANY_DETAIL")
public class CompanyDetail extends Company {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1230539669504427630L;
		
	public CompanyDetail() {}
	
	public CompanyDetail(Company c) {
		this.setId(c.getId());
		this.setCreatedOn(c.getCreatedOn());
		this.setCeo(c.getCeo());
		this.setFoundedBy(c.getFoundedBy());
		this.setName(c.getName());
		this.setUpdatedOn(c.getUpdatedOn());
		this.setCfo(c.getCfo());
	}

	@Column(name = "EMPLOYEE_DETAILS")
	List<EmployeeDetail> employeeDetails = Collections.newList();

	public List<EmployeeDetail> getEmployeeDetails() {
		return employeeDetails;
	}

	public void setEmployeeDetails(List<EmployeeDetail> employeeDetails) {
		this.employeeDetails = employeeDetails;
	}
	
}
