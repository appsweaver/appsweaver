package org.appsweaver.apps.tests.domain.mongo;

import java.util.UUID;


/**
 * @author AN
 *
 */
public class EmployeeBuilder {
	private String name;
	private String title;
	private String companyName;
	private UUID companyId;
	
	public EmployeeBuilder() {
	}
	
	public EmployeeBuilder name(String name) {
		this.name = name;
		return this;
	}

	public EmployeeBuilder title(String title) {
		this.title = title;
		return this;
	}
	
	public EmployeeBuilder companyName(String companyName) {
		this.companyName = companyName;
		return this;
	}
	
	public EmployeeBuilder companyId(UUID companyId) {
		this.companyId = companyId;
		return this;
	}
	
	public Employee build() {
		final Employee employee = new Employee();
		employee.setName(name);
		employee.setTitle(title);
		employee.setCompanyName(companyName);
		employee.setCompanyId(companyId);
		return employee;
	}


}
