package org.appsweaver.apps.tests.domain.mongo;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.appsweaver.commons.annotations.Filterable;
import org.appsweaver.commons.annotations.Searchable;
import org.appsweaver.commons.jpa.models.query.AbstractQueryEntity;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;

import lombok.EqualsAndHashCode;

/**
 * @author AN
 *
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Indexed(index = "EMPLOYEE")
@Table(name = "EMPLOYEE")
public class Employee extends AbstractQueryEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8784321766886478555L;

	@Column(name = "NAME")
	@Filterable
	@Searchable
	@Field(store = Store.YES)
	private String name;
	
	@Column(name = "TITLE")
	@Filterable
	@Searchable
	@Field(store = Store.YES)
	private String title;

	@Column(name = "COMPANY_NAME")
	private String companyName;
	
	@Column(name = "COMPANY_ID")
	private UUID companyId;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public UUID getCompanyId() {
		return companyId;
	}

	public void setCompanyId(UUID companyId) {
		this.companyId = companyId;
	}

}
