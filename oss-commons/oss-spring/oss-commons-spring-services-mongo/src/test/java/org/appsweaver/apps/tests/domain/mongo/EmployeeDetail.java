package org.appsweaver.apps.tests.domain.mongo;

import java.util.List;

import javax.persistence.Column;

import org.appsweaver.commons.utilities.Collections;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author AN
 *
 */
@Document(collection = "EMPLOYEE_DETAIL")
public class EmployeeDetail extends Employee {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1230539669504427630L;

	public EmployeeDetail() {}
	
	public EmployeeDetail(Employee e) {
		this.setId(e.getId());
		this.setCreatedOn(e.getCreatedOn());
		this.setId(e.getId());
		this.setName(e.getName());
		this.setCompanyName(e.getCompanyName());
		this.setTitle(e.getTitle());
	}

	@Column(name = "EMPLOYEE_CARS")
	List<EmployeeCar> employeeCars = Collections.newList();

	@Column(name = "DIRECT_REPORTS")
	List<Employee> directReports = Collections.newList();

	public List<EmployeeCar> getEmployeeCars() {
		return employeeCars;
	}

	public void setEmployeeCars(List<EmployeeCar> employeeCars) {
		this.employeeCars = employeeCars;
	}

	public List<Employee> getDirectReports() {
		return directReports;
	}

	public void setDirectReports(List<Employee> directReports) {
		this.directReports = directReports;
	}

}
