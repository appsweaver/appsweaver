package org.appsweaver.apps.tests.domain.mongo;


/**
 * @author AN
 *
 */
public class CompanyBuilder {

	String name;
	String foundedBy;
	String ceo;
	String cfo;
	Integer numberOfemployees;

	public CompanyBuilder() {
	}
	
	public CompanyBuilder name(String name) {
		this.name = name;
		return this;
	}
	
	public CompanyBuilder foundedBy(String foundedBy) {
		this.foundedBy = foundedBy;
		return this;
	}

	public CompanyBuilder ceo(String ceo) {
		this.ceo = ceo;
		return this;
	}

	public CompanyBuilder numberOfemployees(Integer numberOfemployees) {
		this.numberOfemployees = numberOfemployees;
		return this;
	}
	
	public CompanyBuilder cfo(String cfo) {
		this.cfo = cfo;
		return this;
	}

	
	public Company build() {
		final Company company = new Company();
		company.setName(name);
		company.setFoundedBy(foundedBy);
		company.setCeo(ceo);
		company.setNumberOfemployees(numberOfemployees);
		company.setCfo(cfo);
		return company;
	}



}
