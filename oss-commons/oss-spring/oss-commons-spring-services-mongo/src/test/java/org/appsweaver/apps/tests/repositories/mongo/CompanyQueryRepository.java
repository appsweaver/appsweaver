/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.tests.repositories.mongo;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import org.appsweaver.apps.tests.domain.mongo.Company;
import org.appsweaver.commons.jpa.repositories.query.AbstractQueryRepository;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Component;

/**
 * 
 * @author AN
 *
 */
@Component
@ConditionalOnBean(name = "queryHibernateHelper")
public class CompanyQueryRepository extends AbstractQueryRepository<Company> {
	@Override
	public Class<Company> getEntityClass() {
		return Company.class;
	}

	public Optional<Company> findByName(String name) throws SQLException {
		final List<Company> list = getHibernateHelper().findByAttributeNameAndValue(getEntityClass(), "name", name);
		
		return (list != null && list.size() >= 1) ? Optional.ofNullable(list.get(0)) : Optional.empty();
	}
}