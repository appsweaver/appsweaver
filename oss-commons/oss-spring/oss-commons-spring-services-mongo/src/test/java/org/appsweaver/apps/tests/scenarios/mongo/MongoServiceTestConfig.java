package org.appsweaver.apps.tests.scenarios.mongo;

import org.appsweaver.commons.models.security.SecurityFacade;
import org.appsweaver.commons.services.config.FlywayMigrationConfig;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration;
import org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.ComponentScan.Filter;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;

@TestConfiguration
@ComponentScan(basePackages = {
	// Must load first - Begin
	"org.appsweaver.apps.tests.config",
	"org.appsweaver.apps.tests.components.generic",
	"org.appsweaver.apps.tests.components.query",
	"org.appsweaver.apps.tests.components.mongo",

	// Must load first - End
		
	"org.appsweaver.commons.**.components",
	"org.appsweaver.commons.**.config",
	"org.appsweaver.commons.**.services",
	"org.appsweaver.commons.**.utilities",
	
	// Must load last - Begin
	"org.appsweaver.apps.tests.repositories.query",
	"org.appsweaver.apps.tests.repositories.mongo",
	"org.appsweaver.apps.tests.services.query",
	"org.appsweaver.apps.tests.services.mongo"

	// Must load last - End
},
excludeFilters = {
	@Filter(type = FilterType.ASSIGNABLE_TYPE, value = FlywayMigrationConfig.class),
})
@EnableAutoConfiguration(exclude = {
	HibernateJpaAutoConfiguration.class,
	FlywayAutoConfiguration.class,
	RabbitAutoConfiguration.class,
	SecurityAutoConfiguration.class
})
@EnableConfigurationProperties
@EnableEncryptableProperties

public class MongoServiceTestConfig {
	@MockBean SecurityFacade securityFacade;
}
