/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.tests.services.mongo;

import java.sql.SQLException;
import java.util.Optional;

import org.appsweaver.apps.tests.domain.mongo.Company;
import org.appsweaver.apps.tests.repositories.mongo.CompanyQueryRepository;
import org.appsweaver.commons.jpa.repositories.query.CustomQueryRepository;
import org.appsweaver.commons.services.AbstractQueryDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

/**
 * @author UD
 */
@Service
@ConditionalOnBean(name = "companyQueryRepository")
public class CompanyQueryService extends AbstractQueryDataService<Company> {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired(required = false)
	CompanyQueryRepository companyQueryRepository;

	@Override
	public CustomQueryRepository<Company> getRepository() {
		return companyQueryRepository;
	}

	public Optional<Company> findByName(String name) throws SQLException {
		return companyQueryRepository.findByName(name);
	}

}