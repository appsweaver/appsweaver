/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.tests.config;

import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.spi.PersistenceUnitInfo;
import javax.persistence.spi.PersistenceUnitTransactionType;

import org.appsweaver.apps.tests.domain.mongo.Company;
import org.appsweaver.apps.tests.domain.mongo.Employee;
import org.appsweaver.apps.tests.domain.mongo.EmployeeCar;
import org.appsweaver.commons.utilities.Collections;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.hibernate.ogm.jpa.HibernateOgmPersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.orm.jpa.persistenceunit.MutablePersistenceUnitInfo;

/**
 * 
 * @author UD
 * 
 */
@TestConfiguration
@ConditionalOnProperty(prefix = "webservice.query-repository", name = "enabled", havingValue = "true")
public class QueryDataSourceConfig {
	final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	final static String QUERY_PERSISTENCE_UNIT_NAME = "hibernate-ogm-mongodb";

	@Bean("queryJpaProperties")
	@ConfigurationProperties("webservice.query-repository.jpa")
	public JpaProperties queryJpaProperties() {
		return new JpaProperties();
	}
	
	@Bean("queryEntityManagerFactory")
	public EntityManagerFactory queryEntityManagerFactory() {
		final HibernatePersistenceProvider hibernatePersistenceProvider = new HibernatePersistenceProvider();
		final PersistenceUnitInfo persistenceUnitInfo = new QueryMutablePersistenceUnitInfo();
		
		final EntityManagerFactory queryEntityManagerFactory = hibernatePersistenceProvider.createContainerEntityManagerFactory(
			persistenceUnitInfo, 
			queryJpaProperties().getProperties()
		);
		
		return queryEntityManagerFactory;
	}
	
	class QueryMutablePersistenceUnitInfo extends MutablePersistenceUnitInfo {
		@Override
		public String getPersistenceUnitName() {
			return QUERY_PERSISTENCE_UNIT_NAME;
		}
		
		@Override
		public String getPersistenceProviderClassName() {
			return HibernateOgmPersistence.class.getCanonicalName();
		}
		
		@Override
		public PersistenceUnitTransactionType getTransactionType() {
			return PersistenceUnitTransactionType.JTA;
		}
		
		@Override
		public List<String> getManagedClassNames() {
			/*
			 * TODO: Find a way to auto discover entity classes from managed package names
			 */
			return Collections.newList(
				Company.class.getCanonicalName(),
				Employee.class.getCanonicalName(),
				EmployeeCar.class.getCanonicalName()

			);
		}
		
		@Override
		public List<String> getManagedPackages() {
			return Collections.newList(
				"org.appsweaver.apps.tests.domain.query"
			);
		}
		
		@Override
        public ClassLoader getNewTempClassLoader() {
            return null;
        }
	}
}