package org.appsweaver.apps.tests.config;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;


/**
 * @author AN
 *
 */
@TestConfiguration
@EnableTransactionManagement
@EntityScan(basePackages = { "org.appsweaver.commons.jpa.models.**", "org.appsweaver.apps.tests.domain.mongo" })
@EnableMongoRepositories(basePackages = {"org.appsweaver.commons.mongo.repositories.**","org.appsweaver.apps.tests.repositories.mongo.**" })
public class MongoDataSourceConfig {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	@ConfigurationProperties(prefix = "spring.data")
	public DataSource mongoDataSource() {
		return DataSourceBuilder.create().build();
	}

}
