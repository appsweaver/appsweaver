/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.tests.domain.mongo;

import java.util.UUID;

/**
 * 
 * @author AN
 *
 */
public class EmployeeCarBuilder {
	String model;
	String numberPlate;
	UUID carId;
	UUID employeeId;
	UUID companyId;

	public EmployeeCarBuilder() {
	}

	public EmployeeCarBuilder model(String model) {
		this.model = model;
		return this;
	}

	public EmployeeCarBuilder numberPlate(String numberPlate) {
		this.numberPlate = numberPlate;
		return this;
	}

	public EmployeeCarBuilder carId(UUID carId) {
		this.carId = carId;
		return this;
	}

	public EmployeeCarBuilder employeeId(UUID employeeId) {
		this.employeeId = employeeId;
		return this;
	}

	public EmployeeCarBuilder companyId(UUID companyId) {
		this.companyId = companyId;
		return this;
	}

	public EmployeeCar build() {
		final EmployeeCar address = new EmployeeCar();
		address.setModel(model);
		address.setNumberPlate(numberPlate);
		address.setCarId(carId);
		address.setEmployeeId(employeeId);
		address.setCompanyId(companyId);
		return address;
	}
}
