package org.appsweaver.apps.tests.repositories.mongo;

import java.util.Optional;

import org.appsweaver.apps.tests.domain.mongo.CompanyDetail;
import org.appsweaver.commons.mongo.repositories.CustomMongoRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;


/**
 * @author AN
 *
 */
@Component
@Repository
public interface CompanyDetailQueryRepository extends CustomMongoRepository<CompanyDetail>{

	Optional<CompanyDetail> findByName(String name);

}
