package org.appsweaver.apps.tests.scenarios.mongo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.appsweaver.apps.tests.components.mongo.MongoTestDataProvider;
import org.appsweaver.apps.tests.domain.mongo.Company;
import org.appsweaver.apps.tests.domain.mongo.CompanyDetail;
import org.appsweaver.apps.tests.domain.mongo.Employee;
import org.appsweaver.apps.tests.domain.mongo.EmployeeCar;
import org.appsweaver.apps.tests.scenarios.AbstractMongoServiceTest;
import org.appsweaver.apps.tests.services.mongo.CompanyDetailQueryService;
import org.appsweaver.apps.tests.services.mongo.CompanyQueryService;
import org.appsweaver.commons.jpa.models.criteria.SortOrder;
import org.appsweaver.commons.jpa.models.search.TextSearchType;
import org.appsweaver.commons.jpa.types.FilterConjunction;
import org.appsweaver.commons.json.views.SelectedFacet;
import org.appsweaver.commons.models.persistence.QueryEntity;
import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Jsonifier;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;

/**
 * @author AN
 *
 */
public class TestCompanyService extends AbstractMongoServiceTest {
	
	@Qualifier("mongoTestDataProvider")
	@Autowired(required = false) MongoTestDataProvider mongoTestDataProvider;

	@Qualifier("companyQueryService")
	@Autowired(required = false) CompanyQueryService companyQueryService;

	@Qualifier("companyDetailQueryService")
	@Autowired(required = false) CompanyDetailQueryService<CompanyDetail, QueryEntity> companyDetailQueryService;

	@Test
	public void testAutowiring() throws SQLException {
		if(companyQueryService == null || mongoTestDataProvider == null || companyDetailQueryService == null) return;
		
		Assert.assertNotNull(mongoTestDataProvider);
		Assert.assertNotNull(companyQueryService);
	}

	@Test
	public void testQueryCompanyDetail() {
		if(companyQueryService == null || mongoTestDataProvider == null || companyDetailQueryService == null) return;
		Company randomCompany = mongoTestDataProvider.getRandomCompany();
		final Optional<CompanyDetail> optionalRandomCompany = companyDetailQueryService.findByName(randomCompany.getName());
		Assert.assertTrue(optionalRandomCompany.isPresent());
	}
	
	@Test
	public void testQueryAllCompanyDetail() {
		if(companyQueryService == null || mongoTestDataProvider == null || companyDetailQueryService == null) return;
		final List<CompanyDetail> companyDetails = companyDetailQueryService.findAll();
		Assert.assertEquals(mongoTestDataProvider.getCompanies().size(), companyDetails.size());
	}
	
	@Test
	public void testQueryAllCompanyDetailWithoutQuery() {
		if(companyQueryService == null || mongoTestDataProvider == null || companyDetailQueryService == null) return;
		Map<String, String> filterMap = new HashMap<>();

		List<SelectedFacet> selectedFacets = Collections.newList();
		List<SortOrder> sortOrders = Collections.newList();

		Page<CompanyDetail> companyDetails = null;
		try {
			companyDetails = companyDetailQueryService.find(CompanyDetail.class, TextSearchType.WILDCARD, filterMap, FilterConjunction.AND, sortOrders, selectedFacets, 0, 25);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Assert.assertEquals(mongoTestDataProvider.getCompanies().size(), companyDetails.getTotalElements());
	}


	
	@Test
	public void testSearchEmployeeCarInCompanyDetail() {
		if(companyQueryService == null || mongoTestDataProvider == null || companyDetailQueryService == null) return;
		EmployeeCar randomEmployeeCar = mongoTestDataProvider.getRandomEmployeeCar();
		Map<String, String> filterMap = new HashMap<>();
		filterMap.put("q", randomEmployeeCar.getNumberPlate());
		System.out.println("Searching for licence plate number: "+ randomEmployeeCar.getNumberPlate());

		List<SelectedFacet> selectedFacets = Collections.newList();
		List<SortOrder> sortOrders = Collections.newList();
		try {
			final Page<CompanyDetail> companiesList = companyDetailQueryService.find(CompanyDetail.class, TextSearchType.WILDCARD, filterMap, FilterConjunction.AND, sortOrders, selectedFacets, 0, 25);
			assertNotNull(companiesList);
			companiesList.forEach(companyDetail -> System.out.println(Jsonifier.toJson(companyDetail)));
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testSearchEmployeeInCompanyDetail() {
		if(companyQueryService == null || mongoTestDataProvider == null || companyDetailQueryService == null) return;
		Employee randomEmployee = mongoTestDataProvider.getRandomEmployee();
		Map<String, String> filterMap = new HashMap<>();
		filterMap.put("q", randomEmployee.getTitle());
		System.out.println("Searching for employee Title: "+ randomEmployee.getTitle());
		List<SelectedFacet> selectedFacets = Collections.newList();
		List<SortOrder> sortOrders = Collections.newList();
		try {
			final Page<CompanyDetail> companiesList = companyDetailQueryService.find(CompanyDetail.class, TextSearchType.WILDCARD, filterMap, FilterConjunction.AND, sortOrders, selectedFacets, 0, 25);
			assertNotNull(companiesList);
			companiesList.forEach(companyDetail -> System.out.println(Jsonifier.toJson(companyDetail)));
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testFilterEmployeeInCompanyDetailNull() {
		if(companyQueryService == null || mongoTestDataProvider == null || companyDetailQueryService == null) return;
		List<Company> companies = mongoTestDataProvider.getCompanies();
		System.out.println("Total Companies "+ companies.size());
		Map<String, String> filterMap = new HashMap<>();
		filterMap.put("cfo", "null()");
		
		System.out.println("Get all companies with null cfo: ");
		List<SelectedFacet> selectedFacets = Collections.newList();
		List<SortOrder> sortOrders = Collections.newList();
		try {
			final Page<CompanyDetail> companiesList = companyDetailQueryService.find(CompanyDetail.class, TextSearchType.WILDCARD, filterMap, FilterConjunction.AND, sortOrders, selectedFacets, 0, 25);
			assertNotNull(companiesList);
			System.out.println("Filtered Companies "+ companiesList.getTotalElements());
			assertEquals(1, companiesList.getTotalElements());
			companiesList.forEach(companyDetail -> System.out.println(Jsonifier.toJson(companyDetail)));
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testFilterEmployeeInCompanyDetailNotNull() {
		if(companyQueryService == null || mongoTestDataProvider == null || companyDetailQueryService == null) return;
		List<Company> companies = mongoTestDataProvider.getCompanies();
		System.out.println("Total Companies "+ companies.size());
		Map<String, String> filterMap = new HashMap<>();
		filterMap.put("cfo", "!null()");
		
		System.out.println("Get all companies with not null cfo: ");
		List<SelectedFacet> selectedFacets = Collections.newList();
		List<SortOrder> sortOrders = Collections.newList();
		try {
			final Page<CompanyDetail> companiesList = companyDetailQueryService.find(CompanyDetail.class, TextSearchType.WILDCARD, filterMap, FilterConjunction.AND, sortOrders, selectedFacets, 0, 25);
			assertNotNull(companiesList);
			System.out.println("Filtered Companies "+ companiesList.getTotalElements());
			assertEquals(2, companiesList.getTotalElements());
			companiesList.forEach(companyDetail -> System.out.println(Jsonifier.toJson(companyDetail)));
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}




}
