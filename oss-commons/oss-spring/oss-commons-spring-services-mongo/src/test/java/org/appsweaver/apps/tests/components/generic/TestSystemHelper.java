/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.tests.components.generic;

import java.io.File;

import javax.annotation.PostConstruct;

import org.appsweaver.commons.utilities.Fileinator;
import org.appsweaver.commons.utilities.Stringizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.TestComponent;

/**
 * 
 * @author UD
 *
 */
@TestComponent
public class TestSystemHelper {
	@Value("${webservice.work-dir}")
	String workDir;
	
	@Value("${webservice.search-index-dir}")
	String searchIndexBaseDir;
	
	/*
	 * Static references
	 */
	static String _searchIndexBaseDir;
	static String _workDir;
	
	@PostConstruct
	public void initializePostConstruct() {
		_workDir = workDir;
		_searchIndexBaseDir = searchIndexBaseDir;
	}
	
	public static void cleanup() {
		try {
			Fileinator.delete(new File(_workDir));
		} catch(Throwable t) {
			t.printStackTrace();
		}
	}

	public static void resetIndexSearchBaseDir() {
		if(!Stringizer.isEmpty(_searchIndexBaseDir)) {
			final File file = new File(_searchIndexBaseDir);
			if(file.exists() && file.isDirectory()) {
				try {
					Fileinator.delete(file);
				} catch(Throwable t) {
					t.printStackTrace();
				}
			}
		}
	}
}