package org.appsweaver.apps.tests.domain.mongo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.appsweaver.commons.annotations.Filterable;
import org.appsweaver.commons.annotations.Searchable;
import org.appsweaver.commons.jpa.models.query.AbstractQueryEntity;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;

import lombok.EqualsAndHashCode;


/**
 * @author AN
 *
 */
@EqualsAndHashCode(callSuper = true)
@Indexed(index = "COMPANY")
@Entity
@Table(name = "COMPANY")
public class Company extends AbstractQueryEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8873901147214099433L;
	
	@Column(name = "NAME")
	@Filterable
	@Searchable
	@Field(store = Store.YES)
	private String name;
	
	@Column(name = "FOUNDED_BY")
	@Searchable
	@Field(store = Store.YES)
	private String foundedBy;
	
	@Column(name = "CEO")
	@Searchable
	@Field(store = Store.YES)
	private String ceo;
	
	@Column(name = "NUMBER_OF_EMPLOYEES")
	private Integer numberOfemployees;
	
	@Column(name = "CEO")
	@Searchable
	@Field(store = Store.YES)
	@Filterable
	private String cfo; 
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFoundedBy() {
		return foundedBy;
	}

	public void setFoundedBy(String foundedBy) {
		this.foundedBy = foundedBy;
	}

	public String getCeo() {
		return ceo;
	}

	public void setCeo(String ceo) {
		this.ceo = ceo;
	}

	public Integer getNumberOfemployees() {
		return numberOfemployees;
	}

	public void setNumberOfemployees(Integer numberOfemployees) {
		this.numberOfemployees = numberOfemployees;
	}

	public String getCfo() {
		return cfo;
	}

	public void setCfo(String cfo) {
		this.cfo = cfo;
	}

}
