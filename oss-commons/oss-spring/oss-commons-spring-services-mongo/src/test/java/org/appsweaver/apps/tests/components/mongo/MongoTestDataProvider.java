/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.tests.components.mongo;

import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.appsweaver.apps.tests.domain.mongo.Company;
import org.appsweaver.apps.tests.domain.mongo.CompanyBuilder;
import org.appsweaver.apps.tests.domain.mongo.CompanyDetail;
import org.appsweaver.apps.tests.domain.mongo.Employee;
import org.appsweaver.apps.tests.domain.mongo.EmployeeBuilder;
import org.appsweaver.apps.tests.domain.mongo.EmployeeCar;
import org.appsweaver.apps.tests.domain.mongo.EmployeeCarBuilder;
import org.appsweaver.apps.tests.domain.mongo.EmployeeDetail;
import org.appsweaver.apps.tests.repositories.mongo.CompanyDetailQueryRepository;
import org.appsweaver.apps.tests.repositories.mongo.CompanyQueryRepository;
import org.appsweaver.apps.tests.repositories.mongo.EmployeeCarQueryRepository;
import org.appsweaver.apps.tests.repositories.mongo.EmployeeDetailQueryRepository;
import org.appsweaver.apps.tests.repositories.mongo.EmployeeQueryRepository;
import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Randomizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.test.context.TestComponent;

/**
 * 
 * @author AN
 *
 */
@TestComponent("mongoTestDataProvider")
@ConditionalOnBean(
	name = { 
		"employeeCarQueryRepository",
		"employeeQueryRepository",
		"companyQueryRepository"
	}
)
public class MongoTestDataProvider {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	final List<EmployeeCar> employeeCars = Collections.newList();
	
	final List<Employee> employees = Collections.newList();

	final List<Company> companies = Collections.newList();

	@Autowired(required = false) EmployeeCarQueryRepository employeeCarQueryRepository;

	@Autowired(required = false) EmployeeQueryRepository employeeQueryRepository;
	
	@Autowired(required = false) CompanyQueryRepository companyQueryRepository;
	
	@Autowired(required = false) EmployeeDetailQueryRepository employeeDetailQueryRepository;
	
	@Autowired(required = false) CompanyDetailQueryRepository companyDetailQueryRepository;

	
	@PostConstruct
	public void initialize() throws Throwable {
		if(employeeCarQueryRepository == null || employeeQueryRepository == null) return;
		
		// Delete objects if they exist
		employeeQueryRepository.deleteAll();
		employeeCarQueryRepository.deleteAll();
		companyQueryRepository.deleteAll();
		employeeDetailQueryRepository.deleteAll();
		companyDetailQueryRepository.deleteAll();
		//Create Google 
		Company google = companyQueryRepository.save(new CompanyBuilder().name("Google").ceo("Sundar P").foundedBy("Larry & Sergey").numberOfemployees(13031).cfo("Ruth Porat").build());

		Employee aThomas = employeeQueryRepository.save(new EmployeeBuilder().name("Arya Thomas").companyName(google.getName()).title("Engineering Engineer").companyId(google.getId()).build());
		EmployeeDetail aThomasDetails = new EmployeeDetail(aThomas);
		aThomasDetails.getEmployeeCars().add(employeeCarQueryRepository.save(new EmployeeCarBuilder().numberPlate("7KKL993").model("Elantra").carId(UUID.randomUUID()).companyId(aThomas.getCompanyId()).employeeId(aThomas.getId()).build()));
		employees.add(aThomas);
		employeeCars.addAll(aThomasDetails.getEmployeeCars());
		
		Employee eBrown = employeeQueryRepository.save(new EmployeeBuilder().name("Emmett Brown").companyName(google.getName()).title("Marketing Head").companyId(google.getId()).build());
		EmployeeDetail eBrownDetails = new EmployeeDetail(eBrown);
		eBrownDetails.getEmployeeCars().add(employeeCarQueryRepository.save(new EmployeeCarBuilder().numberPlate("7KOL993").model("Leaf").carId(UUID.randomUUID()).companyId(eBrown.getCompanyId()).employeeId(eBrown.getId()).build()));
		eBrownDetails.getEmployeeCars().add(employeeCarQueryRepository.save(new EmployeeCarBuilder().numberPlate("7KKL323").model("Model 3").carId(UUID.randomUUID()).companyId(eBrown.getCompanyId()).employeeId(eBrown.getId()).build()));
		employees.add(eBrown);
		employeeCars.addAll(eBrownDetails.getEmployeeCars());

		CompanyDetail googleDetail = new CompanyDetail(google);
		googleDetail.getEmployeeDetails().add(aThomasDetails);
		googleDetail.getEmployeeDetails().add(eBrownDetails);

		companyDetailQueryRepository.save(googleDetail);

		//Create Microsoft 
		Company microsoft = companyQueryRepository.save(new CompanyBuilder().name("Microsoft").ceo("Satya N").foundedBy("Bill Gates").numberOfemployees(13421).cfo("Amy Hood").build());

		Employee bKenbi = employeeQueryRepository.save(new EmployeeBuilder().name("Ben Kenobi").companyName(microsoft.getName()).title("Software Engineer").companyId(microsoft.getId()).build());
		EmployeeDetail bKenbiDetails = new EmployeeDetail(bKenbi);
		bKenbiDetails.getEmployeeCars().add(employeeCarQueryRepository.save(new EmployeeCarBuilder().numberPlate("3SKL993").model("Pathfinder").carId(UUID.randomUUID()).companyId(bKenbi.getCompanyId()).employeeId(bKenbi.getId()).build()));			// 1
		employees.add(bKenbi);
		employeeCars.addAll(eBrownDetails.getEmployeeCars());


		CompanyDetail microsoftDetail = new CompanyDetail(microsoft);
		microsoftDetail.getEmployeeDetails().add(bKenbiDetails);
		
		companyDetailQueryRepository.save(microsoftDetail);

		//Create Apple 

		Company apple = companyQueryRepository.save(new CompanyBuilder().name("Apple").ceo("Tim Cook").foundedBy("Steve Jobs").numberOfemployees(12342).build());

		Employee cJames = employeeQueryRepository.save(new EmployeeBuilder().name("Cullen James").companyName(apple.getName()).title("Marketing Head").companyId(apple.getId()).build());
		EmployeeDetail cJamesDetails = new EmployeeDetail(cJames);
		cJamesDetails.getEmployeeCars().add(employeeCarQueryRepository.save(new EmployeeCarBuilder().numberPlate("46KL993").model("Fusion").carId(UUID.randomUUID()).companyId(cJames.getCompanyId()).employeeId(cJames.getId()).build()));				// 2
		employees.add(cJames);
		employeeCars.addAll(cJamesDetails.getEmployeeCars());

		Employee aBux = employeeQueryRepository.save(new EmployeeBuilder().name("Aurra Bux").companyName(apple.getName()).title("MTS 1").companyId(apple.getId()).build());
		EmployeeDetail aBuxDetails = new EmployeeDetail(aBux);
		aBuxDetails.getEmployeeCars().add(employeeCarQueryRepository.save(new EmployeeCarBuilder().numberPlate("7KKL673").model("CRV").carId(UUID.randomUUID()).companyId(aBux.getCompanyId()).employeeId(aBux.getId()).build()));				// 2
		aBuxDetails.getEmployeeCars().add(employeeCarQueryRepository.save(new EmployeeCarBuilder().numberPlate("7KKL899").model("Camry").carId(UUID.randomUUID()).companyId(aBux.getCompanyId()).employeeId(aBux.getId()).build()));				// 3
		employees.add(aBux);
		employeeCars.addAll(aBuxDetails.getEmployeeCars());

		Employee aJackson = employeeQueryRepository.save(new EmployeeBuilder().name("Amelia Jackson").companyName(apple.getName()).title("MTS 2").companyId(apple.getId()).build());
		EmployeeDetail aJacksonDetails = new EmployeeDetail(aJackson);
		aJacksonDetails.getEmployeeCars().add(employeeCarQueryRepository.save(new EmployeeCarBuilder().numberPlate("74DL993").model("Sentra").carId(UUID.randomUUID()).companyId(aJackson.getCompanyId()).employeeId(aJackson.getId()).build()));			// 4
		aJacksonDetails.getEmployeeCars().add(employeeCarQueryRepository.save(new EmployeeCarBuilder().numberPlate("7HNL993").model("Sonata").carId(UUID.randomUUID()).companyId(aJackson.getCompanyId()).employeeId(aJackson.getId()).build()));		// 5
		aJacksonDetails.getEmployeeCars().add(employeeCarQueryRepository.save(new EmployeeCarBuilder().numberPlate("9JKL993").model("Model X").carId(UUID.randomUUID()).companyId(aJackson.getCompanyId()).employeeId(aJackson.getId()).build()));			// 6
		employees.add(aJackson);
		employeeCars.addAll(aJacksonDetails.getEmployeeCars());

		Employee jKane = employeeQueryRepository.save(new EmployeeBuilder().name("Jackson Kane").companyName(apple.getName()).title("Principal Engineer").companyId(apple.getId()).build());
		EmployeeDetail jKaneDetails = new EmployeeDetail(jKane);
		jKaneDetails.getEmployeeCars().add(employeeCarQueryRepository.save(new EmployeeCarBuilder().numberPlate("4MNL993").model("Veron").carId(UUID.randomUUID()).companyId(jKane.getCompanyId()).employeeId(jKane.getId()).build()));		// 7
		employees.add(jKane);
		employeeCars.addAll(jKaneDetails.getEmployeeCars());

		Employee jHolmes = employeeQueryRepository.save(new EmployeeBuilder().name("Jermy Holmes").companyName(apple.getName()).title("Engineering Engineer").companyId(apple.getId()).build());
		EmployeeDetail jHolmesDetails = new EmployeeDetail(jHolmes);
		// Create Addresses
		jHolmesDetails.getEmployeeCars().add(employeeCarQueryRepository.save(new EmployeeCarBuilder().numberPlate("4KAL993").model("Civic").carId(UUID.randomUUID()).companyId(jHolmes.getCompanyId()).employeeId(jHolmes.getId()).build()));			// 8
		employees.add(jHolmes);
		employeeCars.addAll(jHolmesDetails.getEmployeeCars());

		
		CompanyDetail appleDetail = new CompanyDetail(apple);

		appleDetail.getEmployeeDetails().add(cJamesDetails);
		appleDetail.getEmployeeDetails().add(aBuxDetails);
		appleDetail.getEmployeeDetails().add(aJacksonDetails);
		appleDetail.getEmployeeDetails().add(jKaneDetails);
		appleDetail.getEmployeeDetails().add(jHolmesDetails);
		
		companies.add(google);
		companies.add(apple);
		companies.add(microsoft);
		
		companyDetailQueryRepository.save(appleDetail);

	}

	public Company getRandomCompany() {
		if(companies.size() > 0) {
			return companies.get(Randomizer.randomBetween(0, companies.size() - 1));
		}
		return null;
	}

	public EmployeeCar getRandomEmployeeCar() {
		if(employeeCars.size() > 0) {
			return employeeCars.get(Randomizer.randomBetween(0, employeeCars.size() - 1));
		}
		return null;
	}
	
	public Employee getRandomEmployee() {
		if(employees.size() > 0) {
			return employees.get(Randomizer.randomBetween(0, employees.size() - 1));
		}
		return null;
	}

	public List<Company> getCompanies() {
		return companies;
	}



}