/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.tests.services.mongo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.appsweaver.apps.tests.domain.mongo.CompanyDetail;
import org.appsweaver.apps.tests.domain.mongo.Employee;
import org.appsweaver.apps.tests.domain.mongo.EmployeeCar;
import org.appsweaver.apps.tests.repositories.mongo.CompanyDetailQueryRepository;
import org.appsweaver.commons.models.persistence.QueryEntity;
import org.appsweaver.commons.mongo.repositories.CustomMongoRepository;
import org.appsweaver.commons.services.AbstractMongoDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

/**
 * 
 * @author AN
 *
 */
@Service
@ConditionalOnBean(name = "companyDetailQueryRepository")
public class CompanyDetailQueryService<T extends CompanyDetail, S extends QueryEntity>
		extends AbstractMongoDataService<T, S> {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired(required = false)
	CompanyDetailQueryRepository companyQueryRepository;

	@Override
	public CustomMongoRepository<CompanyDetail> getRepository() {
		return companyQueryRepository;
	}

	@Override
	protected List<Class<S>> getSearchableClasses() {
		final List<Class<S>> searchableClasses = new ArrayList<>();
		searchableClasses.add((Class<S>) EmployeeCar.class);
		searchableClasses.add((Class<S>) Employee.class);
		return searchableClasses;
	}

	@Override
	protected List<UUID> getParentUUIDs(Collection<S> collection) {
		final List<UUID> companyIds = new ArrayList<>();
		collection.forEach(entity -> companyIds.add(processes(entity)));
		return companyIds;
	}

	private UUID processes(S entity) {
		if (entity instanceof Employee) {
			return ((Employee) entity).getCompanyId();
		} else if (entity instanceof EmployeeCar) {
			return ((EmployeeCar) entity).getCompanyId();
		}
		return null;
	}

	public Optional<CompanyDetail> findByName(String name) {
		return companyQueryRepository.findByName(name);
	}

	public List<CompanyDetail> findAll() {
		return companyQueryRepository.findAll();
	}

}
