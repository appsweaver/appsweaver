package org.appsweaver.commons.mongo.models.criteria;

import static org.appsweaver.commons.utilities.TypeConverter.toSet;
import static org.appsweaver.commons.utilities.TypeConverter.valueOf;

import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;

import javax.persistence.criteria.Path;

import org.apache.commons.lang3.StringUtils;
import org.appsweaver.commons.models.criteria.FilterableField;
import org.appsweaver.commons.models.persistence.QueryEntity;
import org.appsweaver.commons.spring.utilities.FrameworkHelper;
import org.appsweaver.commons.utilities.Stringizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.query.Criteria;


/**
 * 
 * Create a filter specification<br>
 * <br>
 * Expected filter syntax: <b>name:like(value)</b><br>
 * <br>
 * When operator is not provided; defaults to like<br>
 * <br>
 * operator 		=> 	gt, ge, lt, se, eq, !eq, like, !like, in, !in, between, !between<br>
 * attribute 	=> 	must be field name that can be translated to JPA layer; can think of obfuscation; will defer to later times<br>
 * value 		=> 	a value to look up (can be comma separated list) <br>
 * 					in case of between and not between operator values must be enclosed (x, y) <br>
 * <br>
 * 
 * @author UD
 *
 * @param <T>
 * 
 */
public class MongoFilterSpecification<T extends QueryEntity> {	
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	static final String EXPR_PREFIX = "(";
	static final String EXPR_SUFFIX = ")";

	static boolean isExpression(final String value) {
		if (value == null) {
			return false;
		}
		final String trimmedValue = value.trim();
		return (trimmedValue.contains(EXPR_PREFIX) && trimmedValue.endsWith(EXPR_SUFFIX));
	}

	static String getValue(final String value) {
		return value.substring(value.indexOf(EXPR_PREFIX, 0) + 1, (value.length() - EXPR_SUFFIX.length()));
	}

	static String getOperator(final String value) {
		return value.substring(0, value.indexOf(EXPR_PREFIX, 0));
	}

	final String operator;
	final String attribute;
	final String value;
	final Class<T> clazz;

	public MongoFilterSpecification(Class<T> clazz, String attribute, String value) {
		this.clazz = clazz;

		if (!isExpression(value)) {
			this.operator = "eq"; // Default to "eq"
			this.attribute = attribute;
			this.value = value;
		} else {
			this.operator = getOperator(value);
			this.attribute = attribute;
			this.value = getValue(value);
		}
	}

	public void validate() throws SQLException {
		final Map<String, FilterableField> filterableAttributes = FrameworkHelper.getFilterableFields(clazz);
		if(!filterableAttributes.containsKey(attribute)) {
			throw new SQLException(String.format("Unsupported filterable attribute [name=%s]", attribute));
		}
	}

	final String getValue() {
		if(attribute.equals("id")) {
			if(operator.equals("in") || operator.equals("!in")) {
				final Collection<String> values = toSet(value, String.class);
				final Collection<String> decoded = new HashSet<>(0);

				values.forEach(value -> {
					if(StringUtils.isNumeric(value)) {
						decoded.add(value);
					} else {
						/*
						 * Convert to hash / human id
						 */
						// decoded.add(String.valueOf(Hashes.hashToId(clazz, value)));
						decoded.add(value);
					}
				});

				final String csvString = Stringizer.toCSVString(decoded);
				return csvString;
			} else {
				if(StringUtils.isNumeric(value)) {
					return value;
				}

				/*
				 * Convert to id
				 */
				// final Long id = Hashes.hashToId(clazz, value);
				// return String.valueOf(id);
				return value;
			}
		}

		return value;
	}

	final boolean isChild(String attribute) {
		final String []attribtuesAsArray = attribute.split("\\.");
		return attribtuesAsArray.length > 1;
	}


	final FilterableField resolveAttribute() throws SQLException {
		return FrameworkHelper.getFilterableFields(clazz).get(attribute);
	}
	
	/*
	 * Examples: http://www.baeldung.com/queries-in-spring-data-mongodb
	 */
	public Criteria toCriteria() {
		try {
			final FilterableField attribute = resolveAttribute();

			if(attribute == null) {
				throw new UnsupportedOperationException(String.format("Filter not supported [attribute=%s, class=%s]", this.attribute, this.clazz.getName()));
			}

			switch (operator.toLowerCase()) {
			case "lt":
				return Criteria.where(attribute.getPath()).lt(valueOf(getValue(), Number.class));
			case "le":
				return Criteria.where(attribute.getPath()).lte(valueOf(getValue(), Number.class));

			case "gt":
				return Criteria.where(attribute.getPath()).gt(valueOf(getValue(), Number.class));

			case "ge":
				return Criteria.where(attribute.getPath()).gte(valueOf(getValue(), Number.class));

			case "in": 
			case "!in": {
				Collection<?> collection = toSet(getValue(), attribute.getType());
				
				final Criteria criteria;
				if(operator.startsWith("!")) {
					criteria = Criteria.where(attribute.getPath()).nin(collection);
				} else {
					criteria = Criteria.where(attribute.getPath()).in(collection);
				}
				
				return criteria;
			}

			case "eq": {
				final String value = getValue();
				if(StringUtils.isNumeric(value))  {
					return Criteria.where(attribute.getPath()).is(valueOf(value, Number.class));
				} else {
					return Criteria.where(attribute.getPath()).is(valueOf(value, attribute.getType()));
				}
			}

			case "!eq": {
				final String value = getValue();
				if(StringUtils.isNumeric(value))  {
					return Criteria.where(attribute.getPath()).ne(valueOf(value, Number.class));
				} else {
					return Criteria.where(attribute.getPath()).ne(valueOf(value, attribute.getType()));
				}
			}

			case "like":
			case "!like":{
				Criteria criteria = Criteria.where(attribute.getPath()).regex(getValue());
				if(operator.startsWith("!")) {
					criteria = criteria.norOperator(criteria);
				}
				return criteria;
			}

			case "between": 
			case "!between": {
				final String value = getValue();
				final String[] tokens = value.split(",");

				final Long start = valueOf(tokens[0], Long.class);
				final Long end = valueOf(tokens[1], Long.class);

				Criteria criteria = Criteria.where(attribute.getPath()).gt(start).lt(end);
				if(operator.startsWith("!")) {
					criteria = criteria.norOperator(criteria);
				}
				return criteria;
			}
			
			case "null":
			case "!null": {
				
				// isNotNull
				if(operator.startsWith("!")) {
					return Criteria.where(attribute.getPath()).ne(null);
				}

				// isNull
				return Criteria.where(attribute.getPath()).is(null);
			}

			default:
				throw new UnsupportedOperationException(String.format("Operator [%s] not supported yet!", operator.toLowerCase()));
			}
		} catch(Throwable t) {
			logger.debug(String.format("Unable to resolve attribute [class=%s, attribute=%s]", clazz.getName(), this.attribute), t);
		}
		
		return null;
	}
}