package org.appsweaver.commons.mongo.repositories;


import java.util.List;
import java.util.UUID;

import org.appsweaver.commons.models.persistence.QueryEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * @author AN
 * Apr 9, 2020
 */
@NoRepositoryBean
public interface CustomMongoRepository<T extends QueryEntity> extends MongoRepository<T, UUID> {

	Page<T> findAllByIdIn(List<UUID> ids, Pageable pagable);
}
