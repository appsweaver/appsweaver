/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.uaam.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.appsweaver.commons.jpa.models.BaseUserCredential;
import org.hibernate.annotations.DynamicUpdate;

import lombok.EqualsAndHashCode;

/**
 * 
 * @author UD
 *
 */
@Entity
@Table(name = "USER_CREDENTIALS")
@DynamicUpdate
@EqualsAndHashCode(callSuper = true)
public class UserCredential extends BaseUserCredential {
	private static final long serialVersionUID = -8099918305379623761L;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "USER_ID")
	private User user;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public UserCredential() {
		super();
	}

	public UserCredential(String password, User user) {
		super(password);
		
		setUser(user);
	}
}