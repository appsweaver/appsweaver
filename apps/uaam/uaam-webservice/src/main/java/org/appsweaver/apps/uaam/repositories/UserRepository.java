/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.uaam.repositories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.appsweaver.apps.uaam.domain.User;
import org.appsweaver.commons.jpa.repositories.CustomPrimaryRepository;
import org.appsweaver.commons.models.security.AuthenticationProviderType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author UD
 *
 */
@Repository
@Transactional
public interface UserRepository extends CustomPrimaryRepository<User> {
	/**
	 * Exists check by loginId or secondary login id or email.
	 *
	 * @param loginId
	 *            the user login id
	 * @param secondaryLoginId
	 *            the alternate user login id
	 * @param email
	 *            the user email address
	 * @param id
	 *            the id
	 * @return the boolean
	 */
	@Query(
		"SELECT case " +
			"WHEN count(*) > 0 " +
			"then " +
				"true " +
			"else " +
				"false " +
			"end " +
		"FROM User u " + 
		"WHERE " +
			"(:id is not null and u.id = :id) " +
			"or " +
			"(:loginId is not null and u.loginId = :loginId) " +
			"or " +
			"(:secondaryLoginId is not null and u.secondaryLoginId = :secondaryLoginId) " +
			"or " +
			"(:email is not null and u.email = :email)"
	)
	boolean existsByAny(
		@Param("loginId") String loginId, 
		@Param("secondaryLoginId") Optional<String> secondaryLoginId,
		@Param("email") Optional<String> email,
		@Param("id") Optional<UUID> id
	);
	
	/**
	 * Find user by loginId or secondary login id or email.
	 *
	 * @param loginId
	 *            the user login id
	 * @param secondaryLoginId
	 *            the alternate user login id
	 * @param email
	 *            the user email address
	 * @param id
	 *            the id
	 * @return the boolean
	 */
	@Query(
		"SELECT u " +
		"FROM User u " + 
		"WHERE " +
			"(:id is not null and u.id = :id) " +
			"or " +
			"(:loginId is not null and u.loginId = :loginId) " +
			"or " +
			"(:secondaryLoginId is not null and u.secondaryLoginId = :secondaryLoginId) " +
			"or " +
			"(:email is not null and u.email = :email)"
	)
	Optional<User> findByAny(
		@Param("loginId") String loginId, 
		@Param("secondaryLoginId") Optional<String> secondaryLoginId,
		@Param("email") Optional<String> email,
		@Param("id") Optional<UUID> id
	);
	
	/**
	 * Find user by loginId or secondary login id or email.
	 *
	 * @param loginIds
	 *            the user login id list
	 * @param secondaryLoginId
	 *            the optional alternate user login id
	 * @param email
	 *            the optional user email address
	 * @param id
	 *            the id
	 * @return the boolean
	 */
	@Query(
		"SELECT u " +
		"FROM User u " + 
		"WHERE " +
			"(u.loginId in :loginIds) " +
			"or " +
			"(:secondaryLoginId is not null and u.secondaryLoginId = :secondaryLoginId) " +
			"or " +
			"(:email is not null and u.email = :email)"
	)
	Optional<User> findByAny(
		@Param("loginIds") List<String> loginIds, 
		@Param("secondaryLoginId") Optional<String> secondaryLoginId,
		@Param("email") Optional<String> email
	);

	/**
	 * Find by email optional.
	 *
	 * @param email
	 *            the email
	 * @return true if user exists, false otherwise
	 */
	boolean existsByEmail(@Param("email") String email);
	
	/**
	 * Find by email optional.
	 *
	 * @param email
	 *            the email
	 * @return the optional
	 */
	Optional<User> findByEmail(@Param("email") String email);

	/**
	 * Exists by login id boolean.
	 *
	 * @param loginId
	 *            the login id
	 * @return the boolean
	 */
	boolean existsByLoginId(@Param("loginId") String loginId);

	/**
	 * Find by login id optional.
	 *
	 * @param loginId
	 *            the login id
	 * @return the optional
	 */
	Optional<User> findByLoginId(@Param("loginId") String loginId);

	/**
	 * Find by login id and source system optional.
	 *
	 * @param loginId
	 *            the login id
	 * @param sourceSystem
	 *            the source system
	 * @return the optional
	 */
	Optional<User> findByLoginIdAndAuthenticationProvider(@Param("loginId") String loginId, @Param("authenticationProvider") AuthenticationProviderType authenticationProvider);
	
	/**
	 * Find all users by source system.
	 *
	 * @param authenticationProvider
	 * @return 
	 */
	
 	List<User> findByAuthenticationProvider(@Param("authenticationProvider") AuthenticationProviderType authenticationProvider);

	/**
	 * @param activeDirectory
	 * @param loginIds
	 * @return
	 */
	List<User> findByAuthenticationProviderAndLoginIdIn(@Param("authenticationProvider") AuthenticationProviderType activeDirectory, List<String> loginIds);

}