/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.uaam.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import org.appsweaver.apps.uaam.domain.Role;
import org.appsweaver.apps.uaam.domain.User;
import org.appsweaver.apps.uaam.domain.UserCredential;
import org.appsweaver.apps.uaam.domain.views.json.UserView;
import org.appsweaver.apps.uaam.repositories.RoleRepository;
import org.appsweaver.apps.uaam.repositories.UserCredentialRepository;
import org.appsweaver.apps.uaam.repositories.UserRepository;
import org.appsweaver.commons.jpa.repositories.CustomPrimaryRepository;
import org.appsweaver.commons.jpa.types.Rank;
import org.appsweaver.commons.models.security.AuthenticationProviderType;
import org.appsweaver.commons.models.security.UpdatedBasicCredential;
import org.appsweaver.commons.services.AbstractDataService;
import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Dates;
import org.appsweaver.commons.utilities.Stringizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * The type User service.
 *
 * @author UD
 * 
 */
@Service
public class UserService extends AbstractDataService<User> {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * The Authentication service.
	 */
	@Autowired
	AuthenticationService authenticationService;

	/**
	 * The User repository.
	 */
	@Autowired
	UserRepository userRepository;

	/**
	 * The Role repository.
	 */
	@Autowired
	RoleRepository roleRepository;

	/**
	 * The User credential repository.
	 */
	@Autowired
	UserCredentialRepository userCredentialRepository;

	@Override
	public CustomPrimaryRepository<User> getRepository() {
		return userRepository;
	}
	
	public List<String> getSanitizedLoginIds(String username, List<String> domainNames) {
		final List<String> sanitizedLoginIds = new ArrayList<>();
		
		// User name does not contain a domain name separator, and does not end with default domain name
		if(Stringizer.isValidEmailAddress(username)) {
			sanitizedLoginIds.add(username);
		} else {
			for(String domainName : domainNames) {
				final String sanitizedLoginId = String.format("%s@%s", username, domainName.toLowerCase());
				sanitizedLoginIds.add(sanitizedLoginId);
			}
		}
		
		return sanitizedLoginIds;
	}

	/**
	 * User existence check by login id or secondary login id or email
	 *
	 * @param id
	 *            the id
	 * @param loginId
	 *            the login id
	 * @param secondaryLoginId
	 *            the secondary login id
	 * @param email
	 *            the email
	 * @return the boolean
	 */
	public boolean existsByAny(
		String loginId,
		Optional<String> secondaryLoginId,
		Optional<String> email,
		Optional<UUID> id
	) {
		return userRepository.existsByAny(
			loginId, secondaryLoginId, email, id
		);
	}

	/**
	 * Register user user.
	 *
	 * @param user
	 * 		the user object
	 * @param password
	 * 		the optional password  
	 * @return the user
	 * @throws Exception
	 *             the exception
	 */
	@Transactional
	public User register(User user, Optional<String> optionalPassword) throws Exception {
		final String loginId = user.getLoginId();
		final UUID id = user.getId();
		final String secondaryLoginId = user.getSecondaryLoginId();
		final String email = user.getEmail();
		if (
			userRepository.existsByAny(
				loginId,
				Optional.ofNullable(secondaryLoginId),
				Optional.ofNullable(email),
				Optional.ofNullable(id)
			)
		) {
			throw new Exception(
				String.format(
					"User already registered [loginId=%s, secondaryLoginId=%s, email=%s]", 
					loginId, secondaryLoginId, email
				)
			);
		}

		// Save User
		final User savedUser = userRepository.save(user);
		
		// Save Credentials - Only during initialization
		final UserCredential existingUserCredential = userCredentialRepository.findByUser(savedUser);
		if (optionalPassword.isPresent() && existingUserCredential == null) {
			/*
			 * TODO: Must use password change times to set initial password
			 */
			final UserCredential userCredential = new UserCredential(optionalPassword.get(), savedUser);
			userCredentialRepository.save(userCredential);
		}

		return user;
	}

	/**
	 * Gets user by login id or secondary login id or email.
	 *
	 * @param id
	 *            the id
	 * @param loginId
	 *            the login id
	 * @param secondaryLoginId
	 *            the secondary login id
	 * @param email
	 *            the email
	 * @return return user when found, empty otherwise
	 */
	public Optional<User> findByAny(
		String loginId,
		Optional<String> secondaryLoginId,
		Optional<String> email,
		Optional<UUID> id
	) {
		return userRepository.findByAny(
			loginId, secondaryLoginId, email, id
		);
	}
	
	/**
	 * Gets user by login id or secondary login id or email.
	 *
	 * @param loginId
	 *            the optional login id
	 * @param secondaryLoginId
	 *            the secondary login id
	 * @param email
	 *            the email
	 * @return return user when found, empty otherwise
	 */
	public Optional<User> findByAny(
		List<String> loginId, Optional<String> secondaryLoginId, Optional<String> email
	) {
		return userRepository.findByAny(
			loginId,
			secondaryLoginId,
			email
		);
	}

	/**
	 * Update optional.
	 *
	 * @param optionalUser
	 *            the optional user
	 * @return the optional
	 */
	public Optional<User> update(Optional<User> optionalUser) {
		if (optionalUser.isPresent()) {
			final User user = optionalUser.get();
			final User saved = userRepository.save(user);
			return Optional.of(saved);
		}

		return Optional.empty();
	}

	/**
	 * Exists by login id boolean.
	 *
	 * @param loginId
	 *            the login id
	 * @return the boolean
	 */
	public boolean existsByLoginId(String loginId) {
		return userRepository.existsByLoginId(loginId);
	}

	/**
	 * Find by login id optional.
	 *
	 * @param loginId
	 *            the login id
	 * @return the optional
	 */
	public Optional<User> findByLoginId(String loginId) {
		return userRepository.findByLoginId(loginId);
	}

	/**
	 * Create user.
	 *
	 * @param input
	 *            the input
	 * @param rank
	 *            the rank
	 * @param users
	 *            the users
	 * @return the user
	 * @throws Exception
	 *             the exception
	 */
	public User create(User input, Rank rank, List<User> users) throws Exception {
		final boolean exists = existsByAny(
			input.getLoginId(),
			Optional.ofNullable(input.getSecondaryLoginId()),
			Optional.ofNullable(input.getEmail()),
			Optional.ofNullable(input.getId())
		);
		if (exists) {
			throw new Exception(
				String.format(
					"User already exists! [loginId=%s, secondaryLoginId=%s, email=%s]", 
					input.getLoginId(), input.getSecondaryLoginId(), input.getEmail()
				)
			);
		}

		final Optional<Role> role = roleRepository.findById(input.getRole().getId());
		if (!role.isPresent()) {
			throw new Exception(String.format("Role [id=%s] not found", input.getRole().getId()));
		}

		input.setRole(role.get());

		final User saved = save(input, Optional.ofNullable(rank), users);
		return saved;
	}

	@Transactional
	public User create(UserView userView) throws Exception {
		final User userVO = userView.toUser();
		final boolean exists = existsByAny(
			userVO.getLoginId(), 
			Optional.ofNullable(userVO.getSecondaryLoginId()), 
			Optional.ofNullable(userVO.getEmail()), 
			Optional.ofNullable(userVO.getId())
		);
		
		if (exists) {
			throw new Exception(
				String.format(
					"User already exists! [loginId=%s, secondaryLoginId=%s, email=%s]", 
					userVO.getLoginId(), userVO.getSecondaryLoginId(), userVO.getEmail()
				)
			);
		}

		final Role role = roleRepository.findByName(userView.getRole());
		if (null != role) {
			final User user = userView.toUser();
			user.setRole(role);
			user.setAuthenticationProvider(AuthenticationProviderType.LOCAL_REPOSITORY);

			final User savedUser = save(user);
			final UserCredential userCredential = new UserCredential();
			userCredential.setUser(savedUser);
			userCredential.setPassword(userView.getPassword());
			userCredentialRepository.save(userCredential);

			return savedUser;
		} else {
			throw new Exception(String.format("Role [id=%s] not found", userView.getRole()));
		}
	}

	/**
	 * Change password.
	 *
	 * @param rawPassword
	 *            the raw password
	 * @param user
	 *            the user
	 */
	void changePassword(String rawPassword, Optional<User> user) {
		if (user.isPresent()) {
			final UserCredential userCredential = userCredentialRepository.findByUser(user.get());

			// Set updated time stamp
			userCredential.setUpdatedOn(Dates.toTime());

			// Update credentials
			userCredential.setPassword(rawPassword);

			// Persist changes
			userCredentialRepository.save(userCredential);
		}
	}

	/**
	 * Change password optional.
	 *
	 * @param credential
	 *            the credential
	 * @param id
	 *            the id
	 * @return the optional
	 * @throws Throwable
	 *             the throwable
	 */
	public Optional<User> changePassword(UpdatedBasicCredential credential, UUID id) throws Throwable {
		final String oldPassword = Stringizer.decodeBase64(credential.getOldPassword());
		final String newPassword = Stringizer.decodeBase64(credential.getNewPassword());

		if (!credential.isGood()) {
			throw new Exception("Invalid Credentials");
		}

		final Optional<User> user = findById(id);
		if (user.isPresent()) {
			// Check for pre-conditions
			if (!authenticationService.checkPassword(oldPassword, id)) {
				throw new Exception("Password mismatch");
			}

			changePassword(newPassword, user);

			return user;
		} else {
			throw new Exception("Unknown / Invalid user");
		}
	}

	/**
	 * Gets user credential.
	 *
	 * @param user
	 *            the user
	 * @return the user credential
	 */
	public UserCredential getCredential(User user) {
		final UserCredential userCredential = userCredentialRepository.findByUser(user);

		return userCredential;
	}
	
	public List<User> create(List<User> users, Optional<String> defaultPassword) {
		final AtomicInteger count = new AtomicInteger(0);
		final List<User> created = Collections.newList();
		
		users.forEach(user -> {
			final String loginId = user.getLoginId();
			final String secondaryLoginId = user.getSecondaryLoginId();
			final String email = user.getEmail();
			
			if(!Stringizer.isEmpty(loginId) && !Stringizer.isEmpty(secondaryLoginId)) {
				try {
					final Optional<User> createdUser = create(user, defaultPassword);
					
					if(createdUser.isPresent()) {
						logger.trace("Caching created/refreshed user [loginId={}, secondaryLoginId={}, email={}]", loginId, secondaryLoginId, email);

						count.incrementAndGet();
						created.add(createdUser.get());
					}
				} catch(final Throwable t) {
					logger.error("Error registering user [loginId={}, secondaryLoginId={}, email={}]", loginId, secondaryLoginId, email, t);
				}
			} else {
				logger.warn("Skipping registration of user [loginId={}, secondaryLoginId={}, email={}]", loginId, secondaryLoginId, email);
			}
		});
		
		return created;
	}
	
	@Transactional
	public Optional<User> create(User user, Optional<String> defaultPassword) {
		final Optional<User> optionalUser = findByAny(
			user.getLoginId(),
			Optional.ofNullable(user.getSecondaryLoginId()),
			Optional.ofNullable(user.getEmail()),
			Optional.ofNullable(user.getId())
		);
		
		if(!optionalUser.isPresent()) {
			try {
				logger.info("Creating user [id={}, loginId={}, secondaryLoginId={}, email={}]", user.getId(), user.getLoginId(), user.getSecondaryLoginId(), user.getEmail());

				return Optional.ofNullable(register(user, defaultPassword));
			} catch(final Throwable t) {
				logger.error(
					"Error creating user [loginId={}, secondaryLoginId={}, email={}]", 
					user.getLoginId(), user.getSecondaryLoginId(), user.getEmail(),
					t
				);
			}
		} else {
			final User existing = optionalUser.get();
			
			logger.info(
				"Refreshing user [id={}, loginId={}, secondaryLoginId={}, email={}]", 
				existing.getId(), existing.getLoginId(), user.getSecondaryLoginId(), existing.getEmail()
			);
			
			return mergeAndSave(user, optionalUser, defaultPassword);
		}

		logger.trace(
			"NO-OP for user [id={}, loginId={}, secondaryLoginId={}, email={}]",
			user.getId(), user.getLoginId(), user.getSecondaryLoginId(), user.getEmail()
		);
		
		return null;
	}

	Optional<User> mergeAndSave(User user, Optional<User> optionalUser, Optional<String> optionalPassword) {
		if(optionalUser.isPresent()) {
			final User merged = optionalUser.get();
			
			merged.setLoginId(user.getLoginId());
			merged.setSecondaryLoginId(user.getSecondaryLoginId());
			merged.setEmail(user.getEmail());

			merged.setFirstName(user.getFirstName());
			merged.setLastName(user.getLastName());

			merged.setTitle(user.getTitle());
			merged.setDepartmentName(user.getDepartmentName());
			merged.setNotes(user.getNotes());
			
			merged.setMobileNumber(user.getMobileNumber());
			merged.setAuthenticationProvider(user.getAuthenticationProvider());

			merged.setRole(user.getRole());

			merged.setCreatedOn(user.getCreatedOn());
			merged.setEnabled(user.isEnabled());

			final Optional<User> optionallyUpdated = update(optionalUser);
			
			// Password setting must be requested
			if(optionallyUpdated.isPresent() && optionalPassword.isPresent()) {
				final String password = optionalPassword.get();
				final User updated = optionallyUpdated.get();
				
				UserCredential userCredential = userCredentialRepository.findByUser(updated);
				// Save Credentials
				if (userCredential == null) {
					userCredential = new UserCredential(password, updated);
				} else {
					userCredential.setPassword(password);
				}

				// Save password settings
				userCredentialRepository.save(userCredential);
			}
		
			return optionallyUpdated;
		}
		
		return Optional.empty();
	}
}
