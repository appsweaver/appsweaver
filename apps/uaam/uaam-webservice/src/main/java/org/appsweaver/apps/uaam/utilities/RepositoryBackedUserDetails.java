/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.uaam.utilities;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.appsweaver.apps.uaam.domain.User;
import org.appsweaver.apps.uaam.domain.UserCredential;
import org.appsweaver.apps.uaam.repositories.UserCredentialRepository;
import org.appsweaver.commons.utilities.Collections;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * 
 * @author UD
 *
 */
public class RepositoryBackedUserDetails implements UserDetails {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7072656324294807839L;

	final UserCredentialRepository userCredentialRepository;
	final Optional<User> user;

	public RepositoryBackedUserDetails(UserCredentialRepository userCredentialRepository, Optional<User> user) {
		super();

		this.userCredentialRepository = userCredentialRepository;
		this.user = user;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		final List<GrantedAuthority> grantedAuthorities;
		
		if(getUser().isPresent()) {
			grantedAuthorities = getUser().get().toGrantedAuthorities();
		} else {
			grantedAuthorities = Collections.newList();
		}
		
		return grantedAuthorities;
	}

	@Override
	public String getPassword() {
		if(user.isPresent()) {
			final UserCredential userCredential = userCredentialRepository.findByUser(user.get());
			final String password = userCredential.getPassword();
			return password;
		}
		
		return null;
	}

	@Override
	public String getUsername() {
		if(user.isPresent()) {
			return getUser().get().getEmail();
		}

		return null;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		if(user.isPresent()) {
			return !user.get().isLocked();
		}
		
		return false;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		if(user.isPresent()) {
			return user.get().isEnabled();
		}

		return false;
	}

	public Optional<User> getUser() {
		return user;
	}
}