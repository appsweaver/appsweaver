/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.uaam.domain.views.json;

import org.appsweaver.apps.uaam.domain.Role;
import org.appsweaver.commons.json.views.DefaultView;
import org.appsweaver.commons.utilities.Stringizer;

/**
 * 
 * @author UD
 *
 */
public class RoleView extends DefaultView {
	private String name;
	private String label;

	public RoleView() {
		super();
	}

	public RoleView(Role role) {
		setId(role.getId());
		setName(role.getName());
		setLabel(role.getLabel());
	}

	public String getLabel() {
		return Stringizer.isEmpty(label) ? label : Stringizer.capitalize(label);
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}