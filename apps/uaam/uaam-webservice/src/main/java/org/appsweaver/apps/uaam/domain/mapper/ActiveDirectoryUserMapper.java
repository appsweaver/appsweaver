/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.uaam.domain.mapper;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;

import org.appsweaver.apps.uaam.domain.Role;
import org.appsweaver.apps.uaam.domain.User;
import org.appsweaver.apps.uaam.utilities.AttributesHelper;
import org.appsweaver.commons.models.security.AuthenticationProviderType;
import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Randomizer;
import org.appsweaver.commons.utilities.Stringizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.support.LdapUtils;

/**
 * 
 * @author UD
 *
 */
public class ActiveDirectoryUserMapper implements AttributesMapper<User> {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	final Map<String, Role> roles;
	final Role defaultRole;

	public ActiveDirectoryUserMapper(Map<String, Role> roles, Role defaultRole) {
		this.roles = roles;
		this.defaultRole = defaultRole;
	}
	
	@Override
	public User mapFromAttributes(Attributes attributes) throws NamingException {
		/*
		 * LDAP userPrincipalName is used as login id in UAM
		 */
		final String loginId = getValue(attributes, "userPrincipalName");
		
		/*
		 * Process only normal user account
		 */
		if (loginId != null && isNormalUser(attributes)) {
			logger.trace("Importing/Refreshing AD user [loginId={}]", loginId);

			final User user = new User();

			// Map user source to ACTIVE_DIRECTORY
			user.setAuthenticationProvider(AuthenticationProviderType.ACTIVE_DIRECTORY);
			
			/*
			 * objectGUID must be used to refer user consistently.
			 * Other attribute values are subject to change
			 */
			final String objectGUID = getValue(attributes, "objectGUID");
			user.setId(Randomizer.uuid(objectGUID));
			
			/*
			 * Login ID is userPrincipalName
			 */
			if(loginId != null) {
				user.setLoginId(loginId.toLowerCase());
			}
			
			/*
			 * Set alternate login ID
			 */
			final String secondaryLoginId = getValue(attributes, "sAMAccountName");
			if(secondaryLoginId != null) {
				user.setSecondaryLoginId(secondaryLoginId.toLowerCase());
			}

			user.setEnabled(isEnabled(attributes));
			
			/*
			 * Set user role or set to default
			 */
			user.setRole(resolveRole(attributes, defaultRole));

			final String email = getValue(attributes, "mail");
			if(email != null) {
				user.setEmail(email.toLowerCase());
			}
			
			user.setFirstName(getValue(attributes, "givenName"));
			
			user.setLastName(getValue(attributes, "sn"));
			
			user.setTitle(getValue(attributes, "title"));
			
			user.setNotes(getValue(attributes, "description"));
			
			user.setDepartmentName(getValue(attributes, "department"));
			
			final Long createdOn = toTimestamp(getValue(attributes, "whenCreated"));
			if(createdOn != null) {
				user.setCreatedOn(createdOn);
			}
			
			return user;
		}

		return null;
	}
	
	Long toTimestamp(String dateValue) {
		try {
			if (!Stringizer.isEmpty(dateValue)) {
				final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("uuuuMMddHHmmss[,S][.S]X");
				final OffsetDateTime offsetDateTime = OffsetDateTime.parse(dateValue, dateTimeFormatter);
				final Instant instant = offsetDateTime.toInstant();

				return instant.toEpochMilli();
			}
		} catch (Throwable t) {
		}

		return null;
	}
	
	Boolean isNormalUser(Attributes attributes) {
		/*
		 * Get sAMAccountType
		 * 
		 * 0 			SAM_DOMAIN_OBJECT
		 * 268435456	SAM_GROUP_OBJECT
		 * 268435457	SAM_NON_SECURITY_GROUP_OBJECT
		 * 536870912	SAM_ALIAS_OBJECT
		 * 536870913	SAM_NON_SECURITY_ALIAS_OBJECT
		 * 805306368	SAM_NORMAL_USER_ACCOUNT
		 * 805306369	SAM_MACHINE_ACCOUNT
		 * 805306370	SAM_TRUST_ACCOUNT
		 * 1073741824	SAM_APP_BASIC_GROUP
		 * 1073741825	SAM_APP_QUERY_GROUP
		 * 2147483647	SAM_ACCOUNT_TYPE_MAX
		 * 
		 */
		final String sAMAccountType = getValue(attributes, "sAMAccountType");
		return "805306368".equals(sAMAccountType);
	}
	
	Boolean isEnabled(Attributes attributes) {
		final String userAccountControl = getValue(attributes, "userAccountControl");
		
		/*
		 * userAccountControl attribute is used to decide user must be enabled or disabled.
		 * 
		 * For more details, check out 
		 * http://woshub.com/decoding-ad-useraccountcontrol-value/
		 * https://support.microsoft.com/en-us/help/305144/how-to-use-useraccountcontrol-to-manipulate-user-account-properties
		 * 
		 * enabled:
		 * ===========
		 * 512		Normal Account
		 * 544		Enabled, Password not required
		 * 66048	Enabled, Password does not expire
		 * 262656	Enabled, Smart card required
		 * 
		 * disabled:
		 * ===========
		 * 2		The account is disabled
		 * 514		Disabled Account
		 * 546		Disabled, Password not required
		 * 66050	Disabled, Password does not expire
		 * 66082	Disabled, Password Doesn't Expire & not required
		 * 262658	Disabled, Smart card required
		 * 262690	Disabled, Smart card required & Password not required
		 * 328194	Disabled, Smart card required & Password does not expire
		 * 328226	Smart card required, Password does not expire & not required
		 */
		if(userAccountControl != null) {
			switch(userAccountControl) {
			case "512":
			case "544":
			case "66048":
			case "262656":
				return true;
				
			case "2":
			case "514":
			case "546":
			case "66050":
			case "66082":
			case "262658":
			case "262690":
			case "328194":
			case "328226":
				return false;
			}
		}
		
		return null;
	}
	/*
	 * What aspect of AD must be used to determine the role/department of a user
	 */
	Role resolveRole(Attributes attributes, Role defaultRole) {
		final Set<String> roleNames = Collections.newHashSet();
		final List<String> securityGroups = getValues(attributes, "memberOf");
		
		securityGroups.forEach(value -> {
			final String[] securityGroupTokens = value.split(",");
			Arrays.asList(securityGroupTokens).stream()
				.filter(securityGroupToken -> securityGroupToken.startsWith("CN="))
				.forEach(securityGroupName -> {
					/*
					 * TODO: Verify if the user group name is mapped to a proper role
					 * 
					 * This lookup logic must be revisited, looks buggy as of 2019-12-02
					 */
					final String[] securityGroupNameTokens = securityGroupName.split("=");
					final String roleName = securityGroupNameTokens[1];

					// Cache role names obtained from LDAP 
					if(!roleNames.contains(roleName)) {
						roleNames.add(roleName);
					}
				}
			);
		});
		
		logger.trace("    [memberOf={}]", roleNames);
		final Role role = resolveRole(roleNames);
		
		return (role == null) ? defaultRole : role;
	}

	void buildAttribute(Attributes attributes, String attributeName, StringBuilder builder) {
		final Object object = getValue(attributes, attributeName);
		
		if(object != null) {
			builder.append("AD: ").append(attributeName).append("=").append(getValue(attributes, attributeName)).append("\n");
		}
	}
	
	void trace(Attributes attributes) {
		StringBuilder builder = new StringBuilder();
		
		builder.append("\n");
		builder.append("AD: Record Begin").append("\n");
		for(String attributeName : AttributesHelper.allAttributeNames()) {
			buildAttribute(attributes, attributeName, builder);
		}
		builder.append("AD: Record End").append("\n");

		logger.trace(builder.toString());
	}

	Role resolveRole(Set<String> roleNames) {
		final Iterator<String> iterator = roleNames.iterator();
		
		while(iterator.hasNext()) {
			final String roleName = iterator.next();
			if(roles.containsKey(roleName)) {
				return roles.get(roleName);
			}
		}
		
		return null;
	}

	String getValue(Attributes attributes, String attributeName) {
		String stringValue = null;
		
		try {
			
			if(attributeName.compareTo("objectGUID") == 0) {
				final byte[] value = (byte[])attributes.get(attributeName).get();
				if(value != null) {
					stringValue = guidToString(value);
				}
			} else if(attributeName.compareTo("objectSID") == 0) {
				final byte[] value = (byte[])attributes.get(attributeName).get();
				if(value != null) {
					stringValue = LdapUtils.convertBinarySidToString(value);
				}
			} else {
				final String value = (String)attributes.get(attributeName).get();
				if(value != null) {
					stringValue = value;
				}
			}
		} catch (Throwable t) {
		}
		
		return stringValue;
	}
	
	List<String> getValues(Attributes attributes, String attributeName) {
		final List<String> values = Collections.newList();

		final Attribute attribute = attributes.get(attributeName);
		if (attribute != null && attribute.size() >= 1) {
			for (int index = 0; index < attribute.size(); index++) {
				try {
					final Object object = attribute.get(index);
					if (object != null) {
						final String value = String.valueOf(object);
						if (!Stringizer.isEmpty(value)) {
							values.add(value);
						}
					}
				} catch (NamingException e) {
				}
			}
		}
		
		return values;
	}
	
	String format(int value) {
        return String.format("%02x", value & 0xFF);
    }

	/*
	 * Reference: https://www.codeleading.com/article/92362981509/
	 */
	String guidToString(byte[] objectGUID) {
		final StringBuilder builder = new StringBuilder();

		builder.append(format(objectGUID[3]));
		builder.append(format(objectGUID[2]));
		builder.append(format(objectGUID[1]));
		builder.append(format(objectGUID[0]));
		builder.append("-");
		builder.append(format(objectGUID[5]));
		builder.append(format(objectGUID[4]));
		builder.append("-");
		builder.append(format(objectGUID[7]));
		builder.append(format(objectGUID[6]));
		builder.append("-");
		builder.append(format(objectGUID[8]));
		builder.append(format(objectGUID[9]));
		builder.append("-");
		builder.append(format(objectGUID[10]));
		builder.append(format(objectGUID[11]));
		builder.append(format(objectGUID[12]));
		builder.append(format(objectGUID[13]));
		builder.append(format(objectGUID[14]));
		builder.append(format(objectGUID[15]));

		return builder.toString();
	}
}