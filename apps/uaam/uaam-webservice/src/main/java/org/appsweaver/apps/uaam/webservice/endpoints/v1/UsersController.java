/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.uaam.webservice.endpoints.v1;

import java.util.Optional;
import java.util.UUID;

import org.appsweaver.commons.api.PrimaryDataService;
import org.appsweaver.commons.models.GenericErrorCode;
import org.appsweaver.commons.models.security.UpdatedBasicCredential;
import org.appsweaver.commons.models.web.WebResponse;
import org.appsweaver.commons.models.web.exceptions.ApplicationException;
import org.appsweaver.commons.webservice.endpoints.AbstractPrimarySearchController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.appsweaver.apps.uaam.domain.User;
import org.appsweaver.apps.uaam.domain.views.json.UserView;
import org.appsweaver.apps.uaam.services.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 
 * @author UD
 *
 */
@RestController
@RequestMapping(value = "/api/v1/users", produces = { MediaType.APPLICATION_JSON_VALUE })
@Api(value = "Users Controller")
public class UsersController extends AbstractPrimarySearchController<User> {

	/**
	 * The User service.
	 */
	@Autowired
	private UserService userService;

	@Override
	public Class<User> getEntityClass() {
		return User.class;
	}

	@Override
	public PrimaryDataService<User> getDataService() {
		return userService;
	}

	/**
	 * Get user for given id
	 *
	 * @param id
	 *            the id
	 * @return by id
	 */
	@Transactional(readOnly = true)
	@GetMapping(path = "/{id}")
	@ApiOperation(value = "Get User By ID / UUID")
	public WebResponse<User> getById(@ApiParam @PathVariable("id") UUID id) {
		final String message = String.format("GET: [entityClass=%s, id=%s]", entityName(), id);
		try {
			getLogger().info("START: {}; [internalId={}]", message, id);
			// Check if input is right
			validateInput(id, "GET");

			getLogger().debug("Check if object exists [class={}, id={}]", entityName(), id);

			final Optional<User> entity = userService.findById(id);

			getLogger().info("GET [id={}, class={}, requester={}]", id, entityName(), getDataService().getLoginId());

			return (entity.isPresent()) ? WebResponse.newWebResponse(entity.get()) : WebResponse.newWebResponse(HttpStatus.NOT_FOUND);
		} catch (Throwable t) {
			getLogger().error("Error getting one item {} [id={}]", entityName(), id, t);
			throw new ApplicationException(controllerName(), HttpStatus.INTERNAL_SERVER_ERROR, GenericErrorCode.FAILED_TO_GET_ITEM_BY_ID,
					String.format("Error getting one item [context=%s, id=%s]", entityName(), id), t.getMessage());
		} finally {
			getLogger().info("END: {}", message);
		}
	}

	@Transactional
	@PutMapping(path = "/{id}", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ApiOperation(value = "Update User")
	public @ResponseBody WebResponse<User> update(@ApiParam @PathVariable("id") UUID id, @RequestBody User user) {
		final String message = String.format("UPDATE: [entityClass=%s, id=%s]", entityName(), id);
		try {
			getLogger().info("START: {}", message);
			// Check if input is right
			validateInput(id, "PUT");

			getLogger().debug("Check if object exists [class={}, id={}]", entityName(), id);
			final Optional<User> entity = getDataService().findById(id);
			if (entity.isPresent()) {
				final User savedUser = getDataService().save(user);

				getLogger().info("UPDATED [id={}, class={}, requestor={}]", id, entityName(), getDataService().getLoginId());

				return WebResponse.newWebResponse(savedUser);
			} else {
				throw new ApplicationException(controllerName(), HttpStatus.NOT_MODIFIED, GenericErrorCode.NO_OPERATION,
						String.format("Item not modified [context=%s, id=%s]", entityName(), id));
			}
		} catch (Throwable t) {
			getLogger().error("Error updating {} [id={}]", entityName(), id, t);
			throw new ApplicationException(controllerName(), HttpStatus.INTERNAL_SERVER_ERROR, GenericErrorCode.FAILED_TO_UPDATE_ITEM,
					String.format("Error updating item [context=%s, id=%s]", entityName(), id), t.getMessage());
		} finally {
			getLogger().info("END: {}", message);
		}
	}

	/**
	 * Create user.
	 *
	 * @param userView
	 *            the user view
	 * @return the web response
	 */
	@PostMapping(consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ApiOperation(value = "Create User")
	public WebResponse<User> create(@RequestBody UserView userView) {
		final String message = String.format("CREATE: [entityClass=%s]", getEntityClass().getSimpleName());
		try {
			getLogger().info("START: {}", message);

			User saved = userService.create(userView);

			getLogger().info("CREATED [id={}, class={}, requester={}]", saved.getId(), getEntityClass().getName(), userService.getLoginId());

			return WebResponse.newWebResponse(saved);
		} catch (Throwable t) {
			getLogger().error("Error creating {}", entityName(), t);
			throw new ApplicationException(controllerName(), HttpStatus.NOT_ACCEPTABLE, GenericErrorCode.FAILED_TO_CREATE_ITEM,
					String.format("Error creating item [context=%s]", entityName()));
		} finally {
			getLogger().info("END: {}", message);
		}
	}

	/**
	 * Update credentials web response.
	 *
	 * @param id
	 *            the id
	 * @param credential
	 *            the credential
	 * @return the web response
	 */
	@PutMapping(value = "/{id}/credentials", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ApiOperation(value = "Update Credentials")
	public WebResponse<User> updateCredentials(@ApiParam @PathVariable("id") UUID id, @RequestBody UpdatedBasicCredential credential) {
		final String message = String.format("CHANGE PASSWORD: [entityClass=%s]", getEntityClass().getSimpleName());

		try {
			getLogger().info("START: {}", message);
			// Check if input is right
			validateInput(id, "PUT CREDENTIALS");

			final Optional<User> user = userService.changePassword(credential, id);
			if (user.isPresent()) {
				return WebResponse.newWebResponse(user.get());
			} else {
				throw new ApplicationException(controllerName(), HttpStatus.PRECONDITION_FAILED, GenericErrorCode.UNKNOWN_OR_INVALID_USER,
						"Unknown / Invalid user");
			}
		} catch (Throwable ex) {
			getLogger().error("Error updating {} [id={}]", entityName(), id, ex);
			throw new ApplicationException(controllerName(), HttpStatus.NOT_MODIFIED, GenericErrorCode.FAILED_TO_CHANGE_PASSWORD,
					"Credentials not updated");
		} finally {
			getLogger().info("END: {}", message);
		}
	}
}