/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.uaam.services;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.naming.directory.SearchControls;

import org.appsweaver.apps.uaam.domain.Role;
import org.appsweaver.apps.uaam.domain.User;
import org.appsweaver.apps.uaam.domain.mapper.ActiveDirectoryUserMapper;
import org.appsweaver.apps.uaam.utilities.AttributesHelper;
import org.appsweaver.commons.utilities.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.AbstractContextSource;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.ldap.filter.Filter;
import org.springframework.ldap.support.LdapUtils;
import org.springframework.stereotype.Service;

/**
 * 
 * @author UD
 *
 * Error Info: http://ldapwiki.com/wiki/Common%20Active%20Directory%20Bind%20Errors
 * Attributes: https://msdn.microsoft.com/en-us/library/ms680508(v=vs.85).aspx
 * Valid password characters: https://www.ibm.com/support/knowledgecenter/en/SSAW57_7.0.0/com.ibm.websphere.nd.doc/info/ae/ae/csec_chars.html
 */
@Service
public class ActiveDirectoryService {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired UserService userService;
	
	@Autowired(required = true)
	LdapTemplate ldapTemplate;
	
	@Value("${webservice.security.active-directory.user-search-base}")
	String activeDirectoryGlobalSearchName;
	
	@Value("${webservice.security.active-directory.trace-content:false}")
	Boolean traceContent;
	
	@PostConstruct
	public void initialize() {
		/*
		 * This is MUST to process LDAP binary attributes
		 */
		try {
			logger.debug("Trying to add LDAP binary property support to environment");
			
			final AbstractContextSource contextSource = (AbstractContextSource) ldapTemplate.getContextSource();
			final Map<String,Object> baseEnvironmentProperties = Collections.newHashMap();
			baseEnvironmentProperties.put("java.naming.ldap.attributes.binary", "objectSid objectGUID");
			contextSource.setBaseEnvironmentProperties(baseEnvironmentProperties);
			contextSource.afterPropertiesSet();
			
			logger.debug("LDAP binary property support was added to environment, successfully");
		} catch (Throwable t) {
			logger.warn("Error adding property [java.naming.ldap.attributes.binary = objectSid objectGUID]", t);
		}
	}

	public void importData(Map<String, Role> roles, Role defaultRole) {
		try {
			logger.debug("Importing users from AD");
			
			final Filter filter = new EqualsFilter("objectClass", "person");
			final String[] attributeNames = AttributesHelper.allAttributeNames();
			
			final List<User> users = ldapTemplate.search(
				LdapUtils.newLdapName(activeDirectoryGlobalSearchName),
				filter.encode(),
				SearchControls.SUBTREE_SCOPE,
				attributeNames,
				new ActiveDirectoryUserMapper(roles, defaultRole)
			);

			// Remove null values
			users.removeIf(Objects::isNull);
			logger.info("AD users [fetched={}]", users.size());
			
			final List<User> created = userService.create(users, Optional.empty());
			
			logger.info("AD users [fetched={}, imported={}, skipped={}]", users.size(), created.size(), (users.size() - created.size()));
			
		} catch(Throwable t) {
			logger.error("Could not import users from AD", t);
		} finally {
			logger.debug("Finished importing users from AD");
		}
	}
}
