/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.uaam.components;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.appsweaver.apps.uaam.domain.Role;
import org.appsweaver.apps.uaam.domain.User;
import org.appsweaver.apps.uaam.domain.mapper.JsonFileUserMapper;
import org.appsweaver.apps.uaam.services.ActiveDirectoryService;
import org.appsweaver.apps.uaam.services.RolesAndPermissionsService;
import org.appsweaver.apps.uaam.services.UserService;
import org.appsweaver.commons.models.properties.Security;
import org.appsweaver.commons.models.properties.attributes.AuthenticatonProviderDetails;
import org.appsweaver.commons.models.security.AuthenticationProviderType;
import org.appsweaver.commons.spring.components.WebServiceProperties;
import org.appsweaver.commons.utilities.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 
 * @author UD
 *
 */
@Component
public class ContentProvider {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	ActiveDirectoryService activeDirectoryService;

	@Autowired
	WebServiceProperties webServiceProperties;

	@Autowired
	RolesAndPermissionsService rolesAndPermissionsService;

	@Autowired
	UserService userService;

	@EventListener(ApplicationReadyEvent.class)
	public void initializeUsers() throws Throwable {
		importData();
	}

	@Scheduled(cron = "${webservice.schedulers.sync-scheduler.crontime}")
	@Async
	public void importData() throws Throwable {

		final Security securityProperties = webServiceProperties.getSecurity();
		AuthenticatonProviderDetails authenticatonSourceAttribute;

		// Roles
		try {
			final String rolesAndPermissionsFile = securityProperties.getRolesAndPermissionsFile();
			logger.info("Sync roles and permissions from data file {}", rolesAndPermissionsFile);
			rolesAndPermissionsService.importData(rolesAndPermissionsFile);
		} catch (Throwable t) {
			logger.warn("Error syncing roles and permissions from data file", t);
		}

		// Cache Roles
		final Map<String, Role> roles = Collections.newHashMap();
		rolesAndPermissionsService.findAllRoles().forEach(role -> {
			// Assuming role name is sanitized already
			roles.put(role.getName(), role);
		});

		final String sanitizedDefaultRoleName = Role.getSanitizedRoleName(securityProperties.getDefaultRole());
		final Role defaultRole = rolesAndPermissionsService.findRoleByName(sanitizedDefaultRoleName);

		authenticatonSourceAttribute = webServiceProperties
				.getAuthenticationProviderDetails(AuthenticationProviderType.ACTIVE_DIRECTORY);
		if (authenticatonSourceAttribute.isEnabled() && authenticatonSourceAttribute.isSyncData()) {
			// LDAP Users
			try {
				logger.info("Sync users from LDAP");
				activeDirectoryService.importData(roles, defaultRole);
			} catch (Throwable t) {
				logger.warn("Error syncing users from LDAP", t);
			}
		} else {
			logger.info("Syncing users from active directory is not enabled!");
		}

		authenticatonSourceAttribute = webServiceProperties
				.getAuthenticationProviderDetails(AuthenticationProviderType.LOCAL_REPOSITORY);
		if (authenticatonSourceAttribute.isEnabled() && authenticatonSourceAttribute.isSyncData()) {
			// Local Users
			try {
				final String customUsersFile = securityProperties.getCustomUsersFile();

				logger.info("Sync users from data file {}", customUsersFile);
				final JsonFileUserMapper jsonFileUserMapper = new JsonFileUserMapper(customUsersFile, roles,
						defaultRole);

				// Parse and populate user data
				jsonFileUserMapper.importData();

				final List<User> users = jsonFileUserMapper.getUsers();

				// Send in default password
				final List<User> created = userService.create(users,
						Optional.ofNullable(securityProperties.getDefaultUserPassword()));

				logger.info("Users [fetched={}, imported={}, skipped={}]", users.size(), created.size(),
						(users.size() - created.size()));
			} catch (Throwable t) {
				logger.warn("Error syncing users from data file", t);
			}
		} else {
			logger.info("Syncing users from data file is not enabled!");
		}
	}

}
