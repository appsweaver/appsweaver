/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.uaam.domain;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.appsweaver.apps.uaam.domain.converters.json.PermissionCollectionConverter;
import org.appsweaver.commons.annotations.IdGenerationPolicy;
import org.appsweaver.commons.annotations.Searchable;
import org.appsweaver.commons.jpa.models.BaseRole;
import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Stringizer;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.hibernate.search.annotations.SortableField;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.EqualsAndHashCode;

/**
 * 
 * @author UD
 *
 */
@Entity
@Table(name = "ROLES")
@Indexed(index = "ROLES")
@DynamicUpdate
@IdGenerationPolicy("name")
@EqualsAndHashCode(callSuper = true)
public class Role extends BaseRole {
	private static final long serialVersionUID = 2196571017776227607L;

	@Searchable
	@IndexedEmbedded(includePaths = {"name"})
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "ROLES_PERMISSIONS", joinColumns = @JoinColumn(name = "ROLE_ID"), inverseJoinColumns = @JoinColumn(name = "PERMISSION_ID"))
	@JsonSerialize(converter = PermissionCollectionConverter.class) 
	private Set<Permission> permissions = Collections.newHashSet();
	
	@Field
	@SortableField
	@Searchable
	@Column(name = "LABEL", unique = true, length = 255)
	private String label;
	
	public Role() {
		super();
	}

	public Role(String name, String label) throws Throwable {
		setName(name.trim());
		
		setLabel(label.trim());
	}
	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Role(String name, String label, Set<Permission> permissions) throws Throwable {
		this(name, label);

		setPermissions(permissions);
	}
	
	@Override
	public void setName(String name) throws Throwable {
		super.setName(getSanitizedRoleName(name));
	}

	public void setPermissions(Set<Permission> permissions) {
		this.permissions = permissions;
	}

	public Set<Permission> getPermissions() {
		return permissions;
	}
	
	public static String getSanitizedRoleName(String roleName) throws Throwable {
		if (!Stringizer.isEmpty(roleName)) {
			final String trimmed = roleName.trim();
			
			if (!trimmed.startsWith("ROLE_")) {
				return String.format("ROLE_%s", trimmed.replace(" ", "_"));
			} else {
				return trimmed;
			}
		}

		throw new Throwable("Role name cannot be empty!");
	}
}
