/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.uaam.services;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.appsweaver.apps.uaam.domain.Permission;
import org.appsweaver.apps.uaam.domain.Role;
import org.appsweaver.apps.uaam.domain.mapper.JsonFileRoleAndPermissionMapper;
import org.appsweaver.commons.utilities.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 
 * @author UD
 *
 */
@Component
public class RolesAndPermissionsService {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired RoleService roleService;
	@Autowired PermissionService permissionService;
	
	public void importData(String dataFileName) {
		try {
			final JsonFileRoleAndPermissionMapper jsonFileRoleAndPermissionMapper = new JsonFileRoleAndPermissionMapper(dataFileName);
			jsonFileRoleAndPermissionMapper.importData();
			
			final Map<String, Permission> permissions = Collections.newHashMap();
			jsonFileRoleAndPermissionMapper.getPermissions().forEach(permission -> {
				// Create/Retrieve permissions and cache
				final Permission created = permissionService.create(permission);
				logger.info("Permission created/updated {}", permission.getName());
				permissions.put(created.getName(), created);
			});
			
			jsonFileRoleAndPermissionMapper.getRoles().forEach(role -> {
				final Set<Permission> rolePermissions = Collections.newHashSet();
				
				// Update role permissions with references to persisted models - Begin
				role.getPermissions().forEach(permission -> {
					if(permissions.containsKey(permission.getName())) {
						rolePermissions.add(permissions.get(permission.getName()));
					}
				});
				
				if(role.getName().equals("ROLE_ADMINISTRATOR")) {
					rolePermissions.addAll(permissionService.findAll());
				}
				
				role.setPermissions(rolePermissions);
				// Update role permissions with references to persisted models - End
				
				// Create or update role
				try {
					final String roleName = role.getName();
					if(!roleService.existsByName(roleName)) {
						roleService.save(role);
						
						logger.info("Created role {}", roleName);
					} else {
						final Role existingRole = roleService.findByName(roleName);
						
						existingRole.setLabel(role.getLabel());
						existingRole.setPermissions(rolePermissions);
						roleService.save(existingRole);
						
						logger.info("Updated role {}", roleName);
					}
				} catch (Throwable e) {
					logger.warn("Error processing role [name={}, label={}]", role.getName(), role.getLabel());
				}
			});

		} catch (Throwable e) {
			logger.warn("Error loading/refreshing roles and permissions from file {}", dataFileName);
		}
	}

	public List<Role> findAllRoles() {
		return roleService.findAll();
	}

	public Role findRoleByName(String roleName) {
		return roleService.findByName(roleName);
	}
	
	public Role findRoleByLabel(String roleLabel) {
		return roleService.findByLabel(roleLabel);
	}
}