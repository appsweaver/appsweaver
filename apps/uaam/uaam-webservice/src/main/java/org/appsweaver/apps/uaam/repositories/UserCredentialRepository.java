/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.uaam.repositories;

import org.appsweaver.apps.uaam.domain.User;
import org.appsweaver.apps.uaam.domain.UserCredential;
import org.appsweaver.commons.jpa.repositories.CustomPrimaryRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author UD
 *
 */
@Repository
@Transactional
public interface UserCredentialRepository extends CustomPrimaryRepository<UserCredential> {
	UserCredential findByUser(User user);
}