/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.uaam.utilities;

/**
 * 
 * @author UD
 *
 */
public class AttributesHelper {
	public static String[] allAttributeNames() {
		return new String[] {
			"objectGUID",
			"objectSID",
			"distinguishedName",
			"sAMAccountName",
			"userPrincipalName",
			"mail",
			"givenName",
			"sn",
			"displayName",
			"cn",
			"primaryGroupID",
			"description",
			"uidNumber",
			"gidNumber",
			"whenCreated",
			"memberOf",
			"telephoneNumber",
			"sAMAccountType",
			"instanceType",
			"userAccountControl",
			"company",
			"co",
			"countryCode",
			"department",
			"division",
			"employeeID",
			"employeeNumber",
			"facsimileTelephoneNumber",
			"generationQualifier",
			"l",
			"managerDN",
			"managerUPN",
			"middleName",
			"mobile",
			"personalTitle",
			"physicalDeliveryOfficeName",
			"postalCode",
			"preferredLanguage",
			"st",
			"streetAddress",
			"title"
		};
	}
}