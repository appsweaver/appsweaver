/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.uaam.components;

import org.appsweaver.commons.models.properties.UAMSecurity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

/**
 * @author UD
 */
@Component
@Validated
@ConfigurationProperties(prefix = "webservice")
public class ApplicationProperties {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	private UAMSecurity security;

	public UAMSecurity getSecurity() {
		return security;
	}

	public void setSecurity(UAMSecurity security) {
		this.security = security;
	}
}