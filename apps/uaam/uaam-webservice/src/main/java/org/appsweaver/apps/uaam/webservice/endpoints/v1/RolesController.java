/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.uaam.webservice.endpoints.v1;

import org.appsweaver.commons.api.PrimaryDataService;
import org.appsweaver.commons.webservice.endpoints.AbstractPrimaryController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.appsweaver.apps.uaam.domain.Role;
import org.appsweaver.apps.uaam.services.RoleService;

import io.swagger.annotations.Api;

/**
 * 
 * @author UD
 *
 */
@RestController
@RequestMapping(value = "/api/v1/roles", produces = { MediaType.APPLICATION_JSON_VALUE })
@Api(value = "Roles Controller")
public class RolesController extends AbstractPrimaryController<Role> {
	@Autowired RoleService roleService;

	@Override
	public Class<Role> getEntityClass() {
		return Role.class;
	}

	@Override
	public PrimaryDataService<Role> getDataService() {
		return roleService;
	}
}