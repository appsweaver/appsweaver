/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.uaam.services;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.appsweaver.apps.uaam.components.ApplicationProperties;
import org.appsweaver.apps.uaam.domain.AuthenticationResult;
import org.appsweaver.apps.uaam.domain.Role;
import org.appsweaver.apps.uaam.domain.User;
import org.appsweaver.apps.uaam.domain.UserCredential;
import org.appsweaver.commons.models.security.AuthenticationProviderType;
import org.appsweaver.commons.models.security.BasicCredential;
import org.appsweaver.commons.spring.components.Cryptographer;
import org.appsweaver.commons.utilities.Stringizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

/**
 * 
 * The type Authentication service.
 *
 * @author UD
 * 
 */
@Service
public class AuthenticationService {
	/**
	 * The User service.
	 */
	@Autowired
	UserService userService;

	/**
	 * The Authentication manager.
	 */
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	ApplicationProperties applicationProperties;
	
	/**
	 * Attempt authentication authentication result.
	 *
	 * @param credential
	 *            the credential
	 * @return the authentication result
	 * @throws Throwable
	 *             the throwable
	 */
	public AuthenticationResult<UUID> attemptAuthentication(BasicCredential credential) throws Throwable {

		if (!credential.isGood()) {
			throw new InsufficientAuthenticationException("Insufficient credentials");
		}

		// Extract user name
		final String username = credential.getUsername().toLowerCase();
		
		final List<String> domainNames = applicationProperties.getSecurity().getDomainNames();
		
		// Attach default domain name if only user name is provided
		final List<String> sanitizedLoginIds = userService.getSanitizedLoginIds(username, domainNames);
		
		final Optional<User> optionalUser = userService.findByAny(
			sanitizedLoginIds, Optional.ofNullable(username), Optional.empty()
		);
		
		if (optionalUser.isPresent()) {
			final User user = optionalUser.get();
			
			if (user.isEnabled()) {
				final String password = Stringizer.decodeBase64(credential.getPassword());
	
				final Authentication request = new UsernamePasswordAuthenticationToken(user.getLoginId(), password);
				authenticationManager.authenticate(request);
	
				final String token = Cryptographer.createJwtsToken(user.getLoginId());
				final Role role = user.getRole();
	
				// @formatter:off
				return new AuthenticationResult<>(
					user.getId(),
					
					user.getLoginId(),
					user.getSecondaryLoginId(),
					user.getEmail(),
					
					user.getFirstName(),
					user.getLastName(),
					
					user.getTitle(),
					user.getDepartmentName(),
					
					user.isEnabled(),
					user.isLocked(),
					
					(role != null) ? role.getName() : null,

					token,
					
					user.getGrantedAuthorities()
				);
				// @formatter:on
			}
		}

		throw new BadCredentialsException("Authentication failed! [Possible reasons: bad credentials, invalid or disabled user]");
	}

	/**
	 * Check password boolean.
	 *
	 * @param oldRawPassword
	 *            the old raw password
	 * @param id
	 *            the id
	 * @return the boolean
	 */
	public boolean checkPassword(String oldRawPassword, UUID id) {
		final Optional<User> optionalUser = userService.findById(id);

		if (optionalUser.isPresent() && optionalUser.get().getAuthenticationProvider() == AuthenticationProviderType.LOCAL_REPOSITORY) {
			final User user = optionalUser.get();

			final UserCredential userCredential = userService.getCredential(user);
			final String oldPassword = userCredential.getPassword();
			final String decryptedOldPassword = Cryptographer.decrypt(oldPassword);

			return oldRawPassword.equals(decryptedOldPassword);
		}

		return false;
	}
}
