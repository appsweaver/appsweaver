/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.uaam.services;

import org.appsweaver.apps.uaam.domain.Permission;
import org.appsweaver.apps.uaam.repositories.PermissionRepository;
import org.appsweaver.commons.jpa.repositories.CustomPrimaryRepository;
import org.appsweaver.commons.services.AbstractDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 * @author UD
 *
 */
@Service
public class PermissionService extends AbstractDataService<Permission> {
	@Autowired PermissionRepository permissionRepository;

	@Override
	public CustomPrimaryRepository<Permission> getRepository() {
		return permissionRepository;
	}

	public Permission create(String name) {
		final Permission permission = new Permission(name);
		return create(permission);
	}
	
	public Permission create(Permission permission) {
		if(!permissionRepository.existsByName(permission.getName())) {
			final Permission saved = permissionRepository.save(permission);
			return saved;
		} else {
			return permissionRepository.findByName(permission.getName());
		}
	}

	public boolean existsByName(String permission) {
		return permissionRepository.existsByName(permission);
	}
	
	public Permission findByName(String permission) {
		return permissionRepository.findByName(permission);
	}
}