/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.uaam.domain;

import java.util.Collection;

/**
 * 
 * @author UD
 *
 */
public class AuthenticationResult<T> {
	final T id;

	final String loginId;
	final String secondaryLoginId;
	final String email;
	final String firstName;
	final String lastName;
	final String title;
	final String department;

	final boolean enabled;
	final boolean locked;

	final String token;

	final String role;
	final Collection<String> grantAuthorities;

	public AuthenticationResult(
		T id, 
		String loginId, String secondaryLoginId, String email, 
		String firstName, String lastName, 
		String title, String department,
		boolean enabled, boolean locked,
		String role,
		String token,
		Collection<String> grantAuthorities
	) {
		this.id = id;

		this.loginId = loginId;
		this.secondaryLoginId = secondaryLoginId;
		this.email = email;
		
		this.firstName = firstName;
		this.lastName = lastName;
		
		this.title = title;
		this.department = department;
		
		this.enabled = enabled;
		this.locked = locked;

		this.role = role;

		this.token = token;

		this.grantAuthorities = grantAuthorities;
	}

	public String getToken() {
		return token;
	}

	public T getId() {
		return id;
	}

	public String getLoginId() {
		return loginId;
	}

	public String getEmail() {
		return email;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public boolean isLocked() {
		return locked;
	}

	public String getRole() {
		return role;
	}

	public String getSecondaryLoginId() {
		return secondaryLoginId;
	}

	public String getTitle() {
		return title;
	}

	public String getDepartment() {
		return department;
	}

	public Collection<String> getGrantAuthorities() {
		return grantAuthorities;
	}
}