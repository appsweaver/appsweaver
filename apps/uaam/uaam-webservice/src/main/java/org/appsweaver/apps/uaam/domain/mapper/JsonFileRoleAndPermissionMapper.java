/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.uaam.domain.mapper;

import java.io.File;
import java.util.List;
import java.util.Set;

import org.appsweaver.apps.uaam.domain.Permission;
import org.appsweaver.apps.uaam.domain.Role;
import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Fileinator;
import org.appsweaver.commons.utilities.Jsonifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author UD
 *
 */
public class JsonFileRoleAndPermissionMapper {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	final File file;

	final Set<Role> roles = Collections.newHashSet();
	final Set<Permission> permissions = Collections.newHashSet();
	
	public JsonFileRoleAndPermissionMapper(String dataFileName) {
		this.file = new File(dataFileName);
	}

	public void importData() throws Throwable {
		final List<Role> items = Jsonifier.toList(Fileinator.toString(file), Role.class);

		this.roles.clear();
		this.permissions.clear();
		
		items.forEach(item -> {
			try {
				// Role name must be sanitized - Call Setter Explicitly - JSON to POJO does not see to call setter methods
				final Role role = new Role(item.getName(), item.getLabel());
				this.roles.add(role);
				
				final Set<Permission> permissions = item.getPermissions();

				permissions.forEach(permission -> {
					if(!this.permissions.contains(permission)) {
						this.permissions.add(permission);
					}
				});
			} catch(Throwable t) {
				logger.warn("Error processing role {}", item.getName(), t);
			}
		});
	}

	public Set<Role> getRoles() {
		return roles;
	}
	
	public Set<Permission> getPermissions() {
		return permissions;
	}
}