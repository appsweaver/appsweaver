/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.uaam.webservice.filters;

import static org.appsweaver.commons.SystemConstants.AUTHORIZATION;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.appsweaver.apps.uaam.domain.User;
import org.appsweaver.apps.uaam.repositories.UserRepository;
import org.appsweaver.commons.SystemConstants;
import org.appsweaver.commons.models.exceptions.AuthenticationTokenExpiredException;
import org.appsweaver.commons.spring.components.Cryptographer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

/**
 * 
 * @author UD
 *
 */
public class JWTAuthorizationFilter extends BasicAuthenticationFilter {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	final UserRepository userRepository;

	public JWTAuthorizationFilter(AuthenticationManager authenticationManager, UserRepository userRepository) {
		super(authenticationManager);

		this.userRepository = userRepository;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
		final String token = request.getHeader(AUTHORIZATION);
		if (token == null || !token.startsWith(SystemConstants.TOKEN_PREFIX)) {
			logger.trace("Authorization header does not have JWT token");

			chain.doFilter(request, response);
			return;
		}

		if (token != null) {
			try {
				final UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = getUsernamePasswordAuthenticationToken(request, response);
				if(usernamePasswordAuthenticationToken != null) {
					SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
					
					chain.doFilter(request, response);
					return;
				}
			} catch(Throwable t) {
				logger.debug("Error occurred during authorization", t);
			}
		}

		response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "User is unauthorized for making requested operation");
	}
	
	private UsernamePasswordAuthenticationToken getUsernamePasswordAuthenticationToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
		final String token = request.getHeader(AUTHORIZATION);
		if (token == null || !token.startsWith(SystemConstants.TOKEN_PREFIX)) {
			logger.debug("Authorization header does not have JWT token");
			return null;
		}

		if (token != null) {
			try {
				final String username = getUsername(token);
				if (username != null && userRepository.existsByLoginId(username)) {
						final User user = userRepository.findByLoginId(username).get();
						final UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = newUsernamePasswordAuthenticationToken(user);
						return usernamePasswordAuthenticationToken;
				} else {
					logger.debug("Unable to find user [username={}]", username);
				}
			} catch(AuthenticationTokenExpiredException e) {
				logger.debug("JWT token expired", e);
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Session expired");
			} catch(Throwable t) {
				logger.debug("Error providing authentication token", t);
			}
		}

		logger.trace("Authorization token was not available");
		return null;
	}
	
	UsernamePasswordAuthenticationToken newUsernamePasswordAuthenticationToken(User user) {
		final String loginId = user.getLoginId();

		logger.debug("Making token [loginId={}, id={}]", loginId, user.getId());

		final UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(loginId, null, user.toGrantedAuthorities());
		// Push user into security context
		usernamePasswordAuthenticationToken.setDetails(user.toSecuredUser());
		
		return usernamePasswordAuthenticationToken;
	}

	String getUsername(String token) throws AuthenticationTokenExpiredException {
		final String subject = Cryptographer.getSubjectFromJwtsToken(token);
		logger.debug("Obtained username from token [username={}]", subject);
		return subject;
	}
}