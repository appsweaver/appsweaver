/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.uaam.config;

import static org.appsweaver.commons.SystemConstants.API_VERSIONED_PATH_WILD;
import static org.appsweaver.commons.SystemConstants.AUTHENTICATION_ENDPOINT;
import static org.appsweaver.commons.SystemConstants.STOMP_WHITELISTED_ENDPOINTS;

import org.appsweaver.apps.uaam.repositories.UserRepository;
import org.appsweaver.apps.uaam.webservice.filters.JWTAuthorizationFilter;
import org.appsweaver.commons.models.properties.ActiveDirectory;
import org.appsweaver.commons.models.properties.Security;
import org.appsweaver.commons.models.properties.attributes.AuthenticatonProviderDetails;
import org.appsweaver.commons.models.security.AuthenticationProviderType;
import org.appsweaver.commons.spring.components.Cryptographer;
import org.appsweaver.commons.spring.components.WebSecurityHelper;
import org.appsweaver.commons.spring.components.WebServiceProperties;
import org.appsweaver.commons.spring.config.WebServiceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * 
 * @author UD
 *
 */
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Configuration
public class MultipleAuthenticationProvidersSecurityConfig extends WebSecurityConfigurerAdapter {
	/**
	 * The Logger.
	 */
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private WebServiceConfig webServiceConfig;

	@Autowired
	private WebServiceProperties webServiceProperties;

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private WebSecurityHelper webSecurityHelper;

	@Bean(BeanIds.AUTHENTICATION_MANAGER)
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		final AuthenticationManager authenticationManager = super.authenticationManagerBean();
		return authenticationManager;
	}

	/**
	 * Local password encoder password encoder.
	 *
	 * @return the password encoder
	 */
	@Bean(name = "localPasswordEncoder")
	public PasswordEncoder localPasswordEncoder() {
		return new PasswordEncoder() {
			@Override
			public boolean matches(CharSequence rawPassword, String encodedPassword) {
				return Cryptographer.checkPassword(rawPassword.toString(), encodedPassword);
			}

			@Override
			public String encode(CharSequence rawPassword) {
				return Cryptographer.encryptPassword(rawPassword.toString());
			}
		};
	}

	/**
	 * Config authentication.
	 *
	 * @param authenticationManagerBuilder
	 *            the auth
	 * @throws Exception
	 *             the exception
	 */
	@Autowired
	public void configAuthentication(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
		AuthenticatonProviderDetails authenticatonProviderDetails = null;
		
		authenticatonProviderDetails = webServiceProperties.getAuthenticationProviderDetails(AuthenticationProviderType.ACTIVE_DIRECTORY);
		if(authenticatonProviderDetails.isEnabled()) {
			// LDAP Authentication -- Order #1
			configureForLDAP(authenticationManagerBuilder);
		}

		authenticatonProviderDetails = webServiceProperties.getAuthenticationProviderDetails(AuthenticationProviderType.LOCAL_REPOSITORY);
		if(authenticatonProviderDetails.isEnabled()) {
			// Local Repository Authentication -- Order #2
			configureForUserRepository(authenticationManagerBuilder);
		}
	}
	
	@Override
	public void configure(WebSecurity webSecurity) throws Exception {
		// Allow unauthenticated access to swagger and spring management console
		// @formatter:off
		webSecurity.ignoring()
			.antMatchers(HttpMethod.OPTIONS, webServiceConfig.getWildRootPath())
			.antMatchers(webServiceConfig.getActuatorPath())
			.antMatchers(webServiceConfig.getSwaggerIndexPagePath())
			.antMatchers(webServiceConfig.getSwaggerApiDocsPath())
			.antMatchers(webServiceConfig.getSwaggerResourcesPath())
			.antMatchers(webServiceConfig.getSwaggerUIConfigPath())
			.antMatchers(webServiceConfig.getSwaggerSecurityConfigPath())
			.antMatchers(webServiceConfig.getSwaggerWebJarsPath());
		// @formatter:on
	}

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.
			exceptionHandling()
			.and()
			.anonymous()
			.and()
			.servletApi()
			.and()
			.headers()
			.cacheControl();
		
		final ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry 
			expressionInterceptUrlRegistry = httpSecurity.cors().and().csrf().disable().authorizeRequests();
		
		final Security securityProperties = webServiceProperties.getSecurity();
		webSecurityHelper.addSecuredRoutes(expressionInterceptUrlRegistry, securityProperties.getSecuredResources());
		
		// Allow unauthenticated access to swagger and spring management console
		expressionInterceptUrlRegistry
			.antMatchers(HttpMethod.OPTIONS, webServiceConfig.getWildRootPath()).permitAll()
			.antMatchers(webServiceConfig.getActuatorPath()).permitAll()
			.antMatchers(webServiceConfig.getSwaggerIndexPagePath()).permitAll()
			.antMatchers(webServiceConfig.getSwaggerApiDocsPath()).permitAll()
			.antMatchers(webServiceConfig.getSwaggerResourcesPath()).permitAll()
			.antMatchers(webServiceConfig.getSwaggerUIConfigPath()).permitAll()
			.antMatchers(webServiceConfig.getSwaggerSecurityConfigPath()).permitAll()
			.antMatchers(webServiceConfig.getSwaggerWebJarsPath()).permitAll()
		
			// Permit access to pre-flight requests for all
			.antMatchers(HttpMethod.OPTIONS, API_VERSIONED_PATH_WILD).permitAll()
			
			// Per access to authentication end-point for all
			.antMatchers(HttpMethod.POST, AUTHENTICATION_ENDPOINT).permitAll()

			// Notifications web socket end-point
			.antMatchers(STOMP_WHITELISTED_ENDPOINTS).permitAll()

			// Authenticate rest of the end-points (Secured)
			.antMatchers(API_VERSIONED_PATH_WILD).authenticated()
			.and()
			.addFilter(new JWTAuthorizationFilter(authenticationManager(), userRepository)).sessionManagement()
			.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}
	
	void configureForUserRepository(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
		authenticationManagerBuilder.userDetailsService(userDetailsService).passwordEncoder(localPasswordEncoder());
	}
	
	void configureForLDAP(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
		final Security securityProperties = webServiceProperties.getSecurity();
		final ActiveDirectory activeDirectoryProperties = securityProperties.getActiveDirectory();
		
		final String userSearchBase = String.format("%s,%s", activeDirectoryProperties.getUserSearchBase(), activeDirectoryProperties.getBase());
		final String userSearchFilter = "userPrincipalName={0}";

		authenticationManagerBuilder.ldapAuthentication().contextSource()
			.url(activeDirectoryProperties.getUrl()).managerDn(activeDirectoryProperties.getManagerDn())
			.managerPassword(activeDirectoryProperties.getManagerPassword()).and().userSearchFilter(userSearchFilter)
			.userSearchBase(userSearchBase);
	}
}