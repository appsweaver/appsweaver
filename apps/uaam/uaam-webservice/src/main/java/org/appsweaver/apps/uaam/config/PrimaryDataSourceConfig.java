/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.uaam.config;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

/**
 * 
 * @author UD
 *
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
	basePackages = { 
		"org.appsweaver.commons.jpa.repositories",
		"org.appsweaver.apps.uaam.repositories"
	},
	
	entityManagerFactoryRef = "entityManagerFactory", 
	transactionManagerRef = "transactionManagerFactory"
)
@ConditionalOnProperty(
	prefix = "webservice.repository",
	name = "enabled",
	havingValue = "true",
	matchIfMissing = true
)
public class PrimaryDataSourceConfig {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Bean
	@Primary
	@ConfigurationProperties("spring.datasource")
	public DataSourceProperties dataSourceProperties() {
		return new DataSourceProperties();
	}
	
	@Bean
	@Primary
	@ConfigurationProperties(prefix = "spring.jpa")
	public JpaProperties jpaProperties() {
		return new JpaProperties();
	}
	
	@Bean
	@Primary
	@ConfigurationProperties("spring.datasource")
	public HikariDataSource dataSource(DataSourceProperties dataSourceProperties) {
		logger.info("Datasource properties: [driverClassName={}, type={}]", 
			dataSourceProperties.getDriverClassName(), 
			dataSourceProperties.getType()
		);

		final HikariDataSource dataSource = (HikariDataSource) dataSourceProperties
			.initializeDataSourceBuilder()
			.type(dataSourceProperties.getType()).build();
		
		return dataSource;
	}
	
	@Bean(name = "transactionManagerFactory")
	@Primary
	public PlatformTransactionManager transactionManagerFactory() {
		final JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
		return transactionManager;
	}
	
	@Bean(name = "entityManagerFactory")
	@Primary
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		final LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
		
		final HikariDataSource dataSource = dataSource(dataSourceProperties());
		entityManagerFactoryBean.setDataSource(dataSource);
		entityManagerFactoryBean.setPackagesToScan(
			"org.appsweaver.commons.jpa.models", 
			"org.appsweaver.apps.uaam.domain"
		);
		entityManagerFactoryBean.setPersistenceUnitName("defaultPersistenceUnit");

		final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		entityManagerFactoryBean.setJpaVendorAdapter(vendorAdapter);
		
		final Map<String, String> jpaProperties = jpaProperties().getProperties();
		entityManagerFactoryBean.setJpaPropertyMap(jpaProperties);

		return entityManagerFactoryBean;
	}
}
