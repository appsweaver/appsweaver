/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.uaam.services;

import java.util.Optional;

import org.appsweaver.commons.models.security.AuthenticationProviderType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.appsweaver.apps.uaam.domain.User;
import org.appsweaver.apps.uaam.repositories.UserCredentialRepository;
import org.appsweaver.apps.uaam.repositories.UserRepository;
import org.appsweaver.apps.uaam.utilities.RepositoryBackedUserDetails;

/**
 * 
 * The type Repository backed user details service.
 *
 * @author UD
 * 
 */
@Service
public class RepositoryBackedUserDetailsService implements UserDetailsService {
	/**
	 * The User credential repository.
	 */
	@Autowired UserCredentialRepository userCredentialRepository;

	/**
	 * The User repository.
	 */
	@Autowired UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		final Optional<User> user = userRepository.findByLoginIdAndAuthenticationProvider(username, AuthenticationProviderType.LOCAL_REPOSITORY);

		if (!user.isPresent()) {
			throw new UsernameNotFoundException(username);
		}

		return new RepositoryBackedUserDetails(userCredentialRepository, user);
	}
}