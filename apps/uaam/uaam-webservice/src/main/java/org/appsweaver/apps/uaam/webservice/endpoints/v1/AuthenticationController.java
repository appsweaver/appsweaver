/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.uaam.webservice.endpoints.v1;

import java.util.UUID;

import org.appsweaver.commons.models.GenericErrorCode;
import org.appsweaver.commons.models.security.BasicCredential;
import org.appsweaver.commons.models.web.WebResponse;
import org.appsweaver.commons.models.web.exceptions.ApplicationException;
import org.appsweaver.commons.webservice.endpoints.DefaultSimpleController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.appsweaver.apps.uaam.domain.AuthenticationResult;
import org.appsweaver.apps.uaam.services.AuthenticationService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author UD
 *
 */
@RestController
@RequestMapping(value = "/api/v1/authenticate", produces = { MediaType.APPLICATION_JSON_VALUE })
@Api(value = "Authentication Controller")
public class AuthenticationController extends DefaultSimpleController {
	/**
	 * The Authentication service.
	 */
	@Autowired
	AuthenticationService authenticationService;

	/**
	 * Authenticate web response.
	 *
	 * @param credential
	 *            the credential
	 * @return the web response
	 */
	@PostMapping(consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ApiOperation(value = "Authenticate")
	public WebResponse<AuthenticationResult<UUID>> authenticate(@RequestBody BasicCredential credential) {
		try {
			final AuthenticationResult<UUID> authenticationResult = authenticationService.attemptAuthentication(credential);
			return WebResponse.newWebResponse(authenticationResult);
		} catch (Throwable t) {
			getLogger().error("Authentication Error!", t);
			throw new ApplicationException(controllerName(), HttpStatus.UNAUTHORIZED, GenericErrorCode.UNKNOWN_OR_INVALID_USER,
					t.getMessage());
		}
	}
}