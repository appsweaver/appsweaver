/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.uaam.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.appsweaver.commons.annotations.IdGenerationPolicy;
import org.appsweaver.commons.jpa.models.BasePermission;
import org.appsweaver.commons.utilities.Stringizer;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.search.annotations.Indexed;

import lombok.EqualsAndHashCode;

/**
 * 
 * @author UD
 *
 */
@Entity
@Table(name = "PERMISSIONS")
@Indexed(index = "PERMISSIONS")
@DynamicUpdate
@IdGenerationPolicy("name")
@EqualsAndHashCode(callSuper = true)
public class Permission extends BasePermission {
	private static final long serialVersionUID = -8700673014334767200L;

	public Permission() {
		super();
	}
	
	public Permission(String name) {
		super(name);
	}
	
	public String getDisplayName() {
		if(name != null) {
			final String dehypenated = Stringizer.dehyphenate(name);
			final String displayName = Stringizer.capitalize(dehypenated);
			return displayName;
		}
		
		return name;
	}
}