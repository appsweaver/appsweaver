/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.uaam.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.appsweaver.apps.uaam.domain.converters.json.RoleConverter;
import org.appsweaver.apps.uaam.domain.types.AuthenticationProviderTypeFieldBridge;
import org.appsweaver.commons.HibernateSearch;
import org.appsweaver.commons.annotations.Filterable;
import org.appsweaver.commons.annotations.Filterables;
import org.appsweaver.commons.annotations.IdGenerationPolicy;
import org.appsweaver.commons.annotations.Searchable;
import org.appsweaver.commons.jpa.models.BaseUser;
import org.appsweaver.commons.models.security.AuthenticationProviderType;
import org.appsweaver.commons.models.security.SecuredUser;
import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Stringizer;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.NaturalId;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Facet;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.FieldBridge;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.hibernate.search.annotations.SortableField;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.EqualsAndHashCode;

/**
 * 
 * @author UD
 *
 */
@Entity
@Table(name = "USERS")
@Indexed(index = "USERS")
@DynamicUpdate
@IdGenerationPolicy("email")
@EqualsAndHashCode(callSuper = true)
public class User extends BaseUser {
	private static final long serialVersionUID = -2758890234182477742L;

	// Facet Names
	private static final String USER_ROLE_FACET = "user_role_facet";
	private static final String USER_DEPARTMENT_NAME_FACET = "user_department_name_facet";
	private static final String USER_AUTHENTICATION_PROVIDER_FACET = "user_authentication_provider_facet";
	// Facet Names
	
	@JsonSerialize(converter = RoleConverter.class)
	@Filterables( value = {
		@Filterable(name = "roleName", path = "role.name"),
		@Filterable(name = "roleLabel", path = "role.label")
	})
	@Searchable
	@IndexedEmbedded(includePaths = { "name", "label" })
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ROLE_ID")
	protected Role role;

	@NaturalId(mutable = true)
	@Field
	@SortableField
	@Searchable
	@Column(name = "LOGIN_ID", length=255, nullable = false)
	protected String loginId;

	@Field(analyzer = @Analyzer(definition = HibernateSearch.KEYWORD_ANALYZER), bridge = @FieldBridge(impl = AuthenticationProviderTypeFieldBridge.class))
	@SortableField
	@Searchable
	@Filterable
	@Column(name = "AUTHENTICATION_PROVIDER", length = 100)
	@Enumerated(EnumType.STRING)
	protected AuthenticationProviderType authenticationProvider = AuthenticationProviderType.UNKNOWN;

	@SortableField
	@Searchable
	@Field
	@Column(name = "SECONDARY_LOGIN_ID", length = 128)
	protected String secondaryLoginId;
	
	@SortableField
	@Searchable
	@Field
	@Facet(forField = USER_DEPARTMENT_NAME_FACET)
	@Field(name = USER_DEPARTMENT_NAME_FACET, analyze = Analyze.NO)
	@Column(name = "DEPARTMENT_NAME", length = 255)
	protected String departmentName;
	
	@Searchable
	@Field
	@Column(name = "NOTES", length = 4096)
	protected String notes;
	
	// Hibernate Search Facet Helpers - BEGIN
	@JsonIgnore
	@Searchable(lookup = false)
	@Facet(forField = USER_ROLE_FACET)
	@Field(name = USER_ROLE_FACET, analyze = Analyze.NO)
	public String getRoleNameFacetValue() {
		return (role != null) ? role.getLabel() : null;
	}
	
	@JsonIgnore
	@Searchable(lookup = false)
	@Facet(forField = USER_AUTHENTICATION_PROVIDER_FACET)
	@Field(name = USER_AUTHENTICATION_PROVIDER_FACET, analyze = Analyze.NO)
	public String getAuthenticationProviderNameFacetValue() {
		return (authenticationProvider != null) ? String.valueOf(authenticationProvider) : null;
	}
	// Hibernate Search Facet Helpers - END
	
	public User() {
		super();
	}

	public User(String loginId, String email, String firstName, String lastName, Role role, String title, String mobileNumber, String departmentName) {
		super(email, firstName, lastName, title, mobileNumber);

		setLoginId(loginId);
		setRole(role);
		setDepartmentName(departmentName);
	}

	public List<String> getGrantedAuthorities() {
		final List<String> grantedAuthorities = Collections.newList();

		if (role.getPermissions() != null) {
			role.getPermissions().forEach(permission -> {
				grantedAuthorities.add(permission.getName());
			});
		}

		return grantedAuthorities;
	}

	public List<GrantedAuthority> toGrantedAuthorities() {
		final List<GrantedAuthority> grantedAuthorities = Collections.newList();
		// grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));

		getGrantedAuthorities().forEach(authority -> {
			grantedAuthorities.add(new SimpleGrantedAuthority(authority));
		});

		return grantedAuthorities;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
	
	public String getSecondaryLoginId() {
		return secondaryLoginId;
	}

	public void setSecondaryLoginId(String secondaryLoginId) {
		// secondaryLoginId is sanitized to lower cased
		if(!Stringizer.isEmpty(secondaryLoginId)) {
			this.secondaryLoginId = secondaryLoginId.toLowerCase();
		} else {
			secondaryLoginId = null;
		}
	}

	public AuthenticationProviderType getAuthenticationProvider() {
		return authenticationProvider;
	}

	public void setAuthenticationProvider(AuthenticationProviderType authenticationSource) {
		this.authenticationProvider = authenticationSource;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public SecuredUser toSecuredUser() {
		final SecuredUser securedUser = new SecuredUser();

		securedUser.setId(id);

		securedUser.setLoginId(loginId);
		securedUser.setEmail(email);
		securedUser.setFirstName(firstName);
		securedUser.setLastName(lastName);

		securedUser.setTitle(title);
		securedUser.setMobileNumber(mobileNumber);

		securedUser.setEnabled(enabled);
		securedUser.setLocked(locked);

		securedUser.setRole((role != null) ? role.getName() : null);
		securedUser.setGrantedAuthorities(getGrantedAuthorities());

		return securedUser;
	}
}
