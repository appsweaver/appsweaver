/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.uaam.domain.mapper;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.appsweaver.apps.uaam.domain.Role;
import org.appsweaver.apps.uaam.domain.User;
import org.appsweaver.commons.models.security.AuthenticationProviderType;
import org.appsweaver.commons.utilities.Collections;
import org.appsweaver.commons.utilities.Fileinator;
import org.appsweaver.commons.utilities.Jsonifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author UD
 *
 */
public class JsonFileUserMapper {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	final File file;

	final List<User> users = Collections.newList();
	final Map<String, Role> roles;
	final Role defaultRole;
	
	public JsonFileUserMapper(String dataFileName, Map<String, Role> roles, Role defaultRole) {
		this.file = new File(dataFileName);
		this.roles = roles;
		this.defaultRole = defaultRole;
	}

	public void importData() throws Throwable {
		final List<User> users = Jsonifier.toList(Fileinator.toString(file), User.class);

		// Map user source to LOCAL_USER_REPOSITORY
		users.forEach(user -> {
			user.setAuthenticationProvider(AuthenticationProviderType.LOCAL_REPOSITORY);

			try {
				// Find role based on role name (available in JSON file)
				final String sanitizedRoleName = Role.getSanitizedRoleName(user.getRole().getName());
				final Role role = roles.get(sanitizedRoleName);

				// When a role does not exist, map to default role
				user.setRole((role != null) ? role : defaultRole);
			} catch(Throwable t) {
				logger.warn("Error processing user [loginId={}, email={}]", user.getLoginId(), user.getEmail(), t);
			}
		});

		this.users.clear();
		this.users.addAll(users);
	}

	public List<User> getUsers() {
		return users;
	}
}