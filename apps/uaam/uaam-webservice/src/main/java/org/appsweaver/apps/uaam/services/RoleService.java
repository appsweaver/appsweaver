/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.uaam.services;

import java.util.List;
import java.util.Set;

import org.appsweaver.commons.jpa.repositories.CustomPrimaryRepository;
import org.appsweaver.commons.services.AbstractDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.appsweaver.apps.uaam.domain.Permission;
import org.appsweaver.apps.uaam.domain.Role;
import org.appsweaver.apps.uaam.repositories.RoleRepository;

/**
 * 
 * @author UD
 *
 */
@Service
public class RoleService extends AbstractDataService<Role> {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired RoleRepository roleRepository;
	
	@Override
	public CustomPrimaryRepository<Role> getRepository() {
		return roleRepository;
	}
	
	public boolean existsByName(String roleName) {
		return roleRepository.existsByName(roleName);
	}
	
	public Role findByName(String roleName) {
		return roleRepository.findByName(roleName);
	}
	
	public Role findByLabel(String roleLabel) {
		return roleRepository.findByLabel(roleLabel);
	}
	
	public List<Role> findAll() {
		return roleRepository.findAll();
	}
	
	public Role create(String name, String label, Set<Permission> permissions) throws Throwable {
		final Role role = new Role(name, label);
		role.setPermissions(permissions);

		roleRepository.save(role);
		return role;
	}
}