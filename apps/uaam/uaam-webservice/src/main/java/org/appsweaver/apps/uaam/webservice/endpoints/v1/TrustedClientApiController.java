/*
 * 
 * Copyright (c) 2016 appsweaver.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.appsweaver.apps.uaam.webservice.endpoints.v1;

import java.util.Optional;
import java.util.UUID;

import org.appsweaver.apps.uaam.domain.User;
import org.appsweaver.apps.uaam.repositories.UserRepository;
import org.appsweaver.commons.models.security.SecuredUser;
import org.appsweaver.commons.models.web.WebResponse;
import org.appsweaver.commons.utilities.Randomizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;

/**
 * 
 * @author UD
 *
 */
@RestController
@RequestMapping(value = "/tc-api/v1/users", produces = { MediaType.APPLICATION_JSON_VALUE })
@Api(value = "Trusted Client User Controller")
public class TrustedClientApiController {
	final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private UserRepository userRepository;

	protected Logger getLogger() {
		return logger;
	}

	protected Class<User> getEntityClass() {
		return User.class;
	}

	protected UserRepository getRepository() {
		return userRepository;
	}

	protected String name() {
		return "users";
	}

	/**
	 * Get user for given id
	 *
	 * @param value
	 * @param type
	 * @return
	 */
	@GetMapping
	public WebResponse<SecuredUser> getSecuredUser(@RequestParam("value") String value) {
		final String message = String.format("Get user by id: [value=%s]", value);
		try {
			getLogger().info("START: {}", message);

			UUID id = null;
			if (Randomizer.isUUID(value)) {
				id = Randomizer.uuid(value);
			}
			
			final Optional<User> optionalUser = userRepository.findByAny(
				value,
				Optional.ofNullable(value), 
				Optional.ofNullable(value),
				Optional.ofNullable(id)
			);
			if (optionalUser.isPresent()) {
				final User user = optionalUser.get();
				return WebResponse.newWebResponse(user.toSecuredUser());
			} else {
				throw new UsernameNotFoundException(String.format("Failed to find user [value=%s] does not exist!", value));
			}
		} catch (Throwable t) {
			getLogger().error("Error fetching user [value={}]", value, t);
			throw t;
		} finally {
			getLogger().info("END: {}", message);
		}
	}
}