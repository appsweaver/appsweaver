# See http://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html
# See https://docs.spring.io/spring-boot/docs/current/reference/html/production-ready-endpoints.html for actuator endpoints

# Application specific configuration
webservice:
  enabled: true
  
  project-name: apm
  base-package-name: org.appsweaver.apps
  
  server-env-name: ${APP_SERVER_MODE:development}
  server-name: ${APP_SERVER_NAME:${HOSTNAME}}
  server-port: ${APP_SERVER_PORT:8861}

  build-information:
    version: ${APP_VERSION:BETA}
    number: ${APP_BUILD_NUMBER:vBETA}
    date: ${APP_BUILD_DATE:${random.long(1576859108,1576938327)}}
  
  websocket:
    messaging:
      enabled: ${APP_WEBSOCKET_MESSAGING_ENABLED:false}
      
      user:
        email: ${NOTIFIER_EMAIL:appsweaver-team@googlegroups.com}
        name: ${NOTIFIER_NAME:Appsweaver Team}
        
  eureka-server-name: ${EUREKA_SERVER_NAME:${HOSTNAME}}
  eureka-server-port: ${EUREKA_SERVER_PORT:8761}
  eureka-server-username: ${EUREKA_USERNAME:admin}
  eureka-server-password: ${EUREKA_PASSWORD:admin}
  eureka-enabled: ${EUREKA_ENABLED:true}

spring:
  application:
    name: ${webservice.project-name}

  main:
    banner-mode: console
    
  security:
    enabled: true
    
    user:
      name: ${APM_USERNAME:admin}
      password: ${APM_PASSWORD:admin}
      
  mail:
    host: ${SMTP_MAIL_SERVER}
  
  boot:
    admin:
      ui:
        title: Application Performance Monitoring
        login-icon: <img src="assets/img/icon-spring-boot-admin.svg"><span>Application Performance Monitoring</span>
        brand: <img src="assets/img/icon-spring-boot-admin.svg"><span>Application Performance Monitoring</span>
        
      notify:
        mail:
          enabled: ${APM_EMAIL_NOTIFICATIONS_ENABLED:false}
          ignore-changes: "UNKNOWN:UP"
          from: ${NOTIFIER_EMAIL:appsweaver-team@googlegroups.com}
          to: ${DEVOPS_EMAIL:appsweaver-team@mailinator.com}
          
      context-path: /admin
      discovery:
        enabled: true
        ignored-services: ${spring.application.name}
      
eureka:
  discovery:
    secured: ${APP_SECURED:true}
    
  instance:
    lease-renewal-interval-in-seconds: 10
    hostname: ${webservice.server-name}
    secure-port: ${webservice.server-port}
    secure-port-enabled: ${APP_SECURED:true}
    non-secure-port-enabled: false
    
    metadata-map:
      user.name: ${webservice.eureka-server-username}
      user.password: ${webservice.eureka-server-password}
    
  client:
    registry-fetch-interval-seconds: 5
    register-with-eureka: ${webservice.eureka-enabled}
    fetch-registry: true
    
    service-url:
      defaultZone: https://${webservice.eureka-server-username}:${webservice.eureka-server-password}@${webservice.eureka-server-name}:${webservice.eureka-server-port}/eureka/

management:
  endpoints:
    web:
      exposure:
        include: "*"
  endpoint:
    health:
      show-details: ALWAYS
      
server:
  port: ${webservice.server-port}
  
  compression:
    enabled: true
    mime-types:
      - application/json
      - application/xml
      - text/html
      - text/xml
      - text/plain
      - application/javascript
      - text/css
  
  ssl:
    enabled: ${APP_SECURED:true}
    
    protocol: TLS
    enabled-protocols:
      - TLSv1.2
      - TLSv1.1
    ciphers:
      - TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256
      - TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256
      - TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA256
      - TLS_ECDH_RSA_WITH_AES_128_CBC_SHA256
      - TLS_DHE_DSS_WITH_AES_128_CBC_SHA256
      - TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA
      - TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA
      - TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA
      - TLS_ECDH_RSA_WITH_AES_128_CBC_SHA
      - TLS_DHE_DSS_WITH_AES_128_CBC_SHA
      - TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256
      - TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256
      - TLS_RSA_WITH_AES_128_GCM_SHA256
      - TLS_ECDH_ECDSA_WITH_AES_128_GCM_SHA256
      - TLS_ECDH_RSA_WITH_AES_128_GCM_SHA256
      - TLS_DHE_DSS_WITH_AES_128_GCM_SHA256

    key-alias: ${JKS_KEY_ALIAS}
    key-store-type: JKS
    key-store: ${JKS_KEYSTORE_FILE}
    key-store-password: ${JKS_KEYSTORE_PASSWORD}

    trust-store-type: JKS
    trust-store: ${JKS_TRUSTSTORE_FILE}
    trust-store-password: ${JKS_TRUSTSTORE_PASSWORD}

info:
  app:
    name: ${spring.application.name}
    description: ${spring.application.name}
    version: ${webservice.build-information.version}

logging: 
  level:
    root: info
 