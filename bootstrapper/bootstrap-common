##############################################################################
##
##
## Copyright (c) 2016 appsweaver.org
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
## @author UD
##
## ---------------------------------------------------------------------------
## bootstrap-common
##
## Add customizations which are platform independent to this file
##############################################################################

supportedActions="install,reinstall,update"

function isCommentLine() {
   if [[ "${*}" == "#"* ]] ; then
      return 0
   else
      return 1
   fi
}

function contains() {
   if [[ "${1}" == *"${2}"* ]] ; then
      return 1
   else
      return 0
   fi
}

function extractText() {
   local text=${1}
   local delimiter=${2}
   local position=${3}

   local result=$(echo ${text} | awk -v position="${position}" -F"${delimiter}" '{print $position}')
   if [ "${result}" != "|" ] ; then
      echo ${result}
   fi
}

function getFileType() {
   local value=$(file ${1})

   local fileType="UNKNOWN"

   contains "${value}" 'gzip compressed data'; local status=${?}
   if [ ${status} -eq 1 ] ; then
      fileType="GZIP"
   fi

   contains "${value}" 'Zip archive data'; local status=${?}
   if [ ${status} -eq 1 ] ; then
      fileType="ZIP"
   fi

   contains "${value}" 'bzip2 compressed data'; local status=${?}
   if [ ${status} -eq 1 ] ; then
      fileType="BZIP2"
   fi

   contains "${value}" 'HTML document'; local status=${?}
   if [ ${status} -eq 1 ] ; then
      fileType="HTML"
   fi

   contains "${value}" 'ELF'; local status=${?}
   if [ ${status} -eq 1 ] ; then
      fileType="BINARY"
   fi

   contains "${value}" '64-bit executable'; local status=${?}
   if [ ${status} -eq 1 ] ; then
      fileType="BINARY"
   fi

   contains "${value}" 'XZ compressed data'; local status=${?}
   if [ ${status} -eq 1 ] ; then
      fileType="XZ"
   fi

   if [ "${osName}" == "Darwin" ] ; then
      if [[ ${1} == *".dmg" ]] ; then
         fileType="DMG"
      fi
   fi

   echo ${fileType}
}

function showDisclaimer() {
   local baseDir="${1}"; shift
   local params="${@}"

   local projectNames=""
   for projectItem in ${PROJECT_AND_BRANCH_NAMES} ; do
      projectNames="$(echo ${projectItem} | cut -d'|' -f1) ${projectNames}"
   done

   echo ""
   echo "Organization name   : ${MY_ORG_NAME}"
   echo "Project names       : ${projectNames}"
   echo "Bootstrap directory : ${baseDir}"
   echo "Your platform       : ${params}"
   echo "------------------------------------------------------------------------------"
   echo ""
   echo "Bootstrap will use directory '${baseDir}' to setup environment for you,"
   echo "this directory will contain everything you need to work on this project."
   echo ""
   echo "Platfom tools for '$(echo ${params} | tr -d " \t\n")' will be setup with 64-bit binaries"
   echo ""
   echo "If you need additional information, contact your keymaker!"
   echo "                                                 ******** "
   echo ""

   local dummy=""
   echo "Press [ENTER] to continue"; read dummy
}


function showHelp() {
   echo "You can invoke '${0}' using following options"
   echo ""
   echo "-h                        Display help"
   echo "-o <organization name>    Must use given organization name"
   echo "-p <project name>         Must use given project name"
   echo "-s <sudo password>        User credential for sudo access"
   echo "-d <db password>          Root user credential for database access"
   echo "-a <action>               Perform specified actions"
   echo "-i <base directory>       Base directory for installations"
   echo "-c                        Get code from source repository"
   echo ""

   echo "Available actions: [${supportedActions}]"

   if [ "${1}" == "true" ] ; then
      return 1
   fi

   return 0
}

function validateArguments() {
   local OPTIND
   local options=$(getopt "cha:i:s:m:p:o:" "$@")
   if [ ${?} != 0 ]
   then
      return 1
   fi

   set -- $options
   while true ; do
      case "${1}" in
         # Need to show help screen
         -h)
            showHelp "true"
            shift
         ;;
         -c)
            syncCode="true"
            shift
         ;;
         # A specific action was requested
         -a)
            actions=${2}
            shift 2
         ;;
         # Install base was provided
         -i)
            installBase=${2}
            shift 2
         ;;
         # SUDO password
         -s)
            sudoSecret=${2}
            shift 2
         ;;
         # DB Password
         -d)
            dbSecret=${2}
            shift 2
         ;;
         # Project name
         -p)
            projectAndBranchNames=${2}
            shift 2
         ;;
         # Organization name
         -o)
            orgName=${2}
            shift 2
         ;;
         --)
            shift;
            break
         ;;
      esac
   done

   # Default values
   if [ -z "${sudoSecret}" -a ! -z "${SUDO_PASSWORD}" ] ; then
      sudoSecret="${SUDO_PASSWORD}"
   else
      logger -l ERROR "You must be a sudoer, and your password is requried for sudo access!"
      return 1
   fi

   if [ -z "${dbSecret}" ] ; then
      dbSecret="${DB_PASSWORD}"
   fi

   if [ -z "${projectAndBranchNames}" -a ! -z "${PROJECT_AND_BRANCH_NAMES}" ] ; then
      projectAndBranchNames="${PROJECT_AND_BRANCH_NAMES}"
   fi

   if [ -z "${orgName}" -a ! -z "${MY_ORG_NAME}" ] ; then
      orgName="${MY_ORG_NAME}"
   fi

   if [ -z "${installBase}" -a -z "${DEV_BASE_DIR}" ] ; then
      installBase=${HOME}/${MY_ORG_NAME}
   else
      installBase="${DEV_BASE_DIR}"
   fi

   if [ -z "${actions}" ] ; then
      logger -l ERROR "option -a <action> must be provided"
      logger -l INFO "Available actions: [${supportedActions}]"

      return 1
   fi


   local missingSpecs=""
   for projectAndBranchName in ${projectAndBranchNames} ; do
      local projectName=$(echo ${projectAndBranchName} | cut -d'|' -f1)
      local projectSpec=$(find "${bootstrapBase}" -type f -name "devenv-project-${projectName}" -print 2> /dev/null)

      if [ -z "${projectSpec}" ] ; then
         missingSpecs=$(echo ${missingSpecs} ${projectSpec} | tr -s " ")
      fi
   done

   if [ ! -z "${missingSpecs}" ] ; then
         logger -l ERROR "Boostrap customizations are not available for your project '${projectName}'"
         logger -l ERROR "Make sure the keymaker has customized bootstrap for your project needs!"

         return 1
   fi

   return 0
}

function cleanupDownloadsAndDevtools() {
   local cwDir=$(pwd)

   local installDefFile=${1}
   local downloadsDir=${2}
   local installBase=${3}

   if [ -z "${installDefFile}" -o -z "${downloadsDir}" -o -z "${installBase}" ] ; then
      logger -l ERROR 'Usage: installDevtools <install definition file> <downloads directory> <install base directory>'
      return 1
   fi

   # Read install definition
   logger -l INFO "Using install definition file [file=${installDefFile}]"
   logger -l INFO "Starting cleanup ... please wait!"
   while read line ; do
      [ -z "${line}" ] && continue;

      # Process line and perform action
      isCommentLine ${line}
      local status=${?}

      if [ ${status} -eq 1 ] ; then
         local appId=$(extractText ${line} "|" 1)
         local archiveName=$(extractText ${line} "|" 2)
         local location=$(extractText ${line} "|" 5)
         local locationCheck=$(extractText ${line} "|" 6)

         local isAllowed=$(canInstall ${appId})
         if [ ! -z "${archiveName}" -a ! -z "${location}" -a ! -z "${locationCheck}" -a "${isAllowed}" == "yes" ] ; then
            local artifactName="${location}"
            if [ "${location}" == "default" ] ; then
               local artifactName="${appId}"
            fi
            execute "(rm ${downloadsDir}/*${archiveName}*)"
            execute "(rm -fr ${installBase}/*${artifactName}*)"
         fi
      fi
   done < ${installDefFile}

   # Wait for all cleanup to complete
   wait

   echo ""
   echo ""
   logger -l INFO "Completed cleaning up necessary directories!"
   cd ${cwDir}

   return 0
}

function initBootStrap() {
   local cwDir=$(pwd)

   local baseDir=${1}
   local action=${2}
   local installDefFile=${3}
   local downloadsDir=${4}
   local devtoolsDir=${5}
   local templatesDir=${6}

   if [ -z "${baseDir}" -o -z "${action}" -o -z "${installDefFile}" -o -z "${downloadsDir}" -o -z "${devtoolsDir}" ] ; then
      logger -l ERROR "Usage: initBootStrap <base directory> <action> <install definition file> <downloads directory <devtools directory>"
      return 1
   fi

   [ ! -d ${baseDir} ] && execute "mkdir -p ${baseDir}"
   [ ! -d ${baseDir}/devconfig ] && execute "mkdir -p ${baseDir}/devconfig"
   [ ! -d ${baseDir}/devtools ] && execute "mkdir -p ${baseDir}/devtools"
   [ ! -d ${baseDir}/downloads ] && execute "mkdir -p ${baseDir}/downloads"
   [ ! -d ${baseDir}/workspaces ] && execute "mkdir -p ${baseDir}/workspaces"
   [ ! -d ${baseDir}/repositories ] && execute "mkdir -p ${baseDir}/repositories"
   [ ! -d ${baseDir}/workdir ] && execute "mkdir -p ${baseDir}/workdir"

   if [ "${action}" == "reinstall" ] ; then
      # Remove everything and start from scratch
      execute "rm -fr ${baseDir}"
   else
      # cleanup items that are part of install definition (that user intends to install)
      cleanupDownloadsAndDevtools ${installDefFile} ${downloadsDir} ${devtoolsDir}
   fi


   if [ -d "${templatesDir}/keystore" ] ; then
      logger -l INFO "Setting up keystore"
      execute "cp -R ${templatesDir}/keystore ${baseDir}/."
   else
      logger -l INFO "Skipping keystore setup"
   fi

   cd ${cwDir}

   return 0
}

function getMySQLBaseDir() {
   local oldMySQLBase=$(ps -eaf | grep "bin/mysqld" | grep "basedir" | grep -v "grep" | awk '{print $9}' | awk -F"=" '{print $2}')
   echo "${getMySQLBaseDir}"
}

function getMySQLPID() {
   local mysqlPID=$(ps -eaf | grep "bin/mysqld" | grep -v "grep" | awk '{ print $2 }')
   echo "${mysqlPID}"
}

function setupMySQLUsers() {
   if [ -z "${1}" -o -z "${2}" -o -z "${3}" -o -z "${4}" -o -z "${5}" ] ; then
      logger -l ERROR "Usage: setupMySQLUsers <mysql home directory> <project base directory> <mysql root secret> <secured> <mysql accounts>"
      return 1
   fi

   local mysqlHome=${1}
   local installBase=${2}
   local dbSecret=${3}
   local secured=${4}
   local mysqlAccounts=${5}

      local systems="localhost,127.0.0.1,::1,%,$(hostname -s)"
   if [ ! -z "${MY_ORG_DOMAIN_NAME}" ] ; then
      systems="${systems},$(hostname -s).${MY_ORG_DOMAIN_NAME}"
   fi

   if [ ! -z "${DB_CLIENTS_WHITELIST}" ] ; then
      local trimmed=$(echo ${DB_CLIENTS_WHITELIST} | tr -d " ")
      local systems="${systems},${trimmed}"
   fi

   # Convert values as arrays
   IFS="," systems=(${systems})
   IFS="," accounts=(${mysqlAccounts})

   local accounts_count="${#accounts[@]}"
   local systems_count="${#systems[@]}"

   local configDir="${installBase}/devconfig"

   # TODO: calling execute for mkdir goes into infinite loop!
   [ ! -d "${configDir}" ] && mkdir -p "${configDir}"
   local tempFile="${configDir}/mysql_user_accounts.sql"
   echo "" > ${tempFile}

   local mysql_password="${dbSecret}"

   for (( i=0; i<${accounts_count}; i++ ));
   do
      local account_name="${accounts[$i]}"

      # Generate drop statement
      for (( j=0; j<${systems_count}; j++ ));
      do
         local system_name="${systems[$j]}"
         if [ ${account_name} == "root" -a ${i} -le 0 ] ; then
            echo "drop user if exists 'root'@'${system_name}';" >> ${tempFile}
         else
            echo "drop user if exists '${account_name}'@'${system_name}';" >> ${tempFile}
         fi
      done
      echo "" >> ${tempFile}

      # Generate create user statement
      for (( j=0; j<${systems_count}; j++ ));
      do
         local system_name="${systems[$j]}"
         if [ ${account_name} == "root" -a ${i} -le 0 ] ; then
            echo "create user if not exists 'root'@'${system_name}' identified by '${mysql_password}' require ssl;" >> ${tempFile}
         else
            echo "create user if not exists '${account_name}'@'${system_name}' identified by '${mysql_password}' require ssl;" >> ${tempFile}
         fi
      done
      echo "" >> ${tempFile}

      # Generate grant statement
      for (( j=0; j<${systems_count}; j++ ));
      do
         local system_name="${systems[$j]}"
         if [ ${account_name} == "root" -a ${i} -le 0 ] ; then
            echo "grant all privileges on *.* to 'root'@'${system_name}' with grant option;" >> ${tempFile}
         else
            echo "grant all privileges on *.* to '${account_name}'@'${system_name}' with grant option;" >> ${tempFile}
         fi
      done
      echo "flush privileges;" >> ${tempFile}
      echo "" >> ${tempFile}

   done

   # Disable anonymous access
   echo "delete from mysql.user where user = '';" >> ${tempFile}
   echo "flush privileges;" >> ${tempFile}

   # TODO: Investigate why this command alone would not work with "execute"
   local command="(${mysqlHome}/bin/mysql --socket=${mysqlHome}/mysql.sock -u root -p${dbSecret} < ${tempFile})"
   # if [ "${secured}" == "true" ] ; then
   #    echo "###  INFO: \e[0;32mConfiguring MySQL user accounts in secure mode\e[0m"
   #    local command="(${mysqlHome}/bin/mysql --socket=${mysqlHome}/mysql.sock --ssl-ca=${MYSQL_HOME}/${DB_SECURE_CA_CERT} --ssl-cert=${MYSQL_HOME}/${DB_SECURE_CERT} --ssl-key=${MYSQL_HOME}/${DB_SECURE_KEY} --host=$(hostname -s) -u root -p${dbSecret} < ${tempFile})"
   # else
   #    echo "###  INFO: \e[0;32mConfiguring MySQL user accounts in normal mode\e[0m"
   #    local command="(${mysqlHome}/bin/mysql --socket=${mysqlHome}/mysql.sock -u root -p${dbSecret} < ${tempFile})"
   # fi

   echo "*** DEBUG: (run-command): ${command}"
   eval ${command}

   return ${?}
}

function setupMySQL() {
   local _fName="setupMySQL"

   local mysqlBaseDir=${1}
   local dbSecret=${2}

   if [ -z "${dbSecret}" -o -z "${mysqlBaseDir}" ] ; then
      echo "USAGE: ${0} <mysql base dir> <db secret>"
      exit 1
   fi

   local mysqlPID=$(getMySQLPID)
   if [ ! -z "${mysqlPID}" ] ; then
      logger -l INFO "${fName}: Shutdown running instance of MySQL"

      local mysqlBase=$(getMySQLBaseDir)
      if [ ! -z "${mysqlBase}" ] ; then
         if [ "${SECURE_DB_CONFIG}" == "true" ] ; then
            execute "(echo 'shutdown' | ${mysqlBase}/bin/mysql --socket=${mysqlBase}/mysql.sock --ssl-ca=${MYSQL_HOME}/${DB_SECURE_CA_CERT} --ssl-cert=${MYSQL_HOME}/${DB_SECURE_CERT} --ssl-key=${MYSQL_HOME}/${DB_SECURE_KEY} --host=$(hostname -s) -u root -p${dbSecret} 2>/dev/null)"
         else
            execute "(echo 'shutdown' | ${mysqlBase}/bin/mysql --socket=${mysqlBase}/mysql.sock -u root -p${dbSecret} 2>/dev/null)"
         fi
      fi

      local mysqlPID=$(getMySQLPID)
      if [ ! -z "${mysqlPID}" ] ; then
         execute "sleep 10s; kill -9 ${mysqlPID}"
      fi
   fi

   [ ! -d ${mysqlBaseDir}/data ] && execute "mkdir -p ${mysqlBaseDir}/data"
   [ ! -d ${mysqlBaseDir}/tmp ] && execute "mkdir -p ${mysqlBaseDir}/tmp"
   [ ! -d ${mysqlBaseDir}/log ] && execute "mkdir -p ${mysqlBaseDir}/log"
   [ ! -d ${mysqlBaseDir}/run ] && execute "mkdir -p ${mysqlBaseDir}/run"

   execute "cp ${mysqlBaseDir}/my.cnf.template ${mysqlBaseDir}/my.cnf"

   # Initialize MySQL Server
   execute "(cd ${mysqlBaseDir}; ${mysqlBaseDir}/bin/mysqld --defaults-file=${mysqlBaseDir}/my.cnf --basedir=${mysqlBaseDir} --datadir=${mysqlBaseDir}/data --socket=${mysqlBaseDir}/mysql.sock --user=${USER} --initialize)"

   # Extract temporary password from install log
   local tmpKey=$(grep "A temporary password is generated" ${mysqlBaseDir}/log/error.log | tail -1 | awk -F "root@localhost: " '{ print $2 }')

   # Start MySQL Server
   execute "(cd ${mysqlBaseDir}; ${mysqlBaseDir}/bin/mysqld --defaults-file=${mysqlBaseDir}/my.cnf --basedir=${mysqlBaseDir} --datadir=${mysqlBaseDir}/data --socket=${mysqlBaseDir}/mysql.sock --user=${USER} &)"
   sleep 30s

   # Setup default root password
   execute "(cd ${mysqlBaseDir}; ${mysqlBaseDir}/bin/mysqladmin --socket=${mysqlBaseDir}/mysql.sock -u root -p'${tmpKey}' password '${dbSecret}')"

   return 0
}

function getCassandraPID() {
   local pid=$(ps -eaf | grep "apache-cassandra" | grep -v "grep" | awk '{ print $2 }')
   echo "${pid}"
}

function setupCassandra() {
   local _fName="setupCassandra"

   local cassandraBaseDir=${1}
   local dbSecret=${2}

   local cassandraPID=$(getCassandraPID)
   if [ ! -z "${cassandraPID}" ] ; then
      logger -l INFO "${fName}: Shutdown running instance of Cassandra"

      execute "sleep 5s; kill -9 ${cassandraPID}"
   fi

   [ ! -d ${cassandraBaseDir}/data ] && execute "mkdir -p ${cassandraBaseDir}/data"
   [ ! -d ${cassandraBaseDir}/commitlog ] && execute "mkdir -p ${cassandraBaseDir}/commitlog"
   [ ! -d ${cassandraBaseDir}/saved_caches ] && execute "mkdir -p ${cassandraBaseDir}/saved_caches"
   [ ! -d ${cassandraBaseDir}/log ] && execute "mkdir -p ${cassandraBaseDir}/log"

   execute "cat ${cassandraBaseDir}/conf/cassandra.yaml | sed -e 's|authenticator: AllowAllAuthenticator|authenticator: PasswordAuthenticator|g' | sed -e 's|/var/lib/cassandra/data|${cassandraBaseDir}/data|g' | sed -e 's|/var/lib/cassandra/commitlog|${cassandraBaseDir}/commitlog|g' | sed -e 's|/var/lib/cassandra/saved_caches|${cassandraBaseDir}/saved_caches|g' > ${cassandraBaseDir}/conf/cassandra.yaml.new"
   execute "mv ${cassandraBaseDir}/conf/cassandra.yaml.new ${cassandraBaseDir}/conf/cassandra.yaml"

   execute "cat ${cassandraBaseDir}/conf/log4j-server.properties | sed -e 's|/var/log/cassandra/system.log|${cassandraBaseDir}/log/system.log|g' > ${cassandraBaseDir}/conf/log4j-server.properties.new"
   execute "mv ${cassandraBaseDir}/conf/log4j-server.properties.new ${cassandraBaseDir}/conf/log4j-server.properties"

   execute "${cassandraBaseDir}/bin/cassandra -f" &
   execute "sleep 20s"

   local configDir="${installBase}/devconfig"
   if [ ! -z "${branchName}" ] ; then
      configDir="${configDir}/${branchName}"
   fi

   [ ! -d ${configDir} ] && execute "mkdir -p ${configDir}"
   local tempFile="${configDir}/cassandra_accounts.cql"

   echo "create user georoo with password 'georoo' superuser;" > ${tempFile}
   echo "" >> ${tempFile}
   echo "ALTER USER cassandra WITH PASSWORD '${dbSecret}';" >> ${tempFile}
   echo "" >> ${tempFile}
   echo "exit;" >> ${tempFile}

   execute "${cassandraBaseDir}/bin/cqlsh -u cassandra -p cassandra -f ${tempFile} localhost"
   return 0
}

function canInstall() {
   local product=${1}

   if [ -z "${product}" ] ; then
      echo "no"
      return
   fi

   # Ignore commented lines
   local contains=$(grep "${product}" ${bootstrapBase}/templates/app-install.spec | grep -v "^#")
   if [ ! -z "${contains}" ] ; then
      echo "yes"
   else
      echo "no"
   fi
}

function configureDevtools() {
   local _fName="configureDevtools"

   local cwDir=$(pwd)

   local installDefFile=${1}
   local templatesDir=${2}
   local installBase=${3}
   local orgName=${4}
   local dbSecret=${5}
   local branchName=${6}
   local projectAndBranchNames=${7}

   local installBaseDir="${installBase}/devtools"

   # Read install definition
   logger -l CLI "Customizing installation ... please wait!"
   local tempFile="/tmp/install.def"
   local java8Home=""
   local eclipseHome=""

   local installCassandraLater="false"
   local installEclipseLater="false"

   execute "(cat ${installDefFile} | grep -v 'customize=none' | grep -v '^#' > ${tempFile})"
   while read line ; do
      [ -z "${line}" ] && continue;

      isCommentLine ${line}; local status=${?}
      # Process line and perform action
      if [ ${status} -eq 1 ] ; then
         logger -l INFO "${_fName}: Processing Line '${line}'"


         local appId=$(extractText "${line}" '|' 1)
         local isAllowed=$(canInstall ${appId})
         if [ "${isAllowed}" == "no" ] ; then
            continue
         fi

         local location=$(extractText "${line}" '|' 5)
         local locationCheck=$(extractText "${line}" '|' 6)
         local customize=$(extractText "${line}" '|' 7)

         [ ! -z "${location}" -a "${location}" == "default" ] && unset location

         local installLocation="${installBaseDir}"
         if [ ! -z "${location}" ] ; then
            installLocation="${installBaseDir}/${location}"
         fi

         local checkLocation=$(extractText "${locationCheck}" '=' 2)
         local product=$(extractText "${customize}" "=" 2)

         local newLocation="${installLocation}/${checkLocation}"
         if [ "${product}" == "tomcat" ] ; then
            logger -l INFO "${_fName}: Customizing Apache Tomcat"
            execute "cp -R ${templatesDir}/tomcat/* ${newLocation}/."
            execute "rm ${newLocation}/conf/project-context.xml"
         elif [ "${product}" == "java" ] ; then
            logger -l INFO "${_fName}: Customizing java"

            # Enable Java full security policy
            local javaHome="${newLocation}"

            if [[ "${javaHome}" == *'jdk1.8'* ]] ; then
               java8Home=${javaHome}
               execute "cp ${templatesDir}/jre-security-updates/jdk1.8/* ${javaHome}/jre/lib/security"
            fi

         elif [ "${product}" == "mysql" ] ; then
            logger -l INFO "${_fName}: Customizing mysql"
            # Configure MySQL configuration

            if [ "${SECURE_DB_CONFIG}" == "true" ] ; then
               execute "(mkdir -p ${newLocation}/keystore/certificates)"
               execute "(cp ${templatesDir}/${DB_SECURE_CA_CERT} ${newLocation}/keystore/certificates/.)"
               execute "(cp ${templatesDir}/${DB_SECURE_CERT} ${newLocation}/keystore/certificates/.)"
               execute "(cp ${templatesDir}/${DB_SECURE_KEY} ${newLocation}/keystore/certificates/.)"
               execute "(chmod 600 ${newLocation}/keystore/certificates/*)"

               execute "(cat ${templatesDir}/my.cnf.secured | sed -e 's|_USER_|${USER}|g' | sed -e 's|_MYSQL_HOME_|${newLocation}|g' | sed -e 's|_DB_SECURE_CA_CERT_|${DB_SECURE_CA_CERT}|g' | sed -e 's|_DB_SECURE_CERT_|${DB_SECURE_CERT}|g' | sed -e 's|_DB_SECURE_KEY_|${DB_SECURE_KEY}|g' > ${newLocation}/my.cnf.template)"
            else
               execute "(cat ${templatesDir}/my.cnf | sed -e 's|_USER_|${USER}|g' | sed -e 's|_MYSQL_HOME_|${newLocation}|g' > ${newLocation}/my.cnf.template)"
            fi

            # Setup MySQL Server Instance
            setupMySQL ${newLocation} ${dbSecret}

            # Setup MySQL Users per project
            local mysqlAccounts="root"
            for projectAndBranchName in ${projectAndBranchNames} ; do
                local projectName=$(echo ${projectAndBranchName} | cut -d'|' -f1)
                local branchName=$(echo ${projectAndBranchName} | cut -d'|' -f2)

                mysqlAccounts="${mysqlAccounts},${projectName}"
            done

            local oldIFS="${IFS}"
            logger -l INFO "${_fName}: setup mysql users"
            setupMySQLUsers ${newLocation} ${installBase} ${dbSecret} "true" "${mysqlAccounts}"
            IFS="${oldIFS}"

         elif [ "${product}" == "cassandra" ] ; then
            logger -l INFO "${_fName}: Customizing Cassandra"
            # Depends on java home directory; defer cassandra installation

            installCassandraLater="true"
         elif [ "${product}" == "eclipse" ] ; then
            local eclipseHome="${newLocation}"
            # Depends on java home directory; defer eclipse installation

            installEclipseLater="true"
         fi
      fi
   done < ${tempFile}

   local jdkVersion="jdk1.8"
   local myJavaHome=""
   local eclipseIni="${eclipseHome}/eclipse.ini"

   if [ ! -z "${java8Home}" ] ; then
      if [[ "${JDK_VERSION}" == *'jdk1.8'* ]] ; then
         jdkVersion="jdk1.8"
         myJavaHome="${java8Home}"
      fi
   else
      logger -l ERROR "Java is not available!, this may lead to issues with your project!"
   fi

   if [ "${osName}" == "Darwin" ] ; then
      eclipseIni="${eclipseHome}/Eclipse.app/Contents/MacOS/eclipse.ini"
   fi

   if [ "${installEclipseLater}" == "true" -a ! -z "${eclipseHome}" -a -d "${eclipseHome}" ] ; then
      logger -l INFO "Performing deferred Eclipse customizations"

      # if [ -f "${eclipseIni}" ] ; then
      #    logger -l INFO "Customizing eclipse.ini"

      #    echo "-vm" > "${eclipseIni}.new"
      #    echo "${myJavaHome}/bin/java" >> "${eclipseIni}.new"

      #    mv ${eclipseIni} ${eclipseIni}.backup
      #    cat ${eclipseIni}.backup >> ${eclipseIni}.new

      #    mv ${eclipseIni}.new ${eclipseIni}
      # fi
   fi

   if [ "${installCassandraLater}" == "true" ] ; then
      logger -l INFO "Performing deferred Cassandra installation"
      export JAVA_HOME="${myJavaHome}"
      export PATH="${JAVA_HOME}/bin:${PATH}"

      setupCassandra ${newLocation} ${dbSecret}
   fi

   execute "[ -d ${templatesDir}/devenv ] && (cp -R ${templatesDir}/devenv ${installBase}/.)"

   local sccsRoot="${installBase}/workspaces/${branchName}"

   logger -l INFO "${_fName}: Updating devenv-main"
   if [ -f "${templatesDir}/devenv/devenv-main" ] ; then
      execute "(cat ${templatesDir}/devenv/devenv-main | sed -e 's|_MY_ORG_NAME_|${orgName}|g' | sed -e 's|_JDK_VERSION_|${jdkVersion}|g' | sed -e 's|_DB_PASSWORD_|${dbSecret}|g' | sed -e 's|_SUDO_PASSWORD_|${sudoSecret}|g' | sed -e 's|_PROJECT_SCCS_|${SCCS}|g' | sed -e 's|_SCCS_PASSWORD_|${SCCS_PASSWORD}|g' | sed -e 's|_SCCS_NAME_|${SCCS_NAME}|g' |  sed -e 's|_SCCS_EMAIL_|${SCCS_EMAIL}|g' | sed -e 's|_DEV_BASE_DIR_|${installBase}|g' > ${installBase}/devenv/devenv-main)"
   fi

   logger -l INFO "Completed customizing necessary softwares!"
   cd ${cwDir}

   return 0
}

function initPermissions() {
   local installBase=${1}/devtools
   [ -f ${installBase}/perforce/p4 ] && execute "chmod +x ${installBase}/perforce/p4"

   return 0
}

function setupProjectWorkspace() {
   local _fName="setupProjectWorkspace"

   local cwDir=$(pwd)

   local orgName=${1}
   local installBase=${2}
   local templatesDir=${3}
   local syncCode=${4}
   local projectAndBranchNames=${5}

   local devEnvScript="${installBase}/devenv/devenv-main"

   logger -l INFO "${_fName}: sourcing devenv [file=${devEnvScript}]"
   # Assume main as default branch when bootstrap is run
   source ${devEnvScript} main


   logger -l INFO "${_fName}: Initializing project specific artifacts"
   initProjectArtifacts

   logger -l INFO "${_fName}: Initializing Workspace"
   initWorkspace "${orgName}" "${installBase}" "${templatesDir}" "${syncCode}" "true" "${projectAndBranchNames}"

   case "${osName}" in
     Linux)
        local profileFile=${HOME}/.bashrc
     ;;

     Darwin)
        local profileFile=${HOME}/.bash_profile
     ;;
   esac

   # Update profile
   [ ! -f ${profileFile} ] && touch ${profileFile}

   local devenvAdded=$(grep "^# bootstrap - 99eaacbf-7613-47ed-a39d-055e2c047840" ${profileFile})
   if [ -z "${devenvAdded}" ] ; then
      cat ${templatesDir}/profiles/.bash_profile | sed -e "s|_MY_ORG_NAME_|${orgName}|g" | sed -e "s|_PROJECT_NAME_|${projectName}|g" >> ${profileFile}
   fi

   local devenvAdded=$(grep "^# bootstrap - 99eaacbf-7613-47ed-a39d-055e2c047840" ${HOME}/.inputrc)
   if [ -z "${devenvAdded}" ] ; then
      cat ${templatesDir}/profiles/.inputrc >> ${HOME}/.inputrc
   fi

   local devenvAdded=$(grep "^\" bootstrap - 99eaacbf-7613-47ed-a39d-055e2c047840" ${HOME}/.vimrc)
   if [ -z "${devenvAdded}" ] ; then
      cat ${templatesDir}/profiles/.vimrc >> ${HOME}/.vimrc
   fi

   cd ${cwDir}
   return 0
}

function doDownload() {
   local cwDir=$(pwd)

   local appId=${1}
   local downloadsDir=${2}
   local archiveName=${3}
   local url=${4}
   local wgetOption=${5}

   if [ -d "${downloadsDir}" ] ; then
      cd ${downloadsDir}

      local download=0
      if [ ! -f "${archiveName}" -o ! -s "${archiveName}" ] ; then
         # TODO: Verify checksum of file if possible
         [ -f ${archiveName} ] && execute "rm ${archiveName}"
         download=1
      fi

      # Remove P4V archive - force update it
      if [ "${appId}" == "p4v" ] ; then
         [ -f ${archiveName} ] && execute "rm ${archiveName}"
         download=1
      fi

      if [[ "${archiveName}" == *".dmg" ]] ; then
         # Do no download DMG files when run on other platforms
         if [ "${osName}" != "Darwin" ] ; then
            download=0
         fi
      fi

      if [ ${download} -eq 1 ] ; then
         if [ "${wgetOption}" == "oracle-jdk" ] ; then
            execute "(wget -q -O '${archiveName}' --no-cookies --no-check-certificate --header 'Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie' ${url} 2>&1)"
         else
            execute "(wget -q -O '${archiveName}' ${url} 2>&1)"
         fi
      fi
   fi

   cd ${cwDir}

   return 0
}

function downloadDevtools() {
   local cwDir=$(pwd)

   local installDefFile=${1}
   local downloadsDir=${2}

   if [ -z "${installDefFile}" -o -z "${downloadsDir}" ] ; then
      logger -l ERROR 'Usage: downloadDevtools <install definition file> <downloads directory>'
      return 1
   fi

   # Read install definition
   logger -l INFO "Using install def file [file=${installDefFile}]"
   logger -l INFO "Starting downloads ... please wait!"
   while read line ; do
      # Process line and perform action
      isCommentLine ${line}
      local status=${?}

      if [ ${status} -eq 1 ] ; then
         local appId=$(extractText ${line} "|" 1)
         local archiveName=$(extractText ${line} "|" 2)
         local url=$(extractText ${line} "|" 3)
         local wgetOption=$(extractText ${line} "|" 4)

         local isAllowed=$(canInstall ${appId})
         if [ ! -z "${archiveName}" -a ! -z "${url}" -a ! -z "${wgetOption}" -a "${isAllowed}" == "yes" ] ; then
            execute "(doDownload ${appId} ${downloadsDir} ${archiveName} ${url} ${wgetOption})"
         fi
      fi
   done < ${installDefFile}

   # Wait for all downloads to complete
   wait

   logger -l INFO "Completed downloading necessary softwares!"
   cd ${cwDir}

   local zeroBytesFileCount=0
   for file in $(find ${downloadsDir} -type f); do
      if [ -z ${file} ] ; then
         echo "File [path=${file}] is 0 bytes"
         zeroBytesFileCount=$((zeroBytesFileCount+1))
      fi
   done

   if [ ${zeroBytesFileCount} -ge 1 ] ; then
      echo "**** CRITICAL ERROR ****"
      echo "${zeroBytesFileCount} files are 0 bytes in size, installation cannot proceed"
      echo "Some of the URL's may be outdated, kindly fix them and run bootstrap again"

      exit 1
   fi

   return 0
}

function installFromDMG() {
   local cwDir=$(pwd)

   local appId=${1}
   local targetDir=${2}
   local dmgArchive=${3}

   case ${appId} in
     p4v)
       # targetDir is not used for p4v
       if [ -d /Applications/p4v.app ] ; then
          execute "sudo 'rm -fr /Applications/p4v.app'"
       fi

       logger -l INFO "Mounting DMG ${dmgArchive}"
       local hdInfo=$(hdiutil attach ${dmgArchive} | grep "P4V")
       local volume="$(echo ${hdInfo} | cut -d ' ' -f 3-)"
       local disk="$(echo ${hdInfo} | cut -d ' ' -f 1)"

       execute "sudo 'cp -R ${volume}/p4v.app /Applications/p4v.app'"
       execute "sudo 'cp -R ${volume}/p4merge.app /Applications/p4merge.app'"

       logger -l INFO "Unmounting DMG ${dmgArchive}"
       local outcome=$(hdiutil detach ${disk})
     ;;

     jdk-8)
       logger -l INFO "Mounting DMG ${dmgArchive}"
       local hdInfo=$(hdiutil attach ${dmgArchive} | grep "JDK 8 Update")
       local volume="$(echo ${hdInfo} | cut -d ' ' -f 3-)"
       local disk="$(echo ${hdInfo} | cut -d ' ' -f 1)"

       local installPkgFile=$(find "${volume}" -name "*.pkg" -print 2> /dev/null)

       execute "mkdir -p ${targetDir}"
       local tmpJdkDir="/tmp/jdk8pkgdir"
       ([ -d ${tmpJdkDir} ] && rm -fr ${tmpJdkDir}; pkgutil --expand "${installPkgFile}" ${tmpJdkDir})

       local jdkPkgDir=$(find ${tmpJdkDir} -type d -name "jdk*.pkg" -print 2> /dev/null)
       local jdkPayloadFile=$(find "${jdkPkgDir}" -name "Payload" -type f -print 2> /dev/null)
       (cd ${tmpJdkDir}; cpio -i < "${jdkPayloadFile}"; [ -d ${tmpJdkDir}/Contents/Home ] && cp -R ${tmpJdkDir}/Contents/Home/* ${targetDir})

       logger -l INFO "Unmounting DMG ${dmgArchive}"
       local outcome=$(hdiutil detach ${disk})
     ;;

     mysql-workbench)
       # targetDir is not used for p4v
       if [ -d /Applications/MySQLWorkbench.app ] ; then
          execute "sudo 'rm -fr /Applications/MySQLWorkbench.app'"
       fi

       logger -l INFO "Mounting DMG ${dmgArchive}"
       local hdInfo=$(hdiutil attach ${dmgArchive} | grep "MySQL Workbench")

       local volume="$(echo ${hdInfo} | cut -d ' ' -f 2-)"
       local disk="$(echo ${hdInfo} | cut -d ' ' -f 1)"


       local appDir=$(find "${volume}" -type d -name MySQLWorkbench.app -print 2> /dev/null)
       execute "sudo 'cp -R \"${appDir}\" /Applications/MySQLWorkbench.app'"

       logger -l INFO "Unmounting DMG ${dmgArchive}"
       local outcome=$(hdiutil detach ${disk})
     ;;

     eclipse)
       if [ -d /Applications/Eclipse.app ] ; then
          execute "sudo 'rm -fr /Applications/Eclipse.app'"
       fi

       logger -l INFO "Mounting DMG ${dmgArchive}"
       local hdInfo=$(hdiutil attach ${dmgArchive} | grep "Eclipse")

       local volume="$(echo ${hdInfo} | cut -d ' ' -f 3-)"
       local disk="$(echo ${hdInfo} | cut -d ' ' -f 1)"


       local appDir=$(find "${volume}" -type d -name Eclipse.app -print 2> /dev/null)
       execute "sudo 'cp -R \"${appDir}\" /Applications/Eclipse.app'"

       logger -l INFO "Unmounting DMG ${dmgArchive}"
       local outcome=$(hdiutil detach ${disk})
     ;;

     postgresql)
       logger -l INFO "Mounting DMG ${dmgArchive}"
       local hdInfo=$(hdiutil attach ${dmgArchive} | grep "PostgreSQL")
       local volume="$(echo ${hdInfo} | cut -d ' ' -f 3-)"
       if [ -z "${volume}" ] ; then
          local volume="$(echo ${hdInfo} | cut -d ' ' -f 2-)"
       fi
       local disk="$(echo ${hdInfo} | cut -d ' ' -f 1)"

       local installPkgDir=$(find "${volume}" -name "$(basename ${dmgArchive} '.dmg').app" -print 2> /dev/null)

        execute "sudo '\"${installPkgDir}\"/Contents/MacOS/installbuilder.sh --mode unattended --unattendedmodeui none --disable-components stackbuilder --superpassword \"${dbSecret}\"'"

       logger -l INFO "Unmounting DMG ${dmgArchive}"
       local outcome=$(hdiutil detach ${disk})
     ;;

     *)
       logger -l ERROR "DMG support is not implemented for this app [Skipping appId=${appId} targetDir=${targetDir} dmgArchive=${dmgArchive}]"
     ;;
   esac

   return 0
}

function checkAndInstall() {
   local cwDir=$(pwd)

   local appId=${1}
   local checkType=${2}
   local checkLocation=${3}
   local archiveName=${4}

   local fileType=$(getFileType ${archiveName})
   local exists=0

   if [ "${checkType}" == "file" ] ; then
      [ -f ${checkLocation} ] && exists=1
   elif [ "${checkType}" == "dir" ] ; then
      [ -d ${checkLocation} ] && exists=1
   fi

   if [ "${exists}" -eq 0 ] ; then
      case "${fileType}" in
         ZIP)
            execute "(unzip ${archiveName} > /dev/null 2>&1)"
         ;;

         BZIP2)
            execute "(tar xfj ${archiveName} > /dev/null 2>&1)"
         ;;

         GZIP|XZ)
            execute "(tar -xf ${archiveName} > /dev/null 2>&1)"
         ;;

         BINARY)
            execute "(cp ${archiveName} . > /dev/null 2>&1)"
         ;;

         # Disk Image must be processed only on OSX
         DMG)
            [ "${osName}" == "Darwin" ] && (installFromDMG ${appId} ${checkLocation} ${archiveName})
         ;;

      esac
   fi

   cd ${cwDir}

   return 0
}

function doInstall() {
   local cwDir=$(pwd)

   local appId=${1}
   local archiveName=${2}
   local installBaseDir=${3}
   local location=${4}
   local locationCheck=${5}

   [ "${location}" == "default" ] && unset location

   local installLocation="${installBaseDir}"
   if [ ! -z "${location}" ] ; then
      local installLocation="${installBaseDir}/${location}"
   fi

   [ ! -f ${installLocation} ] && mkdir -p ${installLocation}

   local checkType=$(extractText ${locationCheck} "=" 1)
   local checkLocation=$(extractText ${locationCheck} "=" 2)

   if [ ! -z "${checkType}" -a ! -z "${checkLocation}" ] ; then
      execute "(cd ${installLocation}; checkAndInstall ${appId} ${checkType} ${installLocation}/${checkLocation} ${archiveName})"
   fi

   cd ${cwDir}

   return 0
}

function installDevtools() {
   local cwDir=$(pwd)

   local installDefFile=${1}
   local downloadsDir=${2}
   local installBase=${3}

   if [ -z "${installDefFile}" -o -z "${downloadsDir}" -o -z "${installBase}" ] ; then
      logger -l ERROR 'Usage: installDevtools <install definition file> <downloads directory> <install base directory>'
      return 1
   fi

   # Read install definition
   logger -l INFO "Using install def file [file=${installDefFile}]"
   logger -l INFO "Starting installation ... please wait!"
   while read line ; do
      [ -z "${line}" ] && continue;

      # Process line and perform action
      isCommentLine ${line}
      local status=${?}

      if [ ${status} -eq 1 ] ; then
         local appId=$(extractText ${line} "|" 1)
         local archiveName=$(extractText ${line} "|" 2)
         local location=$(extractText ${line} "|" 5)
         local locationCheck=$(extractText ${line} "|" 6)

         local isAllowed=$(canInstall ${appId})
         if [ ! -z "${archiveName}" -a ! -z "${location}" -a ! -z "${locationCheck}" -a "${isAllowed}" == "yes" ] ; then
            execute "(doInstall ${appId} ${downloadsDir}/${archiveName} ${installBase} ${location} ${locationCheck})"
         fi
      fi
   done < ${installDefFile}

   # Wait for all downloads to complete
   wait

   echo ""
   echo ""
   logger -l INFO "Completed installing necessary softwares!"
   cd ${cwDir}

   return 0
}

function copyReadme() {
   local orgName="${1}"
   local projectName="${2}"
   local templatesDir="${3}"
   local installBase="${4}"

   [ ! -d ${installBase}/README ] && execute "mkdir -p ${installBase}/README"

   local readmeFile=$(find ${templatesDir}/projects/${orgName}/${projectName} -type f -name "*.docx" -print 2>/dev/null)
   execute "cp -f ${readmeFile} ${installBase}/README/"

   return 0
}
