To generate a public key:
ssh-keygen

To enable access using this public key:
Copy public key to required host and login to enable this key based login.

Example:
ssh-copy-id -i key_file.pub some_user@some_host
