##############################################################################
##
##
## Copyright (c) 2016 appsweaver.org
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
## @author UD
##
## ---------------------------------------------------------------------------
## .bash_profile
##
## Auto source devenv
##############################################################################

# bootstrap - 99eaacbf-7613-47ed-a39d-055e2c047840
export MY_ORG_NAME="_MY_ORG_NAME_"
if [ -f ${HOME}/${MY_ORG_NAME}/devenv/devenv-main ] ; then
  source ${HOME}/${MY_ORG_NAME}/devenv/devenv-main main
fi

# Setting PATH for Python 3.5
# The original version is saved in .bash_profile.pysave
PATH="/Library/Frameworks/Python.framework/Versions/3.5/bin:${PATH}"
export PATH
