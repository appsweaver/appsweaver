##############################################################################
##
##
## Copyright (c) 2016 appsweaver.org
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
## @author UD
##
## ---------------------------------------------------------------------------
## app-install.spec
##
## Enable install
##############################################################################

### Comment out tools that you don't want to install
### Build tools
apache-maven
jdk-8

### Development tools
eclipse
python-tools

### Web/App Servers
node
tomcat

### Search Engine
solr

### Message Servers
rabbitmq-server

### DB Servers
mongodb
postgresql
mysql
# cassandra

### DB Clients
mysql-workbench

### Caching Servers
# redis
