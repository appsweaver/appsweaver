*****************************************************************************************
  Bootstrap is a simple shell script used for setting up development environment
  for projects

  Objective of bootstrap is to capture automatable tasks of installing and customizing 
  products as required a project

  The use of shell script is to keep it easily portable across environments, without 
  the need to install additional tools for bootstrapping.
*****************************************************************************************


Bootstrap change log
====================

Dec 09, 2014
------------
Bootstrap now strongly enforces concept of Organization to support multiple projects


Generic:
1. Enforce use of Organization name when using bootstrap


MacOS [Darwin]
1. For MacOS, install python httplib2 support

2. Install P4 Merge to enable file/revision diff



Nov 28, 2014
------------
Bootstrap now has notion of projects; one should be able to use bootstrap for
many projects. Installation of tools can be selected per project.

Generic:
1. Following products updated to new versions
   Perforce, JDK, Node

2. Shell Profile .bashrc (Linux)/.bash_profile (Darwin) will get updated automatically

3. Added detail logging to trace what bootstrap does


MacOS [Darwin]:
1. Added much asked support for Darwin
   On Darwin, bootstrap will use brew for installing additional packages

2. devenv.sh renamed to devenv; devenv-common.sh renamed to devenv-common

3. At end of bootstrap, report time taken to run bootstrap.
