# AppsWeaver

AppsWeaver is an open source, web/mobile application development libraries and tools started by a bunch of creative and curious code weavers. This open source initiative is an outcome from decades of experience in application development, innovation, engineering and support. AppsWeaver is targeted towards anyone who needs to kickstart ideas and build towards production readiness.


## License
AppsWeaver is licensed under the [Apache 2.0 License](https://www.apache.org/licenses/LICENSE-2.0), sponsored and supported by [AppsWeaver](http://appsweaver.org).


## Technology stack
* [ReactJS](https://reactjs.org) and [React Native](https://facebook.github.io/react-native/)
* [Node](https://nodejs.org)
* [Spring Boot](https://spring.io/projects/spring-boot)
* [Hibernate](http://hibernate.org)


## Features
* Sample application (Work in progress)
* Documentation (Work in progress)
* Blogs and video tutorials (Work in progress)
* Highly customizable, common patterns separated out from proprietary business logic.
* Framework driven by principles of convention, configuration and convenience.
* Templates geared towards inspiring and empowering developers to go from an idea to working products quickly.
* Inspired by opinionated applications & frameworks.