#!/usr/bin/env bash

## Script directory
export scriptDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

## Save current context
export cwDir=$(pwd)
export workdir="__APP_WORKDIR__"

function terminator() {
    echo "Shutting down application"

    export USER="${USER:-$(whoami)}"
    ${workdir}/appManager.sh -c stop

    local pid="$(ps -eaf | grep ${USER} | grep "tail*server.log" | grep -v grep | awk '{ print $2 }')"

    if [ ! -z "${pid}" ] ; then
       kill -9 ${pid}
    fi
}

trap terminator INT

${workdir}/appManager.sh start
tail -F ${workdir}/logs/server.log
