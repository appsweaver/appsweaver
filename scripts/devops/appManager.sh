#!/usr/bin/env bash

## Script directory
export scriptDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

## Save current context
export cwDir=$(pwd)

## System specific
export USER="${USER:-$(whoami)}"
export APP_SERVER_NAME="__APP_SERVER_NAME__"
export APP_SERVER_PORT="__APP_SERVER_PORT__"
export APP_SERVER_MODE="__APP_SERVER_MODE__"

## Product specific
export APP_SECURED="__APP_SECURED__"
export APP_NAME="__APP_NAME__"
export APP_VERSION="__APP_VERSION__"
export APP_BUILD_NUMBER="__APP_BUILD_NUMBER__"
export APP_WORKDIR="__APP_WORKDIR__"
export APP_HOME="${scriptDir}"
export APP_UI_DIR="${APP_HOME}/ui"
export APP_BIN_DIR="${APP_HOME}/bin"
export APP_CONFIG_DIR="${APP_HOME}/config"

## Product Security
if [ "${APP_SECURED}" == "true" ] ; then
   export JKS_KEY_ALIAS="__JKS_KEY_ALIAS__"
   export JKS_KEYSTORE_PASSWORD="__JKS_KEYSTORE_PASSWORD__"
   export JKS_TRUSTSTORE_PASSWORD="__JKS_TRUSTSTORE_PASSWORD__"
   export JKS_KEYSTORE_FILE="${APP_HOME}/security/keystore.jks"
   export JKS_TRUSTSTORE_FILE="${APP_HOME}/security/truststore.jks"
fi

## Product JARS
export APP_JAR="${APP_NAME}-webservice-${APP_VERSION}.jar"

## Source environment
if [ -f "${scriptDir}/functions" ] ; then
   source ${scriptDir}/functions
else
   echo "${scriptDir}/functions file is not present, cannot continue"
   exit 1
fi

## Initialize LOG directory
if [ -z "${LOG_DIR}" ] ; then
   export LOG_DIR="${APP_WORKDIR}/logs"
fi

if [ -z "${CORE_DUMP_DIR}" ] ; then
   export CORE_DUMP_DIR="${APP_WORKDIR}/core-dumps"
fi

[ ! -d ${LOG_DIR} ] && mkdir -p ${LOG_DIR}
[ ! -d ${CORE_DUMP_DIR} ] && mkdir -p ${CORE_DUMP_DIR}

function showHelp() {
   echo ""
   echo "  USAGE : ${0} [options] <command>"
   echo ""
   echo "  -t               : show traces (spring)"
   echo "  -c               : override spring profile to console mode"
   echo "  <command>        : start, stop, restart, start-ws, stop-ws, start-ui, stop-ui,"
   echo "                     start (all), stop (all), restart (all)"
   echo "  -h               : show help"
   echo ""

   exit 1
}

function validateArguments() {
   local OPTIND
   local options=$(getopt ":htc" "$@")
   if [ ${?} != 0 ] ; then
      echo "getopt failed!"
      exit 1
   fi

   set -- ${options}
   while true ; do
      case ${1} in
         -h)
            shouldShowHelp="true"
            shift
         ;;

         -t)
            showTrace="true"
            shift
         ;;

         -c)
            consoleMode="true"
            shift
         ;;

         --)
            shift
            break
         ;;
      esac
   done

   local optionCount=$(( ${OPTIND} ))
   shift ${optionCount}

   while [ ${#} -eq 1 ] ; do
     command="${1}"
     shift
   done
}

function startWebservice() {
   local pid="$(ps -eaf | grep ${USER} | grep ${APP_JAR} | grep -v grep | awk '{ print $2 }')"
   if [ -z "${pid}" ] ; then
      echo "Starting ${APP_NAME} webservice"

      # Exporting spring profile name is necessary for logging to work right!
      export TIMESTAMP="$(date +%s)"

      local jvmOptions=""
      if [ "${APP_NAME}" == "eureka" ] ; then
          local jvmOptions="${jvmOptions} -Deureka.environment=${EUREKA_SERVER_ENV_NAME:-${APP_SERVER_MODE}}"
          local jvmOptions="${jvmOptions} -Deureka.datacenter=${EUREKA_DATACENTER_NAME:-US_WEST}"
      fi

      local appOptions=""
      # Toggle trace mode
      if [ "${showTrace}" == true ] ; then
         local appOptions="${appOptions} --trace"
      fi

      # Choose appropriate spring profile name
      if [ "${consoleMode}" == true ] ; then
         local jvmOptions="${jvmOptions} -Dspring.profiles.active=console"
      else
         local jvmOptions="${jvmOptions} -Dspring.profiles.active=${APP_SERVER_MODE}"
      fi

      nohup java \
          -Dcom.arjuna.ats.arjuna.objectstore.objectStoreDir=${LOG_DIR}/object-store \
          -XX:ErrorFile=${CORE_DUMP_DIR}/jvm_core_${TIMESTAMP}.dump \
          -XX:+HeapDumpOnOutOfMemoryError \
          -XX:HeapDumpPath=${CORE_DUMP_DIR}/jvm_heap_${TIMESTAMP}.hprof \
          ${jvmOptions} \
          -jar ${APP_BIN_DIR}/${APP_JAR} \
          ${appOptions} \
          > ${LOG_DIR}/server-nohup.log 2>&1 &
      echo "Done!"
   else
      echo "${APP_NAME} webservice is already running [pid=${pid}]"
   fi
}

function stopWebservice() {
   local pid=$(ps -eaf | grep ${USER} | grep "${APP_JAR}" | grep -v grep | awk '{ print $2 }')
   if [ ! -z "${pid}" ] ; then
      echo "Stopping ${APP_NAME} webservice"
      kill -9 ${pid}
      echo "Done!"
   else
      echo "${APP_NAME} webservice is not running"
   fi
}

function startUI() {
   [ ! -d ${APP_UI_DIR} ] && return 1

   local cwDir=$(pwd)
   local pid=$(ps -eaf | grep ${USER} | grep "node ${APP_NAME}-ui-server.js" | grep -v grep | awk '{ print $2 }')
   if [ -z "${pid}" ] ; then
      echo "Starting ${APP_NAME} node server"
      cd ${APP_UI_DIR}
      nohup node ${APP_NAME}-ui-server.js > ${LOG_DIR}/node-server.log 2>&1 &
      echo "Done!"
   else
      echo "${APP_NAME} node server is already running [pid=${pid}]"
   fi
   cd ${cwDir}
}

function stopUI() {
   [ ! -d ${APP_UI_DIR} ] && return 1

   local pid=$(ps -eaf | grep ${USER} | grep "node ${APP_NAME}-ui-server.js" | grep -v grep | awk '{ print $2 }')
   if [ ! -z "${pid}" ] ; then
      echo "Stopping ${APP_NAME} node server"
      kill -9 ${pid}
      echo "Done!"
   else
      echo "${APP_NAME} node server is not running"
   fi
}

function doMain() {
   # Validate command line arguments
   validateArguments "${@}"

   if [ "${shouldShowHelp}" == "true" ] ; then
      showHelp

      exit 1
   fi

   if [ -z "${command}" ] ; then
      logger -l ERROR "command must be specified"

      showHelp
      exit 1
   fi

   case ${command} in
   start-ui)
      startUI
   ;;

   stop-ui)
      stopUI
   ;;

   start-ws)
      startWebservice
   ;;

   stop-ws)
      stopWebservice
   ;;

   start)
      startWebservice

      sleep 5s
      startUI
   ;;

   stop)
      stopUI
      stopWebservice
   ;;

   restart)
      stopUI
      stopWebservice

      sleep 5s
      startWebservice

      sleep 5s
      startUI
   ;;

   *)
      logger -l ERROR "Unrecognised command '${command}"
   ;;

   esac
}

# Invoke main
echo "Working on $(hostname)"
time doMain "${@}"
