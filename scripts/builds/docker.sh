#!/usr/bin/env bash

## Script directory
export scriptDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export functionsDir="$(dirname ${scriptDir})/helpers"

## Save current context
export cwDir=$(pwd)
export parentDir=$(dirname ${scriptDir})

## Docker Host and Instance Names
export AWE_ROOT_DIR="$(dirname ${parentDir})"
export AWE_MAVEN_REPOSITORY_MP="/root/.maven/repository"
export AWE_MAVEN_CONTAINER_NAME="maven"
export AWE_MONGODB_CONTAINER_NAME="mongodb"
export AWE_NETWORK="appsweaver.org"
export AWE_MONGODB_HOSTNAME="${AWE_MONGODB_CONTAINER_NAME}.${AWE_NETWORK}"
export AWE_MONGODB_PORT="37017"

function showHelp() {
  echo ""
  echo "  USAGE : ${0} [options] [maven commands]"
  echo "  -h                  : show help"
  echo "  -n <network name>   : network name"
  echo "  -c <command>        : up, down"
  echo ""

  exit 1
}

function validateArguments() {
  local OPTIND
  local options=$(getopt ":n:c:h" "$@")
  if [ ${?} != 0 ] ; then
    echo "getopt failed!"
    exit 1
  fi

  set -- ${options}
  while true ; do
    case ${1} in
      -h)
        shouldShowHelp="true"
        shift
      ;;

      -c)
        command="${2}"
        shift 2
      ;;

      -n)
        networkName="${2}"
        shift 2
      ;;

      --)
        shift
        break
      ;;
    esac
  done
}

function doMain() {
  # Validate command line arguments
  validateArguments "${@}"

  if [ "${shouldShowHelp}" == "true" ] ; then
    showHelp
    return 1
  fi

  if [ -z "${AWE_MAVEN_REPOSITORY}" ] ; then
    echo "AWE_MAVEN_REPOSITORY must be set to maven repository path as per your Docker host"
    exit 1
  fi

  echo "Working on $(hostname)"
  if [ -f "${functionsDir}/functions.docker" ] ; then
    source ${functionsDir}/functions.docker
  else
    echo "${functionsDir}/functions.docker not found, can't proceed!"
    return 1
  fi

  export AWE_MAVEN_CONTAINER_NAME="${AWE_MAVEN_CONTAINER_NAME}.${AWE_NETWORK}"
  export AWE_MONGODB_CONTAINER_NAME="${AWE_MONGODB_CONTAINER_NAME}.${AWE_NETWORK}"
  # Setup network names

  local command="${command:-up}"
  if [ "${command}" == "up" ] ; then
     docker-compose -f ${scriptDir}/docker-compose.yml up --detach

     # start maven build
     local mavenCommand="${*}"
     if [ -z "${mavenCommand}" ] ; then
        local mavenCommand="mcist"
     fi

     # echo "Executing maven command: ${mavenCommand}"
     # docker exec --detach ${AWE_MAVEN_CONTAINER_NAME} /bin/bash -lc "${mavenCommand}"

     echo "Showing maven build log, login into container to build!"
     docker logs --follow ${AWE_MAVEN_CONTAINER_NAME}
  elif [ "${command}" == "down" ] ; then
     docker-compose -f ${scriptDir}/docker-compose.yml ${command}
  else
     echo "Unsupported command: ${command}"
     return 1
  fi
}

# Invoke main
echo "===================================================================="
echo "HOSTNAME: $(hostname)"
echo "UNAME: $(uname -a)"
[ -f /proc/version ] && echo "VERSION: $(cat /proc/version)"
echo "===================================================================="

## Call Main - Entry Point
doMain "${@}"
