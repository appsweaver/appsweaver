#!/usr/bin/env bash

## Script directory
export scriptDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Assuming that maven is run by root user
export workspace="/workspace"
export buildScriptsDir="${workspace}/scripts/builds"
export workdir="/root/.maven"
export mavenLogFile="${workdir}/maven.log"
export statusFile="${workdir}/initialized"

## Save current context
export cwDir=$(pwd)

function showHelp() {
   echo ""
   echo "  USAGE : ${0}"
   echo ""
   echo ""

   exit 1
}

function terminator() {
    echo "Shutting down application"

    export USER="${USER:-$(whoami)}"
    local pid="$(ps -eaf | grep ${USER} | grep "tail*maven.log" | grep -v grep | awk '{ print $2 }')"

    if [ ! -z "${pid}" ] ; then
       kill -9 ${pid}
    fi
}

# Respond to CTRL+C
trap terminator INT

function validateArguments() {
   local OPTIND
   local options=$(getopt ":h" "$@")
   if [ ${?} != 0 ] ; then
      echo "getopt failed!"
      exit 1
   fi

   set -- ${options}
   while true ; do
      case ${1} in
         -h)
            shouldShowHelp="true"
            shift
         ;;

         --)
            shift
            break
         ;;
      esac
   done
}

function setupAliases() {
   local buildEnvFile="${HOME}/.bashrc"

   cat /usr/local/bin/bash_profile.template | \
       sed "s|__WORKSPACE__|${workspace}|g" | \
       sed "s|__BUILD_SCRIPTS_DIR__|${buildScriptsDir}|g" | \
       sed "s|__MAVEN_LOG_FILE__|${mavenLogFile}|g" \
       >> ${buildEnvFile}

   source ${buildEnvFile}
}

function initializeBuildEnvironment() {
   [ ! -d "${workdir}" ] && mkdir -p ${workdir}

   local buildScriptsDir="${workspace}/scripts/builds"

   if [ ! -f ${statusFile} ] ; then
      echo "Setting up GPG"
      [ -z "${AWE_OPENSSL_PASSWORD}" ] && echo "Open SSL paraphrase is required to setup build environment!" && return 1

      echo "Decrypting private key"
      openssl aes-256-cbc -d -pbkdf2 -pass pass:${AWE_OPENSSL_PASSWORD} -in ${buildScriptsDir}/private-key.gpg.enc -out /tmp/private-key.gpg

      echo "Importing GPG key"
      gpg --batch --import /tmp/private-key.gpg

      setupAliases

      # Make sure this script doesn't run again
      touch ${statusFile}
   fi
}

function doMain() {
  # Validate command line arguments
  validateArguments "${@}"

  if [ "${shouldShowHelp}" == "true" ] ; then
    showHelp
    return 1
  fi

  initializeBuildEnvironment

  # Tail for ever
  touch ${mavenLogFile}
  tail -F ${mavenLogFile}
}

# Invoke main
echo "===================================================================="
echo "HOSTNAME: $(hostname)"
echo "UNAME: $(uname -a)"
[ -f /proc/version ] && echo "VERSION: $(cat /proc/version)"
echo "===================================================================="

## Call Main - Entry Point
doMain "${@}"
